package com.richinfo.dm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.dmyk.android.telephony.DmykAbsTelephonyManager;
import com.dmyk.android.telephony.DmykTelephonyManager;
import com.dmyk.android.telephony.DmykTelephonyManager.MLog;


import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import cn.richinfo.dm.CtmApplication;

public class CtmReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        MLog.d("CtmReceiver receives " + action);
        if ("android.intent.action.SIM_STATE_CHANGED".equals(action)) {
            int subId = intent.getIntExtra("subscription", -1);
            int slotId = SubscriptionManager.getSlotId(subId);
            TelephonyManager tm =
                (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            int state = tm.getSimState(slotId);

            Intent newIntent = new Intent(DmykAbsTelephonyManager.ACTION_SIM_STATE_CHANGE);
            newIntent.putExtra(DmykAbsTelephonyManager.EXTRA_SIM_PHONEID, slotId);
            newIntent.putExtra(DmykAbsTelephonyManager.EXTRA_SIM_STATE, state);
            context.sendBroadcast(newIntent);

            CtmApplication.updateAPNObservers();
        } else if (DmykAbsTelephonyManager.ACTION_VOLTE_STATE_SETTING.equals(action)) {
            Intent newIntent = new Intent(DmykAbsTelephonyManager.ACTION_VOLTE_STATE_SETTING);
            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(newIntent);
        }
    }
}

