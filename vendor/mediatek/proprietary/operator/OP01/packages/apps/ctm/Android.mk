#
# Copyright (C) 2008 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

ifeq ($(strip $(MTK_CTM_SUPPORT)),yes)

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_PACKAGE_NAME := ctm
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_CERTIFICATE := platform

LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_JAVA_LIBRARIES += mediatek-framework ims-common
LOCAL_STATIC_JAVA_LIBRARIES += libdmyksdk
LOCAL_STATIC_JAVA_LIBRARIES += android-support-v4
LOCAL_STATIC_JAVA_LIBRARIES += android-support-v7-appcompat
LOCAL_AAPT_FLAGS += --extra-packages android.support.v7.appcompat

LOCAL_PREBUILT_JNI_LIBS := jni_libs/armeabi-v7a/libaes-jni.so

ifeq ($(TARGET_ARCH),arm64)
LOCAL_PREBUILT_JNI_LIBS += jni_libs/arm64-v8a/libaes-jni.so
endif

LOCAL_MULTILIB := both

include $(BUILD_PACKAGE)

include $(CLEAR_VARS)

LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := libdmyksdk:libs/dm_sdk.jar

include $(BUILD_MULTI_PREBUILT)

include $(CLEAR_VARS)

include $(call all-makefiles-under, $(LOCAL_PATH))

endif

