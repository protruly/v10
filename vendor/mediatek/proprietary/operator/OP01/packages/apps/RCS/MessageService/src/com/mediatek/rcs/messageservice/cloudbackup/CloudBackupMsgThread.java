package com.mediatek.rcs.messageservice.cloudbackup;

import android.content.Context;
import android.os.PowerManager;
import android.os.RemoteException;
import android.util.Log;

import com.mediatek.rcs.messageservice.cloudbackup.aidl.IMsgBRListener;
import com.mediatek.rcs.messageservice.cloudbackup.modules.FavMsgBackup;
import com.mediatek.rcs.messageservice.cloudbackup.modules.MessageBackupComposer;
import com.mediatek.rcs.messageservice.cloudbackup.utils.CloudBrUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * This thread is used to backup message.
 */
public class CloudBackupMsgThread extends Thread {
    private final String CLASS_TAG = CloudBrUtils.MODULE_TAG + "CloudBackupMsgThread";
    private String mFolderPath;
    private IMsgBRListener mIMsgBackupListener;
    protected PowerManager.WakeLock mWakeLock;
    private boolean mCancel = false;
    private Context mContext;

    private FavMsgBackup mFavMsgBackup;
    private MessageBackupComposer mMessageBackupComposer;
    private String mMessageFolderPath;
    private String mFavoriteFolderPath;

    /**
     * set cancel when user press backup key.
     * @param cancel true means cancel.
     */
    public void setCancel(boolean cancel) {
        mCancel = cancel;
        Log.d(CLASS_TAG, "BackupService : cancelBackup");

        if (mMessageBackupComposer != null) {
            mMessageBackupComposer.setCancel(cancel);
        }

        if (mFavMsgBackup != null) {
            mFavMsgBackup.setCancel(cancel);
        }
    }

    /**
     * set context.
     * @param context.
     */
    public void setContext(Context context) {
        mContext = context;
    }

    /**
     * set backup folder path.
     * @param FolderPath folder is used to put backup data.
     * @param excuteListener is used to callback.
     */
    public void setBackupParam(String FolderPath, IMsgBRListener excuteListener) {
        mFolderPath = FolderPath;
        mIMsgBackupListener = excuteListener;
    }

    @Override
    public void run() {
        if (mIMsgBackupListener == null || mFolderPath == null) {
            Log.e(CLASS_TAG, "mIMsgRestoreListener == null || mFolderPath ==null");
            Log.e(CLASS_TAG, "mFolderPath = " + mFolderPath);
            return;
        }

        Log.d(CLASS_TAG, "BackupBinder start backup");
        createWakeLock();
        if (mWakeLock != null) {
            acquireWakeLock();
            Log.d(CLASS_TAG, "BackupService : startBackup: call acquireWakeLock()");
        }

        File folder = new File(mFolderPath);
        if (folder != null && folder.exists()) {
            Log.d(CLASS_TAG, "backup folder exit, delete it");
            folder.delete();
        }
        folder.mkdirs();

        int backupResult = CloudBrUtils.ResultCode.OK;
        if (mCancel) {
            Log.d(CLASS_TAG, "BackupThread is be canceled. return");
            sendBackupResult(CloudBrUtils.ResultCode.SERVICE_CANCELED);
            return;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Log.d(CLASS_TAG, "BackupThread, begin backup message msg");
        mMessageFolderPath = mFolderPath + CloudBrUtils.RemoteFolderName.MSG_BACKUP
                + File.separator + dateFormat.format(new Date(System.currentTimeMillis()));

        Log.d(CLASS_TAG, "mMessageFolderPath = " + mMessageFolderPath);
        mMessageBackupComposer = new MessageBackupComposer(mContext, mMessageFolderPath);
        backupResult = mMessageBackupComposer.backupData();

        if (backupResult != CloudBrUtils.ResultCode.OK) {
            Log.e(CLASS_TAG, "BackupThread, mMessageBackupComposer.backupData error, return");
            sendBackupResult(backupResult);
            return;
        }
        mMessageBackupComposer = null;

        if (mCancel) {
            Log.d(CLASS_TAG, "BackupThread is be canceled. return");
            backupResult = CloudBrUtils.ResultCode.SERVICE_CANCELED;
            sendBackupResult(backupResult);
            return;
        }

        Log.d(CLASS_TAG, "BackupThread, begin backup favorite msg");
        String dateString = dateFormat.format(new Date(System.currentTimeMillis()));
        mFavoriteFolderPath = mFolderPath + CloudBrUtils.RemoteFolderName.MSG_FAVORITE
                + File.separator + dateString;
        Log.d(CLASS_TAG, "favoriteFolderPath = " + mFavoriteFolderPath);

        mFavMsgBackup = new FavMsgBackup(mContext, mFavoriteFolderPath);
        backupResult = mFavMsgBackup.backupData();
        if (backupResult != CloudBrUtils.ResultCode.OK) {
            Log.e(CLASS_TAG, "BackupThread, mFavMsgBackup.backupData error, return");
            sendBackupResult(backupResult);
            return;
        }
        mFavMsgBackup = null;
        Log.d(CLASS_TAG, "BackupThread, backup favorite msg end");

        if (mCancel) {
            Log.d(CLASS_TAG, "BackupThread is be canceled. return");
            backupResult = CloudBrUtils.ResultCode.SERVICE_CANCELED;
            sendBackupResult(backupResult);
            return;
        }

        sendBackupResult(CloudBrUtils.ResultCode.OK);
        Log.d(CLASS_TAG, "BackupThread backup end, return");
    }

    private void sendBackupResult(int backupResult) {
        // send backup result to activity.
        Log.d(CLASS_TAG, "sendBackupResult = " + backupResult);

        if (backupResult != CloudBrUtils.ResultCode.OK) {
            File backupFile = new File(mFolderPath);
            if (backupFile != null && backupFile.exists()) {
                Log.d(CLASS_TAG, "sendBackupResult delete backup local folder");
                backupFile.delete();
            }
            mMessageFolderPath = null;
            mFavoriteFolderPath = null;
        }

        Log.d(CLASS_TAG, "sendBackupResult mMessageFolderPath = " + mMessageFolderPath);
        Log.d(CLASS_TAG, "sendBackupResult mFavoriteFolderPath = " + mFavoriteFolderPath);
        try {
            mIMsgBackupListener.onBackupResult(backupResult,
                    mMessageFolderPath, mFavoriteFolderPath);
        } catch (RemoteException ex) {
            ex.printStackTrace();
        }

        mFolderPath = null;
        mIMsgBackupListener = null;
        mCancel = false;
        if (mWakeLock != null) {
            releaseWakeLock();
            Log.d(CLASS_TAG, "onDestroy(): call releseWakeLock()");
        }
    }

    protected synchronized void createWakeLock() {
        // Create a new wake lock if we haven't made one yet.
        if (mWakeLock == null) {
            PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "RestoreService");
            mWakeLock.setReferenceCounted(false);
            Log.d(CLASS_TAG, "createWakeLock");
        }
    }

    protected void acquireWakeLock() {
        // It's okay to double-acquire this because we are not using it
        // in reference-counted mode.
        mWakeLock.acquire();
        Log.d(CLASS_TAG, "acquireWakeLock");
    }

    protected void releaseWakeLock() {
        // Don't release the wake lock if it hasn't been created and acquired.
        if (mWakeLock != null && mWakeLock.isHeld()) {
            mWakeLock.release();
            mWakeLock = null;
            Log.d(CLASS_TAG, "releaseWakeLock");
        }
        Log.d(CLASS_TAG, "releaseLock");
    }
}
