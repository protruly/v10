package com.mediatek.rcs.pam;

interface IDeviceApiServiceCallback {
    void onServiceConnected();

    void onServiceDisconnected(in int reason);

    void onServiceRegistered();

    void onServiceUnregistered();

    void onNewPublicAccountChat(in String accountnumber, in String msgId);

    void onNewCCPublicAccoutChat(in String accountnumber, in String msgId);

    void onPublicAccoutChatHistory(in String accountnumber, in int errType, in long id);

    void onFollowPublicAccount(in String accountnumber, in int errType, in String statusCode);

    void onUnfollowPublicAccount(in String accountnumber, in int errType, in String statusCode);

    void onGetInfo(in String accountnumber, in int errType, in String statusCode);

    void onSearch(in int errType, in String statusCode, in long id);

    void onGetFollowedPublicAccount(in int errType, in String statusCode);

    void onMenuConfigUpdated(in String accountnumber, in String configInfo, in int errType,
            in String statusCode);

    void onReportPublicAccount(in String accountnumber, in int errType, in String statusCode);

    void onGetPublicAccountStatus(in String accountnumber, in boolean status, in long id);

    void onReportMessageFailed(in String msgId, in int errType, in String statusCode);

    void onReportMessageDelivered(in String msgId);
}