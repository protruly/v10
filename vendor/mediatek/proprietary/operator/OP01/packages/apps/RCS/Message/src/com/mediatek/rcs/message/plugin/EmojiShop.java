package com.mediatek.rcs.message.plugin;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Set;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.WindowManager.BadTokenException;

import cn.com.em.sdk.DownLoadEmListener;
import cn.com.em.sdk.EmShopSDK;
import cn.com.em.sdk.mode.Em;
import cn.com.em.sdk.mode.EmData;
import cn.com.em.sdk.mode.EmPackagef;
import cn.com.em.sdk.mode.EmSimple;
import cn.com.em.sdk.utils.AppNotInstalledException;
import cn.com.em.sdk.utils.FilePathNotFoundException;
import cn.com.em.sdk.utils.OnDataReceivedListener;
import cn.com.em.sdk.utils.OnEmPackageChangedListener;
import cn.com.em.sdk.utils.UninitializedException;

import com.cmcc.sso.sdk.auth.AuthnConstants;
import com.cmcc.sso.sdk.auth.AuthnHelper;
import com.cmcc.sso.sdk.auth.TokenListener;
import com.cmcc.sso.sdk.util.SsoSdkConstants;
import com.mediatek.rcs.common.RCSServiceManager;
import com.mediatek.rcs.common.RCSServiceManager.OnServiceChangedListener;
import com.mediatek.rcs.common.utils.EmojiShopUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;


public class EmojiShop {

    private static final String TAG = "EmojiShop";

    private static EmojiShop sInstance;
    private Context mMmsAppContext;
    private boolean mUserDataAvailable = false;
    private boolean mServiceEnabled = false;
    private boolean mServiceRegistered = false;
    private boolean mNeedUpdate = false;
    private String mUserId;

    // All Emoticons Data
    private List<EmData> mUserEmData = new ArrayList<EmData>();

    // ArrayList store all Em package's ID
    private ArrayList<String> mEmPackageIdList = new ArrayList<String>();

    // All Emoticons package info
    private List<EmojiPackage> mEmPkgInfo = new ArrayList<EmojiPackage>();

    // EmData in user folder, <emId, packageId>
    private HashMap<String, String> mUserEmPkgMap = new HashMap<String, String>();

    // Listeners called when load user data complete
    private HashSet<OnEmDataChangedListener> mEmDataChangedlisteners =
            new HashSet<OnEmDataChangedListener>();

    // Listeners called when load em expression complete
    private HashSet<OnLoadExpressionListener> mLoadExpressionListeners =
            new HashSet<OnLoadExpressionListener>();

    // store the em id which was loading expression
    private Set<String> mLoadExpressionCache = new CopyOnWriteArraySet<String>();

    // em xml string matcher pattern
    public static final String EM_XML_MATCH_PATTERN =
                    "<?xml(.*?)><vemotic?on(.*?)><sms>(.*?)</sms><eid>(.*?)</eid></vemotic?on>";

    private static final String downloadFolder = "Download";
    private static final String mEmDataDir =
                    "Android/data/cn.com.expression.shop/files/EmShop/";
    private static final String mEmDownloadDir = Environment.getExternalStorageDirectory() +
                    "/" + mEmDataDir + downloadFolder + "/";

    /**.
     * Listener for RCS Server state change
     */
    private OnServiceChangedListener mOnServiceChangedListener =
                new OnServiceChangedListener() {
        @Override
        public void onServiceStateChanged(int state, final boolean activated,
                    final boolean configured, final boolean registered) {
            Log.d(TAG, "[onServiceStateChanged]: (state, activated, configured, registered): " +
                state + " " + activated + " " + configured + " " + registered);
            boolean enable = activated && configured;
            if (mServiceEnabled != enable) {
                mServiceEnabled = enable;
            }
            if (mServiceRegistered != registered) {
                mServiceRegistered = registered;
                if (registered) {
                    String myNumber = RCSServiceManager.getInstance().getMyNumber();
                    if (myNumber != null && !myNumber.equals(mUserId)) {
                        mUserId = myNumber;
                        if (initSdk(mMmsAppContext, mUserId)) {
                            loadUserEmData();
                            // set data changed listener
                            setEmPackageChangedListener();
                            mHandler = new EngineHandler();
                        }
                    }
                }
            }
        }
    };

    /**.
     * If SD card was unmounted and remounted again, need reload the em data
     */
    private BroadcastReceiver mMediaChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "mMediaChangedReceiver(), action=" + intent.getAction());
            String action = intent.getAction();
            if (action != null && action.equals(Intent.ACTION_MEDIA_MOUNTED) && mNeedUpdate) {
                if (mUserId != null) {
                    loadUserEmData();
                }
                loadDownLoadEmData();
                mNeedUpdate = false;
            } else if (action != null && action.equals(Intent.ACTION_MEDIA_UNMOUNTED)) {
                mNeedUpdate = true;
            }
        }
    };

    private EmojiShop(Context context) {
        Log.d(TAG, "EmojiShop() context = " + context);
        mMmsAppContext = context;
        if (!isSdkAvailable()) {
            Log.d(TAG, "EmojiShop() EmShopSDK.getInstance() = null, return");
            return;
        }
        RCSServiceManager.getInstance().addOnServiceChangedListener(mOnServiceChangedListener);
        IntentFilter filter = new IntentFilter(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_EJECT);
        filter.addDataScheme("file");
        mMmsAppContext.registerReceiver(mMediaChangedReceiver, filter);
        loadDownLoadEmData();
    }

    /**.
     * Init when message app init in RcsUtilsPlugin.java
     */
    public static void init(Context context) {
        Log.d(TAG, "init() context = " + context);
        if (sInstance == null) {
            sInstance = new EmojiShop(context);
        }
    }

    public static void unInit() {
        Log.d(TAG, "unInit()");
        if (sInstance != null) {
            sInstance.destroy();
            sInstance = null;
        }
    }

    public synchronized static EmojiShop getInstance() {
        if (sInstance == null) {
            throw new RuntimeException("EmojiShop is not initiated");
        }
        return sInstance;
    }

    private void destroy() {
        RCSServiceManager.getInstance().removeOnServiceChangedListener(
                mOnServiceChangedListener);
        mMmsAppContext.unregisterReceiver(mMediaChangedReceiver);
    }

    /**.
     * Init emoticon sdk
     */
    private boolean initSdk(Context context, String userId) {
        if ((context == null) || (userId == null)) {
            Log.d(TAG, "initSdk error, context or userId is null");
            return false;
        }
        if (!isSdkAvailable()) {
            return false;
        }
        if (!EmShopSDK.getInstance().init(context, userId)) {
            Log.d(TAG, "initSdk error, EmShopSDK init failed");
            return false;
        }
        if (!EmShopSDK.getInstance().isAvailable()) {
            Log.d(TAG, "initSdk error, EmShopAPK Not Available.");
            return false;
        }
        Log.d(TAG, "initSdk success");
        return true;
    }

    public static boolean isSdkAvailable() {
        if (EmShopSDK.getInstance() == null) {
            return false;
        }
        return true;
    }

    public boolean isApkAvailable() {
        if (!isSdkAvailable()) {
            return false;
        }
        PackageManager pm = mMmsAppContext.getPackageManager();
        List pInfo = pm.getInstalledPackages(0);
        for (int i = 0; i < pInfo.size(); i++) {
            if (((PackageInfo) pInfo.get(i)).packageName.
                    equalsIgnoreCase("cn.com.expression.shop")) {
                return true;
            }
        }
        return false;
    }

    private void loadDownLoadEmData() {
        Log.d(TAG, "loadEmData, mEmDownloadDir=" + mEmDownloadDir);
        File path = new File(mEmDownloadDir);
        if (!path.exists()) {
            Log.d(TAG, "path not exists, create it");
            path.mkdirs();
            return;
        }
        //Clear all cache data and reinit
        EmojiShopUtils.clearDownloadEmPaths();
        File[] files = path.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                continue;
            }
            String emName = files[i].getName();
            String emPath = files[i].getPath();
            Log.d(TAG, "name=" + emName);
            String emId = emName.split("\\.")[0];
            EmojiShopUtils.addDownloadEmPath(emId, emPath);
        }
    }

    private OnDataReceivedListener mDataReceivedListener = new OnDataReceivedListener() {
        @Override
        public void onDataReceived(List<EmData> allEmData) {
            Log.d(TAG, "onDataReceived, allEmData = " + allEmData);
            mEmPackageIdList.clear();
            EmojiShopUtils.clearUserEmPaths();
            mUserEmPkgMap.clear();
            mUserEmData.clear();
            mUserEmData = allEmData;
            mEmPkgInfo.clear();

            int pkgSize = mUserEmData.size();
            for (int i = 0; i < pkgSize; i++) {
                EmData emData = mUserEmData.get(i);

                EmojiPackage pkgInfo = new EmojiPackage(emData);
                mEmPkgInfo.add(pkgInfo);
                EmPackagef epf = emData.getEmpackagef();
                String packageId = epf.getmId();
                mEmPackageIdList.add(packageId);
                List<Em> emlist = emData.getEmlist();
                for (int j = 0; j < emlist.size(); j++) {
                    EmojiShopUtils.addUserEmPath(emlist.get(j).getmId(), emlist.get(j).getmImage());
                    if (!mUserEmPkgMap.containsKey(emlist.get(j).getmId())) {
                        mUserEmPkgMap.put(emlist.get(j).getmId(), packageId);
                    }
                }
            }
            mUserDataAvailable = true;
            // notify UI
            notifyEmDataChanged();
        }
    };

    private void loadUserEmData() {
        Log.d(TAG, "loadUserEmData, enter");
        try {
            EmShopSDK.getInstance().getEmListData(mDataReceivedListener);
        } catch (UninitializedException ex) {
            Log.d(TAG, "catch UninitializedException");
        } catch (AppNotInstalledException e) {
            Log.d(TAG, "catch AppNotInstalledException");
        } finally {
            // do nothing
        }
    }

    private void setEmPackageChangedListener() {
        Log.d(TAG, "setEmPackageChangedListener");
        EmShopSDK.getInstance().setOnEmPackageChangedListener(new OnEmPackageChangedListener() {
            @Override
            public void onAdd(String idname) {
                // new em data download success
                Log.d(TAG, "onAdd packageId: " + idname);
                loadUserEmData();
            }

            @Override
            public void onDelete(String idname) {
                //em data was delete success
                Log.d(TAG, "onDelete packageId: " + idname);
                loadUserEmData();
            }
        });
    }

    private DownLoadEmListener mDownloadEmListener = new DownLoadEmListener() {
        @Override
        public void loadComplete(String filePath) {
            Log.d(TAG, "loadComplete() filePath=" + filePath);
            File file = new File(filePath);
            String emName = file.getName();
            String emId = emName.split("\\.")[0];
            Log.d(TAG, "loadComplete() emId=" + emId);
            EmojiShopUtils.addDownloadEmPath(emId, filePath);
            if (mLoadExpressionCache.contains(emId)) {
                mLoadExpressionCache.remove(emId);
            }
            notifyLoadExpressionComplete(200, emId);
        }

        @Override
        public void loadFailure(int errorCode) {
            Log.d(TAG, "loadFailure() errorCode=" + errorCode);
            // 426: data not exit
            // 406: wrong token
            // 101: network error
            // 102: storage error, etc. SD full
            notifyLoadExpressionComplete(errorCode, null);
        }
    };

    private static String sToken = "token";
    private static final int MSG_ERROR = -1;
    private static final int MSG_SUCCESS = 0;
    private EngineHandler mHandler;
    private class EngineHandler extends Handler {
        @Override
        public void handleMessage(final Message msg) {
            Log.d(TAG, "handleMessage " + msg);
            switch (msg.what) {
            case MSG_ERROR:
                int errorCode = (int) msg.obj;
                Log.d(TAG, "MSG_ERROR, errorCode=" + errorCode);
                notifyLoadExpressionComplete(errorCode, null);
                break;

            case MSG_SUCCESS:
                final String emId = (String) msg.obj;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.d(TAG, "emId = " + emId);
                            Log.d(TAG, "sToken = " + sToken);
                            // resolutionIndex: 1: 1024x600; 2:800x480; 3:480x320; 4: 640x360
                            EmShopSDK.getInstance().loadExpression(
                                    mEmDownloadDir, sToken, emId, "3", mDownloadEmListener);
                        } catch (FilePathNotFoundException ex) {
                            Log.d(TAG, "catch FilePathNotFoundException, e=" + ex);
                        } catch (UninitializedException e) {
                            Log.d(TAG, "catch UninitializedException, e=" + e);
                        } catch (Exception e) {
                            Log.d(TAG, "catch Exception, e=" + e);
                        } finally {
                            // do nothing
                        }
                    }
                }).start();
                break;

            default:
                super.handleMessage(msg);
                break;
            }
        }
    }

    private boolean getToken(Activity activity, final String emID) {
        Log.d(TAG, "getToken() emID = " + emID);
        final String appId = "01000145";
        final String appKey = "DAA231C8F8E53CC4";
        AuthnHelper authHelper = new AuthnHelper(activity);
        authHelper.setDefaultUI(true);
        try {
            authHelper.getAccessToken(appId, appKey, null,
                        SsoSdkConstants.LOGIN_TYPE_DEFAULT,
                        new TokenListener() {
                @Override
                public void onGetTokenComplete(JSONObject json) {
                    Log.d(TAG, "onGetTokenComplete");
                    if (json == null) {
                        Log.d(TAG, "json is null!");
                        if (mHandler != null) {
                            mHandler.obtainMessage(MSG_ERROR, -1).sendToTarget();
                        }
                        return;
                    }
                    Log.d(TAG, json.toString());
                    try {
                        Integer result = (Integer) json.get("resultCode");
                        if (result == AuthnConstants.CLIENT_CODE_SUCCESS) {
                            sToken = (String) json.get("token");
                            Log.d(TAG, "  resultCode:" + result
                                    + "\n  token:" + sToken
                                    + "\n  passid:" + (String) json.get("passid"));
                            if (mHandler != null) {
                                mHandler.obtainMessage(MSG_SUCCESS, emID).sendToTarget();
                            }
                            return;
                        }
                        if (mHandler != null) {
                            mHandler.obtainMessage(MSG_ERROR, result).sendToTarget();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (BadTokenException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    // resolutionIndex: 1: 1024x600; 2:800x480; 3:480x320; 4: 640x360
    private void loadExpression(Activity activity, final String emId,
                final String resolutionIndex) {
        Log.d(TAG, "loadExpression() " + " emId=" + emId);
        getToken(activity, emId);
    }

    public void startMain() {
        Log.d(TAG, "startMain()");
        if (!isSdkAvailable()) {
            return;
        }
        try {
            EmShopSDK emShopSDK = EmShopSDK.getInstance();
            emShopSDK.init(mMmsAppContext, mUserId);
            emShopSDK.startMain();
        } catch (UninitializedException ex) {
            Log.d(TAG, "catch UninitializedException");
        } catch (AppNotInstalledException e) {
            Log.d(TAG, "catch AppNotInstalledException");
        } finally {
            // do nothing
        }
    }

    public void startManager() {
        Log.d(TAG, "startManager()");
        if (!isSdkAvailable()) {
            return;
        }
        try {
            EmShopSDK emShopSDK = EmShopSDK.getInstance();
            emShopSDK.init(mMmsAppContext, mUserId);
            emShopSDK.startManager();
        } catch (UninitializedException ex) {
            Log.d(TAG, "catch UninitializedException");
        } catch (AppNotInstalledException e) {
            Log.d(TAG, "catch AppNotInstalledException");
        } finally {
            // do nothing
        }
    }

    public void showDetail(String emId) {
        Log.d(TAG, "showDetail() emId=" + emId);
        if (!isSdkAvailable()) {
            return;
        }
        try {
            EmShopSDK emShopSDK = EmShopSDK.getInstance();
            emShopSDK.init(mMmsAppContext, mUserId);
            emShopSDK.showDetail(emId);
        } catch (UninitializedException ex) {
            Log.d(TAG, "catch UninitializedException");
        } catch (AppNotInstalledException e) {
            Log.d(TAG, "catch AppNotInstalledException");
        } finally {
            // do nothing
        }
    }

    private static String createEmXml(EmSimple emSimple) {
        return EmShopSDK.getInstance().createEmXml(emSimple);
    }

    private static EmSimple parseEmXml(String emXml) {
        return EmShopSDK.getInstance().parseEmXml(emXml);
    }

   /**.
     * Get the package Id which the package index is @index
     *
     * @param index index of the package
     * @return String package id
     */
    public String getEmPackageId(int index) {
        if (!isSdkAvailable()) {
            return null;
        }
        if (index >= mEmPackageIdList.size() || index <= -1) {
            return null;
        }
        return mEmPackageIdList.get(index);
    }

    private int getEmPackageIndex(String emPkgId) {
        for (int i = 0; i < mEmPackageIdList.size(); i++) {
            if (mEmPackageIdList.get(i).equals(emPkgId)) {
                return i;
            }
        }
        return -1;
    }


   /**.
     * persist the em xml string  by package id and em id
     *
     * @param emPkgId id of the package
     * @param emId id of the em
     * @return String em string
     */
    public String createEmXml(String emPkgId, String emId) {
        if (!isSdkAvailable()) {
            return null;
        }
        int index = getEmPackageIndex(emPkgId);
        if (index != -1) {
            EmData emData = mUserEmData.get(index);
            List<Em> emList = emData.getEmlist();
            for (int i = 0; i < emList.size(); i++) {
                if (emList.get(i).getmId().equals(emId)) {
                    String content = emList.get(i).getmTip();
                    EmSimple emSimple = new EmSimple(content, emId);
                    return createEmXml(emSimple);
                }
            }
        }
        return null;
    }

    private Em getEmByXml(String emXml) {
        EmSimple emSimple = parseEmXml(emXml);
        final String emId = emSimple.getId();
        String pkgId = mUserEmPkgMap.get(emId);
        if (pkgId != null) {
            int index = getEmPackageIndex(pkgId);
            if (index != -1) {
                EmData emData = mUserEmData.get(index);
                List<Em> emList = emData.getEmlist();
                for (int i = 0; i < emList.size(); i++) {
                    Em em = emList.get(i);
                    if (em.getmId().equals(emId)) {
                        return em;
                    }
                }
            }
        }
        return null;
    }

    public static boolean isLocalEmoticon(String emId) {
        return EmojiShopUtils.containsEmId(emId);
    }

    public static String getEmPathById(String emId) {
        return EmojiShopUtils.getEmPathById(emId);
    }

    // not used now
    public static String getEmPathByXml(String emXml) {
        if (!isSdkAvailable()) {
            return null;
        }
        EmSimple emSimple = parseEmXml(emXml);
        String emId = emSimple.getId();
        if (isLocalEmoticon(emId)) {
            return getEmPathById(emId);
        }
        Log.d(TAG, "getEmPathByXml() no found");
        return null;
    }

    // not used now
    public static Uri getEmResUriByXml(String emXml) {
        String path = getEmPathByXml(emXml);
        if (path != null) {
            return Uri.fromFile(new File(path));
        }
        return null;
    }

    public static String getEmIdByXml(String emXml) {
        if (!isSdkAvailable()) {
            return "0";
        }
        EmSimple emSimple = parseEmXml(emXml);
        return emSimple.getId();
    }

    // if not found the emoticon resource on local, download from server
    public void loadEmIconsFromServer(Activity activity, final String emId) {
        Log.d(TAG, "loadEmIconsFromServer() activity =" + activity + ", emId=" + emId);
        if (!isSdkAvailable()) {
            return;
        }
        // if downloading now, not download again
        if (!mLoadExpressionCache.contains(emId)) {
            mLoadExpressionCache.add(emId);
            getToken(activity, emId);
        }
    }

    public static String parseEmSmsString(String emXml) {
        if (emXml == null) {
            return null;
        }
        if (!isSdkAvailable()) {
            return emXml;
        }
        try {
            EmSimple emMsg = null;
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(new StringReader(emXml));
            int event = parser.getEventType();

            while (event != -1) {
                switch (event) {
                case 0:
                    emMsg = new EmSimple();
                    break;
                case 2:
                    if (emMsg != null) {
                        if ("sms".equalsIgnoreCase(parser.getName())) {
                            return parser.nextText();
                        }
                    }
                default:
                    break;
                }
                event = parser.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return emXml;
    }

    public static boolean matchEmXml(String emXml) {
        if (!isSdkAvailable()) {
            return false;
        }
        Pattern pattern = Pattern.compile(EM_XML_MATCH_PATTERN);
        Matcher matcher = pattern.matcher(emXml);
        while (matcher.find()) {
            return true;
        }
        return false;
    }

    public List<EmojiPackage> getAllPackageInfo() {
        if (isSdkAvailable() && !mUserDataAvailable && initSdk(mMmsAppContext, mUserId)) {
            loadUserEmData();
            // set data changed listener
            setEmPackageChangedListener();
            mHandler = new EngineHandler();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return mEmPkgInfo;
    }

    // not used now
    public EmojiPackage getPackageInfoById(String pkgId) {
        for (int i = 0; i < mEmPkgInfo.size(); i++) {
            if (mEmPkgInfo.get(i).getPkgId().equals(pkgId)) {
                return mEmPkgInfo.get(i);
            }
        }
        return null;
    }

    public class EmojiPackage {
        private String mPkgId;
        private String mPkgIcon;
        // <emId, emPath>
        private HashMap<String, String> mEmIcons = new HashMap<String, String>();

        EmojiPackage(EmData emData) {
            EmPackagef epf = emData.getEmpackagef();
            mPkgId = epf.getmId();
            mPkgIcon = epf.getmIconColor();
            List<Em> emlist = emData.getEmlist();
            for (int i = 0; i < emlist.size(); i++) {
                mEmIcons.put(emlist.get(i).getmId(), emlist.get(i).getmImage());
            }
        }

        public String getPkgId() {
            return mPkgId;
        }

        public String getPkgIcon() {
            return mPkgIcon;
        }

        public HashMap<String, String> getEmIcons() {
            return mEmIcons;
        }
    }

  /**.
    * Callback if em data changed
    * called when download/remove emoticon packages complete, or emojiShop init.
    */
    public interface OnEmDataChangedListener {
        void onEmDataChanged();
    }

    public boolean addOnEmDataChangedListener(OnEmDataChangedListener listener) {
        if (EmShopSDK.getInstance() == null) {
            return false;
        }
        return mEmDataChangedlisteners.add(listener);
    }

    public boolean removeOnEmDataChangedListener(OnEmDataChangedListener listener) {
        if (EmShopSDK.getInstance() == null) {
            return false;
        }
        return mEmDataChangedlisteners.remove(listener);
    }

    private void notifyEmDataChanged() {
        Log.d(TAG, "notifyEmDataChanged");
        for (OnEmDataChangedListener l : mEmDataChangedlisteners) {
            l.onEmDataChanged();
        }
    }

    /**.
     * Callback when download expression from emoticon server success/failed
     */
    public interface OnLoadExpressionListener {
        void onLoadExpressionComplete(int result, String emId);
    }

    public boolean addOnLoadExpressionListener(OnLoadExpressionListener listener) {
        if (EmShopSDK.getInstance() == null) {
            return false;
        }
        return mLoadExpressionListeners.add(listener);
    }

    public boolean removeOnLoadExpressionListener(OnLoadExpressionListener listener) {
        if (EmShopSDK.getInstance() == null) {
            return false;
        }
        return mLoadExpressionListeners.remove(listener);
    }

    private void notifyLoadExpressionComplete(int result, String emId) {
        Log.d(TAG, "notifyLoadExpressionComplete, result = " + result + ", emId = " + emId);
        for (OnLoadExpressionListener l : mLoadExpressionListeners) {
            l.onLoadExpressionComplete(result, emId);
        }
    }
}
