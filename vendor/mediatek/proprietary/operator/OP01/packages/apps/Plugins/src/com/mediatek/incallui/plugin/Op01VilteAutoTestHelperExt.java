package com.mediatek.incallui.plugin;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.android.incallui.CallButtonPresenter;
import com.android.incallui.AnswerPresenter;

import com.mediatek.common.PluginImpl;
import com.mediatek.incallui.ext.DefaultVilteAutoTestHelperExt;
import com.mediatek.incallui.ext.IVilteAutoTestHelperExt;

@PluginImpl(interfaceName = "com.mediatek.incallui.ext.IVilteAutoTestHelperExt")
public class Op01VilteAutoTestHelperExt extends DefaultVilteAutoTestHelperExt {

   private static final String TAG = "Op01VilteAutoTestHelperExt";
   private Context mContextCallButtonFragment;
   private Context mContextAnswerFragment;
   private CallButtonPresenter mCallButtonPresenter;
   private AnswerPresenter mAnswerPresenter;

   public static final String ACTION_ACCEPT_UPGRADE
            = "mediatek.intent.action.ACTION_ACCEPT_UPGRADE";
   public static final String ACTION_REJECT_UPGRADE
            = "mediatek.intent.action.ACTION_REJECT_UPGRADE";
   public static final String ACTION_UPGRADE_VIDEO
            = "mediatek.intent.action.ACTION_UPGRADE_VIDEO";
   public static final String ACTION_DOWNGRADE_AUDIO
            = "mediatek.intent.action.ACTION_DOWNGRADE_AUDIO";
   public static final String ACTION_PAUSE_VIDEO
            = "mediatek.intent.action.ACTION_PAUSE_VIDEO";
   public static final String ACTION_RESTART_VIDEO
            = "mediatek.intent.action.ACTION_RESTART_VIDEO";
   public static final String ACTION_MERGE
            = "mediatek.intent.action.ACTION_MERGE";

   private static int VIDEO_STATE = 3;

   private final BroadcastReceiver mReceiverForAcceptAndRejectUpgrade = new BroadcastReceiver() {
       @Override
       public void onReceive(Context context, Intent intent) {
           String action = intent.getAction();
           log("action: " + action);
           if (action.equals(ACTION_ACCEPT_UPGRADE)) {
               mAnswerPresenter.onAnswer(VIDEO_STATE, mContextAnswerFragment);
           }else if (action.equals(ACTION_REJECT_UPGRADE)) {
               mAnswerPresenter.onDecline(mContextAnswerFragment);
           }
       }
    };

    private final BroadcastReceiver mReceiverForUpgradeAndDowngrade = new BroadcastReceiver() {
       @Override
       public void onReceive(Context context, Intent intent) {
           String action = intent.getAction();
           log("action: " + action);
           if (action.equals(ACTION_UPGRADE_VIDEO)) {
               mCallButtonPresenter.changeToVideoClicked();
           }else if (action.equals(ACTION_DOWNGRADE_AUDIO)) {
               mCallButtonPresenter.changeToVoiceClicked();
           }else if (action.equals(ACTION_PAUSE_VIDEO)) {
               mCallButtonPresenter.pauseVideoClicked(true);
           }else if (action.equals(ACTION_RESTART_VIDEO)) {
               mCallButtonPresenter.pauseVideoClicked(false);
           }else if (action.equals(ACTION_MERGE)) {
               mCallButtonPresenter.mergeClicked();
           }
       }
    };

  /**
    * called when CallButtonFragment execute onActivityCreated.
    * register BroadcastReceiver to receiver vilte auto test  broadcast
    * @param context host Context
    * @param callButtonPresenter the CallButtonPresenter instance
    */

    @Override
    public void registerReceiverForUpgradeAndDowngrade(Context context, Object obj ) {
        mContextCallButtonFragment = context;
        if (obj instanceof CallButtonPresenter) {
            log("get CallButtonPresenter");
            mCallButtonPresenter = (CallButtonPresenter) obj;
        }
        IntentFilter filter= new IntentFilter(ACTION_UPGRADE_VIDEO);
        filter.addAction(ACTION_DOWNGRADE_AUDIO);
        filter.addAction(ACTION_PAUSE_VIDEO);
        filter.addAction(ACTION_RESTART_VIDEO);
        filter.addAction(ACTION_MERGE);
        mContextCallButtonFragment.registerReceiver(mReceiverForUpgradeAndDowngrade, filter);
        log("register up/down/pause/merge receiver");
    }

  /**
    * called when AnswerFragment execute onCreateView.
    * register BroadcastReceiver to receiver vilte auto test  broadcast
    * @param context host Context
    * @param obj the instance of AnswerPresenter
    */
    @Override
    public void registerReceiverForAcceptAndRejectUpgrade(Context context, Object obj ) {
        mContextAnswerFragment = context;
        if (obj instanceof AnswerPresenter) {
            log("get AnswerPresenter");
            mAnswerPresenter = (AnswerPresenter) obj;
        }
        IntentFilter filter= new IntentFilter(ACTION_ACCEPT_UPGRADE);
        filter.addAction(ACTION_REJECT_UPGRADE);
        mContextAnswerFragment.registerReceiver(mReceiverForAcceptAndRejectUpgrade, filter);
        log("register accept/reject receiver");
    }

  /**
    * called when CallButtonPresenter execute onUiUnready
    * unregister BroadcastReceiver
    */
    @Override
    public void unregisterReceiverForUpgradeAndDowngrade( ) {
        if (mContextCallButtonFragment != null ) {
            mContextCallButtonFragment.unregisterReceiver(mReceiverForUpgradeAndDowngrade);
            mContextCallButtonFragment = null;
            mCallButtonPresenter = null;
            log("unregister up/down receiver");
        }
    }

  /**
    * called when AnswerFragment execute  onDestoryView
    * unregister BroadcastReceiver
    */
    @Override
    public void unregisterReceiverForAcceptAndRejectUpgrade( ) {
        if (mContextAnswerFragment != null ) {
            mContextAnswerFragment.unregisterReceiver(mReceiverForAcceptAndRejectUpgrade);
            mContextAnswerFragment = null;
            mAnswerPresenter = null;
            log("unregister accept/reject receiver");
        }
    }

    public void log(String msg) {
        Log.d(TAG, msg);
    }

}

