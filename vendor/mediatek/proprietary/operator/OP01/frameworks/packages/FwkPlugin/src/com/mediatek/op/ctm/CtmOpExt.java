package com.mediatek.op.ctm;

import com.mediatek.common.ctm.ICtmExt;
import com.mediatek.common.PluginImpl;

@PluginImpl(interfaceName="com.mediatek.common.ctm.ICtmExt")
public class CtmOpExt extends CtmExt {
    @Override
    public String getName() {
        return "cn.richinfo.dm";
    }
}
