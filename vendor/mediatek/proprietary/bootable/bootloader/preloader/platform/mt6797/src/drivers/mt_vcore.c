/* Copyright Statement:
*
* This software/firmware and related documentation ("MediaTek Software") are
* protected under relevant copyright laws. The information contained herein is
* confidential and proprietary to MediaTek Inc. and/or its licensors. Without
* the prior written permission of MediaTek inc. and/or its licensors, any
* reproduction, modification, use or disclosure of MediaTek Software, and
* information contained herein, in whole or in part, shall be strictly
* prohibited.
*
* MediaTek Inc. (C) 2016. All rights reserved.
*
* BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
* ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
* WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
* WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
* NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
* RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
* INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
* TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
* RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
* OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
* SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
* RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
* STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
* ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
* RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
* MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
* CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
* The following software/firmware and/or related documentation ("MediaTek
* Software") have been modified by MediaTek Inc. All revisions are subject to
* any receiver's applicable license agreements with MediaTek Inc.
*/

#include "platform.h"
#include "typedefs.h"
#include "sec_devinfo.h"

#define TAG	"xxxxEEM"
#define EEM_INFO(str, ...) do {             \
	print("[%s] " str, TAG, ##__VA_ARGS__); \
} while(0)

#define SOC_VOLTAGE_BIN 0x10206970
#define VCOREFS_FW_LPM 0
#define VCOREFS_FW_HPM 1
#define VCOREFS_FW_ULTRA 2
#define VCORE_VOLT_0 0
#define VCORE_VOLT_1 1
#define VCORE_VOLT_2 2

/* SOC Voltage */
unsigned int DDR1866[4] = {105000, 102500, 100000, 97500};
unsigned int DDR1700[4] = {100000, 97500, 95000, 95000};
unsigned int DDR1270[4] = {90000, 87500, 87500, 87500};
unsigned int DDR1066[4] = {90000, 87500, 87500, 87500};

unsigned int get_vcore_ptp_volt(unsigned int opp, unsigned int speed)
{
	unsigned int ret, vcore0, vcore1, vcore2, soc_efuse;

	/* Read EFUSE */
	soc_efuse = DRV_Reg32(SOC_VOLTAGE_BIN) & 0xFF;
	EEM_INFO("[VCORE] - Kernel Got efuse 0x%0X\n", soc_efuse);

	/* Read SODI level U, H, L */
	switch (speed) {
	case VCOREFS_FW_LPM: /* 1066/1270/1600 */
		EEM_INFO("[DDR] segment = 1600\n");
		vcore0 = DDR1700[((soc_efuse >> 4) & 0x03)];
		vcore1 = DDR1270[((soc_efuse >> 2) & 0x03)];
		vcore2 = DDR1066[(soc_efuse & 0x03)];
		break;

	case VCOREFS_FW_HPM: /* 1066/1270/1700 */
		EEM_INFO("[DDR] segment = 1700\n");
		vcore0 = DDR1700[((soc_efuse >> 4) & 0x03)];
		vcore1 = DDR1270[((soc_efuse >> 2) & 0x03)];
		vcore2 = DDR1066[(soc_efuse & 0x03)];
		break;

	case VCOREFS_FW_ULTRA: /* 1066/1270/1866 */
		EEM_INFO("[DDR] segment = 1866\n");
		vcore0 = DDR1866[((soc_efuse >> 6) & 0x03)];
		vcore1 = DDR1270[((soc_efuse >> 2) & 0x03)];
		vcore2 = DDR1066[(soc_efuse & 0x03)];
		break;

	default:
		vcore0 = DDR1700[((soc_efuse >> 4) & 0x03)];
		vcore1 = DDR1270[((soc_efuse >> 2) & 0x03)];
		vcore2 = DDR1066[(soc_efuse & 0x03)];
		break;
	}

	vcore1 = (vcore1 < vcore2) ? vcore2 : vcore1;
	vcore0 = (vcore0 < vcore1) ? vcore1 : vcore0;

	EEM_INFO("[EEM][VCORE] - Preloader Got from DT (0x%0X, 0x%0X, 0x%0X)\n",
			vcore0, vcore1, vcore2);

	switch (opp) {
	case VCORE_VOLT_0:
		ret = vcore0;
		break;

	case VCORE_VOLT_1:
		ret = vcore1;
		break;

	case VCORE_VOLT_2:
		ret = vcore2;
		break;

	default:
		ret = 100000;
		break;
	}

	return ret;
}
