package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.SwitchPreference;

import com.mediatek.sensorhub.settings.Utils;

public class StationarySensorActivity extends BaseActivity implements OnPreferenceChangeListener {

    public static final String KEY_ENABLE_NOTIFY = "enable_notify_stationary";
    private static final String TAG = "StationarySensorActivity";
    private SwitchPreference mStationaryPreference;
    private SwitchPreference mEnableNotifiSwitch;

    public StationarySensorActivity() {
        super(Utils.KEY_STATIONARY_STATUS);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.sensor_stationary_pref);
        initializeAllPreferences();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatePreferenceStatus();
    }

    private void initializeAllPreferences() {
        mStationaryPreference = (SwitchPreference) Utils.createPreference(Utils.TYPE_SWITCH,
                mSensorKeyMap.get(Sensor.TYPE_STATIONARY).getName(), String
                        .valueOf(Sensor.TYPE_STATIONARY), getPreferenceScreen(), this);
        mStationaryPreference.setOrder(-1);
        mEnableNotifiSwitch = (SwitchPreference) findPreference(KEY_ENABLE_NOTIFY);
        mEnableNotifiSwitch.setOnPreferenceChangeListener(this);
    }

    private void updatePreferenceStatus() {
        mStationaryPreference.setChecked(Utils.getSensorStatus(Utils.KEY_STATIONARY_STATUS));
        mEnableNotifiSwitch.setChecked(Utils.getSensorStatus(Utils.KEY_STATIONARY_NOTIFY_STATUS));
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean bNewValue = (Boolean) newValue;
        if (preference == mStationaryPreference) {
            Utils.setSensorStatus(Utils.KEY_STATIONARY_STATUS, bNewValue);
            if (mBound) {
                mSensorService.registerSensor(preference.getKey(), bNewValue);
            }
        } else if (preference == mEnableNotifiSwitch) {
            Utils.setSensorStatus(Utils.KEY_STATIONARY_NOTIFY_STATUS, bNewValue);
        }
        return true;
    }

    @Override
    public void onSensorChanged(float[] value) {
        // Log.d(TAG, "onSensorChanged value is: " + value[0]);
        mStationaryPreference.setSummary(String.valueOf(value[0]));
    }
}
