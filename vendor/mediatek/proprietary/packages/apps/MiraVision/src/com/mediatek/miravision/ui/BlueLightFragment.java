package com.mediatek.miravision.ui;

import com.mediatek.miravision.setting.MiraVisionJni;
import com.mediatek.miravision.setting.MiraVisionJni.Range;

import android.app.ActionBar;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;

public class BlueLightFragment extends BaseTuningFragment implements
        CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "Miravision/BlueLightFragment";
    private Switch mActionBarSwitch;
    private Range mBlueLightRange;

    public BlueLightFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        View rootView = super.onCreateView(inflater, container, savedInstance);

        // Action bar
        mActionBarSwitch = new Switch(inflater.getContext());
        final int padding = getResources().getDimensionPixelSize(R.dimen.action_bar_switch_padding);
        mActionBarSwitch.setPaddingRelative(0, 0, padding, 0);
        getActivity().getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_SHOW_CUSTOM);
        getActivity().getActionBar()
                .setCustomView(
                        mActionBarSwitch,
                        new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER_VERTICAL
                                        | Gravity.END));
        mActionBarSwitch.setOnCheckedChangeListener(this);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUi();
    }

    @Override
    public void onDestroy() {
        getActivity().getActionBar().setCustomView(null);
        super.onDestroy();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            onSeekBarChange(seekBar.getProgress());
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    private void onSeekBarChange(int value) {
        Log.d(TAG, "onChange value = " + value);
        MiraVisionJni.setBlueLightIndex(value - mBlueLightRange.min);
    }

    private void updateUi() {
        mImageBasicAfter.setVisibility(View.GONE);
        mLineView.setVisibility(View.GONE);
        mAfterText.setVisibility(View.GONE);
        mOrignalText.setVisibility(View.GONE);
        mImageAdvAfter.setVisibility(View.GONE);
        mLastButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);

        // upate ui
        boolean blueLightEnabled = MiraVisionJni.isBlueLightEnabled();
        mActionBarSwitch.setChecked(blueLightEnabled);
        mSeekBar.setEnabled(blueLightEnabled);
        mBlueLightRange = MiraVisionJni.getBlueLightIndexRange();
        mSeekBarText.setText(R.string.blue_light_item);
        mSeekBar.setMax(mBlueLightRange.max - mBlueLightRange.min);
        mSeekBar.setProgress(MiraVisionJni.getBlueLightIndex()- mBlueLightRange.min);
        mImageOrignal.setImageResource(R.drawable.blulight_defender);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Log.d(TAG, "onCheckedChanged " + isChecked);
        MiraVisionJni.enableBlueLight(isChecked);
        mSeekBar.setEnabled(isChecked);
    }
}
