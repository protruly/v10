package com.mediatek.batterywarning;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.internal.app.AlertActivity;
import com.android.internal.app.AlertController;

/**
  * ThermalWarningActivity: show warning dialog when thermal is over temperature.
  */
public class ThermalWarningActivity extends AlertActivity implements
        DialogInterface.OnClickListener {
    private static final String TAG = "ThermalWarningActivity";
    protected static final String KEY_TYPE = "type";

    private static final int[] sWarningMsg = new int[] { R.string.thermal_clear_temperature,
            R.string.thermal_over_temperature };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        int type = intent.getIntExtra(KEY_TYPE, -1);
        Log.d(TAG, "onCreate, type is " + type);
        showWarningDialog(type);
    }

    private void showWarningDialog(int type) {
        warningMessageDialog(sWarningMsg[type]);
    }

    /**
     *
     * @param context
     *            The Context that had been passed to
     *            {@link #warningMessageDialog(int)}
     * @param messageResId
     *            Set the message using the given resource id.
     */
    private void warningMessageDialog(int messageResId) {
        final AlertController.AlertParams p = mAlertParams;
        p.mTitle = "";
        p.mView = createView(messageResId);
        p.mPositiveButtonText = getString(android.R.string.yes);
        p.mPositiveButtonListener = this;
        setupAlert();
    }

    private View createView(int messageResId) {
        View view = getLayoutInflater().inflate(R.layout.thermal_warning, null);
        TextView mMessageView = (TextView) view.findViewById(R.id.subtitle);
        mMessageView.setText(messageResId);
        return view;
    }

    public void onClick(DialogInterface dialogInterface, int button) {
        Log.d(TAG, "onClick");
        if (button == DialogInterface.BUTTON_POSITIVE) {
            finish();
            return;
        }
    }
}
