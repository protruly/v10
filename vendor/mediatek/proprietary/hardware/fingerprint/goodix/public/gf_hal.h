#ifndef __GF_HAL_H__
#define __GF_HAL_H__

#include "gf_error.h"

#ifdef __cplusplus
extern "C" {
#endif

gf_error_t gf_hal_init(void *dev, int max_failed_attempts);
gf_error_t gf_hal_exit(void *dev);
gf_error_t gf_hal_cancel(void *dev);

uint64_t gf_hal_pre_enroll(void *dev);
gf_error_t gf_hal_enroll(void *dev, const void *hat, uint32_t group_id, uint32_t timeout_sec);
gf_error_t gf_hal_post_enroll(void *dev);
gf_error_t gf_hal_authenticate(void *dev, uint64_t operation_id, uint32_t group_id);
uint64_t gf_hal_get_auth_id(void *dev);
gf_error_t gf_hal_remove(void *dev, uint32_t group_id, uint32_t finger_id);
gf_error_t gf_hal_set_active_group(void *dev, uint32_t group_id);

gf_error_t gf_hal_irq();
gf_error_t gf_hal_screen_on();
gf_error_t gf_hal_screen_off();
gf_error_t gf_hal_esd_check();

gf_error_t gf_hal_test_cmd(void* dev, uint32_t cmdId, const uint8_t* param, uint32_t paramLen);

#ifdef __cplusplus
}
#endif

#endif //__GF_HAL_H__
