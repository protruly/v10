#include <math.h>
#include <stdio.h>
#include "Fs16xxCalibration.h"
#include "FsPrint.h"
#include "Fs16xx_regs.h"
#include "Fs16xx_Custom.h"

static Fs16xx_Calib_State_t g_calib_state = Fs16xx_Calib_Unknown;
static int g_calib_count= 0;
static Fs16xx_Calib_Result_t g_calib_result;

const struct fs16xx_regs Fs16xx_reg_cal_normal[] = {
    {
        .reg = 0xB8,
        .value = 0x0000,
    },
    {
        .reg = 0xB4,
        .value = 0x2143,
    },
    {
        .reg = 0xB5,
        .value = 0x0000,
    },
    {
        .reg = 0xB4,
        .value = 0x0000,
    },
    {
        .reg = 0xB5,
        .value = 0x0000,
    },
    {
        .reg = 0xB4,
        .value = 0xDEBD,
    },
    {
        .reg = 0xB5,
        .value = 0x00FF,
    },
    {
        .reg = 0xB4,
        .value = 0xF778,
    },
    {
        .reg = 0xB5,
        .value = 0x007F,
    },
    {
        .reg = 0xB4,
        .value = 0x0861,
    },
    {
        .reg = 0xB5,
        .value = 0x00C0,
    },
    {
        .reg = 0xB4,
        .value = 0x2143,
    },
    {
        .reg = 0xB5,
        .value = 0x0000,
    },
    {
        .reg = 0xB4,
        .value = 0x0000,
    },
    {
        .reg = 0xB5,
        .value = 0x0000,
    },
    {
        .reg = 0xB4,
        .value = 0xDEBD,
    },
    {
        .reg = 0xB5,
        .value = 0x00FF,
    },
    {
        .reg = 0xB4,
        .value = 0xF778,
    },
    {
        .reg = 0xB5,
        .value = 0x007F,
    },
    {
        .reg = 0xB4,
        .value = 0x0861,
    },
    {
        .reg = 0xB5,
        .value = 0x00C0,
    },
};

Fs16xx_Error_t fs16xx_calibration(Fs16xx_devId_t id, int force, int storeResult) {
    int count = 0, maxTryCount = 100;
    unsigned short val, preVal = 0, minVal = 0;
    unsigned int done = 0, i;
    const int zmReadCount = 10;
    Fs16xx_Error_t err;
    unsigned char dataBuf[4];
                
    DEBUGPRINT("%s enter. id=%d force=%d storeResult=%d calibration state=%d",
        __func__, id, force, storeResult, g_calib_state);

    if(force || g_calib_state == Fs16xx_Calib_NotStarted
        || g_calib_state == Fs16xx_Calib_Failed
        || g_calib_state == Fs16xx_Calib_OTP_Failed) {

        g_calib_count++;
        g_calib_state = Fs16xx_Calib_OnGoing;

        // Disable I2S OUT first
        err = fs16xx_read_register16(id, FS16XX_I2SCTRL_REG, &val);
        if(Fs16xx_Error_OK != err) {
            DEBUGPRINT("%s exit. I2C read failed!", __func__);
            return Fs16xx_Error_I2C_NonFatal;
        }
        err = fs16xx_write_register16(id, FS16XX_I2SCTRL_REG, val & (~FS16XX_I2SCTRL_REG_I2SDOE_MSK));
        if(Fs16xx_Error_OK != err) {
            DEBUGPRINT("%s exit. I2C write failed!", __func__);
            return Fs16xx_Error_I2C_NonFatal;
        }

        fs16xx_write_register16(id, Fs16xx_reg_cal_normal[0].reg,
                                    Fs16xx_reg_cal_normal[0].value);
        for (i = 1; i < ARRAY_SIZE(Fs16xx_reg_cal_normal) - 1; i += 2) {
            dataBuf[0] = ((Fs16xx_reg_cal_normal[i].value) >> 8) & 0xFF;
            dataBuf[1] = (Fs16xx_reg_cal_normal[i].value) & 0xFF;
            dataBuf[2] = ((Fs16xx_reg_cal_normal[i + 1].value) >> 8) & 0xFF;
            dataBuf[3] = (Fs16xx_reg_cal_normal[i + 1].value) & 0xFF;
            err |= fs16xx_bulkwrite_register(id, Fs16xx_reg_cal_normal[i].reg, sizeof(dataBuf), dataBuf);
        }

        if(Fs16xx_Error_OK != err) {
            DEBUGPRINT("%s exit. Write coef failed!", __func__);
            return Fs16xx_Error_I2C_NonFatal;
        }
        // Reset I2S OUT
        err = fs16xx_write_register16(id, FS16XX_I2SCTRL_REG, val);
        if(Fs16xx_Error_OK != err) {
            DEBUGPRINT("%s exit. I2C write failed, I2S reset failed!", __func__);
            return Fs16xx_Error_I2C_NonFatal;
        }

        fs16xx_write_register16(id, FS16XX_OTPACC_REG, FS16XX_OTP_ACC_KEY2);

        // Start calibration
        fs16xx_write_register16(id, FS16XX_ZMCONFIG_REG, 0x0010);

        fs16xx_write_register16(id, 0x09, 0x0008);
        fs16xx_write_register16(id, 0xC9, 0x0000);
        fs16xx_write_register16(id, 0xCA, 0x0000);
        fs16xx_write_register16(id, 0xCB, 0x0000);
        fs16xx_write_register16(id, 0xCC, 0x0000);
        fs16xx_write_register16(id, 0xAF, CALIB_REG_NORMAL_AF);
        fs16xx_write_register16(id, 0xB9, 0xFFFF);
        fs16xx_write_register16(id, 0xBA, CALIB_REG_NORMAL_BA);

        Sleep(5000);

        while((!done) && (maxTryCount > 0)) {
            Sleep(100);
            maxTryCount--;
            if(Fs16xx_Error_OK == fs16xx_read_register16(id, FS16XX_ZMDATAL_REG, &val)) {
                if(val != 0) {
                    if (preVal == 0 || fabs(preVal - val) > SPK_CALIBRATION_MAX_ZMDELTA) {
                        preVal = val;
                        count = 1; // Reset counter
                        minVal = val;
                    } else {
                        count++;
                        if(val < minVal) {
                            minVal = val;
                        }
                    }
                }
            }
            if(count >= zmReadCount) {
                done = 1;
            }
        }

        if(done && minVal != 0) {
            g_calib_state = fs16xx_calc_coef_from_zm(id, minVal, storeResult);
            if(Fs16xx_Calib_OK == g_calib_state) {
                // Calibration OK, exit calibration
                fs16xx_write_register16(id, FS16XX_ZMCONFIG_REG, 0x0000);
            }

            if(Fs16xx_Calib_OTP_ExcceedMaxTry == g_calib_state) {
                // Calibration was done. However, OTP write failed due to counter limitation.
                // In this case, we will NOT exit calibration mode in order to report calibration failure.
            }
        } else {
            // Calibration failed
            g_calib_state = Fs16xx_Calib_Failed;
            DEBUGPRINT("%s calibration failed. done=%d minVal=0x%04X", __func__, done, minVal);
        }
    } else {
        fs16xx_write_register16(id, FS16XX_OTPACC_REG, 0);
        return Fs16xx_Error_Calib_Error;
    }

    DEBUGPRINT("%s calibration end, result = %s.", __func__, g_calib_state == Fs16xx_Calib_OK ? "OK" : "Failed");

    fs16xx_write_register16(id, FS16XX_OTPACC_REG, 0);
    return Fs16xx_Error_OK;
}

Fs16xx_Calib_State_t fs16xx_check_calibration(Fs16xx_devId_t id) {
    FILE *fp;
    int count, retry = 10;
    Fs16xx_Calib_Result_t calib_result;
    Fs16xx_Error_t err = Fs16xx_Error_OK;
    unsigned short value = 0, valueD5 = 0, valueC4 = 0;
    float r25;

    DEBUGPRINT("%s enter id=%d, g_calib_state=%d", __func__, id, g_calib_state);

    fs16xx_write_register16(id, FS16XX_OTPACC_REG, FS16XX_OTP_ACC_KEY2);
    err = fs16xx_read_register16(id, FS16XX_ZMCONFIG_REG, &valueD5);
    DEBUGPRINT("%s calibration flag get 0x%04X", __func__, valueD5);
    fs16xx_write_register16(id, FS16XX_OTPACC_REG, 0);
    if(Fs16xx_Error_OK == err && (valueD5 & 0x10)) {
        DEBUGPRINT("%s calibration flag set, need calibration!", __func__);
        g_calib_state = Fs16xx_Calib_NotStarted;
        return Fs16xx_Calib_NotStarted;
    }

    if(Fs16xx_Calib_Unknown != g_calib_state) {
        return g_calib_state;
    }

    g_calib_count = 0;

#ifdef FS16XX_CALIB_STORE_TO_OTP

    err |= fs16xx_read_register16(id, FS16XX_PLLCTRL4_REG, &valueC4);
    err |= fs16xx_write_register16(id, FS16XX_PLLCTRL4_REG, 0x000F);

    err |= fs16xx_write_register16(id, FS16XX_OTPACC_REG, FS16XX_OTP_ACC_KEY);

    // Read from 0xE8 first
    err |= fs16xx_read_register16(id, 0xE8, &value);
    if(Fs16xx_Error_OK == err && value != 0) {
        // Reverse high byte and low byte
        value = ((value >> 8) & 0xFF) | ((value << 8) & 0xFF00);
    } else {
        DEBUGPRINT("%s try to read calibration result by otp cmd.", __func__);
        // OTP page 
        err |= fs16xx_write_register16(id, FS16XX_OTPADDR_REG, 0x10);

        // OTP read clear
        err |= fs16xx_write_register16(id, FS16XX_OTPCMD_REG, 0x00);
        // OTP read
        err |= fs16xx_write_register16(id, FS16XX_OTPCMD_REG, 0x01);

        if(fs16xx_otp_is_busy(id) != 0) {
            g_calib_state = Fs16xx_Calib_OTP_Failed;
            goto calib_exit;
        }
        
        while(retry > 0) {
            fs16xx_read_register16(id, FS16XX_OTPRDATA_REG, &value);
            if(value != 0) break;
            retry--;
            Sleep(3);
        }
    }

    count = fs16xx_otp_bit_counter((value >> 8) & 0xFF);
    DEBUGPRINT("%s read calibration result = 0x%04x count=%d.", __func__, value, count);
    // Calibration Count
    if(count > 0) {
        DEBUGPRINT("%s calibrated already done.", __func__);
        g_calib_count = count;
    } else {
        // Need calibration
        g_calib_state = Fs16xx_Calib_NotStarted;
        DEBUGPRINT("%s calibrated not started.", __func__);
        goto calib_exit;
    }

    r25 = fs16xx_byte_to_r25((unsigned char)(value & 0xFF));
    DEBUGPRINT("%s read calibration result from otp r25=%f.", __func__, r25);
    g_calib_state = fs16xx_update_coef_from_r25(id, r25);

calib_exit:
    err |= fs16xx_write_register16(id, FS16XX_OTPACC_REG, 0);
    //err |= fs16xx_write_register16(id, FS16XX_BSTCTRL_REG, valueC0);
    err |= fs16xx_write_register16(id, FS16XX_PLLCTRL4_REG, valueC4);
    
#else
    fp = fopen( FS16XX_PARAM_DIR FS16XX_CALIB_RESULT_FILE_NAME, "rb");
    if (!fp)
    {
        DEBUGPRINT("%s read calibration result failed.", __func__);
        g_calib_state = Fs16xx_Calib_NotStarted;
        return Fs16xx_Calib_NotStarted;
    }

    count = fread( (void*)&calib_result, 1, sizeof(Fs16xx_Calib_Result_t), fp );
    fclose(fp);

    DEBUGPRINT("%s read calibration file, count=%d.", __func__, count);

    if(count != sizeof(Fs16xx_Calib_Result_t)) {
        // Calibration result file corrupted.
        DEBUGPRINT("%s read calibration result corrupted.", __func__);
        g_calib_state = Fs16xx_Calib_NotStarted;
        return Fs16xx_Calib_NotStarted;
    }

    g_calib_count = calib_result.calib_count;

    fs16xx_calc_coef_from_zm(id, calib_result.calib_result_zm, 0);
#endif

    return g_calib_state;
}

int fs16xx_otp_is_busy(Fs16xx_devId_t id) {
    int retry = 20;
    unsigned short value = 0;
    
    while(retry > 0) {
        // Checking OTP busy bit
        fs16xx_read_register16(id, FS16XX_OTPCMD_REG, &value);
        if((value & FS16XX_OTPCMD_REG_BUSY_MSK) == 0) {
            return 0;
        }
        retry--;
    }
    // Return non-0, otp op timeout...
    return -1;
}

int fs16xx_otp_bit_counter(unsigned short val) {
    // Count of bit 0 in val
    int count = 0, i = 0;
    while (i < FS16XX_CALIB_MAX_TRY)
    {
        if((val & (1 << i)) == 0)
        {
            count++;
        }
        i++;
    }
    return count;
}

unsigned char fs16xx_r25_to_byte(float f) {
    unsigned char val = (unsigned char)((fabs(f - R0_DEFAULT) / FS16XX_CALIB_OTP_R25_STEP) + 0.9f);
    if (val > 0x7F) {
        val = 0x7F;
    }

    return (f - R0_DEFAULT) > 0 ? val : (unsigned char)(val | 0x80);
}

float fs16xx_byte_to_r25(unsigned char b) {
    if((b & 0x80) != 0) {
        // minus value
        return R0_DEFAULT - FS16XX_CALIB_OTP_R25_STEP * (b & 0x7F);
    }

    return R0_DEFAULT + FS16XX_CALIB_OTP_R25_STEP * b;
}

Fs16xx_Calib_State_t fs16xx_calc_coef_from_zm(Fs16xx_devId_t id, unsigned short zm, int storeResult) {
    unsigned long dh, i;
    float cur, r25, re, dv;
    float cf = 63.0957f;
    float cff = 18.0891026f * 0.1f / 2;
    unsigned short spk_err, spk_m24, spk_m6, spk_rec;
    Fs16xx_Error_t err = Fs16xx_Error_OK;
    unsigned short value = 0;
    
    // Validate calibration result
    cur = (zm << 8) / ((1 << 23) * 0.07f);

    r25 = (cf * cff) / cur;
    
    DEBUGPRINT("%s calibration result zm=0x%04X r25=%f.", __func__, zm, r25);
    if (fabs(r25 / R0_DEFAULT - 1) > R0_ALLOWANCE) {
        // Calibration result invalid, need to re-calibrate
        DEBUGPRINT("%s read calibration result invalid r25=%f.", __func__, r25);
        return Fs16xx_Calib_Failed;
    }

    re = r25 + (TCOEF * r25) * (TMAX - 25);
    dv = ((cf * cff) * 0.07f) / re;
    dh = (dv * (1 << 23));
    spk_err = (unsigned short)((dh & 0xFFFF00) >> 9);

    re = r25 + (TCOEF * r25) * (SPKTMPRM24 - 25);
    dv = ((cf * cff) * 0.07f) / re;
    dh = (dv * (1 << 23));
    spk_m24 = (unsigned short)((dh & 0xFFFF00) >> 8);

    re = r25 + (TCOEF * r25) * (SPKTMPRM6 - 25);
    dv = ((cf * cff) * 0.07f) / re;
    dh = (dv * (1 << 23));
    spk_m6 = (unsigned short)((dh & 0xFFFF00) >> 8);

    re = r25 + (TCOEF * r25) * (SPKTMPRECOVERY - 25);
    dv = ((cf * cff) * 0.07f) / re;
    dh = (dv * (1 << 23));
    spk_rec = (unsigned short)((dh & 0xFFFF00) >> 8);

    g_calib_result.spk_err = spk_err;
    g_calib_result.spk_m24 = spk_m24;
    g_calib_result.spk_m6 = spk_m6;
    g_calib_result.spk_recover = spk_rec;

    if(Fs16xx_Calib_OnGoing == g_calib_state) {
        err |= fs16xx_write_register16(id, FS16XX_SPKERR_REG, spk_err);
        err |= fs16xx_write_register16(id, FS16XX_SPKM24_REG, spk_m24);
        err |= fs16xx_write_register16(id, FS16XX_SPKM6_REG, spk_m6);
        err |= fs16xx_write_register16(id, FS16XX_SPKRE_REG, spk_rec);
    } else {
        /* If calibration is not going on, do not write to device
        *  until second time speaker on.
        */
        fs16xx_write_register16(id, FS16XX_SPKERR_REG, 0x0000);
        fs16xx_write_register16(id, FS16XX_SPKM24_REG, 0x0000);
        fs16xx_write_register16(id, FS16XX_SPKM6_REG, 0x0000);
        fs16xx_write_register16(id, FS16XX_SPKRE_REG, 0x0000);

        fs16xx_write_register16(id, 0xB9, 0xFFFF);
        fs16xx_write_register16(id, 0xAF, CALIB_REG_NORMAL_AF);
        fs16xx_write_register16(id, 0xBA, CALIB_REG_INIT_BA);
    
        // Disable I2S OUT first
        fs16xx_read_register16(id, FS16XX_I2SCTRL_REG, &value);
        fs16xx_write_register16(id, FS16XX_I2SCTRL_REG, value & (~FS16XX_I2SCTRL_REG_I2SDOE_MSK));

        for (i = 0; i < ARRAY_SIZE(Fs16xx_reg_cal_normal); i++) {
            fs16xx_write_register16(id, Fs16xx_reg_cal_normal[i].reg,
                Fs16xx_reg_cal_normal[i].value);
        }

        // Reset I2S OUT
        fs16xx_write_register16(id, FS16XX_I2SCTRL_REG, value);
    }

    if(storeResult) {
#ifdef FS16XX_CALIB_STORE_TO_OTP
        err |= fs16xx_write_to_otp(id, fs16xx_r25_to_byte(r25));
#else
        err |= fs16xx_save_calib_result(zm);
#endif
    }

    DEBUGPRINT("%s calibration result spk_err=0x%04X spk_m24=0x%04X spk_m6=0x%04X spk_rec=0x%04X reg set-%s.", 
        __func__, spk_err, spk_m24, spk_m6, spk_rec, (Fs16xx_Error_OK == err) ? "OK" : "Fail");
    return (Fs16xx_Error_OK == err) ? Fs16xx_Calib_OK : g_calib_state;
}

Fs16xx_Calib_State_t fs16xx_update_coef_from_r25(Fs16xx_devId_t id, float r25) {
    unsigned long dh;
    float re, dv;
    float cf = 63.0957f;
    float cff = 18.0891026f * 0.1f / 2;
    unsigned short spk_err, spk_m24, spk_m6, spk_rec;
    unsigned short value = 0;
    Fs16xx_Error_t err = Fs16xx_Error_OK;
    unsigned int i;
    
    if (fabs(r25 / R0_DEFAULT - 1) > R0_ALLOWANCE) {
        // Calibration result invalid, need to re-calibrate
        DEBUGPRINT("%s calibration result invalid r25=%f.", __func__, r25);
        return Fs16xx_Calib_Failed;
    }

    re = r25 + (TCOEF * r25) * (TMAX - 25);
    dv = ((cf * cff) * 0.07f) / re;
    dh = (dv * (1 << 23));
    spk_err = (unsigned short)((dh & 0xFFFF00) >> 9);

    re = r25 + (TCOEF * r25) * (SPKTMPRM24 - 25);
    dv = ((cf * cff) * 0.07f) / re;
    dh = (dv * (1 << 23));
    spk_m24 = (unsigned short)((dh & 0xFFFF00) >> 8);

    re = r25 + (TCOEF * r25) * (SPKTMPRM6 - 25);
    dv = ((cf * cff) * 0.07f) / re;
    dh = (dv * (1 << 23));
    spk_m6 = (unsigned short)((dh & 0xFFFF00) >> 8);

    re = r25 + (TCOEF * r25) * (SPKTMPRECOVERY - 25);
    dv = ((cf * cff) * 0.07f) / re;
    dh = (dv * (1 << 23));
    spk_rec = (unsigned short)((dh & 0xFFFF00) >> 8);

    g_calib_result.spk_err = spk_err;
    g_calib_result.spk_m24 = spk_m24;
    g_calib_result.spk_m6 = spk_m6;
    g_calib_result.spk_recover = spk_rec;

    if(Fs16xx_Calib_OnGoing == g_calib_state) {
        err |= fs16xx_write_register16(id, FS16XX_SPKERR_REG, spk_err);
        err |= fs16xx_write_register16(id, FS16XX_SPKM24_REG, spk_m24);
        err |= fs16xx_write_register16(id, FS16XX_SPKM6_REG, spk_m6);
        err |= fs16xx_write_register16(id, FS16XX_SPKRE_REG, spk_rec);
    } else {
        /* If calibration is not going on, do not write to device
        *  until second time speaker on.
        */
        fs16xx_write_register16(id, FS16XX_SPKERR_REG, 0x0000);
        fs16xx_write_register16(id, FS16XX_SPKM24_REG, 0x0000);
        fs16xx_write_register16(id, FS16XX_SPKM6_REG, 0x0000);
        fs16xx_write_register16(id, FS16XX_SPKRE_REG, 0x0000);

        fs16xx_write_register16(id, 0xB9, 0xFFFF);
        fs16xx_write_register16(id, 0xAF, CALIB_REG_NORMAL_AF);
        fs16xx_write_register16(id, 0xBA, CALIB_REG_INIT_BA);
    
        // Disable I2S OUT first
        fs16xx_read_register16(id, FS16XX_I2SCTRL_REG, &value);
        fs16xx_write_register16(id, FS16XX_I2SCTRL_REG, value & (~FS16XX_I2SCTRL_REG_I2SDOE_MSK));

        for (i = 0; i < ARRAY_SIZE(Fs16xx_reg_cal_normal); i++) {
            fs16xx_write_register16(id, Fs16xx_reg_cal_normal[i].reg,
                Fs16xx_reg_cal_normal[i].value);
        }

        // Reset I2S OUT
        fs16xx_write_register16(id, FS16XX_I2SCTRL_REG, value);
    }

    DEBUGPRINT("%s read calibration result spk_err=0x%04X spk_m24=0x%04X spk_m6=0x%04X spk_rec=0x%04X reg set-%s.", 
        __func__, spk_err, spk_m24, spk_m6, spk_rec, (Fs16xx_Error_OK == err) ? "OK" : "Fail");
    return (Fs16xx_Error_OK == err) ? Fs16xx_Calib_OK : Fs16xx_Calib_Failed;
}


#ifdef FS16XX_CALIB_STORE_TO_OTP

/* DO NOT CHANGE THIS VALUE */
#define FS16XX_CALIB_MAX_USER_TRY   3

Fs16xx_Error_t fs16xx_write_to_otp(Fs16xx_devId_t id, unsigned char valOTP) {
    int count;
    Fs16xx_Error_t err = Fs16xx_Error_OK;
    unsigned short value = 0, valueC0 = 0, valueC4 = 0;

    DEBUGPRINT("%s enter valOTP=0x%02x.", __func__, valOTP);
    err |= fs16xx_read_register16(id, FS16XX_BSTCTRL_REG, &valueC0);
    err |= fs16xx_write_register16(id, FS16XX_BSTCTRL_REG, FS11601_OTP_BST_CFG);

    err |= fs16xx_read_register16(id, FS16XX_PLLCTRL4_REG, &valueC4);
    err |= fs16xx_write_register16(id, FS16XX_PLLCTRL4_REG, 0x000F);

    // OTP page 
    err |= fs16xx_write_register16(id, FS16XX_OTPADDR_REG, 0x10);
    // OTP read clear
    err |= fs16xx_write_register16(id, FS16XX_OTPCMD_REG, 0x00);
    // OTP read
    err |= fs16xx_write_register16(id, FS16XX_OTPCMD_REG, 0x01);

    if(fs16xx_otp_is_busy(id) != 0) {
        g_calib_state = Fs16xx_Calib_OTP_Failed;
        goto otp_op_exit;
    }
    
    fs16xx_read_register16(id, FS16XX_OTPRDATA_REG, &value);
    count = fs16xx_otp_bit_counter((value >> 8) & 0xFF);
    DEBUGPRINT("%s read calibration result = 0x%04x count=%d.", __func__, value, count);
    // Calibration Count
    if(count >= FS16XX_CALIB_MAX_USER_TRY) {
        PRINT_ERROR("%s calibrated count exceeds max.", __func__);
        g_calib_count = count;
        g_calib_state = Fs16xx_Calib_OTP_ExcceedMaxTry;
        err = Fs16xx_Error_Calib_Error;
        goto otp_op_exit;
    }

    // Write OTP page
    err |= fs16xx_write_register16(id, FS16XX_OTPADDR_REG, 0x10);

    // OTP write byte
    err |= fs16xx_write_register16(id, FS16XX_OTPWDATA_REG, (unsigned short)valOTP);

    // OTP Write cmd
    err |= fs16xx_write_register16(id, FS16XX_OTPCMD_REG, 0x02);

    g_calib_count = count + 1;
    
    if(fs16xx_otp_is_busy(id) != 0) {
        g_calib_state = Fs16xx_Calib_OTP_Failed;
        err = Fs16xx_Error_Calib_Error;
        goto otp_op_exit;
    }

    // OTP page
    err |= fs16xx_write_register16(id, FS16XX_OTPADDR_REG, 0x10);
    // OTP read clear
    err |= fs16xx_write_register16(id, FS16XX_OTPCMD_REG, 0x00);
    // OTP Read
    err |= fs16xx_write_register16(id, FS16XX_OTPCMD_REG, 0x01);

    if(fs16xx_otp_is_busy(id) != 0) {
        g_calib_state = Fs16xx_Calib_OTP_Failed;
        err = Fs16xx_Error_Calib_Error;
        goto otp_op_exit;
    }

    fs16xx_read_register16(id, FS16XX_OTPRDATA_REG, &value);
    /* Reload OTP */
    /*err |= fs16xx_write_register16(id, FS16XX_BSTCTRL_REG, FS11601_OTP_BST_CFG | 0x01);
    err |= fs16xx_write_register16(id, FS16XX_BSTCTRL_REG, FS11601_OTP_BST_CFG);
    Sleep(10);
    err |= fs16xx_read_register16(id, 0xE8, &value);
    if(Fs16xx_Error_OK == err && value != 0) {
        // Reverse high byte and low byte
        value = ((value >> 8) & 0xFF) | ((value << 8) & 0xFF00);
    } */
    if((value & 0xFF) == valOTP) {
        // Update count
        g_calib_count = fs16xx_otp_bit_counter((value >> 8) & 0xFF);
        DEBUGPRINT("%s readback succeeded with count = %d.", __func__, g_calib_count);
    } else {
        err = Fs16xx_Error_Calib_Error;
        PRINT_ERROR("%s calibrated read back failed with value = %d(expected = %d). Should not happen.", __func__, value, valOTP);
    }

otp_op_exit:
    err |= fs16xx_write_register16(id, FS16XX_BSTCTRL_REG, valueC0);
    err |= fs16xx_write_register16(id, FS16XX_PLLCTRL4_REG, valueC4);

    DEBUGPRINT("%s exit, error = %d.", __func__, err);

    return err;
}

#else

Fs16xx_Error_t fs16xx_save_calib_result(unsigned short zmdata) {
    FILE *fp;
    int count;
    Fs16xx_Calib_Result_t calib_result;
    
    DEBUGPRINT("%s enter.", __func__);
    
    // Write to file
    fp = fopen( FS16XX_PARAM_DIR FS16XX_CALIB_RESULT_FILE_NAME, "wb");
    if (!fp)
    {
        DEBUGPRINT("%s open calibration file failed.", __func__);
        return Fs16xx_Error_Calib_Error;
    }

    calib_result.calib_count = g_calib_count;
    calib_result.calib_ver = FS16XX_CALIB_VERSION;
    calib_result.calib_result_zm = zmdata;
    count = fwrite( (void*)&calib_result, 1, sizeof(Fs16xx_Calib_Result_t), fp );
    fclose(fp);

    DEBUGPRINT("%s write to calibration file, count=%d.", __func__, count);

    return Fs16xx_Error_OK;
}
#endif

Fs16xx_Error_t fs16xx_set_tempr_protection(Fs16xx_devId_t id, int enable) {
    Fs16xx_Error_t err = Fs16xx_Error_OK;
    unsigned short value = 0, valBack, i;
    int maxTry = 50, zmdataOK = 0, coefOK = 1;
    unsigned char dataBuf[4];
    static int flag = 0;
    static int bypassed = 0;

    DEBUGPRINT("%s enter. enable=%d", __func__, enable);
    if(enable) {
        if(bypassed) {
            bypassed = 0;
            fs16xx_write_register16(id, FS16XX_OTPACC_REG, FS16XX_OTP_ACC_KEY2);
            fs16xx_read_register16(id, FS16XX_ANACTRL_REG, &value);
            fs16xx_write_register16(id, FS16XX_ANACTRL_REG, value & (~FS16XX_ANACTRL_REG_BPSPKOT_MSK));
            fs16xx_write_register16(id, FS16XX_OTPACC_REG, 0);
        }
        fs16xx_check_calibration(id);
        if(Fs16xx_Calib_OK != g_calib_state
            && Fs16xx_Calib_OnGoing != g_calib_state
            && Fs16xx_Calib_OTP_ExcceedMaxTry != g_calib_state) {
            fs16xx_calibration(id, 0, 1);
            flag = -1;
        } else {
            if(!flag) {
                flag++;
                DEBUGPRINT("%s hold one time.", __func__);
            } else if(flag == 1){
                if(Fs16xx_Calib_OK == g_calib_state) {
                    fs16xx_write_register16(id, FS16XX_ADCTIME_REG, CALIB_REG_NORMAL_BA);
                    fs16xx_write_register16(id, FS16XX_TSCTRL_REG, CALIB_REG_NORMAL_AF);

                    err |= fs16xx_write_register16(id, FS16XX_SPKERR_REG, g_calib_result.spk_err);
                    err |= fs16xx_write_register16(id, FS16XX_SPKM24_REG, g_calib_result.spk_m24);
                    err |= fs16xx_write_register16(id, FS16XX_SPKM6_REG, g_calib_result.spk_m6);
                    err |= fs16xx_write_register16(id, FS16XX_SPKRE_REG, g_calib_result.spk_recover);

                    DEBUGPRINT("%s set values on second time.", __func__);
                }
                flag++;
            }
        }
    }else {
        DEBUGPRINT("%s disable temperature protection.", __func__);
        fs16xx_write_register16(id, FS16XX_OTPACC_REG, FS16XX_OTP_ACC_KEY2);
        fs16xx_read_register16(id, FS16XX_ANACTRL_REG, &value);
        fs16xx_write_register16(id, FS16XX_ANACTRL_REG, value | FS16XX_ANACTRL_REG_BPSPKOT_MSK);
        fs16xx_write_register16(id, FS16XX_OTPACC_REG, 0);
        bypassed = 1;
    }

    DEBUGPRINT("%s exit. flag=%d", __func__, flag);
    return err;
}

