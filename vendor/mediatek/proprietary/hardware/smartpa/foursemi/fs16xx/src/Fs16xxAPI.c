/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/
#include "Fs16xxAPI.h"
#include "Fs16xxCalibration.h"
#include "Fs16xxPreset.h"

Fs16xx_Error_t Fs16xx_Open(unsigned char i2cDevAddr,
                 Fs16xx_devId_t *pDevId) {
    return fs16xx_open(i2cDevAddr, pDevId);
}

Fs16xx_Error_t Fs16xx_handle_is_open(Fs16xx_devId_t id) {
    return fs16xx_handle_is_open(id);
}


Fs16xx_Error_t Fs16xx_ReadRegister16(Fs16xx_devId_t id,
                       unsigned char subaddress,
                       unsigned short *pValue) {
    return fs16xx_read_register16(id, subaddress, pValue);
}

Fs16xx_Error_t Fs16xx_WriteRegister16(Fs16xx_devId_t id,
                    unsigned char subaddress,
                    unsigned short value) {
    return fs16xx_write_register16(id, subaddress, value);
}

Fs16xx_Error_t Fs16xx_BulkReadRegister(Fs16xx_devId_t id,
                    unsigned char subaddress, int num_bytes, unsigned char *pValue) {
    return fs16xx_bulkread_register(id, subaddress, num_bytes, pValue);
}

Fs16xx_Error_t Fs16xx_BulkWriteRegister(Fs16xx_devId_t id,
                    unsigned char subaddress, int num_bytes, const unsigned char *pValue) {
    return fs16xx_bulkwrite_register(id, subaddress, num_bytes, pValue);
}

Fs16xx_Error_t Fs16xx_Init(Fs16xx_devId_t id){
    return fs16xx_init(id);
}

Fs16xx_Error_t Fs16xx_Deinit(Fs16xx_devId_t id){
    return fs16xx_deinit(id);
}

Fs16xx_Error_t Fs16xx_Close(Fs16xx_devId_t id){
    return fs16xx_close(id);
}

Fs16xx_Error_t Fs16xx_SetPower(Fs16xx_devId_t id, int powerdown) {
    return fs16xx_powerdown(id, powerdown);
}

Fs16xx_Error_t Fs16xx_SelectI2SOutputLeft(Fs16xx_devId_t id,
                        Fs16xx_OutputSel_t output_sel) {
    return fs16xx_selectI2S_output_left(id, output_sel);
}

Fs16xx_Error_t Fs16xx_SelectI2SOutputRight(Fs16xx_devId_t id,
                        Fs16xx_OutputSel_t output_sel) {
    return fs16xx_selectI2S_output_right(id, output_sel);
}

Fs16xx_Error_t Fs16xx_SetVolumeLevel(Fs16xx_devId_t id,
                  unsigned short vLevel) {
    return fs16xx_set_volume_level(id, vLevel);
}

Fs16xx_Error_t Fs16xx_SetSampleRate(Fs16xx_devId_t id, int sampleRate) {
    return fs16xx_set_samplerate(id, sampleRate);
}

Fs16xx_Error_t Fs16xx_GetSampleRate(Fs16xx_devId_t id, int *pSampleRate) {
    return fs16xx_get_samplerate(id, pSampleRate);
}

Fs16xx_Error_t Fs16xx_SelectChannel(Fs16xx_devId_t id, Fs16xx_Channel_t channel) {
    return fs16xx_select_channel(id, channel);
}

Fs16xx_Error_t Fs16xx_SelectMode(Fs16xx_devId_t id, Fs16xx_Mode_t mode) {
    return fs16xx_select_mode(id, mode);
}

Fs16xx_Error_t Fs16xx_GetMode(Fs16xx_devId_t id, Fs16xx_Mode_t *pMode) {
    return fs16xx_get_mode(id, pMode);
}

Fs16xx_Error_t Fs16xx_EnableAECOutput(Fs16xx_devId_t id) {
    return fs16xx_enable_aec_output(id);
}

Fs16xx_Error_t Fs16xx_DisableAECOutput(Fs16xx_devId_t id) {
    return fs16xx_disable_aec_output(id);
}

Fs16xx_Error_t Fs16xx_SetMute(Fs16xx_devId_t id, int mute) {
    return fs16xx_set_mute(id, mute);
}

Fs16xx_Error_t Fs16xx_GetMute(Fs16xx_devId_t id, int *pMute) {
    return fs16xx_get_mute(id, pMute);
}

unsigned short Fs16xx_GetDeviceRevision(Fs16xx_devId_t id) {
    return fs16xx_get_device_revision(id);
}

Fs16xx_Calib_State_t Fs16xx_Check_Calibration(Fs16xx_devId_t id) {
    return fs16xx_check_calibration(id);
}

Fs16xx_Error_t Fs16xx_Re0Calibration(Fs16xx_devId_t id, int force, int storeResult) {
    return fs16xx_re0_calibration(id, force, storeResult);
}

Fs16xx_Error_t Fs16xx_F0Calibration(Fs16xx_devId_t id) {
    return fs16xx_f0_calibration(id);
}

Fs16xx_Error_t Fs16xx_OscEnable(Fs16xx_devId_t id, int enable) {
    return fs16xx_osc_enable(id, enable);
}

Fs16xx_Error_t Fs16xx_PllEnable(Fs16xx_devId_t id, int enable) {
    return fs16xx_pll_enable(id, enable);
}

Fs16xx_Error_t Fs16xx_PllRefSel(Fs16xx_devId_t id, int sel) {
    return fs16xx_pll_ref_sel(id, sel);
}

Fs16xx_Error_t Fs16xx_InitializePresets() {
    return fs16xx_initialize_presets();
}

Fs16xx_Error_t Fs16xx_DeinitializePresets() {
    return fs16xx_deinitialize_presets();
}

Fs16xx_Error_t fs16xx_LoadPresets() {
    return fs16xx_load_presets();
}

Fs16xx_Error_t Fs16xx_SetPreset(Fs16xx_devId_t id, int preset) {
    return fs16xx_set_preset(id, preset);
}

Fs16xx_Error_t Fs16xx_SetTemprProtection(Fs16xx_devId_t id, int enable) {
    return fs16xx_set_tempr_protection(id, enable);
}

Fs16xx_Error_t Fs16xx_WaitCLKStable(Fs16xx_devId_t id) {
    return fs16xx_wait_clk_stable(id);
}

