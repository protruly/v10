/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/

#ifndef FS16XX_REG_DEF_H
#define FS16XX_REG_DEF_H

#include "Fs16xx_regs.h"
#include "Fs16xx.h"

/* I2S_CONTROL bits for sample rates */
#define FS16XX_I2SCTRL_RATE_08000 (0<<FS16XX_I2SCTRL_REG_I2SSR_POS)
#define FS16XX_I2SCTRL_RATE_16000 (3<<FS16XX_I2SCTRL_REG_I2SSR_POS)
#define FS16XX_I2SCTRL_RATE_44100 (7<<FS16XX_I2SCTRL_REG_I2SSR_POS)
#define FS16XX_I2SCTRL_RATE_48000 (8<<FS16XX_I2SCTRL_REG_I2SSR_POS)

const struct fs16xx_regs Fs16xx_reg_defaults[] = {
{
    .reg = 0x06,
    .value = 0xFF00, // volume to 0dB
},
{
    .reg = 0x0B,
    .value = 0xCA91,
},
{
    .reg = 0xD0,
    .value = 0x0010, // ov bypass, key protected.
},
{
    .reg = 0xD3,
    .value = 0x0008, // damp, key protected.
},
{
    .reg = 0x0B,
    .value = 0x0000,
},
{
    .reg = 0xB0,
    .value = 0x800A, // Set dem to 1st order
},
{
    .reg = 0xC0,
    .value = 0x5B80, // DC/DC mode: Follow, enable boost
},
{
    .reg = 0xC4,
    .value = 0x000F,
},
{
    .reg = 0xA6,
    .value = 0x0005,
},
{
    .reg = 0xA2,
    .value = 0xD371,
},
{
    .reg = 0xA3,
    .value = 0x003E,
},
{
    .reg = 0xA2,
    .value = 0x591D,
},
{
    .reg = 0xA3,
    .value = 0x0082,
},
{
    .reg = 0xA2,
    .value = 0xD371,
},
{
    .reg = 0xA3,
    .value = 0x003E,
},
{
    .reg = 0xA2,
    .value = 0xA160,
},
{
    .reg = 0xA3,
    .value = 0x007D,
},
{
    .reg = 0xA2,
    .value = 0x539A,
},
{
    .reg = 0xA3,
    .value = 0x00C2,
},
{
    .reg = 0xB8,
    .value = 0x0000,
},
{
    .reg = 0xB4,
    .value = 0x19C2,
},
{
    .reg = 0xB5,
    .value = 0x0001,
},
{
    .reg = 0xB4,
    .value = 0x0000,
},
{
    .reg = 0xB5,
    .value = 0x0000,
},
{
    .reg = 0xB4,
    .value = 0xE63E,
},
{
    .reg = 0xB5,
    .value = 0x00FE,
},
{
    .reg = 0xB4,
    .value = 0xB8E7,
},
{
    .reg = 0xB5,
    .value = 0x007F,
},
{
    .reg = 0xB4,
    .value = 0x46F2,
},
{
    .reg = 0xB5,
    .value = 0x00C0,
},
{
    .reg = 0xB4,
    .value = 0x19C2,
},
{
    .reg = 0xB5,
    .value = 0x0001,
},
{
    .reg = 0xB4,
    .value = 0x0000,
},
{
    .reg = 0xB5,
    .value = 0x0000,
},
{
    .reg = 0xB4,
    .value = 0xE63E,
},
{
    .reg = 0xB5,
    .value = 0x00FE,
},
{
    .reg = 0xB4,
    .value = 0xB8E7,
},
{
    .reg = 0xB5,
    .value = 0x007F,
},
{
    .reg = 0xB4,
    .value = 0x46F2,
},
{
    .reg = 0xB5,
    .value = 0x00C0,
},
{
    .reg = 0xAE,
    .value = 0x0200,
}, 
{
    .reg = 0xC0,
    .value = 0xE39C, // Follow Mode
}
};

/*const struct fs16xx_regs Fs16xx_reg_dsp_music[] = {
{
    .reg = 0xC0,
    .value = 0xE1FC, // Boost Mode
},
{
    .reg = 0xA6,
    .value = 0x0000,
},
{
    .reg = 0xA2,
    .value = 0x7A35,
},
{
    .reg = 0xA3,
    .value = 0x002C,
},
{
    .reg = 0xA6,
    .value = 0x0001,
},
{
    .reg = 0xA2,
    .value = 0x0B96,
},
{
    .reg = 0xA3,
    .value = 0x00A7,
},
{
    .reg = 0xA6,
    .value = 0x0002,
},
{
    .reg = 0xA2,
    .value = 0x7A35,
},
{
    .reg = 0xA3,
    .value = 0x002C,
},
{
    .reg = 0xA6,
    .value = 0x0003,
},
{
    .reg = 0xA2,
    .value = 0xA160,
},
{
    .reg = 0xA3,
    .value = 0x007D,
},
{
    .reg = 0xA6,
    .value = 0x0004,
},
{
    .reg = 0xA2,
    .value = 0x5399,
},
{
    .reg = 0xA3,
    .value = 0x00C2,
},
{
    .reg = 0xA6,
    .value = 0x000A,
},
{
    .reg = 0xA2,
    .value = 0xDDF1,
},
{
    .reg = 0xA3,
    .value = 0x003E,
},
{
    .reg = 0xA6,
    .value = 0x000B,
},
{
    .reg = 0xA2,
    .value = 0x11D6,
},
{
    .reg = 0xA3,
    .value = 0x0087,
},
{
    .reg = 0xA6,
    .value = 0x000C,
},
{
    .reg = 0xA2,
    .value = 0xBCE6,
},
{
    .reg = 0xA3,
    .value = 0x003A,
},
{
    .reg = 0xA6,
    .value = 0x000D,
},
{
    .reg = 0xA2,
    .value = 0xEE2A,
},
{
    .reg = 0xA3,
    .value = 0x0078,
},
{
    .reg = 0xA6,
    .value = 0x000E,
},
{
    .reg = 0xA2,
    .value = 0x6529,
},
{
    .reg = 0xA3,
    .value = 0x00C6,
},
{
    .reg = 0xA6,
    .value = 0x000F,
},
{
    .reg = 0xA2,
    .value = 0x4F24,
},
{
    .reg = 0xA3,
    .value = 0x0040,
},
{
    .reg = 0xA6,
    .value = 0x0010,
},
{
    .reg = 0xA2,
    .value = 0x73DE,
},
{
    .reg = 0xA3,
    .value = 0x0083,
},
{
    .reg = 0xA6,
    .value = 0x0011,
},
{
    .reg = 0xA2,
    .value = 0x6930,
},
{
    .reg = 0xA3,
    .value = 0x003C,
},
{
    .reg = 0xA6,
    .value = 0x0012,
},
{
    .reg = 0xA2,
    .value = 0x8C22,
},
{
    .reg = 0xA3,
    .value = 0x007C,
},
{
    .reg = 0xA6,
    .value = 0x0013,
},
{
    .reg = 0xA2,
    .value = 0x47AC,
},
{
    .reg = 0xA3,
    .value = 0x00C3,
},
{
    .reg = 0xA6,
    .value = 0x0014,
},
{
    .reg = 0xA2,
    .value = 0x0000,
},
{
    .reg = 0xA3,
    .value = 0x0040,
},
{
    .reg = 0xA6,
    .value = 0x0015,
},
{
    .reg = 0xA2,
    .value = 0x2533,
},
{
    .reg = 0xA3,
    .value = 0x0084,
},
{
    .reg = 0xA6,
    .value = 0x0016,
},
{
    .reg = 0xA2,
    .value = 0xFC1B,
},
{
    .reg = 0xA3,
    .value = 0x003B,
},
{
    .reg = 0xA6,
    .value = 0x0017,
},
{
    .reg = 0xA2,
    .value = 0xDACD,
},
{
    .reg = 0xA3,
    .value = 0x007B,
},
{
    .reg = 0xA6,
    .value = 0x0018,
},
{
    .reg = 0xA2,
    .value = 0x03E5,
},
{
    .reg = 0xA3,
    .value = 0x00C4,
},
{
    .reg = 0xA6,
    .value = 0x0019,
},
{
    .reg = 0xA2,
    .value = 0xF314,
},
{
    .reg = 0xA3,
    .value = 0x003D,
},
{
    .reg = 0xA6,
    .value = 0x001A,
},
{
    .reg = 0xA2,
    .value = 0x2C40,
},
{
    .reg = 0xA3,
    .value = 0x009E,
},
{
    .reg = 0xA6,
    .value = 0x001B,
},
{
    .reg = 0xA2,
    .value = 0x5BE0,
},
{
    .reg = 0xA3,
    .value = 0x003D,
},
{
    .reg = 0xA6,
    .value = 0x001C,
},
{
    .reg = 0xA2,
    .value = 0xD3C0,
},
{
    .reg = 0xA3,
    .value = 0x0061,
},
{
    .reg = 0xA6,
    .value = 0x001D,
},
{
    .reg = 0xA2,
    .value = 0xB10C,
},
{
    .reg = 0xA3,
    .value = 0x00C4,
},
{
    .reg = 0xA6,
    .value = 0x001E,
},
{
    .reg = 0xA2,
    .value = 0xD2A6,
},
{
    .reg = 0xA3,
    .value = 0x003E,
},
{
    .reg = 0xA6,
    .value = 0x001F,
},
{
    .reg = 0xA2,
    .value = 0x49D0,
},
{
    .reg = 0xA3,
    .value = 0x0090,
},
{
    .reg = 0xA6,
    .value = 0x0020,
},
{
    .reg = 0xA2,
    .value = 0xBBE9,
},
{
    .reg = 0xA3,
    .value = 0x003D,
},
{
    .reg = 0xA6,
    .value = 0x0021,
},
{
    .reg = 0xA2,
    .value = 0xB630,
},
{
    .reg = 0xA3,
    .value = 0x006F,
},
{
    .reg = 0xA6,
    .value = 0x0022,
},
{
    .reg = 0xA2,
    .value = 0x7171,
},
{
    .reg = 0xA3,
    .value = 0x00C3,
},
{
    .reg = 0xA6,
    .value = 0x0023,
},
{
    .reg = 0xA2,
    .value = 0xE272,
},
{
    .reg = 0xA3,
    .value = 0x003F,
},
{
    .reg = 0xA6,
    .value = 0x0024,
},
{
    .reg = 0xA2,
    .value = 0x9798,
},
{
    .reg = 0xA3,
    .value = 0x0080,
},
{
    .reg = 0xA6,
    .value = 0x0025,
},
{
    .reg = 0xA2,
    .value = 0xB2B9,
},
{
    .reg = 0xA3,
    .value = 0x003F,
},
{
    .reg = 0xA6,
    .value = 0x0026,
},
{
    .reg = 0xA2,
    .value = 0x6868,
},
{
    .reg = 0xA3,
    .value = 0x007F,
},
{
    .reg = 0xA6,
    .value = 0x0027,
},
{
    .reg = 0xA2,
    .value = 0x6AD6,
},
{
    .reg = 0xA3,
    .value = 0x00C0,
},
{
    .reg = 0xA6,
    .value = 0x0028,
},
{
    .reg = 0xA2,
    .value = 0x0000,
},
{
    .reg = 0xA3,
    .value = 0x0040,
},
{
    .reg = 0xA8,
    .value = 0x00BF,
},
{
    .reg = 0xAA,
    .value = 0x00A7,
},
{
    .reg = 0xA9,
    .value = 0x0248,
},
{
    .reg = 0xA7,
    .value = 0x0006,
},
{
    .reg = 0xA1,
    .value = 0x1E34,
},

};
*/

#endif
