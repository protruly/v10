/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/

#ifndef FS16XX_CALIBRATION_H
#define FS16XX_CALIBRATION_H

#include "Fs16xx.h"

#ifdef __cplusplus
extern "C" {
#endif

#define R0_DEFAULT                  8

#define R0_ALLOWANCE                0.3f

#define TMAX            120
#define TMIN            -20
#define SPKTMPRM24      115
#define SPKTMPRM6       110
#define SPKTMPRECOVERY  100
#define TCOEF           0.0034f

#define OTP_OP_MAX_TRY              10

#define FS16XX_CALIB_MAX_TRY        7

#define FS16XX_CALIB_OTP_R25_STEP    ((float)(R0_DEFAULT / 2) / 128)

#define SPK_CALIBRATION_MAX_ZMDELTA 0x150

#define FS16XX_CALIB_VERSION  1

// Store calibration result to OTP, remove following macro to store calibration result to file system.
#define FS16XX_CALIB_STORE_TO_OTP   1

#define FS16XX_CALIB_RESULT_FILE_NAME "fs16xxcali.bin"

#define CALIB_REG_INIT_BA   0x0028
#define CALIB_REG_NORMAL_BA 0x0030
//#define CALIB_REG_INIT_AF   0x0008
#define CALIB_REG_NORMAL_AF 0x000C

/* Type containing all the possible errors that can occur
 *
 */
enum Fs16xx_Calib_State {
    Fs16xx_Calib_OK = 0,
    Fs16xx_Calib_NotStarted = 1,
    Fs16xx_Calib_OnGoing = 2,
    Fs16xx_Calib_Failed = 3,
    Fs16xx_Calib_OTP_Failed = 4,
    Fs16xx_Calib_OTP_ExcceedMaxTry = 5,
    Fs16xx_Calib_Unknown = 10
};
typedef enum Fs16xx_Calib_State Fs16xx_Calib_State_t;

struct Fs16xx_Calib_Result {
    unsigned short calib_ver;
    unsigned short calib_count;
    unsigned short calib_result_zm;
    unsigned short spk_err;
    unsigned short spk_m24;
    unsigned short spk_m6;
    unsigned short spk_recover;
};
typedef struct Fs16xx_Calib_Result Fs16xx_Calib_Result_t;

Fs16xx_Error_t fs16xx_calibration(Fs16xx_devId_t id, int force, int storeResult);

Fs16xx_Calib_State_t fs16xx_check_calibration(Fs16xx_devId_t id);

Fs16xx_Calib_State_t fs16xx_calc_coef_from_zm(Fs16xx_devId_t id, unsigned short zm, int storeResult);

Fs16xx_Calib_State_t fs16xx_update_coef_from_r25(Fs16xx_devId_t id, float r25);

int           fs16xx_otp_is_busy(Fs16xx_devId_t id);

int           fs16xx_otp_bit_counter(unsigned short val);

unsigned char fs16xx_r25_to_byte(float f);

float         fs16xx_byte_to_r25(unsigned char b);

Fs16xx_Error_t fs16xx_set_tempr_protection(Fs16xx_devId_t id, int enable);

#ifdef FS16XX_CALIB_STORE_TO_OTP
Fs16xx_Error_t fs16xx_write_to_otp(Fs16xx_devId_t id, unsigned char valOTP);
#else
Fs16xx_Error_t fs16xx_save_calib_result(unsigned short zmdata);
#endif

#ifdef __cplusplus
}
#endif

#endif