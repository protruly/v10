/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/

#ifndef FS16XX_PRESET_H
#define FS16XX_PRESET_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define PRESET_NAME_SIZE  256

struct fs16xx_preset_header {
    char customer_name[16]; /* Customer info */
    int64_t create_time;    /* Time tick when the preset created */
    unsigned short data_len;/* Length of the data in int16_t */
    unsigned short crc16;   /* CRC check sum of the data */
    unsigned short *data;   /* Point to preset data */
};
typedef struct fs16xx_preset_header Fs16xx_preset_header_t;

enum _fs16xx_preset_type {
    fs16xx_music = 0,
    fs16xx_voice = 1,
    fs16xx_ringtone = 2,
    fs16xx_preset_type_max = 3, /* Set to count of preset types */
};
typedef enum _fs16xx_preset_type fs16xx_preset_type;

struct fs16xx_preset_content {
    int valid;
    Fs16xx_preset_header_t preset_header;
};
typedef struct fs16xx_preset_content Fs16xx_preset;

/**
 * Fs16xx initialize presets
 */
Fs16xx_Error_t fs16xx_initialize_presets();

/**
 * Fs16xx deinitialize presets
 */
Fs16xx_Error_t fs16xx_deinitialize_presets();

/**
 * Fs16xx load presets from device
 */
Fs16xx_Error_t fs16xx_load_presets();

unsigned short fs16xx_calc_checksum(unsigned short *data, int len);

/**
 * Fs16xx set dsp preset
 * @param id to opened instance
 * @param preset to set in dsp
 */
Fs16xx_Error_t fs16xx_set_preset(Fs16xx_devId_t id, fs16xx_preset_type preset);

#ifdef __cplusplus
}
#endif

#endif

