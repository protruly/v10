/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/
#ifndef FS16XXAPI_H
#define FS16XXAPI_H

#include "Fs16xx.h"
#include "Fs16xxCalibration.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Open an instance to a given Fs16xx device.
 */
Fs16xx_Error_t Fs16xx_Open(unsigned char i2cDevAddr,
                 Fs16xx_devId_t *pDevId);

/**
 * Return Fs16xx_Error_OK if device is opened, otherwise return Fs16xx_Error_NotOpen.
 */
Fs16xx_Error_t Fs16xx_handle_is_open(Fs16xx_devId_t id);

/**
 * Read an arbitrary I2C register.
 * @param id to opened instance
 * @param subaddress: 8 bit subaddress of the I2C register
 * @param *pValue
 * @return 16 bit value that was stored in the selected I2C register
 */
Fs16xx_Error_t Fs16xx_ReadRegister16(Fs16xx_devId_t id,
                       unsigned char subaddress,
                       unsigned short *pValue);

/**
 * Write an arbitrary I2C register of the Fs16XX.
 * @param id to opened instance
 * @param subaddress: 8 bit subaddress of the I2C register
 * @param value: 16 bit value to be stored in the selected I2C register
 */
Fs16xx_Error_t Fs16xx_WriteRegister16(Fs16xx_devId_t id,
                    unsigned char subaddress,
                    unsigned short value);

/**
 * Read num_bytes of registers values from specified address.
 * @param id to opened instance
 * @param subaddress: 8 bit subaddress of the I2C register
 * @num_bytes  number of bytes to read
 * @param *pValue, point to bytes values from register
 */
Fs16xx_Error_t Fs16xx_BulkReadRegister(Fs16xx_devId_t id,
                    unsigned char subaddress, int num_bytes, unsigned char *pValue);

/**
 * Write num_bytes of registers values to specified address.
 * @param id to opened instance
 * @param subaddress: 8 bit subaddress of the I2C register
 * @num_bytes  number of bytes to read
 * @param *pValue, point to bytes values to write
 */
Fs16xx_Error_t Fs16xx_BulkWriteRegister(Fs16xx_devId_t id,
                    unsigned char subaddress, int num_bytes, const unsigned char *pValue);

/**
 * Reset all I2C registers
 * @param id to opened instance
 */
Fs16xx_Error_t Fs16xx_Init(Fs16xx_devId_t id);

/**
 * Deinitialize fs16xx device
 * @param id to opened instance
 */
Fs16xx_Error_t Fs16xx_Deinit(Fs16xx_devId_t id);

/**
 * Close the instance with given id.
 * @param id to opened instance
 */
Fs16xx_Error_t Fs16xx_Close(Fs16xx_devId_t id);


/**
 * Set Fs16xx power down or power up state according to the param.
 * @param id to opened instance
 * @param powerdown must be 1 or 0,
 *   1: Fs16xx power down.
 *   0: Fs16xx power on.
 */
Fs16xx_Error_t Fs16xx_SetPower(Fs16xx_devId_t id, int powerdown);

/**
 * Select the signal to be put on the left channel of the I2S output.
 * Default value:
 * @param id to opened instance
 * @param output_sel: channel selection, refer to Fs16xx_OutputSel in the header file
 */
Fs16xx_Error_t Fs16xx_SelectI2SOutputLeft(Fs16xx_devId_t id,
                        Fs16xx_OutputSel_t output_sel);

/**
 * Select the signal to be put on the right channel of the I2S output.
 * Default value:
 * @param id to opened instance
 * @param output_sel: channel selection, refer to Fs16xx_OutputSel in the header file
 */
Fs16xx_Error_t Fs16xx_SelectI2SOutputRight(Fs16xx_devId_t id,
                        Fs16xx_OutputSel_t output_sel);

/**
 * Set the volume of fs16xx
 * Default value:
 * @param id to opened instance
 * @param vLevel volume in level. must be between 0 and 255
 */
Fs16xx_Error_t Fs16xx_SetVolumeLevel(Fs16xx_devId_t id,
                  unsigned short vLevel);

/**
 * Set sampleRate
 * Default value:
 * @param id to opened instance
 * @param Samplerate in Hz.  must be 32000, 44100 or 48000
 */
Fs16xx_Error_t Fs16xx_SetSampleRate(Fs16xx_devId_t id, int sampleRate);

/**
 * Get sampleRate
 * @param id to opened instance
 * @param pSampleRate, pointer to sample rate in Hz.
 */
Fs16xx_Error_t Fs16xx_GetSampleRate(Fs16xx_devId_t id, int *pSampleRate);


/**
 * Select the I2S input channel.
 * Default value:
 * @param id to opened instance
 * @param channel, channel selection: refer to Fs16xx_Channel_t definition
 */
Fs16xx_Error_t Fs16xx_SelectChannel(Fs16xx_devId_t id, Fs16xx_Channel_t channel);


/**
 * Select the device mode.
 * Default mode: normal mode
 * @param id to opened instance
 * @param mode, mode selection: refer to Fs16xx_Mode_t definition
 */
Fs16xx_Error_t Fs16xx_SelectMode(Fs16xx_devId_t id, Fs16xx_Mode_t mode);

/**
 * Get the device mode: normal mode or RCV mode
 * @param id to opened instance
 * @param mode, point to mode value to return: refer to Fs16xx_Mode_t definition
 */
Fs16xx_Error_t Fs16xx_GetMode(Fs16xx_devId_t id, Fs16xx_Mode_t *pMode);

/**
 * Enable the AEC output.
 */
Fs16xx_Error_t Fs16xx_EnableAECOutput(Fs16xx_devId_t id);

/**
 * Disable AEC output.
 */
Fs16xx_Error_t Fs16xx_DisableAECOutput(Fs16xx_devId_t id);

/**
 * Set mute state on/off
 * Default mode: mute off
 * @param id to opened instance
 * @param mute state:
 *  0   unmute
 *  1   mute
 */
Fs16xx_Error_t Fs16xx_SetMute(Fs16xx_devId_t id, int mute);

/**
 * Get mute state on/off
 * @param id to opened instance
 * @param pMute, point to mute state
 *  0   unmute
 *  1   mute
 */
Fs16xx_Error_t Fs16xx_GetMute(Fs16xx_devId_t id, int *pMute);

/**
 * Return Fs16xx revision id.
 * @param id to opened instance
 */
unsigned short Fs16xx_GetDeviceRevision(Fs16xx_devId_t id);

/**
 * Fs16xx calibration procedure. Return OK on success, otherwise
 * return failure.
 * @param id to opened instance
 */
Fs16xx_Error_t Fs16xx_Re0Calibration(Fs16xx_devId_t id, int force, int storeResult);

/**
 * Fs16xx check calibration state.
 * @param id to opened instance
 */
Fs16xx_Calib_State_t Fs16xx_Check_Calibration(Fs16xx_devId_t id);

/**
 * Fs16xx f0 calibration procedure. Return OK on success, otherwise
 * return failure.
 * @param id to opened instance
 */
Fs16xx_Error_t Fs16xx_F0Calibration(Fs16xx_devId_t id);

/**
 * Fs16xx enable osc clock.
 * @param id to opened instance
 * @param enable, 0 disable, other values enabled
 */
Fs16xx_Error_t Fs16xx_OscEnable(Fs16xx_devId_t id, int enable);

/**
 * Fs16xx enable/disable pll.
 * @param id to opened instance
 * @param enable, 0 set disable, other values set enable
 */
Fs16xx_Error_t Fs16xx_PllEnable(Fs16xx_devId_t id, int enable);

/**
 * Fs16xx select pll reference.
 * @param id to opened instance
 * @param enable, 0 set disable, other values set enable
 */
Fs16xx_Error_t Fs16xx_PllRefSel(Fs16xx_devId_t id, int sel);

/**
 * Fs16xx initialize presets
 */
Fs16xx_Error_t Fs16xx_InitializePresets();

/**
 * Fs16xx deinitialize presets
 */
Fs16xx_Error_t Fs16xx_DeinitializePresets();

/**
 * Fs16xx load presets from device
 */
Fs16xx_Error_t fs16xx_LoadPresets();

/**
 * Fs16xx set dsp preset
 * @param id to opened instance
 * @param preset to set in dsp
 */
Fs16xx_Error_t Fs16xx_SetPreset(Fs16xx_devId_t id, int preset);

Fs16xx_Error_t Fs16xx_SetTemprProtection(Fs16xx_devId_t id, int enable);

/**
 * Fs16xx wait clock stable
 */
Fs16xx_Error_t Fs16xx_WaitCLKStable(Fs16xx_devId_t id);

#ifdef __cplusplus
}
#endif

#endif //FS16XXAPI_H