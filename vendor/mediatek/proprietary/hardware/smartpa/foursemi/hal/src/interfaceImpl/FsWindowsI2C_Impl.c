/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/
#include <Windows.h>
#include <stdint.h>
#include "FTCI2C.h"
#include "FS_I2C.h"

#pragma comment (lib, "FTCI2C.lib")

#define ControlByteCount 2
#define MAX_FREQ_M24C64_CLOCK_DIVISOR 15   // equivalent to 400KHz

WriteControlByteBuffer g_WriteControlBuffer;
WriteDataByteBuffer g_WriteDataBuffer;
ReadDataByteBuffer g_ReadDataBuffer;

/****************************************************
 * I2C Read operation for Windows.
 * @param fd, file descriptor for i2c device(non zero for a valid fd)
 * @param nWriteBytes, number of bytes to write(size of write control bytes)
 * @param writeData, point to write data buffer
 * @param nReadBytes, [read address(1 byte)] + [number of bytes to read]
 * @param readData, point to read data buffer. readData[0] is the address,
 *                  the read back data is start from readData[1]
 * @param pError, point to error number on failure
 * @return on success: > 0, [bytes read] + [read address(1 byte)]
 *         on failure: = 0, pError will be set to error code
*****************************************************/
int __FsWindowsI2CWriteRead(int fd, int nWriteBytes, const uint8_t* writeData,
                                    int nReadBytes, uint8_t *readData, unsigned int *pError) {
    FTC_STATUS Status = FTC_SUCCESS;
    int readType;
    int ret = 0;

    if( 0 != fd ) {
        g_WriteControlBuffer[0] = ((writeData[0]) | 1);
        g_WriteControlBuffer[1] = writeData[1];

        if (nReadBytes > 1) {
            readType = BLOCK_READ_TYPE;
        } else {
            readType = BYTE_READ_TYPE;
        }
        Status = I2C_Read(fd, &g_WriteControlBuffer, nWriteBytes, TRUE, 20, readType,
                                   &g_ReadDataBuffer, nReadBytes-1);

        if(FTC_SUCCESS == Status) {
            memcpy(readData+1, g_ReadDataBuffer, nReadBytes-1);
            ret = nReadBytes;
        } else {
            *pError = FS_I2C_NoAck;
        }
    } else {
        *pError = FS_I2C_InvalidDev;
    }

    return ret;
}   

/****************************************************
 * I2C Write operation for Windows.
 * @param fd, file descriptor for i2c device(non zero for a valid fd)
 * @param size, number of bytes to write. [size of write control bytes(2 bytes)] + [write bytes count]
 * @param buffer, point to write data buffer
 * @param pError, point to error number on failure
 * @return on success: > 0, bytes write
 *         on failure: = 0, pError will be set to error code
*****************************************************/
int __FsWindowsI2CWrite(int fd, int size, uint8_t *buffer, unsigned int *pError){
    FTC_STATUS Status = FTC_SUCCESS;
    int numBytesToWrite = 0;
    FTC_PAGE_WRITE_DATA pageWriteData;
    int writeType;
    int ret = 0;

    if( 0 != fd ) {
        g_WriteControlBuffer[0] = ((buffer[0]) | 0);
        g_WriteControlBuffer[1] = buffer[1];

        numBytesToWrite = size - ControlByteCount;
        if(numBytesToWrite > MAX_WRITE_DATA_BYTES_BUFFER_SIZE) {
            numBytesToWrite = MAX_WRITE_DATA_BYTES_BUFFER_SIZE;
        }
        if (numBytesToWrite > 1) {
            writeType = PAGE_WRITE_TYPE;
            pageWriteData.dwNumPages = 1;
            pageWriteData.dwNumBytesPerPage = numBytesToWrite;
        } else {
            writeType = BYTE_WRITE_TYPE;
            pageWriteData.dwNumPages = 0;
            pageWriteData.dwNumBytesPerPage = 0;
        }
        memcpy(g_WriteDataBuffer, &(buffer[ControlByteCount]), numBytesToWrite);
        Status = I2C_Write(fd, &g_WriteControlBuffer, ControlByteCount, TRUE, 20, TRUE, writeType,
                                   &(g_WriteDataBuffer), numBytesToWrite, TRUE, 20, &pageWriteData);
        if(FTC_SUCCESS == Status) {
            ret = numBytesToWrite;
        } else {
            *pError = FS_I2C_NoAck;
        }
    } else {
        *pError = FS_I2C_InvalidDev;
    }

    return ret;
}

int __FsWindowsI2CInit(char *devpath) {
    FTC_STATUS Status = FTC_SUCCESS;
    DWORD dwNumDevices = 0;
    char szDeviceName[100];
    DWORD dwLocationID = 0;
    DWORD dwClockFrequencyHz = 0;
    FTC_HANDLE ftHandle;
    Status = I2C_GetNumDevices(&dwNumDevices);

    //Status = I2C_GetDllVersion(szDllVersion, 10);

    if ((Status == FTC_SUCCESS) && (dwNumDevices > 0)){
        if (dwNumDevices == 1){
            Status = I2C_GetDeviceNameLocID(0, szDeviceName, 100, &dwLocationID);

            if (Status == FTC_SUCCESS){
                Status = I2C_OpenEx(szDeviceName, dwLocationID, &ftHandle);
                //Status = I2C_Open(&ftHandle);
            }
        } else {
            if (dwNumDevices == 2){
                Status = I2C_GetDeviceNameLocID(1, szDeviceName, 50, &dwLocationID);

                if (Status == FTC_SUCCESS){
                    Status = I2C_OpenEx(szDeviceName, dwLocationID, &ftHandle);
                    //Status = I2C_Open(&ftHandle);
                }
            }
        }
    }

    if ((Status == FTC_SUCCESS) && (dwNumDevices > 0)){
        if (Status == FTC_SUCCESS) {
            Status = I2C_GetClock(MAX_FREQ_M24C64_CLOCK_DIVISOR, &dwClockFrequencyHz);

            //if (Status == FTC_SUCCESS)
            //  Status = I2C_SetLoopback(ftHandle, true);

            if (Status == FTC_SUCCESS){
                //if (Status == FTC_SUCCESS)
                //Status = I2C_SetClock(ftHandle, 0, &dwClockFrequencyHz);

                if (Status == FTC_SUCCESS){
                    Status = I2C_InitDevice(ftHandle, MAX_FREQ_M24C64_CLOCK_DIVISOR); //65535

                    if (Status == FTC_SUCCESS)
                        Status = I2C_SetMode(ftHandle, FAST_MODE);                  
                }
            }
        }
    }

    return (FTC_SUCCESS == Status)? ftHandle : 0;
    
}

/*int __FsWindowsI2CClose(int fd) {
    if(fd > 0) {
        I2C_Close(fd);
    }
}*/

int __FsWindowsI2cVersion(char *buffer, int fd){
    FTC_STATUS Status = FTC_SUCCESS;
    
    Status = I2C_GetDllVersion(buffer, 10);

    return (FTC_SUCCESS == Status)? 0 : -1;
}

