/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include "FsConnRegister.h"
#include "FS_I2C.h"
#include "FsPrint.h"
#include "Fs16xx_Custom.h"

static char gStrDev[FILENAME_MAX]; 
static int  gGlobalFd = 0;

#if ( defined(WIN32) || defined(_X64) )
#define DEV_STR_I2C "ftdi"
#else
#define DEV_STR_I2C_Q "/dev/i2c"
#define DEV_STR_I2C_M_L "/dev/i2c_smartpa"
#define DEV_STR_I2C_M_N "/dev/smartpa_i2c"
#endif

int FsConnRegister(char* strTarget)
{
    if(strTarget) {
#if ( defined(WIN32) || defined(_X64) )
		strncpy_s(gStrDev, sizeof(gStrDev), strTarget, strlen(strTarget));
#else
        strncpy(gStrDev, strTarget, sizeof(gStrDev));
#endif
    } else {
#if ( defined(WIN32) || defined(_X64) )
		strncpy_s(gStrDev, sizeof(gStrDev), FS_I2CDEVICE, sizeof(FS_I2CDEVICE));
#else
        strncpy(gStrDev, FS_I2CDEVICE, sizeof(FS_I2CDEVICE));
#endif
    }

#if ( defined(WIN32) || defined(_X64) )
    if ( strncmp (gStrDev, DEV_STR_I2C,  (sizeof(DEV_STR_I2C) -1)) == 0 ) {
        gGlobalFd = FS_I2C_Interface(gStrDev, __FsWindowsI2CInit, 
                        __FsWindowsI2CWrite, __FsWindowsI2CWriteRead, __FsWindowsI2cVersion);
#else
    if ( strncmp (gStrDev, DEV_STR_I2C_Q,  (sizeof(DEV_STR_I2C_Q) -1)) == 0 
        || strncmp (gStrDev, DEV_STR_I2C_M_L,  (sizeof(DEV_STR_I2C_M_L) -1)) == 0
        || strncmp (gStrDev, DEV_STR_I2C_M_N,  (sizeof(DEV_STR_I2C_M_N) -1)) == 0) {
        gGlobalFd = FS_I2C_Interface(gStrDev, __FsI2CInit, __FsI2CWrite, __FsI2CWriteRead, __FsI2cVersion);
#endif
    } else {
        PRINT_ERROR("%s: invalid device name %s\n", __FUNCTION__,gStrDev);
    }

    return gGlobalFd;
}

int FsConnGetFd() {
    if(gGlobalFd <= 0) {
        PRINT_ERROR("Warning: device is not open.\n");
    }

    return gGlobalFd;
}

char* FsConnGetDeviceStr() {    
    return gStrDev;
}

