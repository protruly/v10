/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/

#define LOG_NDEBUG 0   
#define LOG_TAG "fs16xx_i2c"

#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <string.h>

#include "FsPrint.h"
#include "FsConnRegister.h"
#include "FS_I2C.h"
#include "Fs16xx_Custom.h"

#ifdef _MSC_VER
#pragma once
#define false   0
#define true    1
#define bool int
#else
#include <stdbool.h>
#endif

/* the interface */
static int (*gfnc_Init)(char *devName);
static int (*gfnc_Write)(int fd, int size, uint8_t *buffer, unsigned int *pError);
static int (*gfnc_WriteRead)(int fd, int nWriteBytes, const uint8_t* writeData,
        int nReadBytes, uint8_t *readData, unsigned int *pError);
static int (*gfnc_GetVersion)(char *buffer, int fd);

static bool g_bInitialized = false;

static int g_i2cFd=-1;

static FS_I2C_Error_t FS_I2C_Init_if_needed() {
    FS_I2C_Error_t ret = FS_I2C_OK;
    int fd;

    if(false == g_bInitialized) {
        fd = FsConnRegister(FS_I2CDEVICE);
        if(fd < 0) {
            ret = FS_I2C_InvalidDev;
        } else {
            ret = FS_I2C_OK;
            g_bInitialized = true;
        }
    }

    return ret;
}

static int  recover(void) {
    g_i2cFd = (*gfnc_Init)(NULL);
    return g_i2cFd<0;
}

int FS_I2C_Interface(char *targetName, 
    int (*init)(char *devName),
    int (*write)(int fd, int size, uint8_t *buffer, unsigned int *pError),
    int (*write_read)(int fd, int nWriteBytes, const uint8_t* writeData,
        int nReadBytes, uint8_t *readData, unsigned int *pError),
    int (*get_version)(char *buffer, int fd)) {
    gfnc_Init = init;
    gfnc_Write = write;
    gfnc_WriteRead = write_read;
    gfnc_GetVersion = get_version;
    g_i2cFd = (*init)(targetName);
    g_bInitialized = true;

    return g_i2cFd;
}

FS_I2C_Error_t FS_I2C_Write(uint8_t devAddr, 
    int nWriteBytes, 
    const uint8_t *data) {
    FS_I2C_Error_t ret;
    uint32_t error;

    if(nWriteBytes > FS_I2C_MAX_SIZE) {
        PRINT_ERROR("%s: too many bytes to write: %d\n", __FUNCTION__, nWriteBytes);
        error = FS_I2C_InvalidData;
        return FS_I2C_InvalidData;
    }

    ret = FS_I2C_Init_if_needed();

    if(FS_I2C_OK == ret) {
        uint8_t buffer[FS_I2C_MAX_SIZE+1];
        buffer[0] = devAddr;

        memcpy((void*)(&buffer[1]), data, nWriteBytes + 1);
        (*gfnc_Write)(g_i2cFd, nWriteBytes + 1, buffer, &error);
        ret = (FS_I2C_Error_t)error;
    }
    return ret;
}

FS_I2C_Error_t FS_I2C_WriteRead(uint8_t devAddr, 
    int nWriteBytes, 
    const uint8_t *writeData,
    int nReadBytes,
    const uint8_t *readBuffer) {
    FS_I2C_Error_t ret;
    uint32_t error;
    int maxTry = 25;

    if(nWriteBytes > FS_I2C_MAX_SIZE) {
        PRINT_ERROR("%s: too many bytes to write: %d\n", __FUNCTION__, nWriteBytes);
        error = FS_I2C_InvalidData;
        return FS_I2C_InvalidData;
    }

    if(nReadBytes > FS_I2C_MAX_SIZE) {
        PRINT_ERROR("%s: too many bytes to read: %d\n", __FUNCTION__, nReadBytes);
        error = FS_I2C_InvalidData;
        return FS_I2C_InvalidData;
    }

    ret = FS_I2C_Init_if_needed();

    if(FS_I2C_OK == ret) {
        uint8_t bufferW[FS_I2C_MAX_SIZE+1];
        uint8_t bufferR[FS_I2C_MAX_SIZE+1];

        bufferW[0] = devAddr;
        memcpy((void*)(&bufferW[1]), writeData, nWriteBytes);

        bufferR[0] = devAddr|0x1;

        while(maxTry > 0) {
            nReadBytes = (*gfnc_WriteRead)(g_i2cFd, nWriteBytes + 1, bufferW, nReadBytes + 1, bufferR, &error);
            if(error == FS_I2C_OK) {
                break;
            }
            usleep(1000 * 2);
            maxTry--;
        }

        if(nReadBytes > 0) {
            memcpy((void*)readBuffer, &bufferR[1], nReadBytes - 1);
        } else {
            ret = (FS_I2C_Error_t)error;
            PRINT_ERROR("i2c read failure: %s\n", __FUNCTION__);
// Resetting i2c on Windows will always fail, thus we won't recover for Windows.
#if !( defined(WIN32) || defined(_X64) )
            recover();
#endif
        }
    }

    return ret;
}


