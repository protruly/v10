#ifndef EXP_FS16XX_INTERFACE_H
#define EXP_FS16XX_INTERFACE_H

#ifdef __cplusplus
extern "C" {
#endif

// To be compatible with both lagecy and latest MTK platforms.
#ifndef ANDROID_AUDIO_SMART_PA_CONTROLLER_H
struct SmartPaOps
{
    int (*init)(struct SmartPa *smartPa);
    int (*speakerOn)(struct SmartPaRuntime *runtime);
    int (*speakerOff)();
    int (*deinit)();
};

struct SmartPaRuntime
{
    unsigned int sampleRate;
    int echoReferenceConfig;
};

struct SmartPaAttribute
{
    int haveDsp;
    int outputPort;
    unsigned int chipDelayUs;

    char spkLibPath[128];

    unsigned int supportedRateList[32];
    unsigned int supportedRateMax;
    unsigned int supportedRateMin;

    char codecCtlName[128];
    int isAlsaCodec;
    int isI2sInitBeforeAmpOn;
    int isApllNeeded;
};

struct SmartPa
{
    struct SmartPaOps ops;
    struct SmartPaRuntime runtime;
    struct SmartPaAttribute attribute;
};
#endif

#define FSRESULT_OK           0
#define FSRESULT_INIT_FAILED -1
#define FSRESULT_FAILED      -2

typedef int (*FsSmartPaOps_Init)(struct SmartPa *smartPa);
typedef int (*FsSmartPaOps_Deinit)();
typedef int (*FsSmartPaOps_SpeakerOn)(struct SmartPaRuntime *runtime);
typedef int (*FsSmartPaOps_SpeakerOff)();

int Ex_Fs16xx_Init(struct SmartPa *smartPa);
int Ex_Fs16xx_Deinit();
void Ex_Fs16xx_SetSamplerate(int sampleRate);
int Ex_Fs16xx_SpeakerOn(struct SmartPaRuntime *runtime);
int Ex_Fs16xx_SpeakerOff();
void Ex_Fs16xx_AECConfigure(int config);
void Ex_Fs16xx_SetBypassDspIncall(int bypass);

int mtk_smartpa_init(struct SmartPa *smart_pa);

#ifdef __cplusplus
}
#endif

#endif
