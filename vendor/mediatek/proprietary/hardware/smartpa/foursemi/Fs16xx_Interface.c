#include "Fs16xx.h"
#include "Fs16xxAPI.h"
#include "Fs16xx_Interface.h"
#include "Fs16xx_Custom.h"
#include "FsPrint.h"
#include "Fs16xxCalibration.h"
#include "Fs16xxPreset.h"

#define FS16XX_DEV_INDEX_L 0
#define FS16XX_DEV_INDEX_R 1

static Fs16xx_devId_t gFs16xxId[2] = {-1, -1};

static int gInitialized = 0;

#define STATIC_ASSERT( condition, name )\
    typedef char assert_failed_ ## name [ (condition) ? 1 : -1 ];

#ifndef FSDEVICE_I2C_ADDR_R
const int gFsDeviceStereoMode = 0;
const int gDevI2CAddrLeft = FSDEVICE_I2C_ADDR;
const int gDevI2CAddrRight = 0;

#else

STATIC_ASSERT( FSDEVICE_I2C_ADDR_R != FSDEVICE_I2C_ADDR, "i2c device addresses invalid for stereo mode!");

const int gFsDeviceStereoMode = 1;
const int gDevI2CAddrLeft = FSDEVICE_I2C_ADDR;
const int gDevI2CAddrRight = FSDEVICE_I2C_ADDR_R;
#endif

/**************************************************
    Initialize fs16xx device.
***************************************************/
int Ex_Fs16xx_Init(struct SmartPa *smartPa) {
    int result = FSRESULT_OK;
    Fs16xx_Error_t err;

    DEBUGPRINT("%s enter.", __func__);
    if(gInitialized > 0) {
        PRINT_ERROR("%s Already intialized.", __func__);
        return FSRESULT_OK;;
    }
    Fs16xx_InitializePresets();
    fs16xx_LoadPresets();

    err = Fs16xx_Open(gDevI2CAddrLeft, &gFs16xxId[FS16XX_DEV_INDEX_L]);
    if(Fs16xx_Error_OK != err) {
        result = FSRESULT_INIT_FAILED;
        PRINT_ERROR("%s failed to open device, error = %d", __func__, err);
        return FSRESULT_INIT_FAILED; 
    }

    err = Fs16xx_Init(gFs16xxId[FS16XX_DEV_INDEX_L]);
    if(Fs16xx_Error_OK != err) {
        result = FSRESULT_INIT_FAILED;
        PRINT_ERROR("%s initialize failed, error = %d", __func__, err);
        return FSRESULT_INIT_FAILED;
    }
    
    if(gFsDeviceStereoMode) {
        PRINT_ERROR("%s stereo mode.", __func__);
        err = Fs16xx_Open(gDevI2CAddrRight, &gFs16xxId[FS16XX_DEV_INDEX_R]);
        if(Fs16xx_Error_OK != err) {
            result = FSRESULT_INIT_FAILED;
            PRINT_ERROR("%s failed to open right channel device, error = %d", __func__, err);
            return FSRESULT_INIT_FAILED;
        }

        err = Fs16xx_Init(gFs16xxId[FS16XX_DEV_INDEX_R]);
        if(Fs16xx_Error_OK != err) {
            result = FSRESULT_INIT_FAILED;
            PRINT_ERROR("%s initialize right channel device failed, error = %d", __func__, err);
            return FSRESULT_INIT_FAILED;
        }
    }

    gInitialized = 1;

    fs16xx_check_calibration(gFs16xxId[FS16XX_DEV_INDEX_L]);
    Fs16xx_SetPreset(gFs16xxId[FS16XX_DEV_INDEX_L], 0);
    if(gFsDeviceStereoMode) {
        fs16xx_check_calibration(gFs16xxId[FS16XX_DEV_INDEX_R]);
        Fs16xx_SetPreset(gFs16xxId[FS16XX_DEV_INDEX_R], 0);
    }

    //TODO: Fill in supportedRateList, codecCtlName, etc.
    DEBUGPRINT("Initialized samplerate=%d .", smartPa->runtime.sampleRate);

    // Set samplerate after initialized.
    Ex_Fs16xx_SetSamplerate(DEFAULT_SAMPLERATE);

    DEBUGPRINT("%s exit.", __func__);
    return FSRESULT_OK;
}

/**************************************************
    Deinitialize fs16xx device.
***************************************************/
int Ex_Fs16xx_Deinit() {
    DEBUGPRINT("%s enter.", __func__);
    if(!gInitialized) {
        PRINT_ERROR("%s device not intialized.", __func__);
        return FSRESULT_INIT_FAILED;
    }
    
    Fs16xx_SetPower(gFs16xxId[FS16XX_DEV_INDEX_L], (int)Fs16xx_PwrOff);
    Fs16xx_Close(gFs16xxId[FS16XX_DEV_INDEX_L]);
    Fs16xx_Deinit(gFs16xxId[FS16XX_DEV_INDEX_L]);

    if(gFsDeviceStereoMode) {
        Fs16xx_SetPower(gFs16xxId[FS16XX_DEV_INDEX_R], (int)Fs16xx_PwrOff);
        Fs16xx_Close(gFs16xxId[FS16XX_DEV_INDEX_R]);
        Fs16xx_Deinit(gFs16xxId[FS16XX_DEV_INDEX_R]);
    }
    gInitialized = 0;

    Fs16xx_DeinitializePresets();
    DEBUGPRINT("%s exit.", __func__);

    return FSRESULT_OK;
}

/**************************************************
    Set samplerate to fs16xx device.
***************************************************/
void Ex_Fs16xx_SetSamplerate(int sampleRate) {
    DEBUGPRINT("%s enter.", __func__);

    if(!gInitialized) {
        DEBUGPRINT("%s failed, device not initialized.", __func__);
        return;
    }

    Fs16xx_SetSampleRate(gFs16xxId[FS16XX_DEV_INDEX_L], sampleRate);
    if(gFsDeviceStereoMode) {
       Fs16xx_SetSampleRate(gFs16xxId[FS16XX_DEV_INDEX_R], sampleRate);
    }
    DEBUGPRINT("%s exit.", __func__);
}

/**************************************************
    Reset fs16xx device.
***************************************************/
void Ex_Fs16xx_Reset() {
    Fs16xx_Error_t err;
    
    DEBUGPRINT("%s enter.", __func__);
    if(!gInitialized) {
        PRINT_ERROR("%s device not intialized.", __func__);
        return;
    }

    err = Fs16xx_Init(gFs16xxId[FS16XX_DEV_INDEX_L]);
    if(Fs16xx_Error_OK != err) {
        PRINT_ERROR("%s initialize failed, error = %d", __func__, err);
        return;
    }

    if(gFsDeviceStereoMode) {
        err = Fs16xx_Init(gFs16xxId[FS16XX_DEV_INDEX_R]);
        if(Fs16xx_Error_OK != err) {
            PRINT_ERROR("%s initialize right channel device failed, error = %d", __func__, err);
            return;
        }
    }

    fs16xx_check_calibration(gFs16xxId[FS16XX_DEV_INDEX_L]);
    Fs16xx_SetPreset(gFs16xxId[FS16XX_DEV_INDEX_L], 0);
    if(gFsDeviceStereoMode) {
        fs16xx_check_calibration(gFs16xxId[FS16XX_DEV_INDEX_R]);
        Fs16xx_SetPreset(gFs16xxId[FS16XX_DEV_INDEX_R], 0);
    }

    DEBUGPRINT("%s exit.", __func__);
}

/**************************************************
    Set fs16xx device on.
***************************************************/
int Ex_Fs16xx_SpeakerOn(struct SmartPaRuntime *runtime) {
    Fs16xx_Calib_State_t calib_state;
    DEBUGPRINT("%s enter. sample rate=%d", __func__, runtime->sampleRate);

    if(!gInitialized) {
        PRINT_ERROR("%s failed, device not initialized.", __func__);
        return FSRESULT_INIT_FAILED;
    }

    if(44100 == runtime->sampleRate ||
       48000 == runtime->sampleRate ||
       16000 == runtime->sampleRate) {
        Ex_Fs16xx_SetSamplerate(runtime->sampleRate);
    } else {
        PRINT_ERROR("SpeakerOn samplerate=%d not supported!", runtime->sampleRate);
    }

    Fs16xx_SetPower(gFs16xxId[FS16XX_DEV_INDEX_L], (int)Fs16xx_PwrOn);
    //Fs16xx_SetPreset(gFs16xxId[FS16XX_DEV_INDEX_L], 0);

    if(Fs16xx_WaitCLKStable(gFs16xxId[FS16XX_DEV_INDEX_L]) != Fs16xx_Error_OK) {
        PRINT_ERROR("SpeakerOn failed to wait for PLL lock!");
        return FSRESULT_OK;
    }

    Fs16xx_SetMute(gFs16xxId[FS16XX_DEV_INDEX_L], FS_UMUTE);
    if(44100 == runtime->sampleRate || 48000 == runtime->sampleRate) {
        Fs16xx_SetTemprProtection(gFs16xxId[FS16XX_DEV_INDEX_L], FS_ENABLE);
    } else {
        Fs16xx_SetTemprProtection(gFs16xxId[FS16XX_DEV_INDEX_L], FS_DISABLE);
    }
    
    if(gFsDeviceStereoMode) {
        Fs16xx_SetPower(gFs16xxId[FS16XX_DEV_INDEX_R], (int)Fs16xx_PwrOn);
        Fs16xx_SetMute(gFs16xxId[FS16XX_DEV_INDEX_R], FS_UMUTE);
        //FIXME:  Calibrate right channel
        //Fs16xx_SetPreset(gFs16xxId[FS16XX_DEV_INDEX_R], 0);

        if(Fs16xx_WaitCLKStable(gFs16xxId[FS16XX_DEV_INDEX_R]) != Fs16xx_Error_OK) {
            PRINT_ERROR("SpeakerOn(R) failed to wait for PLL lock!");
            return FSRESULT_OK;
        }
    }

    DEBUGPRINT("%s exit.", __func__);
    return FSRESULT_OK;
}

/**************************************************
    Set fs16xx device off.
***************************************************/
int Ex_Fs16xx_SpeakerOff() {
    DEBUGPRINT("%s enter.", __func__);

    if(!gInitialized) {
        DEBUGPRINT("%s failed, device not initialized.", __func__);
        return FSRESULT_INIT_FAILED;
    }

    Fs16xx_SetMute(gFs16xxId[FS16XX_DEV_INDEX_L], FS_MUTE);
    Sleep(30);
    Fs16xx_SetPower(gFs16xxId[FS16XX_DEV_INDEX_L], (int)Fs16xx_PwrOff);
    if(gFsDeviceStereoMode) {
        Fs16xx_SetMute(gFs16xxId[FS16XX_DEV_INDEX_R], FS_MUTE);
        Sleep(30);
        Fs16xx_SetPower(gFs16xxId[FS16XX_DEV_INDEX_R], (int)Fs16xx_PwrOff);
    }

    DEBUGPRINT("%s exit.", __func__);
    return FSRESULT_OK;
}

/**************************************************
    Set fs16xx aec config.
***************************************************/
void Ex_Fs16xx_AECConfigure(int config) {
    DEBUGPRINT("%s enter. config=%d", __func__, config);

    if(!gInitialized) {
        DEBUGPRINT("%s failed, device not initialized.", __func__);
        return;
    }

    DEBUGPRINT("%s exit.", __func__);
}

/**************************************************
    Set fs16xx bypass dsp
***************************************************/
void Ex_Fs16xx_SetBypassDspIncall(int bypass) {
    DEBUGPRINT("%s enter. bypass=%d", __func__, bypass);

    DEBUGPRINT("%s exit.", __func__);
}

int fs_smartpa_init_check(struct SmartPa *smart_pa)
{
    Fs16xx_Error_t err;
	DEBUGPRINT("%s enter.", __func__);

    err = Fs16xx_Open(gDevI2CAddrLeft, &gFs16xxId[FS16XX_DEV_INDEX_L]);
    if(Fs16xx_Error_Not_FsDev == err) {
        PRINT_ERROR("%s not foursemi device.", __func__);
        return -1;
    }

    fs16xx_close(gFs16xxId[FS16XX_DEV_INDEX_L]);
    DEBUGPRINT("%s foursemi device init check OK.", __func__);
	smart_pa->ops.init = (FsSmartPaOps_Init)Ex_Fs16xx_Init;
	smart_pa->ops.deinit = (FsSmartPaOps_Deinit)Ex_Fs16xx_Deinit;
	smart_pa->ops.speakerOn = (FsSmartPaOps_SpeakerOn)Ex_Fs16xx_SpeakerOn;
	smart_pa->ops.speakerOff = (FsSmartPaOps_SpeakerOff)Ex_Fs16xx_SpeakerOff;
    
    DEBUGPRINT("%s exit.", __func__);
	return 0;
}

int mtk_smartpa_init(struct SmartPa *smart_pa)
{
	DEBUGPRINT("%s enter foursemi.", __func__);
	smart_pa->ops.init = Ex_Fs16xx_Init;
	smart_pa->ops.deinit = Ex_Fs16xx_Deinit;
	smart_pa->ops.speakerOn = Ex_Fs16xx_SpeakerOn;
	smart_pa->ops.speakerOff = Ex_Fs16xx_SpeakerOff;

    DEBUGPRINT("%s exit foursemi.", __func__);
	return 0;
}


//EXPORT_SYMBOL(mtk_smartpa_init);
//EXPORT_SYMBOL(fs_smartpa_init_check);

/***************************************
EXPORT_SYMBOL(Ex_Fs16xx_Init);
EXPORT_SYMBOL(Ex_Fs16xx_Deinit);
EXPORT_SYMBOL(Ex_Fs16xx_Reset);
EXPORT_SYMBOL(Ex_Fs16xx_SpeakerOff);
EXPORT_SYMBOL(Ex_Fs16xx_SpeakerOn);
EXPORT_SYMBOL(Ex_Fs16xx_SetSamplerate);
EXPORT_SYMBOL(Ex_Fs16xx_AECConfigure);
*****************************************/
