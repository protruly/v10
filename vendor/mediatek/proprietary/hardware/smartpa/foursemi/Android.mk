LOCAL_PATH:= $(call my-dir)

###################### libfshal ####################
include $(CLEAR_VARS)

LOCAL_C_INCLUDES:=  $(LOCAL_PATH)/hal/inc \
					$(LOCAL_PATH)/utils/inc \
					$(LOCAL_PATH)

LOCAL_SRC_FILES:=	hal/src/interfaceImpl/FsI2C_Impl.c \
					hal/src/FsConnRegister.c \
					hal/src/FS_I2C.c

LOCAL_MODULE := libfshal
LOCAL_SHARED_LIBRARIES:= libcutils libutils
LOCAL_MODULE_TAGS := optional
LOCAL_PRELINK_MODULE := false

include $(BUILD_STATIC_LIBRARY)

###################### libfs16xx_interface ####################
include $(CLEAR_VARS)

LOCAL_C_INCLUDES:=  $(LOCAL_PATH)/hal/inc \
					$(LOCAL_PATH)/utils/inc \
					$(LOCAL_PATH)/fs16xx/inc \
					$(LOCAL_PATH)

LOCAL_SRC_FILES:=	fs16xx/src/Fs16xx.c \
					fs16xx/src/Fs16xxAPI.c \
					fs16xx/src/Fs16xxPreset.c\
					fs16xx/src/Fs16xxCalibration.c\
					hal/src/FsConnRegister.c \
					Fs16xx_Interface.c

LOCAL_MODULE := libfs16xx_interface
LOCAL_SHARED_LIBRARIES:= liblog libcutils libutils
LOCAL_STATIC_LIBRARIES:= libfshal
LOCAL_MODULE_TAGS := optional
LOCAL_PRELINK_MODULE := false

include $(BUILD_SHARED_LIBRARY)

################# fstool executable #################
include $(CLEAR_VARS)
LOCAL_C_INCLUDES:=  $(LOCAL_PATH)/hal/inc \
					$(LOCAL_PATH)/utils/inc \
					$(LOCAL_PATH)/fs16xx/inc \
					$(LOCAL_PATH)

LOCAL_SRC_FILES:=	app/fstool/FsTool.cpp

LOCAL_MODULE := fstool
LOCAL_SHARED_LIBRARIES:= libcutils libutils  libmedia libbinder libfs16xx_interface
LOCAL_STATIC_LIBRARIES:= libfshal
LOCAL_MODULE_TAGS := optional
LOCAL_PRELINK_MODULE := false

include $(BUILD_EXECUTABLE)
