/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/
#ifndef _FS16XX_CUSTOM_H_
#define _FS16XX_CUSTOM_H_

#if !( defined(WIN32) || defined(_X64) )
/* Android */
//#define FS_I2CDEVICE        "/dev/i2c-1"    /* For Qualcomm platform */
//#define FS_I2CDEVICE       "/dev/smartpa_i2c"     /* For MTK platform */
#define FS_I2CDEVICE       "/dev/i2c_smartpa"     /* For MTK platform */
#else
/* Windows */
#define FS_I2CDEVICE "ftdi"
#endif

// Mono/Left channel
#define FSDEVICE_I2C_ADDR  0x34

// Right channel for stereo mode, please uncomment for stereo mode
//#define FSDEVICE_I2C_ADDR_R  0x35

#define DEFAULT_SAMPLERATE 44100


#define FS16XX_PARAM_DIR "/system/etc/smartpa_params"

#endif
