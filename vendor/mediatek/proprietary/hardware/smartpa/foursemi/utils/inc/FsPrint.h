/*
 *Copyright (C) 2016 Fourier Semiconductor Inc. 
 */

#ifndef _FSPRINT_H
#define _FSPRINT_H

#ifdef __cplusplus
extern "C" {
#endif

// FOURIER_DEBUG_CODE could be defined here or in Android.mk file
#ifndef FOURIER_DEBUG_CODE
    #define FOURIER_DEBUG_CODE
#endif

#ifndef FS_TAG
    #define FS_TAG "fs16xx: "
#endif

#ifdef __KERNEL__

/* Linux kernel */
#ifdef FOURIER_DEBUG_CODE
    #define _DEBUGPRINT(msg,va...) printk(FS_TAG "%s:%d: "msg,__func__,__LINE__,##va);
#else
    #define _DEBUGPRINT(msg,va...) do {} while(0)
#endif

#define PRINT(msg...)	         printk(msg) 
#define PRINT_ERROR(msg,va...)   printk(FS_TAG "ERROR %s:%d: "msg,__func__,__LINE__, ##va)
#define DEBUGPRINT               _DEBUGPRINT(__VA_ARGS__)

#else 
#if defined(WIN32) || defined(_X64)

#include <Windows.h>

/* Windows */
#ifdef FOURIER_DEBUG_CODE
    #define _DEBUGPRINT(msg,...) printf(FS_TAG "%s:%d: "msg,__FUNCTION__,__LINE__,__VA_ARGS__);
#else
    #define _DEBUGPRINT(msg,...) do {} while(0)
#endif

#define _PRINT_ERROR(msg,...) printf(FS_TAG "%s:%s:%d: "msg,__FILE__,__FUNCTION__,__LINE__,__VA_ARGS__)

#define PRINT(msg, ...)            printf(msg, __VA_ARGS__)
#define PRINT_ERROR(msg, ...)      _PRINT_ERROR(msg, __VA_ARGS__)
#define DEBUGPRINT(msg, ...)       _DEBUGPRINT(msg, __VA_ARGS__)

#else 
#ifdef __ANDROID__
 /* Android user mode */
#include <utils/Log.h>

#ifndef LOG_NDEBUG
#define LOG_NDEBUG 1
#endif

#ifdef FOURIER_DEBUG_CODE
    #define _DEBUGPRINT(msg,va...) ALOGD(FS_TAG "%s:%d: " msg,__func__,__LINE__,##va);
#else
    #define _DEBUGPRINT(msg...)   do {} while(0)
#endif

#define PRINT(msg...)         ALOGD(msg)
#define PRINT_ERROR(msg...)   ALOGE(msg)
#define DEBUGPRINT(msg...)    _DEBUGPRINT(msg)

#endif

#endif

#endif

#ifdef __cplusplus
}
#endif

#endif

