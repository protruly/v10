/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2015. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 * Include
 *****************************************************************************/

#include "RpImsController.h"
#include "RfxStatusDefs.h"
#include "RfxRootController.h"
#include <cutils/properties.h>

#define RFX_LOG_TAG "RP_IMS"
/// M: add for op09 default volte setting @{
#define PROPERTY_3G_SIM          "persist.radio.simswitch"
#define PROPERTY_VOLTE_STATE     "persist.radio.volte_state"
#define PROPERTY_VOLTE_ENABLE    "persist.mtk.volte.enable"
#define PROPERTY_OPERATOR_OPTR   "ro.operator.optr"
#define PROPERTY_CT_VOLTE_SUPPORT   "persist.mtk_ct_volte_support"
#define CARD_TYPE_INVALID          -1
#define CARD_TYPE_NONE             0
#define READ_ICCID_RETRY_TIME      500
#define READ_ICCID_MAX_RETRY_COUNT 8
static const char PROPERTY_ICCID_SIM[4][25] = {
    "ril.iccid.sim1",
    "ril.iccid.sim2",
    "ril.iccid.sim3",
    "ril.iccid.sim4"
};
static const char PROPERTY_LAST_ICCID_SIM[4][29] = {
    "persist.radio.lastsim1_iccid",
    "persist.radio.lastsim2_iccid",
    "persist.radio.lastsim3_iccid",
    "persist.radio.lastsim4_iccid"
};
bool RpImsController::sInitDone = false;
char RpImsController::sLastBootIccId[4][21] = {{0}, {0}, {0}, {0}};
int RpImsController::sLastBootVolteState = 0;
/// @}

/*****************************************************************************
 * Class RfxImsController
 * The class is created if the slot is single mode, LWG or C,
 * During class life time always communicate with one modem, gsm or c2k.
 *****************************************************************************/

/// M: add for op09 volte setting @{
int RpImsController::sIsOp09 = -1;
int RpImsController::sIsCtVolteSupport = -1;
/// @}

RFX_IMPLEMENT_CLASS("RpImsController", RpImsController, RfxController);

RpImsController::RpImsController() :
    mNoIccidTimerHandle(NULL),
    mNoIccidRetryCount(0),
    mMainSlotId(0),
    mIsBootUp(true),
    mIsSimSwitch(false),
    mCardReady(false) {
}

RpImsController::~RpImsController() {
}

void RpImsController::onInit() {
    RfxController::onInit();  // Required: invoke super class implementation

    logD(RFX_LOG_TAG, "onInit");
    /// M: add for op09 default volte setting @{
    initOp09Ims();
    /// @}
}

void RpImsController::onDeinit() {
    logD(RFX_LOG_TAG, "onDeinit");
    /// M: add for op09 default volte setting @{
    deinitOp09Ims();
    /// @}
    RfxController::onDeinit();
}

bool RpImsController::onHandleResponse(const sp<RfxMessage>& message) {
    logD(RFX_LOG_TAG, "Handle response %s.", message->toString().string());
    switch (message->getId()) {
        /// M: add for op09 default volte setting @{
        case RIL_LOCAL_REQUEST_UPDATE_VOLTE_SETTING:
            break;
        /// @}
    default:
        logD(RFX_LOG_TAG, "unknown request, ignore!");
        return false;
    }
    return true;
}

bool RpImsController::onHandleUrc(const sp<RfxMessage>& message) {
    logD(RFX_LOG_TAG, "Handle URC %s", message->toString().string());

    switch (message->getId()) {
        /// M: add for op09 volte setting @{
        case RIL_UNSOL_VOLTE_SETTING: {
            setVolteSettingStatus();
        /// @}
        break;
    }

    default:
        logD(RFX_LOG_TAG, "unknown urc, ignore!");
        return false;
    }

    return true;
}

/// M: add for op09 volte setting @{
void RpImsController::initOp09Ims() {
    if (!isCtVolteSupport()) {
        return;
    }

    char tempstr[PROPERTY_VALUE_MAX] = { 0 };
    property_get(PROPERTY_3G_SIM, tempstr, "1");
    mMainSlotId = atoi(tempstr) - 1;

    char volteEnable[PROPERTY_VALUE_MAX] = { 0 };
    property_get(PROPERTY_VOLTE_ENABLE, volteEnable, "0");
    int volteValue = atoi(volteEnable);
    logD(RFX_LOG_TAG, "initOp09Ims: volteEnable = %d, mainSlotId = %d",
         volteValue, mMainSlotId);

    if (sInitDone == false) {
        int setValue;
        for (int slot = 0; slot < SIM_COUNT; slot++) {
            property_get(PROPERTY_LAST_ICCID_SIM[slot], sLastBootIccId[slot], "null");
            if (slot == mMainSlotId) {
                getStatusManager(slot)
                    -> setIntValue(RFX_STATUS_KEY_VOLTE_STATE, volteValue);
            } else {
                getStatusManager(slot)
                    -> setIntValue(RFX_STATUS_KEY_VOLTE_STATE, 0);
            }
        }
        char tempstr[PROPERTY_VALUE_MAX] = { 0 };
        property_get(PROPERTY_VOLTE_STATE, tempstr, "0");
        sLastBootVolteState = atoi(tempstr);
        logD(RFX_LOG_TAG, "initOp09Ims, sLastBootVolteState = %d", sLastBootVolteState);
        sInitDone = true;
    }

    const int urc_id_list[] = {
        RIL_UNSOL_VOLTE_SETTING,         // volte setting urc
    };

    const int request_id_list[] = {
        RIL_LOCAL_REQUEST_UPDATE_VOLTE_SETTING,  // update volte setting request
    };

    // register request & URC id list
    // NOTE. one id can only be registered by one controller
    registerToHandleRequest(request_id_list,
            sizeof(request_id_list) / sizeof(int));
    registerToHandleUrc(urc_id_list, sizeof(urc_id_list) / sizeof(int));

    getStatusManager()->registerStatusChanged(RFX_STATUS_KEY_CARD_TYPE,
        RfxStatusChangeCallback(this, &RpImsController::onCardTypeChanged));
}

void RpImsController::deinitOp09Ims() {
    if (!isCtVolteSupport()) {
        return;
    }
    getStatusManager()->unRegisterStatusChanged(RFX_STATUS_KEY_CARD_TYPE,
        RfxStatusChangeCallback(this, &RpImsController::onCardTypeChanged));
}

void RpImsController::setVolteSettingStatus() {
    if (!isCtVolteSupport() || !mCardReady) {
        return;
    }

    int setValue;
    int oldStatus;
    char volteEnable[PROPERTY_VALUE_MAX] = { 0 };
    property_get(PROPERTY_VOLTE_ENABLE, volteEnable, "0");
    int volteValue = atoi(volteEnable);
    logD(RFX_LOG_TAG, "setVolteSettingStatus: volteEnable = %d, mainSlotId = %d",
        volteValue, mMainSlotId);

    for (int slotCheckingBit = 0; slotCheckingBit < SIM_COUNT; slotCheckingBit++) {
        if (slotCheckingBit == mMainSlotId) {
            setValue = volteValue;
        } else {
            setValue = 0;
        }

        oldStatus = getStatusManager(slotCheckingBit)
                        -> getIntValue(RFX_STATUS_KEY_VOLTE_STATE, -1);
        if (oldStatus != setValue) {
            getStatusManager(slotCheckingBit)
                -> setIntValue(RFX_STATUS_KEY_VOLTE_STATE, setValue);
        }
    }
}

void RpImsController::onCardTypeChanged(RfxStatusKeyEnum key,
    RfxVariant old_value, RfxVariant value) {
    // main slot status only update after switching finish, so must use property.
    char tempstr[PROPERTY_VALUE_MAX] = { 0 };
    property_get(PROPERTY_3G_SIM, tempstr, "1");
    int mainSlotId = atoi(tempstr) - 1;
    int curSlotId = getSlotId();

    if (mainSlotId != mMainSlotId) {
        mMainSlotId = mainSlotId;
        mIsSimSwitch = true;
    }
    int old_type = old_value.asInt();
    int new_type = value.asInt();
    logD(RFX_LOG_TAG, "onCardTypeChanged, old_type is %d, new_type is %d.", old_type, new_type);
    // if old_type == -1 and new_type == 0, it is bootup without sim.
    // if old_type == -1 and new_type > 0, it is bootup with sim or sim switch finish.
    // if old_type == 0 and new_type > 0, it is plug in sim.
    // if old_type == 0 and new_type == 0, it is still no sim.
    // if old_type > 0 and new_type == -1, it is sim switch start, no need deal yet.
    if (old_type == CARD_TYPE_INVALID || old_type == CARD_TYPE_NONE) {
        if (new_type == CARD_TYPE_NONE) {
            // no sim inserted case
            property_set(PROPERTY_LAST_ICCID_SIM[curSlotId], "null");
            mIsBootUp = false;
            mIsSimSwitch = false;
            mCardReady = false;
        } else if (new_type > 0) {
            if (!isOp09() &&
                mMainSlotId != curSlotId) {
                // For om project, only do the check for main capability sim.
                // Because om's design only need check CT card first insert or switch,
                // other operator sim will not set default volte setting.
                return;
            }
            char newIccIdStr[PROPERTY_VALUE_MAX] = { 0 };
            property_get(PROPERTY_ICCID_SIM[curSlotId], newIccIdStr, "");
            logD(RFX_LOG_TAG, "onCardTypeChanged, newIccIdStr = %s",
                 givePrintableStr(newIccIdStr));
            if (strlen(newIccIdStr) == 0 || strcmp("N/A", newIccIdStr) == 0) {
                logD(RFX_LOG_TAG, "onCardTypeChanged, iccid not ready");
                // start timer waiting icc id loaded
                if (mNoIccidTimerHandle != NULL) {
                    RfxTimer::stop(mNoIccidTimerHandle);
                }
                mNoIccidRetryCount = 0;
                mNoIccidTimerHandle = RfxTimer::start(RfxCallback0(this,
                            &RpImsController::onNoIccIdTimeout), ms2ns(READ_ICCID_RETRY_TIME));
            } else {
                setDefaultVolteState(curSlotId, newIccIdStr, new_type);
            }
        }
    } else {
        // if old_type > 0 and new_type == 0, it is plug out sim.
        if (new_type == CARD_TYPE_NONE) {
            property_set(PROPERTY_LAST_ICCID_SIM[curSlotId], "null");
            mIsBootUp = false;
        }
        mIsSimSwitch = false;
        mCardReady = false;
    }
}

void RpImsController::onNoIccIdTimeout() {
    mNoIccidTimerHandle = NULL;
    char newIccIdStr[PROPERTY_VALUE_MAX] = { 0 };
    int slotId = getSlotId();
    property_get(PROPERTY_ICCID_SIM[slotId], newIccIdStr, "");
    logD(RFX_LOG_TAG, "onNoIccIdTimeout, newIccIdStr = %s", givePrintableStr(newIccIdStr));

    if (strlen(newIccIdStr) == 0 || strcmp("N/A", newIccIdStr) == 0) {
        // if still fail, need revise timer
        if (mNoIccidRetryCount < READ_ICCID_MAX_RETRY_COUNT &&
            mNoIccidTimerHandle == NULL) {
            mNoIccidRetryCount++;
            mNoIccidTimerHandle = RfxTimer::start(RfxCallback0(this,
                            &RpImsController::onNoIccIdTimeout), ms2ns(READ_ICCID_RETRY_TIME));
        } else {
            mCardReady = true;
            logD(RFX_LOG_TAG, "onNoIccIdTimeout, error, reach max retry count!!!");
        }
    } else {
        int newType = getStatusManager(slotId)->getIntValue(
                RFX_STATUS_KEY_CARD_TYPE, CARD_TYPE_NONE);
        setDefaultVolteState(slotId, newIccIdStr, newType);
    }
}

void RpImsController::setDefaultVolteState(int slot_id, char new_iccid[],int card_type) {
    mCardReady = true;
    char oldIccIdStr[PROPERTY_VALUE_MAX] = { 0 };
    // Todo: multi ims need save every sim iccid
    property_get(PROPERTY_LAST_ICCID_SIM[slot_id], oldIccIdStr, "null");
    logD(RFX_LOG_TAG, "setDefaultVolteState, oldIccIdStr = %s", givePrintableStr(oldIccIdStr));

    if (strlen(oldIccIdStr) == 0 || strcmp("null", oldIccIdStr) == 0
     || strcmp(new_iccid, oldIccIdStr) != 0
     || (isOp09() && mIsSimSwitch)) {
        property_set(PROPERTY_LAST_ICCID_SIM[slot_id], new_iccid);
        char volteEnable[PROPERTY_VALUE_MAX] = { 0 };
        property_get(PROPERTY_VOLTE_ENABLE, volteEnable, "0");
        int enableValue = atoi(volteEnable);
        int setValue = 1;
        if (isOp09SimCard(slot_id, new_iccid, card_type)) {
            setValue = 0;
        } else {
            if (!isOp09()) {
                return;
            }
        }

        {
            if (isOp09()) {
                if (mIsBootUp) {
                    for (int slot = 0; slot < SIM_COUNT; slot++) {
                        if (strcmp(new_iccid, sLastBootIccId[slot]) == 0) {
                            // For switch sim when powoff case, boot up need get before setting
                            int state = sLastBootVolteState;
                            setValue = (state >> slot) & 1;
                            logD(RFX_LOG_TAG,
                                 "setDefaultVolteState, change sim slot reboot, last= %d, %d",
                                 state, setValue);
                            break;
                        }
                    }
                    mIsBootUp = false;
                }

                char volteState[PROPERTY_VALUE_MAX] = { 0 };
                property_get(PROPERTY_VOLTE_STATE, volteState, "0");
                int stateValue = atoi(volteState);
                if (mIsSimSwitch && (strcmp(new_iccid, oldIccIdStr) == 0)) {
                    setValue = (stateValue >> slot_id) & 1;
                    logD(RFX_LOG_TAG, "setDefaultVolteState, sim switch, setValue = %d", setValue);
                } else {
                    if (setValue == 1) {
                        stateValue = stateValue | (1 << slot_id);
                    } else {
                        stateValue = stateValue & (~(1 << slot_id));
                    }
                    char temp[3] = {0};
                    if (stateValue > 10) {
                        temp[0] = '1';
                        temp[1] = '0' + (stateValue - 10);
                    } else {
                        temp[0] = '0' + stateValue;
                    }
                    property_set(PROPERTY_VOLTE_STATE, temp);
                    logD(RFX_LOG_TAG, "setDefaultVolteState, new volte_state = %d, %s", stateValue,
                        temp);
                }
                mIsSimSwitch = false;
            } else {
                // for OM project, powoff switch sim case no need update as default set.
                logD(RFX_LOG_TAG, "setDefaultVolteState, mIsBootUp = %d", mIsBootUp);
                if (mIsBootUp) {
                    mIsBootUp = false;
                    for (int slot = 0; slot < SIM_COUNT; slot++) {
                        if (strcmp(new_iccid, sLastBootIccId[slot]) == 0) {
                            logD(RFX_LOG_TAG, "setDefaultVolteState, change sim slot reboot");
                            return;
                        }
                    }
                }
            }

            if (mMainSlotId != slot_id) {
                // for single ims, only set volte state and send request for main capability sim.
                return;
            }

            for (int i = 0; i < SIM_COUNT; i++) {
                if (i == slot_id) {
                    if (enableValue != setValue) {
                        // Wos init use property, URC may dealy by imsRilAdapter init delay.
                        // m0.mp9 no wos.
                        // if (setValue == 0) {
                        //    property_set(PROPERTY_VOLTE_ENABLE, "0");
                        // } else {
                        //    property_set(PROPERTY_VOLTE_ENABLE, "1");
                        // }
                        sendDefaultVolteStateRequest(slot_id, setValue);
                    }
                    getStatusManager(i)
                        -> setIntValue(RFX_STATUS_KEY_VOLTE_STATE, setValue);
                } else {
                    getStatusManager(i)
                        -> setIntValue(RFX_STATUS_KEY_VOLTE_STATE, 0);
                }
            }
        }
     }
}


bool RpImsController::isOp09SimCard(int slot_id, char icc_id[], int card_type) {
    String8 home_imsi = getStatusManager(slot_id)->getString8Value(RFX_STATUS_KEY_GSM_IMSI);
    logD(RFX_LOG_TAG, "isOp09SimCard, imsi = %s", givePrintableStr(home_imsi.string()));
    bool isOp09Card = false;
    if (home_imsi == String8("")) {
        if (strncmp(icc_id, "898603", 6) == 0 ||
           strncmp(icc_id, "898611", 6) == 0) {
            isOp09Card = true;
        }
    } else {
        if (strncmp(home_imsi.string(), "460", 3) == 0 && (card_type & RFX_CARD_TYPE_RUIM)) {
            isOp09Card = true;
        }
    }
    return isOp09Card;
}

void RpImsController::sendDefaultVolteStateRequest(int slot_id, int value) {
    // send default volte state to GSM RILD
    sp<RfxMessage> request = RfxMessage::obtainRequest(slot_id,
            RADIO_TECH_GROUP_GSM, RIL_LOCAL_REQUEST_UPDATE_VOLTE_SETTING);
    Parcel* parcel = request->getParcel();
    parcel->writeInt32(2);
    parcel->writeInt32(value);
    parcel->writeInt32(slot_id);
    requestToRild(request);
}

const char* RpImsController::givePrintableStr(const char* iccId) {
    static char iccIdToPrint[PROPERTY_VALUE_MAX] = { 0 };
    if (strlen(iccId) > 6) {
        strncpy(iccIdToPrint, iccId, 6);
        return iccIdToPrint;
    }
    return iccId;
}

bool RpImsController::isOp09() {
    if (sIsOp09 == -1) {
        char optrstr[PROPERTY_VALUE_MAX] = { 0 };
        property_get(PROPERTY_OPERATOR_OPTR, optrstr, "");
        //logD(RFX_LOG_TAG, "isOp09(): optr = %s", optrstr);
        if (strncmp(optrstr, "OP09", 4) == 0) {
            sIsOp09 = 1;
        } else {
            sIsOp09 = 0;
        }
    }
    return (sIsOp09 == 1);
}

bool RpImsController::isCtVolteSupport() {
    if (sIsCtVolteSupport == -1) {
        char ctstr[PROPERTY_VALUE_MAX] = { 0 };
        property_get(PROPERTY_CT_VOLTE_SUPPORT, ctstr, "");
        if (strcmp(ctstr, "1") == 0) {
            sIsCtVolteSupport = 1;
        } else {
            sIsCtVolteSupport = 0;
        }
    }
    return (sIsCtVolteSupport == 1);
}
/// @}
