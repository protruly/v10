/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 * Include
 *****************************************************************************/
#include <cutils/properties.h>
#include "RpNwRatController.h"
#include "RfxLog.h"
#include "RfxStatusDefs.h"
#include "RpNwController.h"
#include "RpGsmNwRatSwitchHandler.h"
#include "RpCdmaNwRatSwitchHandler.h"
#include "RpCdmaLteNwRatSwitchHandler.h"
#include "modecontroller/RpCdmaLteDefs.h"
#include "ril.h"

#define RAT_CTRL_TAG "RpNwRatController"

/*****************************************************************************
 * Class RpNwRatController
 *****************************************************************************/

RFX_IMPLEMENT_CLASS("RpNwRatController", RpNwRatController, RfxController);

bool RpNwRatController::sIsInSwitching = false;
int RpNwRatController::sIsCtVolteSupport = -1;

RpNwRatController::RpNwRatController() :
    mCurAppFamilyType(APP_FAM_UNKNOWN),
    mCurPreferedNetWorkType(-1),
    mCurNwsMode(NWS_MODE_CSFB),
    mCurVolteState(VOLTE_OFF),
    mCapabilitySlotId(0),
    mNewAppFamilyType(APP_FAM_UNKNOWN),
    mNewPreferedNetWorkType(-1),
    mNewNwsMode(NWS_MODE_CSFB),
    mNewVolteState(VOLTE_OFF),
    mNwRatSwitchHandler(NULL),
    mPreferredNetWorkTypeFromRILJ(-1) {
}

RpNwRatController::~RpNwRatController() {
    RFX_OBJ_CLOSE(mNwRatSwitchHandler);
}

void RpNwRatController::setPreferredNetworkType(const int prefNwType,
        const sp<RfxAction>& action) {
    if (getEnginenerMode() == ENGINEER_MODE_AUTO) {
        if (prefNwType == -1) {
            logD(RAT_CTRL_TAG, "setPreferredNetworkType() leaving restricted mode");
            mPendingRestrictedRatSwitchRecord.prefNwType = -1;
            doPendingRatSwitchRecord();
        } else {
            logD(RAT_CTRL_TAG, "setPreferredNetworkType() entering restricted mode: %d", prefNwType);
            mPendingRestrictedRatSwitchRecord.prefNwType = prefNwType;
            mPendingRestrictedRatSwitchRecord.appFamType = APP_FAM_3GPP2;
            mPendingRestrictedRatSwitchRecord.ratSwitchCaller = RAT_SWITCH_RESTRICT;
            mPendingRestrictedRatSwitchRecord.nwsMode = mCurNwsMode;
            mPendingRestrictedRatSwitchRecord.volteState = mCurVolteState;
            if (mPendingInitRatSwitchRecord.prefNwType == -1
                    && mPendingNormalRatSwitchRecord.prefNwType == -1) {
                // save current state to pending queue
                queueRatSwitchRecord(mCurAppFamilyType, mCurPreferedNetWorkType, mCurNwsMode, mCurVolteState,
                        RAT_SWITCH_INIT, action, NULL);
            }
            switchNwRat(APP_FAM_3GPP2, PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA, mCurNwsMode, mCurVolteState,
                    RAT_SWITCH_RESTRICT, action, NULL);
        }
    }
}

void RpNwRatController::setPreferredNetworkType(const RatSwitchInfo ratSwtichInfo) {
    const AppFamilyType appFamType = ratSwtichInfo.app_family_type;
    const int prefNwType = ratSwtichInfo.network_type;
    const int type = ratSwtichInfo.card_state;
    const sp<RfxAction>& action = ratSwtichInfo.action;
    const int ratMode = ratSwtichInfo.rat_mode;

    logD(RAT_CTRL_TAG, "setPreferredNetworkType() from mode controller, "
            "appFamType is %d, prefNwType is %d, type is %d, ratMode is %d",
            appFamType, prefNwType, type, ratMode);
    if (getChipTestMode() == 1) {
        logD(RAT_CTRL_TAG, "ChipTest! setPreferredNetworkType not executed! action->act()");
        if (action != NULL) {
            action->act();
        }
        return;
    } else {
        int capabilitySlotId = getCapabilitySlotId();
        if (type == CARD_STATE_NO_CHANGED && appFamType == mNewAppFamilyType
                && mCapabilitySlotId == capabilitySlotId) {
            if (action != NULL) {
                action->act();
            }
            return;
        }
        if (type == CARD_STATE_NOT_HOT_PLUG || type == CARD_STATE_HOT_PLUGIN) {
            clearInvalidPendingRecords();
        }
        mCapabilitySlotId = capabilitySlotId;
    }

    int oldAppFamilyType = mNewAppFamilyType;
    mNewAppFamilyType = appFamType;

    logD(RAT_CTRL_TAG, "setPreferredNetworkType() prefNwType is %d, mPreferredNetWorkTypeFromRILJ is %d",
            prefNwType, mPreferredNetWorkTypeFromRILJ);
    int newPrefNwType = prefNwType;
    if (mPreferredNetWorkTypeFromRILJ != -1
            && mPreferredNetWorkTypeFromRILJ != PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA) {
        if (appFamType == APP_FAM_3GPP2) {
             newPrefNwType = filterPrefNwType(appFamType,
                     mPreferredNetWorkTypeFromRILJ, prefNwType);
         } else {
             newPrefNwType = mPreferredNetWorkTypeFromRILJ;
         }
         logD(RAT_CTRL_TAG, "mPreferredNetWorkTypeFromRILJ filtered as %d.", newPrefNwType);
    }

    if (appFamType == APP_FAM_3GPP) {
        /* For gsm card, create the ratSwitchHandler and set the related state*/
        switchNwRat(appFamType, newPrefNwType, NWS_MODE_CSFB, mNewVolteState, RAT_SWITCH_INIT,
                action, NULL);
    } else {
        if (getEnginenerMode() == ENGINEER_MODE_AUTO) {
            NwsMode newNwsMode = NWS_MODE_CDMALTE;
            VolteState newVolteState = mNewVolteState;
            if (mPendingNormalRatSwitchRecord.prefNwType != -1) {
                newNwsMode = mPendingNormalRatSwitchRecord.nwsMode;
                newVolteState = mPendingNormalRatSwitchRecord.volteState;
            } else {
                /* For ct 3g dual mode card Home and roaming switch */
                if (type == CARD_STATE_CARD_TYPE_CHANGED && oldAppFamilyType == appFamType) {
                    newNwsMode = mNewNwsMode;
                }
            }

            // [VoLTE] filter VolteState by newPrefNwType and ratMode
            newVolteState = filterVolteState(newPrefNwType, newVolteState, ratMode);
            newNwsMode = filterNwsMode(newNwsMode, newVolteState);

            switchNwRat(appFamType, newPrefNwType, newNwsMode, newVolteState,
                        RAT_SWITCH_INIT, action, NULL);
        } else {
            doNwSwitchForEngMode(action);
        }
    }
}

void RpNwRatController::setPreferredNetworkType(const int prefNwType,
        const sp<RfxMessage>& message) {
    logD(RAT_CTRL_TAG, "setPreferredNetworkType() from normal, prefNwType is %d,mNewPreferedNetWorkType is %d. ",
            prefNwType, mNewPreferedNetWorkType);
    if (getChipTestMode() == 1) {
        logD(RAT_CTRL_TAG, "ChipTest! setPreferredNetworkType not executed!");
        sp<RfxMessage> resToRilj = RfxMessage::obtainResponse(
                RIL_E_GENERIC_FAILURE, message);
        responseToRilj(resToRilj);
        return;
    }
    AppFamilyType appFamilyType = mNewAppFamilyType;
    VolteState volteState = mNewVolteState;

    if (appFamilyType == APP_FAM_3GPP) {
        /* For gsm card, create the ratSwitchHandler and set the related state*/
        switchNwRat(appFamilyType, prefNwType, NWS_MODE_CSFB, volteState, RAT_SWITCH_NORMAL,
                    NULL, message);
    } else if (appFamilyType == APP_FAM_3GPP2) {
        RpCdmaLteModeController *modeController = (RpCdmaLteModeController *) findController(-1,
                    RFX_OBJ_CLASS_INFO(RpCdmaLteModeController));
        int capability = modeController->getDefaultNetworkType(getSlotId());
        int targetPrefNwType = filterPrefNwType(appFamilyType, prefNwType, capability);
        logD(RAT_CTRL_TAG, "the prefer network type is filtered as %d, capability is %d .",
                      targetPrefNwType, capability);

        int engineerMode = getEnginenerMode();
        if ((engineerMode == ENGINEER_MODE_AUTO)
                || (engineerMode == ENGINEER_MODE_CDMA &&
                        (targetPrefNwType == PREF_NET_TYPE_CDMA_ONLY
                        || targetPrefNwType == PREF_NET_TYPE_EVDO_ONLY
                        || targetPrefNwType == PREF_NET_TYPE_CDMA_EVDO_AUTO))) {
            NwsMode nwsMode = mNewNwsMode;

            if (mPendingNormalRatSwitchRecord.prefNwType != -1) {
                nwsMode = mPendingNormalRatSwitchRecord.nwsMode;
                volteState = mPendingNormalRatSwitchRecord.volteState;
                logD(RAT_CTRL_TAG, "Has pending normal: update nwsMode from %d to %d.",
                        mNewNwsMode, nwsMode);
            } else if (mPendingInitRatSwitchRecord.prefNwType != -1) {
                nwsMode = mPendingInitRatSwitchRecord.nwsMode;
                volteState = mPendingInitRatSwitchRecord.volteState;
                logD(RAT_CTRL_TAG, "Has pending init: update nwsMode from %d to %d.",
                        mNewNwsMode, nwsMode);
            }

            // [VoLTE] filter VolteState by PreferredNetWorkTypeFromRILJ
            VolteState newVolteState = filterVolteState(targetPrefNwType, volteState, -1);
            NwsMode newNwsMode = filterNwsMode(nwsMode, newVolteState);

            switchNwRat(appFamilyType, targetPrefNwType, newNwsMode, newVolteState, RAT_SWITCH_NORMAL,
                    NULL, message);
        } else {
            logD(RAT_CTRL_TAG, "setPreferredNetworkType: return directly with nothing to do.");
            sp<RfxMessage> resToRilj = RfxMessage::obtainResponse(
                    RIL_E_SUCCESS, message);
            responseToRilj(resToRilj);
        }
    } else {
        logD(RAT_CTRL_TAG, "setPreferredNetworkType APP_FAM_UNKNOWN!");
        sp<RfxMessage> resToRilj = RfxMessage::obtainResponse(
                RIL_E_GENERIC_FAILURE, message);
        responseToRilj(resToRilj);
    }
}

int RpNwRatController::filterPrefNwType(const AppFamilyType appFamilyType,
        int prefNwType, int capability) {
    int targetPrefNwType = prefNwType;
    switch (prefNwType) {
    case PREF_NET_TYPE_LTE_CDMA_EVDO:
    case PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA:
        if (appFamilyType == APP_FAM_3GPP2) {
            if (capability == PREF_NET_TYPE_CDMA_EVDO_AUTO) {
                targetPrefNwType = PREF_NET_TYPE_CDMA_EVDO_AUTO;
            }
            if (capability == PREF_NET_TYPE_LTE_CDMA_EVDO) {
                targetPrefNwType = PREF_NET_TYPE_LTE_CDMA_EVDO;
            }
        } else if (appFamilyType == APP_FAM_3GPP) {
            if (capability == PREF_NET_TYPE_LTE_GSM_WCDMA) {
                targetPrefNwType = PREF_NET_TYPE_LTE_GSM_WCDMA;
            }
            if (capability == PREF_NET_TYPE_GSM_WCDMA) {
                targetPrefNwType = PREF_NET_TYPE_GSM_WCDMA;
            }
            if (capability == PREF_NET_TYPE_GSM_ONLY) {
                targetPrefNwType = PREF_NET_TYPE_GSM_ONLY;
            }
        }
        break;
    case PREF_NET_TYPE_CDMA_EVDO_AUTO:
    case PREF_NET_TYPE_CDMA_ONLY:
    case PREF_NET_TYPE_EVDO_ONLY:
    case PREF_NET_TYPE_GSM_WCDMA_CDMA_EVDO_AUTO:
    case PREF_NET_TYPE_GSM_WCDMA:
    case PREF_NET_TYPE_GSM_ONLY:
    case PREF_NET_TYPE_WCDMA:
    case PREF_NET_TYPE_GSM_WCDMA_AUTO:
    case PREF_NET_TYPE_LTE_ONLY:
    case PREF_NET_TYPE_LTE_TDD_ONLY:
        break;
    case PREF_NET_TYPE_LTE_GSM_WCDMA:
    case PREF_NET_TYPE_LTE_WCDMA:
    case PREF_NET_TYPE_LTE_GSM:
        /* the cdma card is in 3g mode, filter the 4g mode to 3g mode.*/
        if (appFamilyType == APP_FAM_3GPP2) {
            if (capability == PREF_NET_TYPE_CDMA_EVDO_AUTO) {
                targetPrefNwType = PREF_NET_TYPE_GSM_WCDMA;
            }
        }
        break;
    }
    return targetPrefNwType;
}

void RpNwRatController::creatRatSwitchHandlerIfNeeded(const AppFamilyType appFamType){
    logD(RAT_CTRL_TAG,"creatRatSwitchHandlerIfNeeded(), appFamType is %d. ",appFamType);
    if (appFamType == APP_FAM_3GPP) {
        // boot up work flow for gsm card.
        if (mNwRatSwitchHandler == NULL) {
            RFX_OBJ_CREATE_EX(mNwRatSwitchHandler, RpGsmNwRatSwitchHandler, this, (this));
            // for sim card hot plug in/out
        } else if (appFamType != mCurAppFamilyType) {
            if (!isRestrictedModeSupport()) {
                clearInvalidPendingRecords();
            }
            RFX_OBJ_CLOSE(mNwRatSwitchHandler);
            RFX_OBJ_CREATE_EX(mNwRatSwitchHandler, RpGsmNwRatSwitchHandler, this, (this));
        }
    } else if (appFamType == APP_FAM_3GPP2) {
        // boot up work flow for cdma card.
        if (mNwRatSwitchHandler == NULL) {
            RFX_OBJ_CREATE_EX(mNwRatSwitchHandler, RpCdmaLteNwRatSwitchHandler, this, (this));
            // for sim card hot plug in/out
        } else if (appFamType != mCurAppFamilyType) {
            if (!isRestrictedModeSupport()) {
                clearInvalidPendingRecords();
            }
            RFX_OBJ_CLOSE(mNwRatSwitchHandler);
            RFX_OBJ_CREATE_EX(mNwRatSwitchHandler, RpCdmaLteNwRatSwitchHandler, this, (this));
        }
    }
    logD(RAT_CTRL_TAG, "creatRatSwitchHandlerIfNeeded(), mNwRatSwitchHandler is %p. ",
            mNwRatSwitchHandler);
}

AppFamilyType RpNwRatController::getAppFamilyType() {
    logD(RAT_CTRL_TAG,"getAppFamilyType(), mCurAppFamilyType is %d. ",mCurAppFamilyType);
    return mCurAppFamilyType;
}

int RpNwRatController::getPreferredNetworkType() {
    logD(RAT_CTRL_TAG, "getPreferredNetworkType(), mCurPreferedNetWorkType is %d.",
            mCurPreferedNetWorkType);
    return mCurPreferedNetWorkType;
}

void RpNwRatController::setNwsMode(const NwsMode nwsMode, const sp<RfxAction>& action) {
    logD(RAT_CTRL_TAG, "setNwsMode(), nwsMode is %d, mNewNwsMode is %d.", nwsMode, mNewNwsMode);
    if (getChipTestMode() == 1) {
        logD(RAT_CTRL_TAG, "ChipTest! setNwsMode not executed! action->act()");
        if (action != NULL) {
            action->act();
        }
        return;
    }
    if (getEnginenerMode() == ENGINEER_MODE_AUTO) {
        AppFamilyType appFamilyType = mNewAppFamilyType;
        if (appFamilyType == APP_FAM_3GPP2 && nwsMode != mNewNwsMode) {
            int prefNwType = mNewPreferedNetWorkType;
            VolteState volteState = mNewVolteState;

            if (mPendingNormalRatSwitchRecord.prefNwType != -1) {
                prefNwType = mPendingNormalRatSwitchRecord.prefNwType;
                volteState = mPendingNormalRatSwitchRecord.volteState;
            }

            // [VoLTE] If VoLTE on, need recalculate nw type to switch LWG/LTE Only
            if (volteState == VOLTE_ON) {
                prefNwType = calculateVolteNetworkType(appFamilyType, nwsMode, volteState);
            }

            switchNwRat(appFamilyType, prefNwType, nwsMode, volteState, RAT_SWITCH_NWS, action,
                    NULL);
        }
    }
}

NwsMode RpNwRatController::getNwsMode() {
    logD(RAT_CTRL_TAG,"getNwsMode(), mCurNwsMode is %d. ",mCurNwsMode);
    return mCurNwsMode;
}

VolteState RpNwRatController::getVolteState() {
    logD(RAT_CTRL_TAG, "getVolteState(), mCurVolteState is %d. ", mCurVolteState);
    return mCurVolteState;
}

void RpNwRatController::onInit() {
    // Required: invoke super class implementation
    RfxController::onInit();

    logD(RAT_CTRL_TAG,"onInit");

    // define and register request & urc id list
    const int request_id_list[] = {
        RIL_REQUEST_SET_PREFERRED_NETWORK_TYPE,
        RIL_REQUEST_GET_PREFERRED_NETWORK_TYPE,
        RIL_REQUEST_CONFIG_EVDO_MODE,
        RIL_REQUEST_VOICE_RADIO_TECH,
        RIL_REQUEST_SET_SVLTE_RAT_MODE};
    const int atci_request_id_list[] = {
        RIL_LOCAL_REQUEST_OEM_HOOK_ATCI_INTERNAL};
    const int urc_id_list[] = { };
    registerToHandleRequest(request_id_list, sizeof(request_id_list)/sizeof(int));
    if (getChipTestMode() != 0) {
        registerToHandleRequest(atci_request_id_list,
                sizeof(atci_request_id_list)/sizeof(int), HIGHEST);
    }
    registerToHandleUrc(urc_id_list, 0);

    getStatusManager()->setIntValue(RFX_STATUS_KEY_NWS_MODE, mCurNwsMode);
    getStatusManager()->setIntValue(RFX_STATUS_KEY_PREFERRED_NW_TYPE, mCurPreferedNetWorkType);

    if (isCtVolteSupport()) {
        getStatusManager()->registerStatusChanged(RFX_STATUS_KEY_VOLTE_STATE,
                RfxStatusChangeCallback(this, &RpNwRatController::onVolteStateChanged));
        getStatusManager()->registerStatusChanged(RFX_STATUS_KEY_CT_CARD,
                RfxStatusChangeCallback(this, &RpNwRatController::onCTVolteCardTypeChanged));
    }
}

void RpNwRatController::onDeinit() {
    if (isCtVolteSupport()) {
        getStatusManager()->unRegisterStatusChanged(RFX_STATUS_KEY_VOLTE_STATE,
                RfxStatusChangeCallback(this, &RpNwRatController::onVolteStateChanged));
        getStatusManager()->unRegisterStatusChanged(RFX_STATUS_KEY_CT_CARD,
                RfxStatusChangeCallback(this, &RpNwRatController::onCTVolteCardTypeChanged));
    }

    RfxController::onDeinit();
}

void RpNwRatController::doNwSwitchForEngMode(const sp<RfxAction>& action) {
    logD(RAT_CTRL_TAG, "Radio Avaliable, have get the app family type ");
    if (mNewAppFamilyType == APP_FAM_3GPP2) {
        switch (getEnginenerMode()) {
        case ENGINEER_MODE_CDMA:
            logD(RAT_CTRL_TAG, "Radio Avaliable, CDMA only mode. ");
            switchNwRat(APP_FAM_3GPP2, PREF_NET_TYPE_CDMA_EVDO_AUTO,
                    NWS_MODE_CDMALTE, VOLTE_OFF, RAT_SWITCH_INIT, action, NULL);
            break;
        case ENGINEER_MODE_CSFB:
            logD(RAT_CTRL_TAG, "Radio Avaliable, CSFB only mode. ");
            switchNwRat(APP_FAM_3GPP2, PREF_NET_TYPE_LTE_GSM_WCDMA,
                    NWS_MODE_CSFB, VOLTE_OFF, RAT_SWITCH_INIT, action, NULL);
            break;
        case ENGINEER_MODE_LTE:
            logD(RAT_CTRL_TAG, "Radio Avaliable, LTE only mode. ");
            switchNwRat(APP_FAM_3GPP2, PREF_NET_TYPE_LTE_ONLY, NWS_MODE_CSFB, VOLTE_OFF,
                    RAT_SWITCH_INIT, action, NULL);
            break;
        default:
            logD(RAT_CTRL_TAG, "Radio Avaliable, auto mode, do nothing. ");
            break;
        }
    }
}

bool RpNwRatController::onHandleRequest(const sp<RfxMessage>& message) {
    int msg_id = message->getId();
    logD(RAT_CTRL_TAG,"handle req %s.", requestToString(msg_id));

    //  This is only for restart, setting is earlier than mode controller.
    if (msg_id == RIL_REQUEST_SET_PREFERRED_NETWORK_TYPE
        && mNwRatSwitchHandler == NULL) {
        int32_t stgCount;
        int32_t nwType;
        message->getParcel()->readInt32(&stgCount);
        message->getParcel()->readInt32(&nwType);
        mPreferredNetWorkTypeFromRILJ = nwType;
        logD(RAT_CTRL_TAG, " setting is earlier than mode controller,"
                " send failure response, nwType is %d.", nwType);
        sp<RfxMessage> resToRilj = RfxMessage::obtainResponse(
                RIL_E_GENERIC_FAILURE, message);
        responseToRilj(resToRilj);
    }

    if (mNwRatSwitchHandler != NULL) {
        switch (msg_id) {
        case RIL_REQUEST_SET_PREFERRED_NETWORK_TYPE:
            int32_t stgCount;
            int32_t nwType;
            message->getParcel()->readInt32(&stgCount);
            message->getParcel()->readInt32(&nwType);
            mPreferredNetWorkTypeFromRILJ = nwType;
            setPreferredNetworkType(nwType, message);
            return true;
            
        case RIL_REQUEST_GET_PREFERRED_NETWORK_TYPE:
            logD(RAT_CTRL_TAG, "[handleGetPreferredNwType] mPreferredNetworkTypeFromRILJ:%d",
                mPreferredNetWorkTypeFromRILJ);
            switch (mPreferredNetWorkTypeFromRILJ) {
                case PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA:
                case PREF_NET_TYPE_GSM_WCDMA_CDMA_EVDO_AUTO:
                case PREF_NET_TYPE_LTE_CDMA_EVDO: {
                    sp<RfxMessage> resToRilj = RfxMessage::obtainResponse(RIL_E_SUCCESS, message);
                    resToRilj->getParcel()->writeInt32(1);
                    resToRilj->getParcel()->writeInt32(mPreferredNetWorkTypeFromRILJ);
                    responseToRilj(resToRilj);
                    return true;
                 }
                 default:
                     break;
            }
            mNwRatSwitchHandler->requestGetPreferredNetworkType(message);
            return true;

        case RIL_REQUEST_VOICE_RADIO_TECH:
            logD(RAT_CTRL_TAG, "request voice radio tech, mNewNwsMode = %d.", mNewNwsMode);
            if (mNewNwsMode == NWS_MODE_CSFB || mNewNwsMode == NWS_MODE_LTEONLY) {
                mNwRatSwitchHandler->requestGetVoiceRadioTech(message);
            } else {
                sp<RfxMessage> resToRilj = RfxMessage::obtainResponse(
                        RIL_E_SUCCESS, message);
                resToRilj->getParcel()->writeInt32(1);
                resToRilj->getParcel()->writeInt32(RADIO_TECH_1xRTT);
                logD(RAT_CTRL_TAG, "request voice radio tech, send response.voiceRadioTech = 6.");
                responseToRilj(resToRilj);
            }
            return true;
        }
    }
    return false;
}

bool RpNwRatController::onHandleAtciRequest(const sp<RfxMessage>& message) {
    int msg_id = message->getId();
    int targetSlotId = 0;
    char simNo[PROPERTY_VALUE_MAX] = {0};

    switch (msg_id) {
        case RIL_LOCAL_REQUEST_OEM_HOOK_ATCI_INTERNAL: {
            sp<RfxMessage> request
                    = RfxMessage::obtainRequest(RADIO_TECH_GROUP_GSM, msg_id, message, true);
            Parcel *p = request->getParcel();
            p->setDataPosition(0);
            int len;
            char *data;

            p->readInt32(&len);
            logD(RAT_CTRL_TAG, "len: %d", len);

            if (len > 0) {
                data = (char*) p->readInplace(len);
                logD(RAT_CTRL_TAG, "data: %s", data);
                if (strncmp(data, "AT+ERAT=", 8) == 0) {
                    int rat = -1;
                    property_get("persist.service.atci.sim", simNo, "0");
                    logD(RAT_CTRL_TAG, "[onHandleAtciRequest] simNo: %d.", simNo[0]);

                    if (simNo[0] == '0') {
                        targetSlotId = 0;
                    } else if (simNo[0] == '1') {
                        targetSlotId = 1;
                    } else {
                        logD(RAT_CTRL_TAG, "Not support slot: %d.", simNo[0]);
                        return true;
                    }

                    if (targetSlotId == m_slot_id) {
                        sscanf(data, "AT+ERAT=%d", &rat);
                        switch (rat) {
                            case 0:
                                mCurAppFamilyType = APP_FAM_3GPP;
                                mCurNwsMode = NWS_MODE_CSFB;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_GSM_ONLY;
                                break;
                            case 1:
                                mCurAppFamilyType = APP_FAM_3GPP;
                                mCurNwsMode = NWS_MODE_CSFB;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_WCDMA;
                                break;
                            case 2:
                                mCurAppFamilyType = APP_FAM_3GPP;
                                mCurNwsMode = NWS_MODE_CSFB;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_GSM_WCDMA;
                                break;
                            case 3:
                                mCurAppFamilyType = APP_FAM_3GPP;
                                mCurNwsMode = NWS_MODE_CSFB;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_LTE_ONLY;
                                break;
                            case 4:
                                mCurAppFamilyType = APP_FAM_3GPP;
                                mCurNwsMode = NWS_MODE_CSFB;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_LTE_GSM;
                                break;
                            case 5:
                                mCurAppFamilyType = APP_FAM_3GPP;
                                mCurNwsMode = NWS_MODE_CSFB;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_LTE_WCDMA;
                                break;
                            case 6:
                                mCurAppFamilyType = APP_FAM_3GPP;
                                mCurNwsMode = NWS_MODE_CSFB;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_LTE_GSM_WCDMA;
                                break;
                            case 7:
                                mCurAppFamilyType = APP_FAM_3GPP2;
                                mCurNwsMode = NWS_MODE_CDMALTE;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_CDMA_EVDO_AUTO;
                                break;
                            case 11:
                                mCurAppFamilyType = APP_FAM_3GPP2;
                                mCurNwsMode = NWS_MODE_CDMALTE;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_LTE_CDMA_EVDO;
                                break;
                            case 14:
                                mCurAppFamilyType = APP_FAM_3GPP2;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA;
                                break;
                            default:
                                break;
                        }
                    }
                }
            } else {
                logD(RAT_CTRL_TAG, "[onHandleAtciRequest] len=0");
                break;
            }
        }
        break;
        default:
        break;
    }
    return false;
}

bool RpNwRatController::onHandleResponse(const sp<RfxMessage>& message) {
    int msg_id = message->getId();
    logD(RAT_CTRL_TAG,"handle %s response.", requestToString(msg_id));

    if (mNwRatSwitchHandler != NULL) {
        switch (msg_id) {
        case RIL_REQUEST_SET_PREFERRED_NETWORK_TYPE:
            if (mNewAppFamilyType == APP_FAM_3GPP) {
                mNwRatSwitchHandler->responseSetPreferredNetworkType(message);
            }
            return true;
        case RIL_REQUEST_GET_PREFERRED_NETWORK_TYPE:
            mNwRatSwitchHandler->responseGetPreferredNetworkType(message);
            return true;
        case RIL_REQUEST_CONFIG_EVDO_MODE:
            mNwRatSwitchHandler->responseSetEvdoMode(message);
            return true;
        case RIL_REQUEST_SET_SVLTE_RAT_MODE: {
            if (mNwRatSwitchHandler->isCdma3gDualModeCard()) {
                logD(RAT_CTRL_TAG, "3gCdmaSim handle RIL_REQUEST_SET_SVLTE_RAT_MODE,"
                        " error is %d", message->getError());
                sp<RfxMessage> msg = sp < RfxMessage > (NULL);
                ResponseStatus responseStatus = preprocessResponse(message, msg,
                        RfxWaitResponseTimedOutCallback(mNwRatSwitchHandler,
                                &RpBaseNwRatSwitchHandler::onResponseTimeOut),
                        s2ns(10));
                if (message->getError() != RIL_E_SUCCESS
                        || responseStatus == RESPONSE_STATUS_HAVE_MATCHED) {
                    mNwRatSwitchHandler->responseSetRatMode(message);
                }
            } else {
                mNwRatSwitchHandler->responseSetRatMode(message);
            }
            return true;
        }
        case RIL_REQUEST_VOICE_RADIO_TECH:
            mNwRatSwitchHandler->responseGetVoiceRadioTech(message);
            return true;
        }
    }
    return false;
}

bool RpNwRatController::onHandleUrc(const sp<RfxMessage>& message) {
    int msg_id = message->getId();
    logD(RAT_CTRL_TAG,"handle urc %s.", urcToString(msg_id));

    switch (msg_id) {
    default:
        break;
    }
    return true;
}

char* RpNwRatController::requestToString(int reqId) {
    switch (reqId) {
    case RIL_REQUEST_SET_PREFERRED_NETWORK_TYPE:
        return "RIL_REQUEST_SET_PREFERRED_NETWORK_TYPE";
    case RIL_REQUEST_GET_PREFERRED_NETWORK_TYPE:
        return "RIL_REQUEST_GET_PREFERRED_NETWORK_TYPE";
    case RIL_REQUEST_CONFIG_EVDO_MODE:
        return "RIL_REQUEST_CONFIG_EVDO_MODE";
    case RIL_REQUEST_SET_SVLTE_RAT_MODE:
        return "RIL_REQUEST_SET_SVLTE_RAT_MODE";
    case RIL_REQUEST_VOICE_RADIO_TECH:
        return "RIL_REQUEST_VOICE_RADIO_TECH";
    case RIL_LOCAL_REQUEST_OEM_HOOK_ATCI_INTERNAL:
        return "RIL_LOCAL_REQUEST_OEM_HOOK_ATCI_INTERNAL";
    default:
        logD(RAT_CTRL_TAG,"<UNKNOWN_REQUEST>");
        break;
    }
    return "";
}

char* RpNwRatController::urcToString(int reqId) {
    switch (reqId) {
    default:
        logD(RAT_CTRL_TAG,"<UNKNOWN_URC>");
        break;
    }
    return "";
}

void RpNwRatController::registerRatSwitchCallback(IRpNwRatSwitchCallback* callback) {
    mRpNwRatSwitchListener = callback;
}

void RpNwRatController::unRegisterRatSwitchCallback(IRpNwRatSwitchCallback* callback) {
    mRpNwRatSwitchListener = NULL;
}

void RpNwRatController::onRatSwitchStart(const int prefNwType, const NwsMode newNwsMode,
        const VolteState volteState) {
    if (mRpNwRatSwitchListener != NULL) {
        mRpNwRatSwitchListener->onRatSwitchStart(mCurPreferedNetWorkType,
                prefNwType, mCurNwsMode, newNwsMode, mCurVolteState, volteState);
    }
}
void RpNwRatController::onRatSwitchDone(const int prefNwType){
    if (mRpNwRatSwitchListener != NULL) {
        mRpNwRatSwitchListener->onRatSwitchDone(mCurPreferedNetWorkType,
                prefNwType);
    }
}
void RpNwRatController::onEctModeChangeDone(const int prefNwType){
    if (mRpNwRatSwitchListener != NULL) {
        mRpNwRatSwitchListener->onEctModeChangeDone(mCurPreferedNetWorkType,
                prefNwType);
    }
}
void RpNwRatController::switchNwRat(const AppFamilyType appFamType,
        int prefNwType, NwsMode nwsMode, VolteState volteState,
        const RatSwitchCaller ratSwitchCaller,
        const sp<RfxAction>& action, const sp<RfxMessage>& message) {
    logD(RAT_CTRL_TAG, "switchNwRat(), appFamType is %d, prefNwType is %d, nwsMode is %d . "
            "volteState is %d, ratSwitchCaller is %d, sIsInSwitching is %s. ",
            appFamType, prefNwType, nwsMode, volteState, ratSwitchCaller,
            sIsInSwitching ? "true" : "false");

    if (sIsInSwitching) {
        queueRatSwitchRecord(appFamType, prefNwType, nwsMode, volteState, ratSwitchCaller, action, message);
    } else {
        if (ratSwitchCaller != RAT_SWITCH_RESTRICT
                && mPendingRestrictedRatSwitchRecord.prefNwType != -1) {
            logD(RAT_CTRL_TAG, "switchNwRat(), in restricted mode!");
            queueRatSwitchRecord(appFamType, prefNwType, nwsMode, volteState, ratSwitchCaller, action, message);
            return;
        }
        sIsInSwitching = true;

        /* create the rat switch handler if need. */
        creatRatSwitchHandlerIfNeeded(appFamType);
        mCurAppFamilyType = appFamType;

        if (appFamType == APP_FAM_3GPP2) {
            /* Calculate the target preferred network type. */
            int targetPrefNwType = calculateTargetPreferredNwType(appFamType, prefNwType, nwsMode);

            /* Handle the network preferred network switch. */
            if (isValidPreferredNwType(appFamType, targetPrefNwType, nwsMode)) {
                mNewPreferedNetWorkType = targetPrefNwType;
                mNewNwsMode = nwsMode;
                mNewVolteState = volteState;
                ModemSettings mdSettings = mNwRatSwitchHandler->calculateModemSettings(
                        targetPrefNwType, appFamType, nwsMode, volteState);
                mNwRatSwitchHandler->doNwRatSwitch(mdSettings, ratSwitchCaller, action, message);
            } else {
                logD(RAT_CTRL_TAG, "switchNwRat(), invalid prefNwType is %d.", prefNwType);
                if (ratSwitchCaller == RAT_SWITCH_NORMAL && message != NULL) {
                    mNwRatSwitchHandler->responseSetPreferredNetworkType(message);
                }

                // Reset state and callback action if the RAT is invalid.
                updateState(mCurNwsMode, mCurPreferedNetWorkType, mCurVolteState);
                if (action != NULL) {
                    action->act();
                }
            }
        } else if (appFamType == APP_FAM_3GPP) {
            ModemSettings mdSettings = mNwRatSwitchHandler->calculateModemSettings(prefNwType,
                    appFamType, nwsMode, volteState);
            if (mdSettings.prefNwType != -1) {
                logD(RAT_CTRL_TAG, "switchNwRat(), set radio capability as 1.");
                RpRadioController* radioController =
                        (RpRadioController *) findController(RFX_OBJ_CLASS_INFO(RpRadioController));
                RpSuggestRadioCapabilityCallback callback =
                        RpSuggestRadioCapabilityCallback(this,
                                &RpNwRatController::onSuggestRadioCapabilityResult);
                radioController->suggestedCapability(
                        getSuggestedRadioCapability(mdSettings), callback);
                mNewPreferedNetWorkType = mdSettings.prefNwType;
                mNewNwsMode = nwsMode;
                mNewVolteState = volteState;
                mNwRatSwitchHandler->doNwRatSwitch(mdSettings, ratSwitchCaller, action, message);
                mNwRatSwitchHandler->updatePhone(mdSettings);
                updateState(mNewNwsMode, mNewPreferedNetWorkType, mNewVolteState);
            } else {
                logD(RAT_CTRL_TAG, "switchNwRat(), invalid prefNwType is %d.", prefNwType);
                if (message != NULL) {
                    sp<RfxMessage> rlt = RfxMessage::obtainResponse(
                            RIL_E_GENERIC_FAILURE, message);
                    responseToRilj(rlt);
                }
                updateState(mCurNwsMode, mCurPreferedNetWorkType, mCurVolteState);
            }
            if (action != NULL) {
                action->act();
            }
            logD(RAT_CTRL_TAG, "switchNwRat(), rat switch done for 3gpp card.");
        }
    }
}

int RpNwRatController::getSuggestedRadioCapability(ModemSettings mdSettings) {
    int suggestedRadio = RIL_CAPABILITY_NONE;
    bool md1Radio = mdSettings.md1Radio;
    bool md3Radio = mdSettings.md3Radio;
    if (md1Radio && md3Radio) {
        suggestedRadio = RIL_CAPABILITY_CDMA_ON_LTE;
    } else if (md1Radio && !md3Radio) {
        suggestedRadio = RIL_CAPABILITY_GSM_ONLY;
    } else if (!md1Radio && md3Radio) {
        suggestedRadio = RIL_CAPABILITY_CDMA_ONLY;
    }
    logD(RAT_CTRL_TAG, "suggestedRadio:%d", suggestedRadio);
    return suggestedRadio;
}

void RpNwRatController::queueRatSwitchRecord(const AppFamilyType appFamType,
        int prefNwType, NwsMode nwsMode, VolteState volteState,
        const RatSwitchCaller ratSwitchCaller,
        const sp<RfxAction>& action, const sp<RfxMessage>& message) {
    /* Pending if in switching. */
    logD(RAT_CTRL_TAG, "queueRatSwitchRecord(), ratSwitchCaller:%d prefNwType:%d",
            ratSwitchCaller, prefNwType);
    if (ratSwitchCaller == RAT_SWITCH_RESTRICT) {
        mPendingRestrictedRatSwitchRecord.appFamType = appFamType;
        mPendingRestrictedRatSwitchRecord.prefNwType = prefNwType;
        mPendingRestrictedRatSwitchRecord.nwsMode = nwsMode;
        mPendingRestrictedRatSwitchRecord.volteState = volteState;
        mPendingRestrictedRatSwitchRecord.ratSwitchCaller = ratSwitchCaller;
        mPendingRestrictedRatSwitchRecord.action = action;
        mPendingRestrictedRatSwitchRecord.message = message;
    } else if (ratSwitchCaller == RAT_SWITCH_INIT) {
        mPendingInitRatSwitchRecord.appFamType = appFamType;
        mPendingInitRatSwitchRecord.prefNwType = prefNwType;
        mPendingInitRatSwitchRecord.nwsMode = nwsMode;
        mPendingInitRatSwitchRecord.volteState = volteState;
        mPendingInitRatSwitchRecord.ratSwitchCaller = ratSwitchCaller;
        mPendingInitRatSwitchRecord.action = action;
        mPendingInitRatSwitchRecord.message = message;
    } else {
        if (mPendingNormalRatSwitchRecord.prefNwType != -1
                && mPendingNormalRatSwitchRecord.message != NULL) {
            logD(RAT_CTRL_TAG, "switchNwRat(), request set prefer network type is pending, "
                    "will be ignored, send response.");
            mNwRatSwitchHandler->responseSetPreferredNetworkType(
                    mPendingNormalRatSwitchRecord.message);
        }
        mPendingNormalRatSwitchRecord.appFamType = appFamType;
        mPendingNormalRatSwitchRecord.prefNwType = prefNwType;
        mPendingNormalRatSwitchRecord.nwsMode = nwsMode;
        mPendingNormalRatSwitchRecord.volteState = volteState;
        mPendingNormalRatSwitchRecord.ratSwitchCaller = ratSwitchCaller;
        mPendingNormalRatSwitchRecord.action = action;
        mPendingNormalRatSwitchRecord.message = message;
    }
}

bool RpNwRatController::isValidPreferredNwType(const AppFamilyType appFamType,
        int prefNwType, NwsMode nwsMode) {
    bool isValidPreferredNwType = true;
    if (appFamType == APP_FAM_3GPP2) {
        if (nwsMode == NWS_MODE_CDMALTE) {
            switch (prefNwType) {
                case PREF_NET_TYPE_CDMA_EVDO_AUTO:
                case PREF_NET_TYPE_CDMA_ONLY:
                case PREF_NET_TYPE_EVDO_ONLY:
                case PREF_NET_TYPE_LTE_CDMA_EVDO:
                case PREF_NET_TYPE_LTE_ONLY:
                case PREF_NET_TYPE_LTE_TDD_ONLY:
                    isValidPreferredNwType = true;
                    break;

                case PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA:
                    if (isRestrictedModeSupport()) {
                        isValidPreferredNwType = true;
                        break;
                    }
                case PREF_NET_TYPE_GSM_WCDMA_CDMA_EVDO_AUTO:
                case PREF_NET_TYPE_GSM_WCDMA:
                case PREF_NET_TYPE_GSM_ONLY:
                case PREF_NET_TYPE_WCDMA:
                case PREF_NET_TYPE_GSM_WCDMA_AUTO:
                case PREF_NET_TYPE_LTE_GSM_WCDMA:
                case PREF_NET_TYPE_LTE_WCDMA:
                case PREF_NET_TYPE_LTE_GSM:
                    isValidPreferredNwType = false;
                    break;
            }
        }

        if (nwsMode == NWS_MODE_CSFB) {
            switch (prefNwType) {
            case PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA:
                if (isRestrictedModeSupport()) {
                    isValidPreferredNwType = true;
                    break;
                }
            case PREF_NET_TYPE_CDMA_EVDO_AUTO:
            case PREF_NET_TYPE_CDMA_ONLY:
            case PREF_NET_TYPE_EVDO_ONLY:
            case PREF_NET_TYPE_GSM_WCDMA_CDMA_EVDO_AUTO:
            case PREF_NET_TYPE_LTE_CDMA_EVDO:
                isValidPreferredNwType = false;
                break;

            case PREF_NET_TYPE_GSM_WCDMA:
            case PREF_NET_TYPE_GSM_ONLY:
            case PREF_NET_TYPE_WCDMA:
            case PREF_NET_TYPE_GSM_WCDMA_AUTO:
            case PREF_NET_TYPE_LTE_GSM_WCDMA:
            case PREF_NET_TYPE_LTE_ONLY:
            case PREF_NET_TYPE_LTE_TDD_ONLY:
            case PREF_NET_TYPE_LTE_WCDMA:
            case PREF_NET_TYPE_LTE_GSM:
                isValidPreferredNwType = true;
                break;
            }
        }

        if (nwsMode == NWS_MODE_LTEONLY) {
            if (prefNwType == PREF_NET_TYPE_LTE_ONLY) {
                isValidPreferredNwType = true;
            } else {
                isValidPreferredNwType = false;
            }
        }
    }
    return isValidPreferredNwType;
}

void RpNwRatController::doPendingRatSwitchRecord() {
    // Handle the pending item
    if (mPendingRestrictedRatSwitchRecord.prefNwType != -1) {
        logD(RAT_CTRL_TAG, "doPendingRestrictedRatSwitchRecord(), appFamType is %d, "
                "prefNwType is %d, nwsMode is %d, volteState is %d. ratSwitchCaller is %d",
                mPendingRestrictedRatSwitchRecord.appFamType,
                mPendingRestrictedRatSwitchRecord.prefNwType,
                mPendingRestrictedRatSwitchRecord.nwsMode,
                mPendingRestrictedRatSwitchRecord.volteState,
                mPendingRestrictedRatSwitchRecord.ratSwitchCaller);
        logD(RAT_CTRL_TAG, "doPendingRestrictedRatSwitchRecord(), "
                "mCurPreferedNetWorkType is %d, mCurNwsMode is %d.",
                mCurPreferedNetWorkType, mCurNwsMode);
        if (mCurPreferedNetWorkType == mPendingRestrictedRatSwitchRecord.prefNwType) {
            logD(RAT_CTRL_TAG, "doPendingRestrictedRatSwitchRecord(), "
                    "in restricted mode: prefNwType=%d, nwsMode=%d",
                    mCurPreferedNetWorkType, mCurNwsMode);
        } else {
            switchNwRat(mPendingRestrictedRatSwitchRecord.appFamType,
                    mPendingRestrictedRatSwitchRecord.prefNwType,
                    mPendingRestrictedRatSwitchRecord.nwsMode,
                    mPendingRestrictedRatSwitchRecord.volteState,
                    mPendingRestrictedRatSwitchRecord.ratSwitchCaller,
                    mPendingRestrictedRatSwitchRecord.action,
                    mPendingRestrictedRatSwitchRecord.message);
        }
    } else if (mPendingInitRatSwitchRecord.prefNwType != -1) {
        logD(RAT_CTRL_TAG, "doPendingInitRatSwitchRecord(), appFamType is %d, "
                "prefNwType is %d, nwsMode is %d, volteState is %d. ratSwitchCaller is %d",
                mPendingInitRatSwitchRecord.appFamType,
                mPendingInitRatSwitchRecord.prefNwType,
                mPendingInitRatSwitchRecord.nwsMode,
                mPendingInitRatSwitchRecord.volteState,
                mPendingInitRatSwitchRecord.ratSwitchCaller);
        PendingRatSwitchRecord tempInitRatSwitchRecord;
        tempInitRatSwitchRecord.appFamType = mPendingInitRatSwitchRecord.appFamType;
        tempInitRatSwitchRecord.prefNwType = mPendingInitRatSwitchRecord.prefNwType;
        tempInitRatSwitchRecord.nwsMode = mPendingInitRatSwitchRecord.nwsMode;
        tempInitRatSwitchRecord.volteState = mPendingInitRatSwitchRecord.volteState;
        tempInitRatSwitchRecord.ratSwitchCaller = mPendingInitRatSwitchRecord.ratSwitchCaller;
        tempInitRatSwitchRecord.action = mPendingInitRatSwitchRecord.action;
        tempInitRatSwitchRecord.message = mPendingInitRatSwitchRecord.message;
        mPendingInitRatSwitchRecord.prefNwType = -1;
        switchNwRat(tempInitRatSwitchRecord.appFamType,
                tempInitRatSwitchRecord.prefNwType,
                tempInitRatSwitchRecord.nwsMode,
                tempInitRatSwitchRecord.volteState,
                tempInitRatSwitchRecord.ratSwitchCaller,
                tempInitRatSwitchRecord.action,
                tempInitRatSwitchRecord.message);

    } else if (mPendingNormalRatSwitchRecord.prefNwType != -1) {
        logD(RAT_CTRL_TAG, "doPendingNormalRatSwitchRecord(), appFamType is %d, "
                "prefNwType is %d, nwsMode is %d, volteState is %d. ratSwitchCaller is %d",
                mPendingNormalRatSwitchRecord.appFamType,
                mPendingNormalRatSwitchRecord.prefNwType,
                mPendingNormalRatSwitchRecord.nwsMode,
                mPendingNormalRatSwitchRecord.volteState,
                mPendingNormalRatSwitchRecord.ratSwitchCaller);
        PendingRatSwitchRecord tempNormalRatSwitchRecord;
        tempNormalRatSwitchRecord.appFamType = mPendingNormalRatSwitchRecord.appFamType;
        tempNormalRatSwitchRecord.prefNwType = mPendingNormalRatSwitchRecord.prefNwType;
        tempNormalRatSwitchRecord.nwsMode = mPendingNormalRatSwitchRecord.nwsMode;
        tempNormalRatSwitchRecord.volteState = mPendingNormalRatSwitchRecord.volteState;
        tempNormalRatSwitchRecord.ratSwitchCaller = mPendingNormalRatSwitchRecord.ratSwitchCaller;
        tempNormalRatSwitchRecord.action = mPendingNormalRatSwitchRecord.action;
        tempNormalRatSwitchRecord.message = mPendingNormalRatSwitchRecord.message;
        mPendingNormalRatSwitchRecord.prefNwType = -1;
        switchNwRat(tempNormalRatSwitchRecord.appFamType,
                tempNormalRatSwitchRecord.prefNwType,
                tempNormalRatSwitchRecord.nwsMode,
                tempNormalRatSwitchRecord.volteState,
                tempNormalRatSwitchRecord.ratSwitchCaller,
                tempNormalRatSwitchRecord.action,
                tempNormalRatSwitchRecord.message);

    } else {
        RpNwRatController *another = (RpNwRatController *) findController(
                getSlotId() == 0 ? 1 : 0,
                RFX_OBJ_CLASS_INFO(RpNwRatController));
        if (another != NULL && another->hasPendingRecord()) {
            logD(RAT_CTRL_TAG, "doPendingRatSwitchRecord, another SIM has pending record, "
                    "current is %d", getSlotId());
            another->doPendingRatSwitchRecord();
        }

        logD(RAT_CTRL_TAG, "doPendingRatSwitchRecord(), no pending record, "
                "another sim has no pending record also, finish");
    }
}

bool RpNwRatController::hasPendingRecord() {
    if (mPendingInitRatSwitchRecord.prefNwType != -1
            || mPendingNormalRatSwitchRecord.prefNwType != -1
            || mPendingRestrictedRatSwitchRecord.prefNwType != -1) {
        return true;
    }
    return false;
}

int RpNwRatController::calculateTargetPreferredNwType(const AppFamilyType appFamType, int prefNwType, NwsMode nwsMode) {
    int targetPrefNwType = -1;
    if (nwsMode == NWS_MODE_CDMALTE) {
        if (appFamType == APP_FAM_3GPP) {
            targetPrefNwType = prefNwType;
        } else if (appFamType == APP_FAM_3GPP2) {
            switch (prefNwType) {
            case PREF_NET_TYPE_CDMA_EVDO_AUTO:
            case PREF_NET_TYPE_CDMA_ONLY:
            case PREF_NET_TYPE_EVDO_ONLY:
            case PREF_NET_TYPE_LTE_ONLY:
            case PREF_NET_TYPE_LTE_TDD_ONLY:
                targetPrefNwType = prefNwType;
                break;
            case PREF_NET_TYPE_GSM_WCDMA_CDMA_EVDO_AUTO:
            case PREF_NET_TYPE_GSM_WCDMA:
            case PREF_NET_TYPE_GSM_ONLY:
            case PREF_NET_TYPE_WCDMA:
            case PREF_NET_TYPE_GSM_WCDMA_AUTO:
                targetPrefNwType = PREF_NET_TYPE_CDMA_EVDO_AUTO;
                break;
            case PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA:
                if (isRestrictedModeSupport()) {
                    targetPrefNwType = prefNwType;
                    break;
                }
            case PREF_NET_TYPE_LTE_CDMA_EVDO:
            case PREF_NET_TYPE_LTE_GSM_WCDMA:
            case PREF_NET_TYPE_LTE_WCDMA:
            case PREF_NET_TYPE_LTE_GSM:
                targetPrefNwType = PREF_NET_TYPE_LTE_CDMA_EVDO;
                break;
            }
        }
    }

    if (nwsMode == NWS_MODE_CSFB) {
        switch (prefNwType) {
        case PREF_NET_TYPE_CDMA_EVDO_AUTO:
        case PREF_NET_TYPE_CDMA_ONLY:
        case PREF_NET_TYPE_EVDO_ONLY:
        case PREF_NET_TYPE_GSM_WCDMA_CDMA_EVDO_AUTO:
            targetPrefNwType = PREF_NET_TYPE_GSM_WCDMA;
            break;
        case PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA:
            if (isRestrictedModeSupport()) {
                targetPrefNwType = prefNwType;
                break;
            }
        case PREF_NET_TYPE_LTE_CDMA_EVDO:
            targetPrefNwType = PREF_NET_TYPE_LTE_GSM_WCDMA;
            break;
        case PREF_NET_TYPE_GSM_WCDMA:
        case PREF_NET_TYPE_GSM_ONLY:
        case PREF_NET_TYPE_WCDMA:
        case PREF_NET_TYPE_GSM_WCDMA_AUTO:
        case PREF_NET_TYPE_LTE_GSM_WCDMA:
        case PREF_NET_TYPE_LTE_ONLY:
        case PREF_NET_TYPE_LTE_TDD_ONLY:
        case PREF_NET_TYPE_LTE_WCDMA:
        case PREF_NET_TYPE_LTE_GSM:
            targetPrefNwType = prefNwType;
            break;
        }
    }

    if (nwsMode == NWS_MODE_LTEONLY) {
        if (appFamType == APP_FAM_3GPP) {
            targetPrefNwType = prefNwType;
        } else if (appFamType == APP_FAM_3GPP2) {
            switch (prefNwType) {
            case PREF_NET_TYPE_LTE_CDMA_EVDO:
            case PREF_NET_TYPE_LTE_ONLY:
            case PREF_NET_TYPE_LTE_TDD_ONLY:
            case PREF_NET_TYPE_LTE_GSM_WCDMA:
            case PREF_NET_TYPE_LTE_WCDMA:
            case PREF_NET_TYPE_LTE_GSM:
                targetPrefNwType = PREF_NET_TYPE_LTE_ONLY;
                break;
            case PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA:
                if (isRestrictedModeSupport()) {
                    targetPrefNwType = prefNwType;
                } else {
                    targetPrefNwType = PREF_NET_TYPE_LTE_ONLY;
                }
                break;
            case PREF_NET_TYPE_CDMA_EVDO_AUTO:
            case PREF_NET_TYPE_CDMA_ONLY:
            case PREF_NET_TYPE_EVDO_ONLY:
            case PREF_NET_TYPE_GSM_WCDMA_CDMA_EVDO_AUTO:
            case PREF_NET_TYPE_GSM_WCDMA:
            case PREF_NET_TYPE_GSM_ONLY:
            case PREF_NET_TYPE_WCDMA:
            case PREF_NET_TYPE_GSM_WCDMA_AUTO:
                // ignore this prefNwType
                break;
            }
        }
    }

    logD(RAT_CTRL_TAG, "calculateTargetPreferredNwType, prefNwType=%d, targetPrefNwType=%d",
            prefNwType, targetPrefNwType);
    return targetPrefNwType;
}

void RpNwRatController::updateState(NwsMode nwsMode, int prefNwType, VolteState volteState) {
    logD(RAT_CTRL_TAG,"updateNwsMode(), nwsMode is %d, prefNwType is %d. ", nwsMode, prefNwType);
    mCurNwsMode = nwsMode;
    mCurPreferedNetWorkType = prefNwType;
    mCurVolteState = volteState;
    getStatusManager()->setIntValue(RFX_STATUS_KEY_NWS_MODE, mCurNwsMode);
    getStatusManager()->setIntValue(RFX_STATUS_KEY_PREFERRED_NW_TYPE, mCurPreferedNetWorkType);
    sIsInSwitching = false;
}

bool RpNwRatController::getSwitchState() {
    return sIsInSwitching;
}

int RpNwRatController::getEnginenerMode() {
    char property_value[PROPERTY_VALUE_MAX] = { 0 };
    property_get("persist.radio.ct.ir.engmode", property_value, "0");
    int engineerMode = atoi(property_value);
    logD(RAT_CTRL_TAG,"getEnginenerMode(), engineerMode is %d. ",engineerMode);
    return engineerMode;
}

int RpNwRatController::getChipTestMode() {
    int mode = 0;
    char chipsetMode[PROPERTY_VALUE_MAX] = { 0 };
    property_get("persist.chiptest.enable", chipsetMode, "0");
    mode = atoi(chipsetMode);
    logD(RAT_CTRL_TAG,"getChipTestMode():%d", mode);
    return mode;
}

void RpNwRatController::clearSuggetRadioCapability() {
    if (getChipTestMode() != 1) {
        RpRadioController* radioController =
                (RpRadioController *) findController(RFX_OBJ_CLASS_INFO(RpRadioController));
        RpSuggestRadioCapabilityCallback callback = RpSuggestRadioCapabilityCallback(this, &RpNwRatController::onSuggestRadioCapabilityResult);
        radioController->suggestedCapability(RIL_CAPABILITY_NONE, callback);
        logD(RAT_CTRL_TAG, "clearSuggetRadioCapability");
    }
}

void RpNwRatController::clearInvalidPendingRecords() {
    if (mPendingInitRatSwitchRecord.prefNwType != -1) {
        logD(RAT_CTRL_TAG, "has pending init rat switch record: appFamType is %d, "
                "prefNwType is %d, nwsMode is %d . ratSwitchCaller is %d",
                mPendingInitRatSwitchRecord.appFamType,
                mPendingInitRatSwitchRecord.prefNwType,
                mPendingInitRatSwitchRecord.nwsMode,
                mPendingInitRatSwitchRecord.ratSwitchCaller);
        if (mPendingInitRatSwitchRecord.action != NULL) {
            mPendingInitRatSwitchRecord.action->act();
        }
        if (mPendingInitRatSwitchRecord.message != NULL) {
            mNwRatSwitchHandler->responseSetPreferredNetworkType(
                    mPendingInitRatSwitchRecord.message);
        }
        mPendingInitRatSwitchRecord.prefNwType = -1;
    }
    if (mPendingNormalRatSwitchRecord.prefNwType != -1) {
        logD(RAT_CTRL_TAG, "has pending normal rat switch record: appFamType is %d, "
                "prefNwType is %d, nwsMode is %d . ratSwitchCaller is %d",
                mPendingNormalRatSwitchRecord.appFamType,
                mPendingNormalRatSwitchRecord.prefNwType,
                mPendingNormalRatSwitchRecord.nwsMode,
                mPendingNormalRatSwitchRecord.ratSwitchCaller);
        if (mPendingNormalRatSwitchRecord.action != NULL) {
            mPendingNormalRatSwitchRecord.action->act();
        }
        if (mPendingNormalRatSwitchRecord.message != NULL) {
            mNwRatSwitchHandler->responseSetPreferredNetworkType(
                    mPendingNormalRatSwitchRecord.message);
        }
        mPendingNormalRatSwitchRecord.prefNwType = -1;
    }
    logD(RAT_CTRL_TAG, "clearInvalidPendingRecords");
}

bool RpNwRatController::isRestrictedModeSupport() {
    bool mode = false;
    char prop_val[PROPERTY_VALUE_MAX] = { 0 };
    property_get("ro.operator.optr", prop_val, "");
    if (strcmp("OP12", prop_val) == 0) {
        mode = true;
    }
    return mode;
}

void RpNwRatController::onSuggestRadioCapabilityResult(SuggestRadioResult result) {
    logD(RAT_CTRL_TAG, "onSuggestRadioCapabilityResult, result is %d .",
            result);
}

NwsMode RpNwRatController::getNwsModeForSwitchCardType() {
    if (mNwRatSwitchHandler != NULL) {
        logD(RAT_CTRL_TAG, "getNwsModeForSwitchCardType(), NwsMode is %d. ",
                mNwRatSwitchHandler->getNwsModeForSwitchCardType());
        return mNwRatSwitchHandler->getNwsModeForSwitchCardType();
    } else {
        logD(RAT_CTRL_TAG, "getNwsModeForSwitchCardType(), mNwRatSwitchHandler has not "
                "been initialized ");
        return NWS_MODE_CSFB;
    }
}

int RpNwRatController::getCapabilitySlotId() {
    char tempstr[PROPERTY_VALUE_MAX];
    memset(tempstr, 0, sizeof(tempstr));
    property_get("persist.radio.simswitch", tempstr, "1");
    int capabilitySlotId = atoi(tempstr) - 1;
    logD(RAT_CTRL_TAG, "getCapabilitySlotId, capability slot is %d .",capabilitySlotId);
    return capabilitySlotId;
}

bool RpNwRatController::isCtVolteSupport() {
    if (sIsCtVolteSupport == -1) {
        char volte_prop[PROPERTY_VALUE_MAX] = { 0 };
        property_get("persist.mtk_ct_volte_support", volte_prop, "0");

        if (strcmp(volte_prop, "1") == 0) {
            sIsCtVolteSupport = 1;
        } else {
            sIsCtVolteSupport = 0;
        }
    }
    return (sIsCtVolteSupport == 1);
}

bool RpNwRatController::isCtSimCard() {
    bool ret = false;
    int cdmaCardType = getStatusManager()->getIntValue(RFX_STATUS_KEY_CDMA_CARD_TYPE);
    if (cdmaCardType == CT_4G_UICC_CARD || cdmaCardType == CT_UIM_SIM_CARD
            || cdmaCardType == CT_3G_UIM_CARD) {
        ret = true;
    }
    logD(RAT_CTRL_TAG, "isCtSimCard, CdmaCardType=%d, ret = %s", cdmaCardType,
            (ret ? "true" : "false"));
    return ret;
}

bool RpNwRatController::is4GNetworkMode(int prefNwType) {
    bool is4GNetworkMode = true;
    if (prefNwType != -1) {
        switch (prefNwType) {
            case PREF_NET_TYPE_LTE_CDMA_EVDO:
            case PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA:
            case PREF_NET_TYPE_LTE_ONLY:
            case PREF_NET_TYPE_LTE_GSM_WCDMA:
            case PREF_NET_TYPE_LTE_WCDMA:
            case PREF_NET_TYPE_LTE_GSM:
                break;
            default:
                is4GNetworkMode = false;
                break;
        }
    }

    logD(RAT_CTRL_TAG, "is4GNetworkMode=%s, prefNwType=%d, mPreferredNetWorkTypeFromRILJ=%d, "
            "mCurPreferedNetWorkType=%d, mNewPreferedNetWorkType=%d",
            (is4GNetworkMode ? "true" : "false"), prefNwType, mPreferredNetWorkTypeFromRILJ,
            mCurPreferedNetWorkType, mNewPreferedNetWorkType);
    return is4GNetworkMode;
}

int RpNwRatController::getLastPreferredNetworkType() {
    // Get last prefered network type from pending rat switch record
    int prefNwType = mNewPreferedNetWorkType;
    if (mPendingNormalRatSwitchRecord.prefNwType != -1) {
        prefNwType = mPendingNormalRatSwitchRecord.prefNwType;
        logD(RAT_CTRL_TAG, "getLastPrefNwType, Has pending normal: update prefNwType from %d to %d",
                mNewPreferedNetWorkType, prefNwType);
    } else if (mPendingInitRatSwitchRecord.prefNwType != -1) {
        prefNwType = mPendingInitRatSwitchRecord.prefNwType;
        logD(RAT_CTRL_TAG, "getLastPrefNwType, Has pending init: update prefNwType from %d to %d",
                mNewPreferedNetWorkType, prefNwType);
    } else {
        logD(RAT_CTRL_TAG, "getLastPrefNwType, use mNewPreferedNetWorkType=%d",
                mNewPreferedNetWorkType);
    }
    return prefNwType;
}

VolteState RpNwRatController::getLastVolteState() {
    // Get last VolteState from pending rat switch record
    VolteState volteState = mNewVolteState;
    if (mPendingNormalRatSwitchRecord.prefNwType != -1) {
        volteState = mPendingNormalRatSwitchRecord.volteState;
        logD(RAT_CTRL_TAG, "getLastVolteState, Has pending normal: update volteState from %d to %d",
                mNewVolteState, volteState);
    } else if (mPendingInitRatSwitchRecord.prefNwType != -1) {
        volteState = mPendingInitRatSwitchRecord.volteState;
        logD(RAT_CTRL_TAG, "getLastVolteState, Has pending init: update volteState from %d to %d",
                mNewVolteState, volteState);
    } else {
        logD(RAT_CTRL_TAG, "getLastVolteState, use mNewVolteState=%d", mNewVolteState);
    }
    return volteState;
}

NwsMode RpNwRatController::getLastNwsMode() {
    // Get last NwsMode from pending rat switch record
    NwsMode nwsMode = mNewNwsMode;
    if (mPendingNormalRatSwitchRecord.prefNwType != -1) {
        nwsMode = mPendingNormalRatSwitchRecord.nwsMode;
        logD(RAT_CTRL_TAG, "getLastNwsMode, Has pending normal: update nwsMode from %d to %d",
                mNewNwsMode, nwsMode);
    } else if (mPendingInitRatSwitchRecord.prefNwType != -1) {
        nwsMode = mPendingInitRatSwitchRecord.nwsMode;
        logD(RAT_CTRL_TAG, "getLastNwsMode, Has pending init: update nwsMode from %d to %d",
                mNewNwsMode, nwsMode);
    } else {
        logD(RAT_CTRL_TAG, "getLastNwsMode, use mNewNwsMode=%d", mNewNwsMode);
    }
    return nwsMode;
}

NwsMode RpNwRatController::filterNwsMode(const NwsMode nwsMode, const VolteState volteState) {
    NwsMode targetNwsMode = nwsMode;
    if (volteState == VOLTE_OFF && nwsMode == NWS_MODE_LTEONLY) {
        // Turn off VoLTE, switch NwsMode to CDMALTE
        targetNwsMode = NWS_MODE_CDMALTE;
    } else if (volteState == VOLTE_ON && nwsMode == NWS_MODE_CDMALTE) {
        // Turn on VoLTE, switch NwsMode to LTEONLY
        targetNwsMode = NWS_MODE_LTEONLY;
    }
    logD(RAT_CTRL_TAG, "filterNwsMode, volteState=%d, nwsMode=%d, targetNwsMode=%d", volteState,
            nwsMode, targetNwsMode);
    return targetNwsMode;
}

VolteState RpNwRatController::filterVolteState(const int prefNwType, const VolteState volteState,
        const int ratMode) {
    if (!isCtVolteSupport() || getEnginenerMode() != ENGINEER_MODE_AUTO) {
        return VOLTE_OFF;
    }

    // [VoLTE] Get current VolteState from StatusManager
    VolteState newVolteState = volteState;
    int volteValue = getStatusManager()->getIntValue(RFX_STATUS_KEY_VOLTE_STATE, -1);
    if (volteValue == VOLTE_OFF) {
        newVolteState = VOLTE_OFF;
    } else if (volteValue == VOLTE_ON) {
        newVolteState = VOLTE_ON;
    }
    logD(RAT_CTRL_TAG, "filterVolteState, prefNwType=%d, ratMode=%d, volteState=%d, newVolteState=%d",
            prefNwType, ratMode, volteState, newVolteState);

    // [VoLTE] filter VolteState only for newVolteState is VOLTE_ON.
    if (newVolteState != VOLTE_ON) {
        return newVolteState;
    }

    // [VoLTE] For ct 3g dual mode card fix VolteState to VOLTE_OFF
    bool is3gCdmaSim = getStatusManager()->getBoolValue(RFX_STATUS_KEY_CT3G_DUALMODE_CARD, false);
    if (newVolteState == VOLTE_ON && is3gCdmaSim) {
        logD(RAT_CTRL_TAG, "filterVolteState, For CT3G fix VolteState to OFF");
        newVolteState = VOLTE_OFF;
    }

    // [VoLTE] For ECC fix VolteState to VOLTE_OFF
    if (newVolteState == VOLTE_ON
            && (RAT_MODE_CDMA_EVDO_AUTO == ratMode || RAT_MODE_LTE_WCDMA_GSM == ratMode)) {
        logD(RAT_CTRL_TAG, "filterVolteState, For ECC fix VolteState to OFF");
        newVolteState = VOLTE_OFF;
    }

    // [VoLTE] For Not CT Cdma card to VOLTE_OFF
    if (newVolteState == VOLTE_ON && !isCtSimCard()) {
        int ctCardType = getStatusManager()->getIntValue(RFX_STATUS_KEY_CT_CARD,
                RFX_CDMA_CARD_UNKNOWN);
        logD(RAT_CTRL_TAG, "filterVolteState, CTCardType=%d", ctCardType);
        if (ctCardType == RFX_CDMA_CARD_NO) {
            logD(RAT_CTRL_TAG, "filterVolteState, For Not CT Cdma card fix VolteState to OFF");
            newVolteState = VOLTE_OFF;
        }
    }
    // [VoLTE] For not 4G preferred network type fix VolteState to VOLTE_OFF
    if (newVolteState == VOLTE_ON && !is4GNetworkMode(prefNwType)) {
        logD(RAT_CTRL_TAG, "filterVolteState, For not 4G prefNwType fix VolteState to OFF");
        newVolteState = VOLTE_OFF;
    }

    return newVolteState;
}

int RpNwRatController::calculateVolteNetworkType(const AppFamilyType appFamilyType,
        const NwsMode nwsMode, const VolteState volteState) {
    int prefNwType = PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA;
    if (mPreferredNetWorkTypeFromRILJ != -1) {
        // Has users settings
        prefNwType = mPreferredNetWorkTypeFromRILJ;
    } else {
        // Get VoLTE default network type
        if (volteState == VOLTE_ON) {
            if (nwsMode == NWS_MODE_CDMALTE || nwsMode == NWS_MODE_LTEONLY) {
                prefNwType = PREF_NET_TYPE_LTE_ONLY;
            } else if (nwsMode == NWS_MODE_CSFB) {
                prefNwType = PREF_NET_TYPE_LTE_GSM_WCDMA;
            }
        } else if (volteState == VOLTE_OFF) {
            if (nwsMode == NWS_MODE_CDMALTE || nwsMode == NWS_MODE_LTEONLY) {
                prefNwType = PREF_NET_TYPE_LTE_CDMA_EVDO;
            } else if (nwsMode == NWS_MODE_CSFB) {
                prefNwType = PREF_NET_TYPE_LTE_GSM_WCDMA;
            }
        }
    }

    // Get default network type from  mode controller
    RpCdmaLteModeController *modeController = (RpCdmaLteModeController *) findController(-1,
            RFX_OBJ_CLASS_INFO(RpCdmaLteModeController));
    int capability = modeController->getDefaultNetworkType(getSlotId());

    // Filter preferred network type by capability
    int targetPrefNwType = filterPrefNwType(appFamilyType, prefNwType, capability);

    logD(RAT_CTRL_TAG, "calculateVolteNetworkType, to setPreferredNetworkType: "
            "nwsMode=%d, capability=%d, prefNwType=%d, targetPrefNwType=%d", nwsMode, capability,
            prefNwType, targetPrefNwType);
    return targetPrefNwType;
}

void RpNwRatController::onVolteStateChanged(RfxStatusKeyEnum key, RfxVariant old_value,
        RfxVariant value) {
    int oldType = old_value.asInt();
    int newType = value.asInt();
    logD(RAT_CTRL_TAG, "onVolteStateChanged, old: %d, new: %d", oldType, newType);

    if (getChipTestMode() == 1) {
        logD(RAT_CTRL_TAG, "ChipTest! onVolteStateChanged not executed!");
        return;
    }

    if (getEnginenerMode() == ENGINEER_MODE_AUTO) {
        if (oldType == newType) {
            return;
        }

        // Get volteState
        VolteState volteState = mNewVolteState;
        if (newType == VOLTE_OFF) {
            volteState = VOLTE_OFF;
        } else if (newType == VOLTE_ON) {
            volteState = VOLTE_ON;
        } else {
            volteState = VOLTE_OFF;
            logE(RAT_CTRL_TAG, "onVolteStateChanged, UNKNOWN VolteState: %d", newType);
        }
        logD(RAT_CTRL_TAG,
                "onVolteStateChanged, mCurVolteState=%d, mNewVolteState=%d, Change to: %d",
                mCurVolteState, mNewVolteState, volteState);

        AppFamilyType appFamilyType = mNewAppFamilyType;
        if (appFamilyType == APP_FAM_3GPP2) {
            int lastPrefNwType = getLastPreferredNetworkType();
            volteState = filterVolteState(lastPrefNwType, volteState, -1);
            NwsMode lastNwsMode = getLastNwsMode();
            NwsMode nwsMode = filterNwsMode(lastNwsMode, volteState);
            int prefNwType = calculateVolteNetworkType(appFamilyType, nwsMode, volteState);
            switchNwRat(appFamilyType, prefNwType, nwsMode, volteState, RAT_SWITCH_NORMAL, NULL,
                    NULL);
        } else {
            mNewVolteState = volteState;
            mCurVolteState = volteState;
        }
    }
}

void RpNwRatController::onCTVolteCardTypeChanged(RfxStatusKeyEnum key, RfxVariant old_value,
        RfxVariant value) {
    int oldType = old_value.asInt();
    int newType = value.asInt();
    logD(RAT_CTRL_TAG, "onCTVolteCardTypeChanged, old: %d, new: %d", oldType, newType);

    if (getChipTestMode() == 1) {
        logD(RAT_CTRL_TAG, "ChipTest! onCTVolteCardTypeChanged not executed!");
        return;
    }

    if (getEnginenerMode() == ENGINEER_MODE_AUTO) {
        if (oldType == newType || newType != RFX_CDMA_CARD_NO) {
            logD(RAT_CTRL_TAG, "onCTVolteCardTypeChanged ignore");
            return;
        }

        AppFamilyType appFamilyType = mNewAppFamilyType;
        if (appFamilyType == APP_FAM_3GPP2) {
            int volteValue = getStatusManager()->getIntValue(RFX_STATUS_KEY_VOLTE_STATE, -1);
            if (volteValue == VOLTE_ON && getLastVolteState() == VOLTE_ON) {
                logD(RAT_CTRL_TAG, "onCTVolteCardTypeChanged, switch VOLTE_OFF: RFX_CDMA_CARD_NO");
                VolteState volteState = VOLTE_OFF;
                int lastPrefNwType = getLastPreferredNetworkType();
                volteState = filterVolteState(lastPrefNwType, volteState, -1);
                NwsMode lastNwsMode = getLastNwsMode();
                NwsMode nwsMode = filterNwsMode(lastNwsMode, volteState);
                int prefNwType = calculateVolteNetworkType(appFamilyType, nwsMode, volteState);
                switchNwRat(appFamilyType, prefNwType, nwsMode, volteState, RAT_SWITCH_NORMAL, NULL,
                        NULL);
            }
        }
    }
}
