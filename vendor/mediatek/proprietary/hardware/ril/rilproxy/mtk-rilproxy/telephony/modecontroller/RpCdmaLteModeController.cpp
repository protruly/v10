/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/***************************************************************************** 
 * Include
 *****************************************************************************/

#include <assert.h>
#include <cutils/properties.h>
#include <stdlib.h>
#include <string.h>
#include "at_tok.h"
#include "nw/RpNwRatController.h"
#include "power/RpModemController.h"
#include "RfxAction.h"
#include "RfxLog.h"
#include "RpCardModeControllerFactory.h"
#include "RpCardTypeReadyController.h"
#include "RpCdmaLteModeController.h"
#include "utils/String8.h"
#include "power/RadioConstants.h"
#include "RfxTimer.h"
#include "sim/RpSimController.h"
#include "RfxRootController.h"
#include "RfxSocketStateManager.h"
#include "power/RpRadioController.h"
#include "RpModeInitController.h"

using ::android::String8;

/*****************************************************************************
 * Class RpCdmaLteModeController
 *****************************************************************************/

RFX_IMPLEMENT_CLASS("RpCdmaLteModeController", RpCdmaLteModeController, RfxController);

int RpCdmaLteModeController::sCdmaSocketSlotId = getCdmaSocketSlotId();

RpCdmaLteModeController::RpCdmaLteModeController() :
    mInSwitching(false),
    mIsSwitchingCardType(false),
    m_pending_card_types(NULL),
    m_pending_card_states(NULL),
    mCloseRadioCount(0) {

    m_card_types = new int[RFX_SLOT_COUNT];
    m_card_states = new int[RFX_SLOT_COUNT];
    mSlotBeSwitchCardType = new int[RFX_SLOT_COUNT];
    m_card_modes = new int[RFX_SLOT_COUNT];
    m_old_card_modes = new int[RFX_SLOT_COUNT];
    m_rat_mode = new int[RFX_SLOT_COUNT];

    mCdmaLteModeSlotId = getActiveCdmaLteModeSlotId();
    for (int slotId = SIM_ID_1; slotId < RFX_SLOT_COUNT; slotId++) {
        m_card_types[slotId] = CARD_TYPE_INVALID;
        m_card_states[slotId] = CARD_STATE_INVALID;
        mSlotBeSwitchCardType[slotId] = -1;
        if (slotId == mCdmaLteModeSlotId) {
            m_card_modes[mCdmaLteModeSlotId] = RADIO_TECH_MODE_CDMALTE;
            m_old_card_modes[mCdmaLteModeSlotId] = RADIO_TECH_MODE_CDMALTE;
        } else {
            m_card_modes[slotId] = RADIO_TECH_MODE_CSFB;
            m_old_card_modes[slotId] = RADIO_TECH_MODE_CSFB;
        }
        m_rat_mode[slotId] = RAT_MODE_INVALID;
    }

    mCardTypeReadyController = RFX_OBJ_GET_INSTANCE(RpCardTypeReadyController);
}

RpCdmaLteModeController::~RpCdmaLteModeController() {
    if (m_card_types != NULL) {
        delete[] m_card_types;
        m_card_types = NULL;
    }
    if (m_card_states != NULL) {
        delete[] m_card_states;
        m_card_states = NULL;
    }
    if (m_card_modes != NULL) {
        delete[] m_card_modes;
        m_card_modes = NULL;
    }
    if (m_old_card_modes != NULL) {
        delete[] m_old_card_modes;
        m_old_card_modes = NULL;
    }
    if (m_pending_card_types != NULL) {
        delete[] m_pending_card_types;
        m_pending_card_types = NULL;
    }
    if (m_pending_card_states != NULL) {
        delete[] m_pending_card_states;
        m_pending_card_states = NULL;
    }
    if (mSlotBeSwitchCardType != NULL) {
        delete[] mSlotBeSwitchCardType;
        mSlotBeSwitchCardType = NULL;
    }
    if (m_rat_mode != NULL) {
        delete[] m_rat_mode;
        m_rat_mode = NULL;
    }
    RFX_OBJ_CLOSE_INSTANCE(RpCardTypeReadyController);
}

void RpCdmaLteModeController::onInit() {
    RfxController::onInit();
    const int request_id_list[] = {
        RIL_REQUEST_ENTER_RESTRICT_MODEM,
        RIL_REQUEST_ENTER_RESTRICT_MODEM_C2K,
        RIL_REQUEST_LEAVE_RESTRICT_MODEM,
        RIL_REQUEST_LEAVE_RESTRICT_MODEM_C2K,
        RILPROXY_LOCAL_REQUEST_DEAL_PENDING_MODE_SWITCH,
        RIL_REQUEST_SWITCH_MODE_FOR_ECC
    };
    for (int index = 0; index < RFX_SLOT_COUNT; index++) {
        registerToHandleRequest(index, request_id_list, sizeof(request_id_list)/sizeof(const int));
    }
}

void RpCdmaLteModeController::onDeinit() {
    mSwitchQueue.clear();

    RfxController::onDeinit();
}

void RpCdmaLteModeController::enterModeSwitch(int* card_types, int* card_state,
        int slotNum) {
    if (!isEnableSwitchMode(card_types, card_state, slotNum)) {
        return;
    }

    if (!prepareForModeSwitch(card_types, card_state, slotNum)) {
        mInSwitching = false;
        return;
    }

    if (!waitC2KSocketConnectDone()) {
        return;
    }

    /// Set Flag for exclusion with SimSwitch & IPO OFF.
    getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_MODEM_OFF_STATE,
               MODEM_OFF_BY_MODE_SWITCH);
    if (sCdmaSocketSlotId != getNewCdmaSocketSlotId()) {
        getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_CSLOT_CHANGED_STATUS,
                CSLOT_WILL_NOT_CHANGED);
    } else {
        getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_CSLOT_CHANGED_STATUS,
                CSLOT_WILL_BE_CHANGED);
    }
    startSwitchMode();

    if (isEmdStatusChanged(card_types, slotNum)) {
        enterRestrictMode(sCdmaSocketSlotId);
        return;
    } else {
        doSwitchMode();
    }
}

void RpCdmaLteModeController::onC2kSocketConnected(int slotId, RfxStatusKeyEnum key,
        RfxVariant old_value, RfxVariant value) {
    RFX_UNUSED(key);
    RfxSocketState newSocketState = value.asSocketState();
    RfxSocketState oldSocketState = old_value.asSocketState();

    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][onC2kSocketConnected] Slot %d, SocketState : %s -> %s",
            slotId, oldSocketState.toString().string(), newSocketState.toString().string());
    if (newSocketState.getSocketState(newSocketState.SOCKET_C2K)
            && !oldSocketState.getSocketState(oldSocketState.SOCKET_C2K)) {
        getStatusManager(slotId)->unRegisterStatusChangedEx(RFX_STATUS_KEY_SOCKET_STATE,
                RfxStatusChangeCallbackEx(this, &RpCdmaLteModeController::onC2kSocketConnected));
        if (mIsSwitchingCardType) {
            mIsSwitchingCardType = false;
            RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][onC2kSocketConnected] ignore mode switch"
                    " for switch card type");
            return;
        }
        enterModeSwitch(m_card_types, m_card_states, RFX_SLOT_COUNT);
    }
}

void RpCdmaLteModeController::onCardTypeReady(int* card_types, int* card_state, int slotNum,
    CardTypeReadyReason ctrr) {
    RFX_UNUSED(ctrr);
    assert(slotNum != RFX_SLOT_COUNT);
    for (int i = 0; i < slotNum; i++) {
        RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                "[SMC][onC2kCardTypeReady] Slot:%d, card type:%d, card state:%d",
                i, card_types[i], card_state[i]);
    }
    if (m_pending_card_types != NULL) {
        delete[] m_pending_card_types;
        m_pending_card_types = NULL;
        delete[] m_pending_card_states;
        m_pending_card_states = NULL;
    }
    enterModeSwitch(card_types, card_state, slotNum);
}

void RpCdmaLteModeController::doSwitchMode() {
    setupCdmaLteMode();
    setupSwitchQueue(sCdmaSocketSlotId);
    doSwitchRadioTech();
}

bool RpCdmaLteModeController::isEnableSwitchMode(int* card_types, int* card_state,
        int slotNum) {
     getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_CDMALTE_MODE_SLOT_READY,
            CDMALTE_MODE_NOT_READY);
    int modemOffState = getNonSlotScopeStatusManager()->getIntValue(RFX_STATUS_KEY_MODEM_OFF_STATE,
            MODEM_OFF_IN_IDLE);

    if (modemOffState != MODEM_OFF_IN_IDLE && modemOffState != MODEM_OFF_BY_MODE_SWITCH
            && modemOffState != MODEM_OFF_BY_SIM_SWITCH) {
        RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                "[SMC][onC2kCardTypeReady]modemOffState = %d, return", modemOffState);
        return false;
    }
    int worldModeState = getNonSlotScopeStatusManager()->getIntValue(
            RFX_STATUS_KEY_GSM_WORLD_MODE_STATE, 1);
    if (mInSwitching || modemOffState == MODEM_OFF_BY_SIM_SWITCH || worldModeState == 0) {
        RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][onC2kCardTypeReady] Switching now, pended");
        if (m_pending_card_types == NULL) {
            m_pending_card_types = new int[slotNum];
        }
        if (m_pending_card_states == NULL) {
            m_pending_card_states = new int[slotNum];
        }
        for (int slotId = SIM_ID_1; slotId < slotNum; slotId++) {
            m_pending_card_types[slotId] = card_types[slotId];
            m_pending_card_states[slotId] = card_state[slotId];
        }
        if (modemOffState == MODEM_OFF_BY_SIM_SWITCH) {
            getNonSlotScopeStatusManager()->registerStatusChangedEx(RFX_STATUS_KEY_MODEM_OFF_STATE,
                    RfxStatusChangeCallbackEx(this, &RpCdmaLteModeController::onModemOffStateChanged));
            RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC]listen modem_off_state,"
                    " for modemOffState == MODEM_OFF_BY_SIM_SWITCH");
        } else if (worldModeState == 0) {
            getNonSlotScopeStatusManager()->registerStatusChangedEx(
                    RFX_STATUS_KEY_GSM_WORLD_MODE_STATE, RfxStatusChangeCallbackEx(this,
                    &RpCdmaLteModeController::onWorldModeStateChange));
            RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC]listen WORL_MODE_STATE,"
                    " for worldModeState == 0");
        }
        return false;
    }
    return true;
}

void RpCdmaLteModeController::onWorldModeStateChange(int slotId, RfxStatusKeyEnum key,
        RfxVariant old_value, RfxVariant value) {
    int worldModeState = value.asInt();
    RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                "[SMC][onWorldModeStateChange] slotId:%d, oldValue:%d, newValue%d",
                slotId, old_value.asInt(), worldModeState);

    if (worldModeState == 0) {
        RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                "[SMC][onWorldModeStateChange]worldModeState == 0, Just return");
        return;
    }
    getNonSlotScopeStatusManager()->unRegisterStatusChangedEx(RFX_STATUS_KEY_GSM_WORLD_MODE_STATE,
                RfxStatusChangeCallbackEx(this, &RpCdmaLteModeController::onWorldModeStateChange));
    if (mInSwitching) {
        RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                "[SMC][onWorldModeStateChange] Mode Switch is in Switching, just return;");
        return;
    }
    dealPendedModeSwitch();
}

void RpCdmaLteModeController::onModemOffStateChanged(int slotId, RfxStatusKeyEnum key,
        RfxVariant old_value, RfxVariant value) {
    int modemOffState = value.asInt();
    if (modemOffState != MODEM_OFF_IN_IDLE && modemOffState != MODEM_OFF_BY_SIM_SWITCH) {
        getNonSlotScopeStatusManager()->unRegisterStatusChangedEx(RFX_STATUS_KEY_MODEM_OFF_STATE,
                RfxStatusChangeCallbackEx(this, &RpCdmaLteModeController::onModemOffStateChanged));
        RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                "[SMC][onModemOffStateChanged]modemOffState = %d, return", modemOffState);
        return;
    } else if (modemOffState == MODEM_OFF_BY_SIM_SWITCH) {
        RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                "[SMC][onModemOffStateChanged]modemOffState = %d, return", modemOffState);
        return;
    }
    getNonSlotScopeStatusManager()->unRegisterStatusChangedEx(RFX_STATUS_KEY_MODEM_OFF_STATE,
            RfxStatusChangeCallbackEx(this, &RpCdmaLteModeController::onModemOffStateChanged));
    dealPendedModeSwitch();
}

bool RpCdmaLteModeController::prepareForModeSwitch(int* card_types, int* card_state,
        int slotNum) {
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][onC2kCardTypeReady] Start switch");
    mIsSwitchingCardType = false;
    for (int slotId = SIM_ID_1; slotId < slotNum; slotId++) {
        m_card_types[slotId] = card_types[slotId];
        m_card_states[slotId] = card_state[slotId];
        m_old_card_modes[slotId] = m_card_modes[slotId];
    }

    calculateCardMode();

    if (needSwtichCardType()) {
        mIsSwitchingCardType = true;
        RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][onC2kCardTypeReady] switchcardType and return");
        return false;
    }
    return true;
}

void RpCdmaLteModeController::calculateCardMode() {
    RpCardModeControllerFactory::getRpCardModeController()->calculateCardMode(m_card_types,
            m_old_card_modes, m_card_modes, mSlotBeSwitchCardType, RFX_SLOT_COUNT);
}

void RpCdmaLteModeController::configModemStatus(int* card_types, int slotNum) {
    int cardTypes[slotNum+1] = {};
    cardTypes[slotNum] = CARD_TYPE_NONE;
    for (int index = 0; index < slotNum; index++) {
        if (isCt3GDualMode(index)) {
            cardTypes[index] = CARD_TYPE_RUIM;
        } else {
            cardTypes[index] = card_types[index];
        }
    }
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][configModemStatus] start. cdmaSlot : %d,"
            " cardType0:%d, cardType1:%d", sCdmaSocketSlotId, cardTypes[0], cardTypes[1]);
    sp<RfxAction> action = new RfxAction1<int>(this,
            &RpCdmaLteModeController::onConfigModemStatusFinished, sCdmaSocketSlotId);
    RpModemController::getInstance() -> configModemStatus(cardTypes[0], cardTypes[1],
            sCdmaSocketSlotId, action);
}

void RpCdmaLteModeController::onConfigModemStatusFinished(int cSlotId) {
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][configModemStatus] finished. cdmaSlot : %d", cSlotId);
    getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_CSLOT_CHANGED_STATUS,
               CSLOT_WILL_NOT_CHANGED);

    doSwitchRadioTechImpl();
}

int RpCdmaLteModeController::getCurrentNetworkType(int slotId) {
    char tempstr[PROPERTY_VALUE_MAX];
    int netWorkType = -1;

    memset(tempstr, 0, sizeof(tempstr));
    property_get("ro.mtk_lte_support", tempstr, "0");
    int isLteSupport = atoi(tempstr);

    //check whether L+C support first
    memset(tempstr, 0, sizeof(tempstr));
    property_get("ro.operator.optr", tempstr, "");
    int isOP01Support = (strcmp(tempstr, "OP01") == 0) ? 1 : 0;

    memset(tempstr, 0, sizeof(tempstr));
    property_get("persist.radio.simswitch", tempstr, "1");
    int  capabilitySlotId  = atoi(tempstr) - 1;
    if (m_card_modes[slotId] == RADIO_TECH_MODE_CDMALTE) {
        memset(tempstr, 0, sizeof(tempstr));
        property_get("ro.mtk_c2k_support", tempstr, "0");
        int  isC2KSupport  = atoi(tempstr);
        if (isLteSupport && slotId == capabilitySlotId && !isCt3GDualMode(slotId)
                && m_card_types[slotId] != CARD_TYPE_NONE
                && !(m_card_types[slotId] == CARD_TYPE_RUIM
                || m_card_types[slotId] == CARD_TYPE_CSIM
                || m_card_types[slotId] == (CARD_TYPE_CSIM + CARD_TYPE_RUIM))) {
            netWorkType = PREF_NET_TYPE_LTE_CDMA_EVDO;
        } else {
            netWorkType = PREF_NET_TYPE_CDMA_EVDO_AUTO;
        }
    } else if (m_card_modes[slotId] == RADIO_TECH_MODE_CSFB) {
        RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][getCurrentNetworkType]isOP01Support: %d,"
                " getIs4GCCard[%d]: %d",
                isOP01Support, slotId, (is4GCdmaCard(m_card_types[slotId])==true) ? 1 : 0);
        if (isCt3GDualMode(slotId)) {
            // For CDMA 3G Card Case:
            if (getRealCCardCount() == 1) {
                netWorkType = PREF_NET_TYPE_CDMA_EVDO_AUTO;
            } else {
                if (slotId == capabilitySlotId) {
                    netWorkType = PREF_NET_TYPE_CDMA_EVDO_AUTO;
                } else {
                    netWorkType = PREF_NET_TYPE_GSM_WCDMA;
                }
            }
        } else if (is4GCdmaCard(m_card_types[slotId])) {
            // CDMA 4G Card Case:
            if (slotId == capabilitySlotId) {
                if (isLteSupport) {
                    netWorkType = PREF_NET_TYPE_LTE_GSM_WCDMA;
                } else {
                    netWorkType = PREF_NET_TYPE_GSM_WCDMA;
                }
            } else {
                int slotCapability = getStatusManager(slotId)->getIntValue(RFX_STATUS_KEY_SLOT_CAPABILITY, 0);
                RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][getCurrentNetworkType] C card slotCapability: %d",
                        slotCapability);

                if ((slotCapability & RAF_UMTS) == RAF_UMTS) {
                    netWorkType = PREF_NET_TYPE_GSM_WCDMA;
                } else {
                    netWorkType = PREF_NET_TYPE_GSM_ONLY;
                }
            }
        } else {
            // G Card Case:
            if (slotId == capabilitySlotId) {
                if (isLteSupport) {
                    netWorkType = PREF_NET_TYPE_LTE_GSM_WCDMA;
                } else {
                    netWorkType = PREF_NET_TYPE_GSM_WCDMA;
                }
            } else {
                int slotCapability = getStatusManager(slotId)->getIntValue(RFX_STATUS_KEY_SLOT_CAPABILITY, 0);
                RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][getCurrentNetworkType] G card slotCapability: %d",
                        slotCapability);
                if ((slotCapability & RAF_UMTS) == RAF_UMTS) {
                    netWorkType = PREF_NET_TYPE_GSM_WCDMA;
                } else {
                    netWorkType = PREF_NET_TYPE_GSM_ONLY;
                }
            }
        }
    } else {
        assert(0);
    }
   RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][getCurrentNetworkType]isLteSupport: %d,"
            " capabilitySlotId: %d, netWorkType: %d",
           isLteSupport, capabilitySlotId, netWorkType);
   return netWorkType;
}

int RpCdmaLteModeController::getCCardCount() {
    int cCardNum = 0;
    char tempstr[PROPERTY_VALUE_MAX];
    for (int slotId = 0; slotId < RFX_SLOT_COUNT; slotId++) {
        memset(tempstr, 0, sizeof(tempstr));
        property_get(PROPERTY_RIL_FULL_UICC_TYPE[slotId], tempstr, "");
        if ((strstr(tempstr, "CSIM") != NULL) || (strstr(tempstr, "RUIM") != NULL)) {
            cCardNum++;
        }
    }
    return cCardNum;
}

int RpCdmaLteModeController::getRealCCardCount() {
    int cCardNum = 0;
    char tempstr[PROPERTY_VALUE_MAX];
    for (int slotId = 0; slotId < RFX_SLOT_COUNT; slotId++) {
        memset(tempstr, 0, sizeof(tempstr));
        property_get(PROPERTY_RIL_FULL_UICC_TYPE[slotId], tempstr, "");
        if ((strstr(tempstr, "CSIM") != NULL) || (strstr(tempstr, "RUIM") != NULL)) {
            cCardNum++;
        } else if ((strstr(tempstr, "SIM") != NULL) || (strstr(tempstr, "USIM") != NULL)) {
            if (isCt3GDualMode(slotId)) {
                cCardNum++;
            }
        }
    }
    return cCardNum;
}

int RpCdmaLteModeController::getFirstCCardSlot() {
    char tempstr[PROPERTY_VALUE_MAX];
    for (int slotId = 0; slotId < RFX_SLOT_COUNT; slotId++) {
        getUiccType(slotId, tempstr, PROPERTY_VALUE_MAX);
        if (isCdmaCard(tempstr)) {
            return slotId;
        }
    }
    return -1;
}

bool RpCdmaLteModeController::isCt3GDualMode(int slotId) {
    return RpCardModeControllerFactory::getRpCardModeController()->isCt3GDualMode(
            slotId, m_card_types[slotId]);
}

bool RpCdmaLteModeController::isCdmaCard(char* uicc_type) {
    if ((strstr(uicc_type, "CSIM") != NULL) || (strstr(uicc_type, "RUIM") != NULL)) {
        return true;
    }
    return false;
}

void RpCdmaLteModeController::getUiccType(int slotId, char* uiccType, int uiccTypeLength) {
    memset(uiccType, 0, uiccTypeLength);
    property_get(PROPERTY_RIL_FULL_UICC_TYPE[slotId], uiccType, "");
}

int RpCdmaLteModeController::getDefaultNetworkType(int slotId) {
    return getCurrentNetworkType(slotId);
}

void RpCdmaLteModeController::setupSwitchQueue(int old_cdma_socket_slot) {
    int firstSwitchSlot = SIM_ID_1;
    int secondSwitchSlot = SIM_ID_2;
    mSwitchQueue.clear();

    if (RFX_SLOT_COUNT == 1) {
        mSwitchQueue.add(firstSwitchSlot);
        return;
    }

    if (old_cdma_socket_slot != sCdmaSocketSlotId) {
        firstSwitchSlot = old_cdma_socket_slot;
        secondSwitchSlot = sCdmaSocketSlotId;
    }

    mSwitchQueue.clear();
    mSwitchQueue.add(firstSwitchSlot);
    mSwitchQueue.add(secondSwitchSlot);

    if (RFX_SLOT_COUNT > 2) {
        for (int i = 0; i < RFX_SLOT_COUNT; i++) {
            if (i != firstSwitchSlot && i != secondSwitchSlot) {
                mSwitchQueue.add(i);
            }
        }
    }
    assert(mSwitchQueue.size() > 0 && mSwitchQueue.size() <= RFX_SLOT_COUNT);
}

bool RpCdmaLteModeController::needSwtichCardType() {
    // For C+C. just switchCardType and return.
    bool hasSwtiched = false;

    if (mSlotBeSwitchCardType != NULL) {
        for (int index = 0; index < RFX_SLOT_COUNT; index++) {
             RpNwRatController* nwRatController =
                    (RpNwRatController *)findController(index,
                    RFX_OBJ_CLASS_INFO(RpNwRatController));
            if (mSlotBeSwitchCardType[index] > -1) {
                RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                        "[SMC][needSwtichCardType] Just return. AS slot: %d switchCardType to sim.",
                        index);
                RpSimController* simController =
                        (RpSimController*)findController(index,
                        RFX_OBJ_CLASS_INFO(RpSimController));
                simController->switchCardType(0, false);
                hasSwtiched = true;
            } else if (RFX_SLOT_COUNT == 2 && isCt3GDualMode(index)
                    && m_card_types[index] == CARD_TYPE_SIM
                    && !containsCdma(m_card_types[index == 0 ? 1 : 0])
                    // getNwsModeForSwitchCardType only valid when AppFamilyType is APP_FAM_3GPP2
                    // Invalid if we give AppFamilyType 3GPP for CT3G card previous.
                    && ((nwRatController->getNwsModeForSwitchCardType() != NWS_MODE_CSFB
                    && nwRatController->getAppFamilyType() == APP_FAM_3GPP2)
                    || nwRatController->getAppFamilyType() == APP_FAM_3GPP)) {
                RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                        "[SMC][needSwtichCardType] Just return."
                        " AS slot: %d switchCardType: G-->C .",
                        index);
                RpSimController *simController =
                        (RpSimController*)findController(index,
                        RFX_OBJ_CLASS_INFO(RpSimController));
                simController->switchCardType(1, false);
                hasSwtiched = true;
            }
        }
        if (hasSwtiched) {
            for (int slotId = SIM_ID_1; slotId < RFX_SLOT_COUNT; slotId++) {
                mSlotBeSwitchCardType[slotId] = -1;
                m_card_modes[slotId] = m_old_card_modes[slotId];
            }
        }
    }
    RFX_LOG_D(RP_CDMALTE_MODE_TAG,
        "[SMC][needSwtichCardType] hasSwitched: %d", hasSwtiched);
    return hasSwtiched;
}

void RpCdmaLteModeController::doSwitchRadioTech() {
//    startSwitchMode();
    RFX_LOG_D(RP_CDMALTE_MODE_TAG,
            "[SMC][doSwitchRadioTech]");
    configModemStatus(m_card_types, RFX_SLOT_COUNT);
}

void RpCdmaLteModeController::doSwitchRadioTechImpl() {
    int switchSlot = mSwitchQueue.itemAt(0);
    RFX_LOG_D(RP_CDMALTE_MODE_TAG,
            "[SMC][doSwitchRadioTechImpl] Start switch Slot: %d firstly, mSwitchQueue size : %d",
            switchSlot, mSwitchQueue.size());
    mSwitchQueue.removeAt(0);
    switchRadioTechnology(switchSlot);
}
void RpCdmaLteModeController::onRatSwitchDone(int slotId) {
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][onRatSwitchDone] Slot : %d switch done,"
            " mSwitchQueue size : %d", slotId, mSwitchQueue.size());
    m_rat_mode[slotId] = RAT_MODE_INVALID;
    if (mSwitchQueue.isEmpty()) {
        leaveRestrictMode(getCdmaSocketSlotId());
     } else if (mSwitchQueue.size() >= 1) {
         int switchSlot = mSwitchQueue.itemAt(0);
         RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][onRatSwitchDone] Start switch Slot: %d", switchSlot);
         mSwitchQueue.removeAt(0);
         switchRadioTechnology(switchSlot);
    }
}

void RpCdmaLteModeController::switchRadioTechnology(int slotId) {
    sp<RfxAction> action = new RfxAction1<int>(this,
            &RpCdmaLteModeController::onRatSwitchDone, slotId);
    int networkType = getCurrentNetworkType(slotId);

    AppFamilyType app_family_type = getAppFamilyType(networkType);

    if (app_family_type == APP_FAM_3GPP2 && networkType == PREF_NET_TYPE_GSM_ONLY) {
        app_family_type = APP_FAM_3GPP;
        RFX_LOG_D(RP_CDMALTE_MODE_TAG,
            "[SMC][switchRadioTechnology] change app_family_type to APP_FAM_3GPP");
    } else if (app_family_type == APP_FAM_3GPP && m_card_modes[slotId] == RADIO_TECH_MODE_CDMALTE) {
        int insertCardCount = 0;
        for (int index = 0; index < RFX_SLOT_COUNT; index++) {
            if (m_card_types[index] > CARD_TYPE_NONE) {
                insertCardCount++;
            }
        }
        if (insertCardCount == 0) {
            app_family_type = APP_FAM_3GPP2;
            RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                "[SMC][switchRadioTechnology] change app_family_type to APP_FAM_3GPP2");
        }
    }

    RFX_LOG_D(RP_CDMALTE_MODE_TAG,
            "[SMC][switchRadioTechnology] SlotId : %d, %s -> %s, app_family_type : %d, \
            networkType : %d, card_state: %d",
            slotId,
            m_old_card_modes[slotId] == 3 ? "CDMALTE" : "CSFB",
            m_card_modes[slotId] == 3 ? "CDMALTE" : "CSFB",
            app_family_type, networkType, m_card_states[slotId]);
    RpNwRatController* nwRatController =
            (RpNwRatController *)findController(slotId, RFX_OBJ_CLASS_INFO(RpNwRatController));

    nwRatController->setPreferredNetworkType({app_family_type,
            networkType,
            m_card_states[slotId],
            action,
            m_rat_mode[slotId]});
}

/**
 * Get the SVLTE slot id.
 * @return SVLTE slot id.
 *         0 : svlte in slot1
 *         1 : svlte in slot2
 */
int RpCdmaLteModeController::getActiveCdmaLteModeSlotId() {
    int i = 0;
    char tempstr[PROPERTY_VALUE_MAX];
    char* tok = NULL;

    memset(tempstr, 0, sizeof(tempstr));
    // 3 means SVLTE mode, 2 is CSFB mode in this persist.
    property_get("persist.radio.svlte_slot", tempstr, "3,2");
    tok = strtok(tempstr, ",");
    while (tok != NULL) {
        if (3 == atoi(tok)) {
            RFX_LOG_D(RP_CDMALTE_MODE_TAG, "getActiveCdmaLteModeSlotId : %d", i);
            return i;
        }
        i++;
        tok = strtok(NULL, ",");
    }
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "getActiveCdmaLteModeSlotId : -1");
    return CSFB_ON_SLOT;
}

/**
 * Get slot id which connect to c2k rild socket.
 * @return slot id which connect to c2k rild socket
 *         0 : slot1 connect to c2k rild socket
 *         1 : slot2 connect to c2k rild socket
 */
int RpCdmaLteModeController::getCdmaSocketSlotId() {
    int ret = 0;
    char tempstr[PROPERTY_VALUE_MAX] = { 0 };

    memset(tempstr, 0, sizeof(tempstr));
    property_get("persist.radio.cdma_slot", tempstr, "1");

    ret = atoi(tempstr) - 1;
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "getCdmaSocketSlotId : %d", ret);
    return ret;
}

void RpCdmaLteModeController::setCdmaSocketSlotId(int slotId) {
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC]setCdmaSocketSlotId : %d -> %d",
            sCdmaSocketSlotId, slotId);
    if (slotId >= 0 && slotId < RFX_SLOT_COUNT) {
        sCdmaSocketSlotId = slotId;
        char* slot_str = NULL;
        asprintf(&slot_str, "%d", slotId + 1);
        property_set("persist.radio.cdma_slot", slot_str);
        free(slot_str);
    }
}

void RpCdmaLteModeController::setActiveCdmaLteModeSlotId() {
    int cdmaLteModeSlotId = CSFB_ON_SLOT;
    for (int i = 0;i < RFX_SLOT_COUNT; i++) {
        if (m_card_modes[i] == RADIO_TECH_MODE_CDMALTE) {
            cdmaLteModeSlotId = i;
            break;
        }
    }

    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC]setActiveCdmaLteModeSlotId : %d -> %d",
            mCdmaLteModeSlotId, cdmaLteModeSlotId);
    if (mCdmaLteModeSlotId != cdmaLteModeSlotId) {
        mCdmaLteModeSlotId = cdmaLteModeSlotId;
        String8 cardmodestr;
        cardmodestr.clear();
        for (int i = 0;i < RFX_SLOT_COUNT - 1; i++) {
            cardmodestr.appendFormat("%d,", m_card_modes[i]);
        }
        cardmodestr.appendFormat("%d", m_card_modes[RFX_SLOT_COUNT - 1]);
        property_set("persist.radio.svlte_slot", cardmodestr.string());
        RFX_LOG_D(RP_CDMALTE_MODE_TAG, "setupCdmaLteMode : %s", cardmodestr.string());
    }
}

void RpCdmaLteModeController::setupCdmaLteMode() {
    setActiveCdmaLteModeSlotId();
    getNonSlotScopeStatusManager()->setIntValue(
            RFX_STATUS_KEY_ACTIVE_CDMALTE_MODE_SLOT, mCdmaLteModeSlotId);

    setCdmaSocketSlotId(mCdmaLteModeSlotId > -1 ? mCdmaLteModeSlotId : sCdmaSocketSlotId);

    getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_CDMA_SOCKET_SLOT, sCdmaSocketSlotId);
    RFX_OBJ_GET_INSTANCE(RfxSocketStateManager)->setCdmaSocketSlotChange(sCdmaSocketSlotId);

    getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_CDMALTE_MODE_SLOT_READY,
            CDMALTE_MODE_READY);
    RpModeInitController* mic =
            (RpModeInitController *)findController(RFX_OBJ_CLASS_INFO(RpModeInitController));
    mic->mSwitchCdmaSlotFinishSignal.emit();
}

int RpCdmaLteModeController::getNewCdmaSocketSlotId() {
    int cdmaLteModeSlotId = CSFB_ON_SLOT;
    for (int i = 0; i < RFX_SLOT_COUNT; i++) {
        if (m_card_modes[i] == RADIO_TECH_MODE_CDMALTE) {
            cdmaLteModeSlotId = i;
            break;
        }
    }
    if (cdmaLteModeSlotId == CSFB_ON_SLOT) {
        cdmaLteModeSlotId = sCdmaSocketSlotId;
    }
    return cdmaLteModeSlotId;
}

int RpCdmaLteModeController::getRealCardType(int slotId) {
    bool ct3g = getStatusManager(slotId)->getBoolValue(RFX_STATUS_KEY_CT3G_DUALMODE_CARD, false);
    int cardType = getStatusManager(slotId)->getIntValue(RFX_STATUS_KEY_CARD_TYPE, 0);
    RLOGD("getRealCardType, slot %d: cardType1=%d, ct3g=%d", slotId, cardType, ct3g);
    if (ct3g && cardType > 0) {
        cardType = RFX_CARD_TYPE_RUIM;
    }
    return cardType;
}

void RpCdmaLteModeController::startSwitchMode() {
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC]startSwitchMode");
    mInSwitching = true;
    getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_MODE_SWITCH, MODE_SWITCH_START);
}

bool RpCdmaLteModeController::isEmdStatusChanged(int* card_types, int slotNum) {
    int cardTypes[slotNum+1] = {};
    cardTypes[slotNum] = CARD_TYPE_NONE;
    for (int index = 0; index < slotNum; index++) {
        if (isCt3GDualMode(index)) {
            cardTypes[index] = CARD_TYPE_RUIM;
        } else {
            cardTypes[index] = card_types[index];
        }
    }
    int newSlotId = getNewCdmaSocketSlotId();
    bool isChanged = RpModemController::getInstance()->isEmdstatusChanged(cardTypes[0],
            cardTypes[1], newSlotId);
    RFX_LOG_D(RP_CDMALTE_MODE_TAG,
            "[SMC]isEmdStatusChanged : %d, cardType0:%d, cardType1:%d, newCdmsSocketSlotId:%d",
            isChanged, cardTypes[0], cardTypes[1], newSlotId);
    return isChanged;
}

void RpCdmaLteModeController::enterRestrictMode(int cdmaSocketSlotId) {
    startSwitchMode();
    getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_RESTRICT_MODE_STATE,
            RESTRICT_MODE_STATE_ON);
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC]enterRestrictMode, c2kSlot:%d", cdmaSocketSlotId);
    // send ERMS to GSM/C2K RILD
    sp<RfxMessage> parentRequest = RfxMessage::obtainRequest(cdmaSocketSlotId,
            RADIO_TECH_GROUP_GSM, RIL_REQUEST_ENTER_RESTRICT_MODEM);
    Parcel* parcel = parentRequest->getParcel();
    parcel->writeInt32(1);
    parcel->writeInt32(ENTER_RESTRICT_MODE);
    sp<RfxMessage> gsmRequest = RfxMessage::obtainRequest(RADIO_TECH_GROUP_GSM,
            RIL_REQUEST_ENTER_RESTRICT_MODEM, parentRequest, true);
    sp<RfxMessage> c2kRequest = RfxMessage::obtainRequest(RADIO_TECH_GROUP_C2K,
            RIL_REQUEST_ENTER_RESTRICT_MODEM_C2K, parentRequest, true);
    requestToRild(gsmRequest);
    requestToRild(c2kRequest);
}

/**
 * M: Should Close the C slot's radio and another slot's G Radio.
*/
void RpCdmaLteModeController::requestRadioOff() {
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC]requestRadioOff");

    if (sCdmaSocketSlotId != getNewCdmaSocketSlotId()) {
        for (int slot = 0; slot < RFX_SLOT_COUNT; slot++) {
            bool turnOffCRadio = false;
            bool turnOffGRadio = false;
            if (sCdmaSocketSlotId == slot) {
                turnOffCRadio = true;
                turnOffGRadio = false;
            } else {
                turnOffCRadio = false;
                turnOffGRadio = true;
            }
            RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC]requestRadioOff, slotId:%d, CRadioOff: %d,"
                    " GRadioOff:%d ", slot, turnOffCRadio, turnOffGRadio);
            sp<RfxAction> action0 = new RfxAction1<int>(this,
                    &RpCdmaLteModeController::onRequestRadioOffDone, slot);
            RpRadioController* radioController0 =
                    (RpRadioController *)findController(slot,
                    RFX_OBJ_CLASS_INFO(RpRadioController));
            radioController0->dynamicSwitchRadioOff(turnOffCRadio, turnOffGRadio, action0);
        }
    } else {
        mCloseRadioCount = RFX_SLOT_COUNT - 1;
        RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC]requestRadioOff, Just slotId:%d", sCdmaSocketSlotId);
        sp<RfxAction> action = new RfxAction1<int>(this,
                &RpCdmaLteModeController::onRequestRadioOffDone, sCdmaSocketSlotId);
        RpRadioController* radioController =
                (RpRadioController *)findController(sCdmaSocketSlotId,
                RFX_OBJ_CLASS_INFO(RpRadioController));
        radioController->dynamicSwitchRadioOff(true, true, action);
    }
}

void RpCdmaLteModeController::onRequestRadioOffDone(int slotId) {
    mCloseRadioCount++;
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC]onRequestRadioOffDone,%d / %d, slotId:%d",
            mCloseRadioCount, RFX_SLOT_COUNT, slotId);
    if (mCloseRadioCount == RFX_SLOT_COUNT) {
        mCloseRadioCount = 0;
        doSwitchMode();
    }
}

void RpCdmaLteModeController::leaveRestrictMode(int cdmaSocketSlotId) {
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC]leaveRestrictMode, c2kSlot:%d", cdmaSocketSlotId);
    // send ERMS to GSM/C2K RILD
    sp<RfxMessage> parentRequest = RfxMessage::obtainRequest(cdmaSocketSlotId,
            RADIO_TECH_GROUP_GSM, RIL_REQUEST_LEAVE_RESTRICT_MODEM);
    Parcel* parcel = parentRequest->getParcel();
    parcel->writeInt32(1);
    parcel->writeInt32(LEAVE_RESTRICT_MODE);
    sp<RfxMessage> gsmRequest = RfxMessage::obtainRequest(RADIO_TECH_GROUP_GSM,
            RIL_REQUEST_LEAVE_RESTRICT_MODEM, parentRequest, true);
    sp<RfxMessage> c2kRequest = RfxMessage::obtainRequest(RADIO_TECH_GROUP_C2K,
            RIL_REQUEST_LEAVE_RESTRICT_MODEM_C2K, parentRequest, true);

    requestToRild(gsmRequest);
    requestToRild(c2kRequest);
}

void RpCdmaLteModeController::finishSwitchMode() {
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC]finishSwitchMode");

    getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_MODE_SWITCH, MODE_SWITCH_FINISH);
    getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_MODEM_OFF_STATE, MODEM_OFF_IN_IDLE);

    mInSwitching = false;

    if (m_pending_card_types != NULL) {
        RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][finishSwitchMode]enqueue pending message to"
                " main queue.");
        enqueuePendingRequest();
    }
}

bool RpCdmaLteModeController::is4GCdmaCard(int cardType) {
    if (containsUsim(cardType) && containsCdma(cardType)) {
        return true;
    }
    return false;
}

bool RpCdmaLteModeController::containsCdma(int cardType) {
     if ((cardType & CARD_TYPE_RUIM) > 0 ||
         (cardType & CARD_TYPE_CSIM) > 0) {
         return true;
     }
     return false;
}

bool RpCdmaLteModeController::containsGsm(int cardType) {
     if ((cardType & CARD_TYPE_SIM) > 0 ||
         (cardType & CARD_TYPE_USIM) > 0) {
         return true;
     }
     return false;
}

bool RpCdmaLteModeController::containsUsim(int cardType) {
     if ((cardType & CARD_TYPE_USIM) > 0) {
         return true;
     }
     return false;
}

bool RpCdmaLteModeController::isUsimOnlyCard(int cardType) {
      return (containsUsim(cardType) && !containsCdma(cardType));
}

AppFamilyType RpCdmaLteModeController::getAppFamilyType(int networkType) {
    if (networkType == PREF_NET_TYPE_CDMA_EVDO_AUTO || networkType == PREF_NET_TYPE_LTE_CDMA_EVDO) {
        return APP_FAM_3GPP2;
    } else {
        return APP_FAM_3GPP;
    }
}

bool RpCdmaLteModeController::waitC2KSocketConnectDone() {
    bool isC2kSocketConnected = false;
    RfxSocketState socketState =
            getStatusManager(sCdmaSocketSlotId)->getSocketStateValue(RFX_STATUS_KEY_SOCKET_STATE);
    isC2kSocketConnected = socketState.getSocketState(socketState.SOCKET_C2K);
    RFX_LOG_D(RP_CDMALTE_MODE_TAG,
            "[SMC]C2KSocketConnected : %d, cdmaSocketSlotId : %d,  socketState : %s",
            isC2kSocketConnected, sCdmaSocketSlotId, socketState.toString().string());
    if (!isC2kSocketConnected) {
        getStatusManager(sCdmaSocketSlotId)->registerStatusChangedEx(RFX_STATUS_KEY_SOCKET_STATE,
                RfxStatusChangeCallbackEx(this, &RpCdmaLteModeController::onC2kSocketConnected));
        RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                "[SMC]Slot %d wait onC2kSocketConnected", sCdmaSocketSlotId);
    }
    return isC2kSocketConnected;
}

bool RpCdmaLteModeController::onHandleRequest(const sp<RfxMessage>& message) {
    int msg_id = message->getId();
    switch (msg_id) {
        case RILPROXY_LOCAL_REQUEST_DEAL_PENDING_MODE_SWITCH:
            RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                    "[SMC]onHandleRequest, REQUEST_DEAL_PENDING_MODE_SWITCH, msgToken:%d",
                    message->getToken());
            dealPendedModeSwitch();
            break;
        case RIL_REQUEST_SWITCH_MODE_FOR_ECC:
            switchModeForECC(message);
            break;
        default:
            break;
    }
    return true;
}

void RpCdmaLteModeController::switchModeForECC(const sp<RfxMessage>& message) {
    int modemOffState = getNonSlotScopeStatusManager()->getIntValue(RFX_STATUS_KEY_MODEM_OFF_STATE,
            MODEM_OFF_IN_IDLE);
    int worldModeState = getNonSlotScopeStatusManager()->getIntValue(
                RFX_STATUS_KEY_GSM_WORLD_MODE_STATE, 1);

    if (modemOffState != MODEM_OFF_IN_IDLE || worldModeState != 1) {
        RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                "[SMC][switchModeForECC] suspend as modemOffState = %d,worldModeState= %d,"
                " Just return", modemOffState, worldModeState);
        responseToRilj(RfxMessage::obtainResponse(RIL_E_GENERIC_FAILURE, message));
        return;
    }

    int cardTypes[RFX_SLOT_COUNT] = { 0 };
    int cardState[RFX_SLOT_COUNT] = { 0 };
    for (int i = 0; i < RFX_SLOT_COUNT; i++) {
        cardTypes[i] = RFX_OBJ_GET_INSTANCE(RfxRootController)->getStatusManager(i)
                ->getIntValue(RFX_STATUS_KEY_CARD_TYPE, CARD_TYPE_NONE);
        // ECC only trigger mode switch by none slot.
        if (containsCdma(cardTypes[i])
                || (i == message->getSlotId() && cardTypes[i] != CARD_TYPE_NONE)) {
            RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                    "[SMC][switchModeForECC] cardTypes[%d] = %d, ECC Slot = %d"
                    " Just return",  i, cardTypes[i], message->getSlotId());
            responseToRilj(RfxMessage::obtainResponse(RIL_E_GENERIC_FAILURE, message));
            return;
        }
        cardState[i] = CARD_STATE_NOT_HOT_PLUG;
        m_rat_mode[i] = RAT_MODE_INVALID;
    }

    responseToRilj(RfxMessage::obtainResponse(RIL_E_SUCCESS, message));
    message->getParcel()->readInt32(); // pass length
    int32_t mode = message->getParcel()->readInt32();
    // CARD_TYPE_CSIM(04); CARD_TYPE_SIM(01)
    cardTypes[message->getSlotId()] = mode;
    m_rat_mode[message->getSlotId()] =
            (mode == CARD_TYPE_CSIM ? RAT_MODE_CDMA_EVDO_AUTO : RAT_MODE_LTE_WCDMA_GSM);
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][switchModeForECC] mode:%d", mode);
    onCardTypeReady(cardTypes, cardState, RFX_SLOT_COUNT, CARD_TYPE_READY_REASON_COMMON);
}

void RpCdmaLteModeController::enqueuePendingRequest() {
    sp<RfxMessage> pendingRequest = RfxMessage::obtainRequest(0, RADIO_TECH_GROUP_C2K,
        RILPROXY_LOCAL_REQUEST_DEAL_PENDING_MODE_SWITCH);
    RfxMainThread::enqueueMessage(pendingRequest);
}

void RpCdmaLteModeController::dealPendedModeSwitch() {
    RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC]Start switch mode pended");

    int modemOffState = getNonSlotScopeStatusManager()->getIntValue(RFX_STATUS_KEY_MODEM_OFF_STATE,
            MODEM_OFF_IN_IDLE);
    if (modemOffState != MODEM_OFF_IN_IDLE && modemOffState != MODEM_OFF_BY_SIM_SWITCH) {
        RFX_LOG_D(RP_CDMALTE_MODE_TAG,
                "[SMC][dealPendedModeSwitch]modemOffState = %d, return", modemOffState);
        delete[] m_pending_card_types;
        m_pending_card_types = NULL;
        delete[] m_pending_card_states;
        m_pending_card_states = NULL;
        mInSwitching = false;
        return;
    } else if (modemOffState == MODEM_OFF_BY_SIM_SWITCH) {
        RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC][dealPendedModeSwitch] just return, "
                "for modemOffState == MODEM_OFF_BY_SIM_SWITCH");
        getNonSlotScopeStatusManager()->registerStatusChangedEx(RFX_STATUS_KEY_MODEM_OFF_STATE,
                RfxStatusChangeCallbackEx(this, &RpCdmaLteModeController::onModemOffStateChanged));
        return;
    }

    getNonSlotScopeStatusManager()->unRegisterStatusChangedEx(RFX_STATUS_KEY_MODEM_OFF_STATE,
            RfxStatusChangeCallbackEx(this, &RpCdmaLteModeController::onModemOffStateChanged));

    if (m_pending_card_types == NULL) {
        RFX_LOG_D(RP_CDMALTE_MODE_TAG, "[SMC]Deal with Pending request END,"
                " for pending request has been delete");
        return;
    }
    for (int slotId = SIM_ID_1; slotId <RFX_SLOT_COUNT; slotId++) {
        m_card_types[slotId] = m_pending_card_types[slotId];
        m_old_card_modes[slotId] = m_card_modes[slotId];
        m_card_states[slotId] = m_pending_card_states[slotId];
    }
    delete[] m_pending_card_types;
    m_pending_card_types = NULL;
    delete[] m_pending_card_states;
    m_pending_card_states = NULL;

    enterModeSwitch(m_card_types, m_card_states, RFX_SLOT_COUNT);
}
bool RpCdmaLteModeController::onHandleResponse(const sp<RfxMessage>& message) {
    int msg_id = message->getId();
    RFX_LOG_D(RP_CDMALTE_MODE_TAG,
            "[SMC]onHandleResponse, msgId: %d", msg_id);
    switch (msg_id) {
        case RIL_REQUEST_ENTER_RESTRICT_MODEM:
        case RIL_REQUEST_ENTER_RESTRICT_MODEM_C2K: {
            if (message->getError() != RIL_E_SUCCESS) {
                RFX_LOG_E(RP_CDMALTE_MODE_TAG,
                    "[SMC]onHandleResponse, requestId:%d, msgToken: %d  FAILED!!!",
                    msg_id, message->getToken());
                return true;
            }
            sp<RfxMessage> msg = sp<RfxMessage>(NULL);
            ResponseStatus responseStatus;
            responseStatus = preprocessResponse(message, msg);
            if (responseStatus == RESPONSE_STATUS_HAVE_MATCHED) {
                requestRadioOff();
            }
            break;
        }
        case RIL_REQUEST_LEAVE_RESTRICT_MODEM:
        case RIL_REQUEST_LEAVE_RESTRICT_MODEM_C2K: {
            if (message->getError() != RIL_E_SUCCESS) {
                RFX_LOG_E(RP_CDMALTE_MODE_TAG,
                    "[SMC]onHandleResponse, requestId:%d, msgToken: %d  FAILED!!!",
                    msg_id, message->getToken());
                return true;
            }
            sp<RfxMessage> msg = sp<RfxMessage>(NULL);
            ResponseStatus responseStatus;
            responseStatus = preprocessResponse(message, msg);
            if (responseStatus == RESPONSE_STATUS_HAVE_MATCHED) {
                getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_RESTRICT_MODE_STATE,
                        RESTRICT_MODE_STATE_OFF);

                for (int index =0; index < RFX_SLOT_COUNT; index++) {
                    RpRadioController* radioController0 =
                            (RpRadioController *)findController(index,
                            RFX_OBJ_CLASS_INFO(RpRadioController));
                    radioController0->dynamicSwitchRadioOn();
                }
                finishSwitchMode();
            }
            break;
        }
        default:
            break;
    }
    return true;
}

void RpCdmaLteModeController::responseTimedoutCallBack(const sp<RfxMessage>& message) {
    RFX_LOG_D(RP_CDMALTE_MODE_TAG,
            "[SMC]responseTimedoutCallBack, requestId:%d, msgToken: %d",
            message->getId(), message->getToken());
}

void RpCdmaLteModeController::onEnterRestrictModeDone(const sp<RfxMessage>& message) {
    sp<RfxMessage> msg = sp<RfxMessage>(NULL);
    ResponseStatus responseStatus = preprocessResponse(message, msg,
            RfxWaitResponseTimedOutCallback(this,
            &RpCdmaLteModeController::responseTimedoutCallBack), s2ns(60));
    if (responseStatus == RESPONSE_STATUS_HAVE_MATCHED) {
        requestRadioOff();
    }
}
