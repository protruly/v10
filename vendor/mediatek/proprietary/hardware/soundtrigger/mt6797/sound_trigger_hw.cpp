/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "sound_trigger_hw_default"
/*#define LOG_NDEBUG 0*/



#include <errno.h>
#include <pthread.h>
#include <sys/prctl.h>
#include <cutils/log.h>
#include <fcntl.h>
#include <hardware/hardware.h>
#include <system/sound_trigger.h>
#include <hardware/sound_trigger.h>
#include <voiceunlock/vowAPI_AP.h>
#include <media/AudioSystem.h>
#include "soundtrigger.h"



using namespace android;
#define ALLOCATE_MEMORY_SIZE_MAX 50*1024 // the max memory size can be allocated

static const struct sound_trigger_properties hw_properties = {
        "The Soundtrigger HAL Project", // implementor
        "Sound Trigger stub HAL", // description
        1, // version
        { 0xed7a7d60, 0xc65e, 0x11e3, 0x9be4, { 0x00, 0x02, 0xa5, 0xd5, 0xc5, 0x1b } }, // uuid
        1, // max_sound_models
        1, // max_key_phrases
        1, // max_users
        RECOGNITION_MODE_VOICE_TRIGGER, // recognition_modes
        true, // capture_transition
        0, // max_buffer_ms
        false, // concurrent_capture
        false, // trigger_in_event
        0 // power_consumption_mw
};

struct VOW_EINT_DATA_STRUCT_T{
    int size;        // size of data section
    int eint_status; // eint status
    int id;
    char *data;      // reserved for future extension
};


struct stub_sound_trigger_device {
    struct sound_trigger_hw_device device;
    sound_model_handle_t model_handle;
    recognition_callback_t recognition_callback;
    void *recognition_cookie;
    sound_model_callback_t sound_model_callback;
    void *sound_model_cookie;
    pthread_t callback_thread;
    pthread_mutex_t lock;
    pthread_cond_t  cond;
};


enum vow_model_type {
    VOW_SPEAKER_MODE,
    VOW_INITIAL_MODE,
    VOW_MODEL_MODE_NUM
};

enum voice_wakeup {
    VOICE_UNLOCK,
    VOICE_WAKEUP_NO_RECOGNIZE,
    VOICE_WAKEUP_RECOGNIZE,
    VOICE_WAKE_UP_MODE_NUM
};
int mFd = -1;
int mphrase_extrasid=-1;
int stoprecognition=-1;
int sound_trigger_running_state=-1;

struct VOW_EINT_DATA_STRUCT_T m_sINTData;

static void *callback_thread_loop(void *context)
{
    struct stub_sound_trigger_device *stdev = (struct stub_sound_trigger_device *)context;
    ALOGI("%s", __func__);
    prctl(PR_SET_NAME, (unsigned long)"sound trigger callback", 0, 0, 0);
    if (stdev->recognition_callback == NULL) {
        goto exit;
    }

    if (mFd < 0) {
        mFd = open("/dev/vow", O_RDONLY);
    }
    if (stdev->recognition_callback != NULL) {
        ALOGI("mFd%d", mFd);
        if (mFd >= 0) {
            m_sINTData.eint_status = -1;
            ALOGI("1.VoiceWakeup interrupt status, status:%d ,stoprecognition:%d", m_sINTData.eint_status, stoprecognition);
            while(!stoprecognition){
                read(mFd, &m_sINTData, sizeof(struct VOW_EINT_DATA_STRUCT_T));
                ALOGI("2.VoiceWakeup interrupt status, status:%d, stoprecognition:%d", m_sINTData.eint_status, stoprecognition);
                if(m_sINTData.eint_status==0){
                    char *data = (char *)calloc(1, sizeof(struct sound_trigger_phrase_recognition_event) + 1);
                    struct sound_trigger_phrase_recognition_event *event = (struct sound_trigger_phrase_recognition_event *)data;
                    pthread_mutex_lock(&stdev->lock);
                    event->common.status = RECOGNITION_STATUS_SUCCESS;
                    event->common.type = SOUND_MODEL_TYPE_KEYPHRASE;
                    event->common.model = stdev->model_handle;
                    event->common.capture_available = true;
                    event->common.audio_config = AUDIO_CONFIG_INITIALIZER;
                    event->common.audio_config.sample_rate = 16000;
                    event->common.audio_config.channel_mask = AUDIO_CHANNEL_IN_MONO;
                    event->common.audio_config.format = AUDIO_FORMAT_PCM_16_BIT;
                    event->num_phrases = 1;
                    event->phrase_extras[0].id = mphrase_extrasid;
                    event->phrase_extras[0].recognition_modes = RECOGNITION_MODE_VOICE_TRIGGER;
                    event->phrase_extras[0].confidence_level = 100;
                    event->phrase_extras[0].num_levels = 1;
                    event->phrase_extras[0].levels[0].level = 100;
                    event->phrase_extras[0].levels[0].user_id = 0;
                    event->common.data_offset = sizeof(struct sound_trigger_phrase_recognition_event);
                    event->common.data_size = 1;
                    //data[event->common.data_offset] = 8;
                    //ASSERT(stdev->recognition_callback!=NULL);
                    if(stdev->recognition_callback != NULL){
                        stdev->recognition_callback(&event->common, stdev->recognition_cookie);
                    }
                    free(data);
                    pthread_mutex_unlock(&stdev->lock);
                    ALOGI("phrase_extras[0].id %d", mphrase_extrasid);
                    ALOGI("capture_available %d", event->common.capture_available);
                    ALOGI("%s send callback model %d", __func__, stdev->model_handle);
                    sound_trigger_running_state = 0;
                    break;
                } else if (m_sINTData.eint_status==-2 && stoprecognition==1) {
                    ALOGD("sound_trigger callback m_sINTData.eint_status %d stoprecognition %d", m_sINTData.eint_status, stoprecognition);
                    sound_trigger_running_state = 0;
                    break;
                }
            }
        }
    } else {
        ALOGI("%s abort recognition, null callback!! model %d", __func__, stdev->model_handle);
    }
    stdev->recognition_callback = NULL;
exit:
    sound_trigger_running_state = 0;
    ALOGD("Exit sound_trigger callback_thread_loop ");
    return NULL;
}

static int stdev_get_properties(const struct sound_trigger_hw_device *dev,
                                struct sound_trigger_properties *properties)
{
    struct stub_sound_trigger_device *stdev = (struct stub_sound_trigger_device *)dev;

    ALOGI("%s", __func__);
    if (properties == NULL)
        return -EINVAL;
    memcpy(properties, &hw_properties, sizeof(struct sound_trigger_properties));
    return 0;
}

static int stdev_load_sound_model(const struct sound_trigger_hw_device *dev,
                                  struct sound_trigger_sound_model *sound_model,
                                  sound_model_callback_t callback,
                                  void *cookie,
                                  sound_model_handle_t *handle)
{
    struct stub_sound_trigger_device *stdev = (struct stub_sound_trigger_device *)dev;
    int status = 0;
    sound_trigger_uuid_t uuid;
    uuid=sound_model->uuid;
    ALOGI("Start Load Model");
    ALOGI("uuid.timeHi: %d",uuid.timeHiAndVersion);
    ALOGI("%s stdev %p", __func__, stdev);
    pthread_mutex_lock(&stdev->lock);
    if (handle == NULL || sound_model == NULL) {
        status = -EINVAL;
        pthread_mutex_unlock(&stdev->lock);
        return status;
        pthread_mutex_unlock(&stdev->lock);
        return status;
        //goto exit;
    }
    if (sound_model->data_size == 0 ||
            sound_model->data_offset < sizeof(struct sound_trigger_sound_model)) {
        status = -EINVAL;
        pthread_mutex_unlock(&stdev->lock);
        return status;
        //goto exit;
    }

    if (stdev->model_handle == 1) {
        status = -ENOSYS;
        pthread_mutex_unlock(&stdev->lock);
        return status;
        //goto exit;
    }
    char *data = (char *)sound_model + sound_model->data_offset;
    ALOGI("%s data size %d data1 %d - %d", __func__,
    sound_model->data_size, data[0], data[sound_model->data_size - 1]);
    stdev->model_handle = 1;
    stdev->sound_model_callback = callback;
    stdev->sound_model_cookie = cookie;

    *handle = stdev->model_handle;

    //load initial vow
    ALOGI("Initial VOW");
    if (mFd < 0) {
        mFd = open("/dev/vow", O_RDONLY );
    }
    if (mFd <0) {
        ALOGI("open device fail!%s\n", strerror(errno));
    }else {
        ioctl(mFd, VOW_SET_CONTROL, (unsigned long)VOWControlCmd_Init);
    }
    ALOGI("Start Load init Model");
    if(uuid.node[0]=='M'&&uuid.node[1]=='T'&&uuid.node[2]=='K'&&uuid.node[3]=='I'&&uuid.node[4]=='N'&&uuid.node[5]=='C') {
        //load init model
        struct VOW_SoungtriggerTestInfo vow_info;
        vow_info.rawModelFileSizeInByte = sound_model->data_size;
        vow_info.rawModelFilePtr=data;
        ALOGI("Start init vowGetModelSize_Wrap");
        getSizes(&vow_info);
        if (vow_info.rtnModelSize > ALLOCATE_MEMORY_SIZE_MAX) {
            ALOGI("the memory size need to allocate is more than 50K!!!");
            status = -EINVAL;
            pthread_mutex_unlock(&stdev->lock);
            return status;
            //goto exit;
        }
        if (vow_info.rtnModelSize > 0) {
            char *pModelInfo = new char [vow_info.rtnModelSize];//malloc(sizeof(char [vow_info.rtnModelSize]));//new char [vow_info.rtnModelSize];
            if (pModelInfo == NULL) {
                ALOGI("setVoiceUBMFile allocate memory fail!!!");
                status = -EINVAL;
                pthread_mutex_unlock(&stdev->lock);
                return status;
            }
            vow_info.rtnModel = pModelInfo;
            ALOGI("Start init vowTestingInitAP_Wrap(");
            TestingInitAP(&vow_info);
            ALOGI("Start Load speaker Model to VOWKernal");
            VOW_MODEL_INFO_T update_model_info;
            if (mFd < 0) {
                mFd = open("/dev/vow", O_RDONLY );
            }
            if (mFd <0) {
                ALOGI("open device fail!%s\n", strerror(errno));
            }
            else
            {
                update_model_info.size  = (long)vow_info.rtnModelSize;
                update_model_info.addr  = (long)vow_info.rtnModel;
                ioctl(mFd, VOW_SET_SPEAKER_MODEL, (unsigned long)&update_model_info);
            }
            ALOGI("Load speaker Model Finish");
        }
    }
    else {
        if (mFd < 0) {
            mFd = open("/dev/vow", O_RDONLY );
        }
        if (mFd <0) {
            ALOGI("open device fail!%s\n", strerror(errno));
        }
        else {
            VOW_MODEL_INFO_T update_model_info;
            update_model_info.size  = (long)sound_model->data_size;
            update_model_info.addr  = (long)data;
            ioctl(mFd, VOW_SET_SPEAKER_MODEL, (unsigned long)&update_model_info);
            ALOGI("Load 3rd mode Finish");
        }
    }
/*exit:*/
    pthread_mutex_unlock(&stdev->lock);
    return status;
}

static int stdev_unload_sound_model(const struct sound_trigger_hw_device *dev,
                                    sound_model_handle_t handle)
{
    struct stub_sound_trigger_device *stdev = (struct stub_sound_trigger_device *)dev;
    int status = 0;

    ALOGI("%s handle %d", __func__, handle);
    pthread_mutex_lock(&stdev->lock);
    if (handle != 1) {
        status = -EINVAL;
        goto exit;
    }
    if (stdev->model_handle == 0) {
        status = -ENOSYS;
        goto exit;
    }
    stdev->model_handle = 0;
    if (stdev->recognition_callback != NULL) {
        stdev->recognition_callback = NULL;
        pthread_cond_signal(&stdev->cond);
        pthread_mutex_unlock(&stdev->lock);
        pthread_join(stdev->callback_thread, (void **) NULL);
        pthread_mutex_lock(&stdev->lock);
    }

exit:
    pthread_mutex_unlock(&stdev->lock);
    return status;
}

static int stdev_start_recognition(const struct sound_trigger_hw_device *dev,
                                   sound_model_handle_t sound_model_handle,
                                   const struct sound_trigger_recognition_config *config,
                                   recognition_callback_t callback,
                                   void *cookie)
{
    struct stub_sound_trigger_device *stdev = (struct stub_sound_trigger_device *)dev;
    int status = 0;
    ALOGI("Start stdev_start_recognition");
    ALOGI("%s sound model %d", __func__, sound_model_handle);
    pthread_mutex_lock(&stdev->lock);
    if (stdev->model_handle != sound_model_handle) {
        status = -ENOSYS;
        goto exit;
    }
    if (stdev->recognition_callback != NULL) {
        status = -ENOSYS;
        goto exit;
    }
    ALOGI("Start setParameters");
    android::AudioSystem::setParameters(0, String8("MTK_VOW_ENABLE=1"));
    if (config->data_size != 0) {
        char *data = (char *)config + config->data_offset;
        ALOGI("%s data size %d data %d - %d", __func__,
              config->data_size, data[0], data[config->data_size - 1]);
    }
    stoprecognition = 0;
    mphrase_extrasid=config->phrases[0].id;
    ALOGI("startrecognition phrase_extras[0].id %d", mphrase_extrasid);
    stdev->recognition_callback = callback;
    stdev->recognition_cookie = cookie;
    if(sound_trigger_running_state != 1) {
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_create(&stdev->callback_thread, &attr,
                        callback_thread_loop, stdev);
        pthread_attr_destroy(&attr);
        //pthread_create(&stdev->callback_thread, (const pthread_attr_t *) NULL,
        //                callback_thread_loop, stdev);
        sound_trigger_running_state = 1;
    }
exit:
    pthread_mutex_unlock(&stdev->lock);
    return status;
}

static int stdev_stop_recognition(const struct sound_trigger_hw_device *dev,
                                 sound_model_handle_t sound_model_handle)
{
    struct stub_sound_trigger_device *stdev = (struct stub_sound_trigger_device *)dev;
    int status = 0;
    pthread_mutex_lock(&stdev->lock);
    if (stdev->model_handle != sound_model_handle) {
        status = -ENOSYS;
        goto exit;
    }
    if (stdev->recognition_callback == NULL) {
        status = -ENOSYS;
        goto exit;
    }
    stoprecognition = 1;
    ALOGI("+soundtrigger stop recognition");
    android::AudioSystem::setParameters(0, String8("MTK_VOW_ENABLE=0"));
    ALOGI("stdev_stop_recognition: sound model %d",sound_model_handle);
    stdev->recognition_callback = NULL;
    pthread_cond_signal(&stdev->cond);
    pthread_mutex_unlock(&stdev->lock);
    if(sound_trigger_running_state == 1) {
        pthread_join(stdev->callback_thread, (void **) NULL);
    }
    pthread_mutex_lock(&stdev->lock);
exit:
    pthread_mutex_unlock(&stdev->lock);
     ALOGI("-soundtrigger stop recognition");
    return status;
}


static int stdev_close(hw_device_t *device)
{
    free(device);
    return 0;
}

static int stdev_open(const hw_module_t* module, const char* name,
                     hw_device_t** device)
{
    struct stub_sound_trigger_device *stdev;
    int ret;
    ALOGI("Start open device");
    if (strcmp(name, SOUND_TRIGGER_HARDWARE_INTERFACE) != 0)
        return -EINVAL;

    stdev = new stub_sound_trigger_device;//calloc(1, sizeof(struct stub_sound_trigger_device));
    if (!stdev)
        return -ENOMEM;
    ALOGI("Start open device ing");
    stdev->device.common.tag = HARDWARE_DEVICE_TAG;
    stdev->device.common.version = SOUND_TRIGGER_DEVICE_API_VERSION_1_0;
    stdev->device.common.module = (struct hw_module_t *) module;
    stdev->device.common.close = stdev_close;
    stdev->device.get_properties = stdev_get_properties;
    stdev->device.load_sound_model = stdev_load_sound_model;
    stdev->device.unload_sound_model = stdev_unload_sound_model;
    stdev->device.start_recognition = stdev_start_recognition;
    stdev->device.stop_recognition = stdev_stop_recognition;

    pthread_mutex_init(&stdev->lock, (const pthread_mutexattr_t *) NULL);
    pthread_cond_init(&stdev->cond, (const pthread_condattr_t *) NULL);

    *device = &stdev->device.common;
    return 0;
}

static struct hw_module_methods_t hal_module_methods = {
    .open = stdev_open,
};

struct sound_trigger_module HAL_MODULE_INFO_SYM = {
    .common = {
        .tag = HARDWARE_MODULE_TAG,
        .module_api_version = SOUND_TRIGGER_MODULE_API_VERSION_1_0,
        .hal_api_version = HARDWARE_HAL_API_VERSION,
        .id = SOUND_TRIGGER_HARDWARE_MODULE_ID,
        .name = "MTK Audio HW HAL",
        .author = "MTK",
        .methods = &hal_module_methods,
        .dso = NULL,
        .reserved = {0},
    },
};




