/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#ifndef _MTK_CAMERA_VSDOF_BMDENOISE_FEATURE_PIPE_MNR_NODE_H_
#define _MTK_CAMERA_VSDOF_BMDENOISE_FEATURE_PIPE_MNR_NODE_H_

#include "BMDeNoisePipe_Common.h"
#include "BMDeNoisePipeNode.h"

class DpBlitStream;

using namespace NSCam::NSIoPipe::NSPostProc;
using namespace NS3Av3;

namespace NSCam{
namespace NSCamFeature{
namespace NSFeaturePipe{

class MNRNode : public BMDeNoisePipeNode
{
    public:
        MNRNode() = delete;
        MNRNode(const char *name, Graph_T *graph, MINT32 openId);
        virtual ~MNRNode();
        /**
         * Receive EffectRequestPtr from previous node.
         * @param id The id of receiverd data.
         * @param request EffectRequestPtr contains image buffer and some information.
         */
        virtual MBOOL onData(DataID id, EffectRequestPtr &request);
        virtual MBOOL onData(DataID id, ImgInfoMapPtr& data);
    protected:
        virtual MBOOL onInit();
        virtual MBOOL onUninit();
        virtual MBOOL onThreadStart();
        virtual MBOOL onThreadStop();
        virtual MBOOL onThreadLoop();
    private:
        struct EnquedBufPool : public Timer
        {
        public:
            EffectRequestPtr mRequest;
            MNRNode* mpNode;
            Vector<SmartTuningBuffer> mEnqueTuningBuffer;
            KeyedVector<BMDeNoiseBufferID, SmartImageBuffer> mEnquedSmartImgBufMap;
            KeyedVector<BMDeNoiseBufferID, sp<IImageBuffer> > mEnquedIImgBufMap;

            EnquedBufPool(EffectRequestPtr req, MNRNode* deNoiseNode)
            : mRequest(req), mEnquedSmartImgBufMap(), mpNode(deNoiseNode)
            {
            }

            MVOID addBuffData(BMDeNoiseBufferID bufID, SmartImageBuffer pSmBuf)
            { mEnquedSmartImgBufMap.add(bufID, pSmBuf); }

            MVOID addTuningData(SmartTuningBuffer pSmBuf)
            { mEnqueTuningBuffer.add(pSmBuf); }

            MVOID addIImageBuffData(BMDeNoiseBufferID bufID, sp<IImageBuffer> pBuf)
            { mEnquedIImgBufMap.add(bufID, pBuf); }
        };
        class BufferSizeConfig
        {
        public:
            BufferSizeConfig();
            virtual ~BufferSizeConfig();
            MVOID debug() const;

        public:
            MSize BMDENOISE_BMDN_RESULT_SIZE;
        };

        class BufferPoolSet
        {
        public:
            BufferPoolSet();
            virtual ~BufferPoolSet();

            MBOOL init(const BufferSizeConfig& config);
            MBOOL uninit();
        public:
            // buffer pools
            android::sp<ImageBufferPool> mpMNR_Input_bufPool;
            android::sp<TuningBufferPool> mpTuningBuffer_bufPool;
        private:
        };

        struct ISPTuningConfig
        {
            FrameInfoPtr& pInAppMetaFrame;
            FrameInfoPtr& pInHalMetaFrame;
            IHal3A* p3AHAL;
            MBOOL bInputResizeRaw;
        };
    private:
        MVOID cleanUp();
        MVOID doDataDump(IImageBuffer* pBuf, BMDeNoiseBufferID BID, MUINT32 iReqIdx);
        MVOID doInputDataDump(EffectRequestPtr request);
        MVOID doOutputDataDump(EffectRequestPtr request, EnquedBufPool* pEnqueBufferPool);
        MBOOL doMultiPassNR(EffectRequestPtr request);
        TuningParam applyISPTuning(SmartTuningBuffer& targetTuningBuf, const ISPTuningConfig& ispConfig, MBOOL useDefault = MFALSE);
    private:
        WaitQueue<EffectRequestPtr>                     mRequests;

        INormalStream*                                  mpINormalStream = nullptr;
        IHal3A*                                         mp3AHal_Main1 = nullptr;

        MINT32                                          miOpenId = -1;

        BufferPoolSet                                   mBufPoolSet;
        BufferSizeConfig                                mBufConfig;

        StereoSizeProvider*                             mSizePrvider = StereoSizeProvider::getInstance();

        MINT32                                          miEnableMNR = -1;
};
};
};
};
#endif // _MTK_CAMERA_VSDOF_BMDENOISE_FEATURE_PIPE_MNR_NODE_H_