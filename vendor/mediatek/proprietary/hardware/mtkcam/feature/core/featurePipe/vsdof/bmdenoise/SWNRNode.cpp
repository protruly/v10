/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#include <featurePipe/vsdof/vsdof_common.h>
#include <featurePipe/vsdof/vsdof_data_define.h>
// SWNR
#include <capturenr.h>

#include "SWNRNode.h"

#define PIPE_MODULE_TAG "BMDeNoise"
#define PIPE_CLASS_TAG "SWNRNode"
#define PIPE_LOG_TAG PIPE_MODULE_TAG PIPE_CLASS_TAG

// MET tags
#define DO_BM_SWNR "doSoftwareNR"

// buffer alloc size
#define BUFFER_ALLOC_SIZE 1
#define TUNING_ALLOC_SIZE 1

// debug settings
#define USE_DEFAULT_ISP 0
#define USE_DEFAULT_SHADING_GAIN 0

#include <PipeLog.h>

#include <DpBlitStream.h>
#include "../util/vsdof_util.h"

#include <camera_custom_nvram.h>
#include <common/vsdof/hal/stereo_common.h>

using namespace NSCam::NSCamFeature::NSFeaturePipe;
using namespace VSDOF::util;
using namespace NS3Av3;
/*******************************************************************************
 *
 ********************************************************************************/
SWNRNode::BufferSizeConfig::
BufferSizeConfig()
{
    StereoSizeProvider* sizePrvider = StereoSizeProvider::getInstance();
    StereoArea area;

    {
        area = sizePrvider->getBufferSize(E_BM_DENOISE_HAL_OUT);
        BMDENOISE_BMDN_RESULT_SIZE = MSize(area.size.w, area.size.h);
    }

    debug();
}


MVOID
SWNRNode::BufferSizeConfig::debug() const
{
    MY_LOGD("SWNRNode debug size======>\n");
    #define DEBUG_MSIZE(sizeCons) \
        MY_LOGD("size: " #sizeCons " : %dx%d\n", sizeCons.w, sizeCons.h);

    DEBUG_MSIZE( BMDENOISE_BMDN_RESULT_SIZE);
}

SWNRNode::BufferSizeConfig::
~BufferSizeConfig()
{}
/*******************************************************************************
 *
 ********************************************************************************/
SWNRNode::BufferPoolSet::
BufferPoolSet()
{}

SWNRNode::BufferPoolSet::
~BufferPoolSet()
{}

MBOOL
SWNRNode::BufferPoolSet::
init(const BufferSizeConfig& config)
{
    MY_LOGD("BufferPoolSet init +");

    int allocateSize = BUFFER_ALLOC_SIZE;
    int allocateSize_tuning = TUNING_ALLOC_SIZE;

    CAM_TRACE_NAME("SWNRNode::BufferPoolSet::init");
    CAM_TRACE_BEGIN("SWNRNode::BufferPoolSet::init=>create buffer pools");

    // SWNR only support YV12
    mpSWNR_Input_bufPool = ImageBufferPool::create(
        "mpSWNR_Input_bufPool",
        config.BMDENOISE_BMDN_RESULT_SIZE.w,
        config.BMDENOISE_BMDN_RESULT_SIZE.h,
        eImgFmt_YV12, ImageBufferPool::USAGE_HW, MTRUE);
    if(mpSWNR_Input_bufPool == nullptr){
        MY_LOGE("create mpSWNR_Input_bufPool failed!");
        return MFALSE;
    }
    CAM_TRACE_END();

    CAM_TRACE_BEGIN("SWNRNode::BufferPoolSet::init=>allocate buffer pools");
    mpSWNR_Input_bufPool->allocate(allocateSize);
    CAM_TRACE_END();

    MY_LOGD("BufferPoolSet init -");
    return MTRUE;
}

MBOOL
SWNRNode::BufferPoolSet::
uninit()
{
    MY_LOGD("+");

    ImageBufferPool::destroy(mpSWNR_Input_bufPool);

    MY_LOGD("-");
    return MTRUE;
}
/*******************************************************************************
 *
 ********************************************************************************/
/*******************************************************************************
 *
 ********************************************************************************/
SWNRNode::
SWNRNode(const char *name,
    Graph_T *graph,
    MINT32 openId)
    : BMDeNoisePipeNode(name, graph)
    , miOpenId(openId)
{
    MY_LOGD("ctor(0x%x)", this);
    this->addWaitQueue(&mRequests);

    miEnableSWNR = ::property_get_int32("debug.bmdenoise.swnr", 1);
}
/*******************************************************************************
 *
 ********************************************************************************/
SWNRNode::
~SWNRNode()
{
    MY_LOGD("dctor(0x%x)", this);
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
SWNRNode::
onData(
    DataID id,
    EffectRequestPtr &request)
{
    FUNC_START;
    TRACE_FUNC_ENTER();

    MBOOL ret = MFALSE;
    switch(id)
    {
        case BMDENOISE_RESULT:
            mRequests.enque(request);
            ret = MTRUE;
            break;
        default:
            ret = MFALSE;
            MY_LOGE("unknown data id :%d", id);
            break;
    }

    TRACE_FUNC_EXIT();
    FUNC_END;
    return ret;
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
SWNRNode::
onData(
    DataID id,
    ImgInfoMapPtr& pImgInfo)
{
    FUNC_START;
    MY_LOGE("not implemented!");
    FUNC_END;
    return MFALSE;
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
SWNRNode::
onInit()
{
    if(!BMDeNoisePipeNode::onInit()){
        MY_LOGE("BMDeNoisePipeNode::onInit() failed!");
        return MFALSE;
    }

    // BufferPoolSet init
    MY_LOGD("BMSWNRNode::onInit=>BufferPoolSet::init");
    CAM_TRACE_BEGIN("BMSWNRNode::mBufPoolSet::init");

    mBufPoolSet.init(mBufConfig);
    CAM_TRACE_END();

    FUNC_END;
    TRACE_FUNC_EXIT();
    return MTRUE;
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
SWNRNode::
onUninit()
{
    CAM_TRACE_NAME("SWNRNode::onUninit");
    FUNC_START;
    cleanUp();
    FUNC_END;
    return MTRUE;
}
/*******************************************************************************
 *
 ********************************************************************************/
MVOID
SWNRNode::
cleanUp()
{
    FUNC_START;
    mBufPoolSet.uninit();
    FUNC_END;
}
/*******************************************************************************
 *
 ********************************************************************************/
MVOID
SWNRNode::
doDataDump(IImageBuffer* pBuf, BMDeNoiseBufferID BID, MUINT32 iReqIdx)
{
    VSDOF_LOGD("doDataDump: BID:%d +", BID);

    char filepath[1024];
    snprintf(filepath, 1024, "/sdcard/bmdenoise/%d/%s", iReqIdx, getName());

    // make path
    VSDOF_LOGD("makePath: %s", filepath);
    makePath(filepath, 0660);

    const char* writeFileName = onDumpBIDToName(BID);

    char writepath[1024];
    snprintf(writepath,
        1024, "%s/%s_%dx%d.yuv", filepath, writeFileName,
        pBuf->getImgSize().w, pBuf->getImgSize().h);

    pBuf->saveToFile(writepath);

    VSDOF_LOGD("doDataDump: BID:%d -", BID);
}
/*******************************************************************************
 *
 ********************************************************************************/
MVOID
SWNRNode::
doInputDataDump(EffectRequestPtr request)
{
    MUINT32 iReqIdx = request->getRequestNo();
    MY_LOGE("not implemented!");
}
/*******************************************************************************
 *
 ********************************************************************************/
MVOID
SWNRNode::
doOutputDataDump(EffectRequestPtr request, EnquedBufPool* pEnqueBufPool)
{
    MUINT32 iReqIdx = request->getRequestNo();

    if(miTuningDump == 0){
        if(iReqIdx < miDumpStartIdx || iReqIdx >= miDumpStartIdx + miDumpBufSize)
            return;

        FrameInfoPtr framePtr_denoise_final_result = request->vOutputFrameInfo.valueFor(BID_DENOISE_FINAL_RESULT);
        sp<IImageBuffer> frameBuf_denoise_final_result = nullptr;
        framePtr_denoise_final_result->getFrameBuffer(frameBuf_denoise_final_result);
        doDataDump(
                frameBuf_denoise_final_result.get(),
                BID_DENOISE_AND_SWNR_OUT,
                iReqIdx
            );
    }else{
        MY_LOGD("do tuning dump for req(%d) inputs", iReqIdx);
        FrameInfoPtr framePtr_denoise_final_result = request->vOutputFrameInfo.valueFor(BID_DENOISE_FINAL_RESULT);
        sp<IImageBuffer> frameBuf_denoise_final_result = nullptr;
        framePtr_denoise_final_result->getFrameBuffer(frameBuf_denoise_final_result);
        doDataDump(
                frameBuf_denoise_final_result.get(),
                BID_DENOISE_AND_SWNR_OUT,
                iReqIdx
            );
    }
}
/*******************************************************************************
 *
 ********************************************************************************/
MINT32
SWNRNode::
getISOFromMeta(IMetadata* pMeta)
{
    MINT32 ISO = 0;
    MINT32 debugISO = 0;
    IMetadata exifMeta;

    if( tryGetMetadata<IMetadata>(pMeta, MTK_3A_EXIF_METADATA, exifMeta) ) {
        if(!tryGetMetadata<MINT32>(&exifMeta, MTK_3A_EXIF_AE_ISO_SPEED, ISO)){
            MY_LOGE("Get ISO from meta failed, use default value:%d", ISO);
        }
    }
    else {
        MY_LOGE("no tag: MTK_3A_EXIF_METADATA, use default value:%d", ISO);
    }

    debugISO = ::property_get_int32("debug.bmdenoise.iso", -1);

    MY_LOGD("metaISO:%d/debugISO:%d", ISO, debugISO);

    if(debugISO != -1 && debugISO >= 0){
        ISO = debugISO;
    }
    return ISO;
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
SWNRNode::
doSoftwareNR(EffectRequestPtr request)
{
    if(miEnableSWNR != 1){
        MY_LOGD("skip SWNR");
        return MTRUE;
    }

    CAM_TRACE_NAME("SWNRNode::doSoftwareNR");
    MY_LOGD("+, reqID=%d, %d", request->getRequestNo());

    // input metadata
    FrameInfoPtr framePtr_inAppMeta = request->vInputFrameInfo.valueFor(BID_META_IN_APP);
    FrameInfoPtr framePtr_inHalMeta_main1 = request->vInputFrameInfo.valueFor(BID_META_IN_HAL);

    if(framePtr_inAppMeta == nullptr){
        MY_LOGE("framePtr_inAppMeta == nullptr!");
        return MFALSE;
    }

    if(framePtr_inHalMeta_main1 == nullptr){
        MY_LOGE("framePtr_inHalMeta_main1 == nullptr!");
        return MFALSE;
    }

    IMetadata* pMeta_InApp = getMetadataFromFrameInfoPtr(framePtr_inAppMeta);
    IMetadata* pMeta_InHal_main1 = getMetadataFromFrameInfoPtr(framePtr_inHalMeta_main1);

    if(pMeta_InApp == nullptr){
        MY_LOGE("pMeta_InApp == nullptr!");
        return MFALSE;
    }

    if(pMeta_InHal_main1 == nullptr){
        MY_LOGE("pMeta_InHal_main1 == nullptr!");
        return MFALSE;
    }

    // output buffer.
    // This is an in-place processing so input buffer equals output buffer
    FrameInfoPtr framePtr_denoise_final_result = request->vOutputFrameInfo.valueFor(BID_DENOISE_FINAL_RESULT);
    sp<IImageBuffer> frameBuf_denoise_final_result = nullptr;
    framePtr_denoise_final_result->getFrameBuffer(frameBuf_denoise_final_result);
    if(frameBuf_denoise_final_result == nullptr){
        MY_LOGE("frameBuf_denoise_final_result == nullptr!");
        return MFALSE;
    }

    // prepare input buffer
    SmartImageBuffer pImgInput = mBufPoolSet.mpSWNR_Input_bufPool->request();
    if(pImgInput == nullptr){
        MY_LOGE("pImgInput == nullptr!");
        return MFALSE;
    }

    {
        Timer localTimer;
        localTimer.start();

         // convert format
        if (!formatConverter(frameBuf_denoise_final_result.get(), pImgInput->mImageBuffer.get())) {
            MY_LOGE("SWNR input format convert failed");
            return MFALSE;
        }

        localTimer.stop();
        VSDOF_LOGD("BMDeNoise_Profile: featurePipe SWNRNode convert input buffer format time(%d ms) reqID=%d",
            localTimer.getElapsed(),
            request->getRequestNo()
        );
    }

    // do SWNR
    {
        SwNR::SWNRParam swnrParam;
        swnrParam.isDualCam = MTRUE;
        std::unique_ptr<SwNR> swnr = std::unique_ptr<SwNR>(new SwNR(mSensorIdx_Main1));

        swnrParam.iso = getISOFromMeta(pMeta_InHal_main1);
        MY_LOGD("Run SwNR with iso=%d", swnrParam.iso);

        Timer localTimer;
        localTimer.start();
        {
            if (!swnr->doSwNR(swnrParam, pImgInput->mImageBuffer.get())) {
                MY_LOGE("SWNR failed");
            }
            else {
                MY_LOGD("applied SWNR");
            }
        }

        localTimer.stop();
        VSDOF_LOGD("BMDeNoise_Profile: featurePipe SWNRNode doSoftwareNR time(%d ms) reqID=%d",
            localTimer.getElapsed(),
            request->getRequestNo()
        );
    }

    // convert result image back
    {
        Timer localTimer;
        localTimer.start();

         // convert format
        if (!formatConverter(pImgInput->mImageBuffer.get(), frameBuf_denoise_final_result.get())) {
            MY_LOGE("SWNR output format convert failed");
            return MFALSE;
        }

        localTimer.stop();
        VSDOF_LOGD("BMDeNoise_Profile: featurePipe SWNRNode convert output buffer format time(%d ms) reqID=%d",
            localTimer.getElapsed(),
            request->getRequestNo()
        );
    }

    MY_LOGD("-, reqID=%d", request->getRequestNo());
    return MTRUE;
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
SWNRNode::
onThreadStart()
{
    return MTRUE;
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
SWNRNode::
onThreadStop()
{
    return MTRUE;
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
SWNRNode::
onThreadLoop()
{
    FUNC_START;
    EffectRequestPtr effectRequest = nullptr;

    if( !waitAllQueue() )// block until queue ready, or flush() breaks the blocking state too.
    {
        return MFALSE;
    }
    if( !mRequests.deque(effectRequest) )
    {
        MY_LOGD("mRequests.deque() failed");
        return MFALSE;
    }

    CAM_TRACE_NAME("SWNRNode::onThreadLoop");

    // get request type
    const sp<EffectParameter> params = effectRequest->getRequestParameter();
    MINT32 requestType = params->getInt(BMDENOISE_REQUEST_TYPE_KEY);

    // doInputDataDump(effectRequest);

    MY_LOGD("SWNRNode get requestType:%d", requestType);

    if(!doSoftwareNR(effectRequest)){
        MY_LOGE("do SWNR failed, please check error msgs!");
    }

    doOutputDataDump(effectRequest, nullptr);

    handleData(SWNR_RESULT, effectRequest);

    FUNC_END;
    return MTRUE;
}
