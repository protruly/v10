/* Ver 20151228 */
/* parser_print_info_log_to_file can be defined by platform printf for easy debug */
/* all buffer pointers must by 4 bytes aligned */
/* all pdo strides must be x4 bytes */
/* bpci_xsize count from 0 used for boundary check */
/* pdo_a, pdo_b, & pdo_merge with same pdo_ysize count from 0 used for boundary check */
/* pmx_xx, bmx_xx from pass 1 & twin driver */
/* if pdo_merge() returns 1, the function failed & needed to debug */
/* PD_prd_dir[n] recording R/L properties can extend pdo_merge with R/L dispatch function by ME14 */

#ifndef parser_print_info_log_to_file
#define parser_print_info_log_to_file(...)
#endif

#define LOG_TAG "pd_mgr_pdomerger"

#ifndef ENABLE_MY_LOG
#define ENABLE_MY_LOG       (1)
#endif

#include <Trace.h>
#include <common/include/common.h>
#include <cutils/properties.h>
#include <aaa_types.h>
#include <aaa_error_code.h>
#include <aaa_log.h>
#include "pd_mgr.h"
#include <StatisticBuf.h>

#include <pd_buf_mgr_open.h>
#include <pd_buf_mgr.h>

static int getPropSet(char* propStr)
{
    static char value[PROPERTY_VALUE_MAX] = {'\0'};
    property_get( propStr, value, "0");
    return atoi(value);
}

using namespace NSCam;


namespace NS3Av3
{
MINT32 PDMgr::PDOMerge( MVOID *ptrInPDOData, SPDOBUFINFO_T &outsPDOBuf)
{
    MY_LOG_IF( m_bDebugEnable, "%s+", __FUNCTION__);

    StatisticBufInfo *ptrPDOHWData = reinterpret_cast<StatisticBufInfo *>( ptrInPDOData);

    MUINT32    MagicNumber = ptrPDOHWData->mMagicNumber;
    MUINT32    Size = ptrPDOHWData->mSize;
    MUINTPTR   Va = ptrPDOHWData->mVa;
    MUINT32    Stride = ptrPDOHWData->mStride;
    MUINTPTR   Pa_offset = ptrPDOHWData->mPa_offset;

    MVOID     *PrivateData = ptrPDOHWData->mPrivateData;
    MUINT32    PrivateDataSize = ptrPDOHWData->mPrivateDataSize;

    if( Pa_offset!=0)
    {
        MY_LOG_IF( m_bDebugEnable,
                   "%s Magic(%d), Sz(%x), Va(%x), Stride(%x), Pa_offset(%x)",
                   __FUNCTION__,
                   MagicNumber,
                   Size,
                   Va,
                   Stride,
                   Pa_offset);

        MY_LOG_IF( m_bDebugEnable,
                   "%s [0] PMX_A_STR(%x), PMX_A_END(%x)",
                   __FUNCTION__,
                   ((MRect*)PrivateData)[0].p.x,
                   ((MRect*)PrivateData)[0].p.y);

        MY_LOG_IF( m_bDebugEnable,
                   "%s [1] PMX_B_STR(%x), PMX_B_END(%x)",
                   __FUNCTION__,
                   ((MRect*)PrivateData)[1].p.x,
                   ((MRect*)PrivateData)[1].p.y);

        MY_LOG_IF( m_bDebugEnable,
                   "%s [2] BMX_A_STR(%x), BMX_A_END(%x)",
                   __FUNCTION__,
                   ((MRect*)PrivateData)[2].p.x,
                   ((MRect*)PrivateData)[2].p.y);

        MY_LOG_IF( m_bDebugEnable,
                   "%s [3] BMX_B_STR(%x), BMX_B_END(%x)",
                   __FUNCTION__,
                   ((MRect*)PrivateData)[3].p.x,
                   ((MRect*)PrivateData)[3].p.y);

        if( m_rMemBufInfo.virtAddr!=0 && m_pPDOMergeBuf!=NULL && m_pPDOMergeBuf_Stride!=0)
        {
            /* twin mode is on, need do pdo merge*/
            PDO_MERGE_IN_CONFIG_T pdo_merge_cfg;
            pdo_merge_cfg.ptr_in_pdo_a  = (unsigned int *)(ptrPDOHWData->mVa);
            pdo_merge_cfg.ptr_in_pdo_b  = (unsigned int *)(ptrPDOHWData->mVa + ptrPDOHWData->mPa_offset);
            pdo_merge_cfg.ptr_in_bpci   = (unsigned int *)(m_rMemBufInfo.virtAddr); // pd_mgr is in charge of bpci table information.
            pdo_merge_cfg.ptr_out_merge = (unsigned int *)(m_pPDOMergeBuf);
            pdo_merge_cfg.bpci_xsize    = m_sPDOHWInfo.u4Bpci_xsz;
            pdo_merge_cfg.pdo_a_stride  = ptrPDOHWData->mStride;
            pdo_merge_cfg.pdo_b_stride  = ptrPDOHWData->mStride;
            pdo_merge_cfg.merge_stride  = m_pPDOMergeBuf_Stride;
            pdo_merge_cfg.pmx_a_end_x   = ((MRect*)PrivateData)[0].p.y;
            pdo_merge_cfg.bmx_a_end_x   = ((MRect*)PrivateData)[2].p.y;
            pdo_merge_cfg.bmx_b_str_x   = ((MRect*)PrivateData)[3].p.x;
            pdo_merge_cfg.bmx_b_end_x   = ((MRect*)PrivateData)[3].p.y;
            pdo_merge_cfg.pdo_ysize     = m_sPDOHWInfo.u4Pdo_ysz;

            /* merge PDOA and PDOB */
            if (m_i4DbgDisPdoMergeSimple)
            {
                MY_LOG_IF( m_bDebugEnable, "%s pdo_merge +", __FUNCTION__);
                CAM_TRACE_BEGIN("pdo_merge");
                pdo_merge( &pdo_merge_cfg);
                CAM_TRACE_END();
                MY_LOG_IF( m_bDebugEnable, "%s pdo_merge -", __FUNCTION__);
            }
            else
            {
                MY_LOG_IF( m_bDebugEnable, "%s pdo_merge_simple +", __FUNCTION__);
                CAM_TRACE_BEGIN("pdo_merge_simple");
                pdo_merge_simple( &pdo_merge_cfg);
                CAM_TRACE_END();
                MY_LOG_IF( m_bDebugEnable, "%s pdo_merge_simple -", __FUNCTION__);
            }

            if(m_i4DbgPdDump)
            {
                char fileName[128];
                sprintf(fileName, "/sdcard/pdo/pdo_res_%d.raw", MagicNumber);
                FILE *fp = fopen(fileName, "w");
                if (NULL == fp)
                {
                    MINT32 err = mkdir("/sdcard/pdo", S_IRWXU | S_IRWXG | S_IRWXO);
                    MY_LOG("create folder /sdcard/pdo", err);
                }
                else
                {
                    fwrite(reinterpret_cast<void *>(m_pPDOMergeBuf), 1, pdo_merge_cfg.merge_stride*(pdo_merge_cfg.pdo_ysize+1), fp);
                    fclose(fp);
                }

                sprintf(fileName, "/sdcard/pdo/pdo_input_%d.bin", MagicNumber);
                fp = fopen(fileName, "w");
                if (NULL == fp)
                {
                    MINT32 err = mkdir("/sdcard/pdo", S_IRWXU | S_IRWXG | S_IRWXO);
                    MY_LOG("create folder /sdcard/pdo", err);
                }
                else
                {
                    fwrite(reinterpret_cast<void *>(ptrPDOHWData->mVa), 1, ptrPDOHWData->mSize, fp);
                    fclose(fp);
                }
                sprintf(fileName, "/sdcard/pdo/bpci_input_%d.bin", MagicNumber);
                fp = fopen(fileName, "w");
                if (NULL == fp)
                {
                    MINT32 err = mkdir("/sdcard/pdo", S_IRWXU | S_IRWXG | S_IRWXO);
                    MY_LOG("create folder /sdcard/pdo", err);
                }
                else
                {
                    fwrite(reinterpret_cast<void *>(m_rMemBufInfo.virtAddr), 1, m_rMemBufInfo.size, fp);
                    fclose(fp);
                }
                sprintf(fileName, "/sdcard/pdo/pdo_param_%d.txt", MagicNumber);
                fp = fopen(fileName, "w");
                if (NULL == fp)
                {
                    MINT32 err = mkdir("/sdcard/pdo", S_IRWXU | S_IRWXG | S_IRWXO);
                    MY_LOG("create folder /sdcard/pdo", err);
                }
                else
                {
                    fprintf(fp, "ptrPDOHWData->mPa_offset = %d\n", ptrPDOHWData->mPa_offset);
                    fprintf(fp, "pdo_merge_cfg.bpci_xsize = %d\n", pdo_merge_cfg.bpci_xsize);
                    fprintf(fp, "pdo_merge_cfg.pdo_a_stride  = %d\n", pdo_merge_cfg.pdo_a_stride);
                    fprintf(fp, "pdo_merge_cfg.pdo_b_stride  = %d\n", pdo_merge_cfg.pdo_b_stride);
                    fprintf(fp, "pdo_merge_cfg.merge_stride  = %d\n", pdo_merge_cfg.merge_stride);
                    fprintf(fp, "pdo_merge_cfg.pmx_a_end_x  = %d\n", pdo_merge_cfg.pmx_a_end_x);
                    fprintf(fp, "pdo_merge_cfg.bmx_a_end_x  = %d\n", pdo_merge_cfg.bmx_a_end_x);
                    fprintf(fp, "pdo_merge_cfg.bmx_b_str_x  = %d\n", pdo_merge_cfg.bmx_b_str_x);
                    fprintf(fp, "pdo_merge_cfg.bmx_b_end_x  = %d\n", pdo_merge_cfg.bmx_b_end_x);
                    fprintf(fp, "pdo_merge_cfg.pdo_ysize  = %d\n", pdo_merge_cfg.pdo_ysize);
                    fclose(fp);
                }
            }

            // PDO final output
            outsPDOBuf.u1buf    = m_pPDOMergeBuf;
            outsPDOBuf.u4stride = m_pPDOMergeBuf_Stride;
            outsPDOBuf.u4sz     = m_pPDOMergeBuf_Sz;
        }
        else
        {
            outsPDOBuf.u1buf    = (MUINT8 *)ptrPDOHWData->mVa;
            outsPDOBuf.u4stride = ptrPDOHWData->mStride;
            outsPDOBuf.u4sz     = ptrPDOHWData->mSize;
        }
    }

    MY_LOG_IF( m_bDebugEnable, "%s-", __FUNCTION__);

    return 0;
}
MINT32 PDMgr::pdo_merge(PDO_MERGE_IN_CONFIG_T *ptr_in_config)
{
    if (((size_t)ptr_in_config->ptr_in_pdo_a & 0x3) || ((size_t)ptr_in_config->ptr_in_pdo_b & 0x3) ||
        ((size_t)ptr_in_config->ptr_out_merge & 0x3) || ((size_t)ptr_in_config->ptr_in_bpci & 0x3) ||
        ((size_t)ptr_in_config->pdo_a_stride & 0x3) || ((size_t)ptr_in_config->pdo_b_stride & 0x3) ||
        ((size_t)ptr_in_config->merge_stride & 0x3))
    {
        /* error early terminated */
        MY_ERR("early terminated");
        MY_ERR("ptr_in_pdo_a(0x%x), ptr_in_pdo_b(0x%x), ptr_out_merge(0x%x), ptr_in_bpci(0x%x), pdo_a_stride(0x%x), pdo_b_stride(0x%x), merge_stride(0x%x)",
               ptr_in_config->ptr_in_pdo_a,
               ptr_in_config->ptr_in_pdo_b,
               ptr_in_config->ptr_out_merge,
               ptr_in_config->ptr_in_bpci,
               ptr_in_config->pdo_a_stride,
               ptr_in_config->pdo_b_stride,
               ptr_in_config->merge_stride);
        return 1;
    }
    else
    {
        int i = 0;/* pdc short count */
        int bpci_y = 0;
        int pdo_y = 0;
        int bpci_count = (unsigned int)(ptr_in_config->bpci_xsize + 1)>>1;/* short */
        int pdo_a_valid = ptr_in_config->bmx_a_end_x + 1;
        int pdo_a_end = ptr_in_config->pmx_a_end_x;
        int pdo_b_start = pdo_a_valid - ptr_in_config->bmx_b_str_x;
        int pdo_all = ptr_in_config->bmx_b_end_x - ptr_in_config->bmx_b_str_x + 1 + pdo_a_valid;
        unsigned int *ptr_pdo_a = ptr_in_config->ptr_in_pdo_a;
        unsigned int *ptr_pdo_b = ptr_in_config->ptr_in_pdo_b;
        unsigned int *ptr_merge = ptr_in_config->ptr_out_merge;
        unsigned int *ptr_bpci = ptr_in_config->ptr_in_bpci;
        unsigned int bpc_i_buffer_0 = *ptr_bpci++;
        unsigned int bpc_i_buffer_1 = (bpci_count>=4)?*ptr_bpci:0x0;/* check bpci_count>=4 */
        unsigned short bpc_i_buffer = bpc_i_buffer_0 & 0xFFFF;
        while (i < bpci_count)
        {
            /* found pdc line */
            if (bpc_i_buffer >= 0xC000)
            {
                int j = 0;
                unsigned int pdo_a_buffer_0 = *ptr_pdo_a++;
                unsigned int pdo_a_buffer_1 = *ptr_pdo_a;
                unsigned short pdo_a_buffer = pdo_a_buffer_0 & 0xFFFF;
                int k = 0;
                unsigned int pdo_b_buffer_0 = *ptr_pdo_b++;
                unsigned int pdo_b_buffer_1 = *ptr_pdo_b;
                unsigned short pdo_b_buffer = pdo_b_buffer_0 & 0xFFFF;
                int m = 0;
                unsigned int merge_buffer = 0;
                int PD_prd_num = 0;
                int PD_prd_val[16];
                int PD_prd_dir[16];
                int x_pos = 0;/* pos */
                int x_size = 0;/* max pos */
                bool pdo_a_found = false;
                bool pdo_b_found = false;
                bpci_y = (int)bpc_i_buffer - (int)0xC000;
                /* update buffer */
                i++;
                if (i >= bpci_count)
                {
                    /* error early terminated */
                    MY_ERR("early terminated");
                    return 1;
                }
                if (0x0 == (i & 0x3))
                {
                    bpc_i_buffer = bpc_i_buffer_0 & 0xFFFF;
                }
                else if (0x1 == (i & 0x3))
                {
                    bpc_i_buffer = (bpc_i_buffer_0>>16) & 0xFFFF;
                    if (i + 4 < bpci_count)
                    {
                        ptr_bpci++;
                        bpc_i_buffer_0 = *ptr_bpci;
                    }
                    else if (i + 3 < bpci_count)
                    {
                        /* end short */
                        ptr_bpci++;
                        bpc_i_buffer_0 = (*(unsigned short *)ptr_bpci);
                    }
                }
                else if (0x2 == (i & 0x3))
                {
                    bpc_i_buffer = bpc_i_buffer_1 & 0xFFFF;
                }
                else
                {
                    bpc_i_buffer = (bpc_i_buffer_1>>16) & 0xFFFF;
                    if (i + 4 < bpci_count)
                    {
                        ptr_bpci++;
                        bpc_i_buffer_1 = *ptr_bpci;
                    }
                    else if (i + 3 < bpci_count)
                    {
                        /* end short */
                        ptr_bpci++;
                        bpc_i_buffer_1 = (*(unsigned short *)ptr_bpci) & 0xFFFF;
                    }
                }
                /* start offset */
                x_pos = (int)bpc_i_buffer;
                /* update buffer */
                i++;
                if (i >= bpci_count)
                {
                    /* error early terminated */
                    MY_ERR("early terminated");
                    return 1;
                }
                if (0x0 == (i & 0x3))
                {
                    bpc_i_buffer = bpc_i_buffer_0 & 0xFFFF;
                }
                else if (0x1 == (i & 0x3))
                {
                    bpc_i_buffer = (bpc_i_buffer_0>>16) & 0xFFFF;
                    if (i + 4 < bpci_count)
                    {
                        ptr_bpci++;
                        bpc_i_buffer_0 = *ptr_bpci;
                    }
                    else if (i + 3 < bpci_count)
                    {
                        /* end short */
                        ptr_bpci++;
                        bpc_i_buffer_0 = *(unsigned short *)ptr_bpci & 0xFFFF;
                    }
                }
                else if (0x2 == (i & 0x3))
                {
                    bpc_i_buffer = bpc_i_buffer_1 & 0xFFFF;
                }
                else
                {
                    bpc_i_buffer = (bpc_i_buffer_1>>16) & 0xFFFF;
                    if (i + 4 < bpci_count)
                    {
                        ptr_bpci++;
                        bpc_i_buffer_1 = *ptr_bpci;
                    }
                    else if (i + 3 < bpci_count)
                    {
                        /* end short */
                        ptr_bpci++;
                        bpc_i_buffer_1 = *(unsigned short *)ptr_bpci & 0xFFFF;
                    }
                }
                /* valid length */
                x_size = x_pos + (int)bpc_i_buffer;
                if (x_size > pdo_all)
                {
                    x_size = pdo_all;/* limit x size */
                }
                /* update buffer */
                i++;
                if (i >= bpci_count)
                {
                    /* error early terminated */
                    MY_ERR("early terminated");
                    return 1;
                }
                if (0x0 == (i & 0x3))
                {
                    bpc_i_buffer = bpc_i_buffer_0 & 0xFFFF;
                }
                else if (0x1 == (i & 0x3))
                {
                    bpc_i_buffer = (bpc_i_buffer_0>>16) & 0xFFFF;
                    if (i + 4 < bpci_count)
                    {
                        ptr_bpci++;
                        bpc_i_buffer_0 = *ptr_bpci;
                    }
                    else if (i + 3 < bpci_count)
                    {
                        /* end short */
                        ptr_bpci++;
                        bpc_i_buffer_0 = *(unsigned short *)ptr_bpci & 0xFFFF;
                    }
                }
                else if (0x2 == (i & 0x3))
                {
                    bpc_i_buffer = bpc_i_buffer_1 & 0xFFFF;
                }
                else
                {
                    bpc_i_buffer = (bpc_i_buffer_1>>16) & 0xFFFF;
                    if (i + 4 < bpci_count)
                    {
                        ptr_bpci++;
                        bpc_i_buffer_1 = *ptr_bpci;
                    }
                    else if (i + 3 < bpci_count)
                    {
                        /* end short */
                        ptr_bpci++;
                        bpc_i_buffer_1 = *(unsigned short *)ptr_bpci & 0xFFFF;
                    }
                }
                /* period number */
                PD_prd_num = (int)(bpc_i_buffer & 0xF);
                for (int n = 0; n <= PD_prd_num; n++)
                {
                    /* update buffer */
                    i++;
                    if (i >= bpci_count)
                    {
                        /* error early terminated */
                        MY_ERR("early terminated");
                        return 1;
                    }
                    if (0x0 == (i & 0x3))
                    {
                        bpc_i_buffer = bpc_i_buffer_0 & 0xFFFF;
                    }
                    else if (0x1 == (i & 0x3))
                    {
                        bpc_i_buffer = (bpc_i_buffer_0>>16) & 0xFFFF;
                        if (i + 4 < bpci_count)
                        {
                            ptr_bpci++;
                            bpc_i_buffer_0 = *ptr_bpci;
                        }
                        else if (i + 3 < bpci_count)
                        {
                            /* end short */
                            ptr_bpci++;
                            bpc_i_buffer_0 = *(unsigned short *)ptr_bpci & 0xFFFF;
                        }
                    }
                    else if (0x2 == (i & 0x3))
                    {
                        bpc_i_buffer = bpc_i_buffer_1 & 0xFFFF;
                    }
                    else
                    {
                        bpc_i_buffer = (bpc_i_buffer_1>>16) & 0xFFFF;
                        if (i + 4 < bpci_count)
                        {
                            ptr_bpci++;
                            bpc_i_buffer_1 = *ptr_bpci;
                        }
                        else if (i + 3 < bpci_count)
                        {
                            /* end short */
                            ptr_bpci++;
                            bpc_i_buffer_1 = *(unsigned short *)ptr_bpci & 0xFFFF;
                        }
                    }
                    /* period & direction */
                    PD_prd_val[n] = (int)(bpc_i_buffer & 0x3FFF);
                    PD_prd_dir[n] = (int)((bpc_i_buffer & 0x4000)?1:0);
                }
                /* merge */
                while (x_pos < x_size)
                {
                    int n = 0;
                    do
                    {
                        if (x_pos < pdo_a_valid)
                        {
                            /* update merge */
                            if (2*m >= ptr_in_config->merge_stride)
                            {
                                /* error early terminated */
                                MY_ERR("early terminated");
                                return 1;
                            }
                            if (m & 0x1)
                            {
                                merge_buffer |= pdo_a_buffer<<16;
                                *ptr_merge = merge_buffer;
                                ptr_merge++;
                            }
                            else
                            {
                                merge_buffer = pdo_a_buffer;
                            }
                            m++;
                            /* valid pdo_a */
                            pdo_a_found = true;
                            /* update pdo_a */
                            j++;
                            if (2*j >= ptr_in_config->pdo_a_stride)
                            {
                                /* error early terminated */
                                MY_ERR("early terminated");
                                return 1;
                            }
                            if (0x0 == (j & 0x3))
                            {
                                pdo_a_buffer = pdo_a_buffer_0 & 0xFFFF;
                            }
                            else if (0x1 == (j & 0x3))
                            {
                                pdo_a_buffer = (pdo_a_buffer_0>>16) & 0xFFFF;
                                ptr_pdo_a++;
                                pdo_a_buffer_0 = *ptr_pdo_a;
                            }
                            else if (0x2 == (j & 0x3))
                            {
                                pdo_a_buffer = pdo_a_buffer_1 & 0xFFFF;
                            }
                            else
                            {
                                pdo_a_buffer = (pdo_a_buffer_1>>16) & 0xFFFF;
                                ptr_pdo_a++;
                                pdo_a_buffer_1 = *ptr_pdo_a;
                            }
                            if (x_pos >= pdo_b_start)
                            {
                                /* skip pdo_b */
                                pdo_b_found = true;
                                /* update pdo_b */
                                k++;
                                if (2*k >= ptr_in_config->pdo_b_stride)
                                {
                                    /* error early terminated */
                                    MY_ERR("early terminated");
                                    return 1;
                                }
                                if (0x0 == (k & 0x3))
                                {
                                    pdo_b_buffer = pdo_b_buffer_0 & 0xFFFF;
                                }
                                else if (0x1 == (k & 0x3))
                                {
                                    pdo_b_buffer = (pdo_b_buffer_0>>16) & 0xFFFF;
                                    ptr_pdo_b++;
                                    pdo_b_buffer_0 = *ptr_pdo_b;
                                }
                                else if (0x2 == (k & 0x3))
                                {
                                    pdo_b_buffer = pdo_b_buffer_1 & 0xFFFF;
                                }
                                else
                                {
                                    pdo_b_buffer = (pdo_b_buffer_1>>16) & 0xFFFF;
                                    ptr_pdo_b++;
                                    pdo_b_buffer_1 = *ptr_pdo_b;
                                }
                            }
                        }
                        else
                        {
                            /* update merge */
                            if (2*m >= ptr_in_config->merge_stride)
                            {
                                /* error early terminated */
                                MY_ERR("early terminated");
                                return 1;
                            }
                            if (m & 0x1)
                            {
                                merge_buffer |= pdo_b_buffer<<16;
                                *ptr_merge = merge_buffer;
                                ptr_merge++;
                            }
                            else
                            {
                                merge_buffer = pdo_b_buffer;
                            }
                            m++;
                            /* valid pdo_b */
                            pdo_b_found = true;
                            /* update pdo_b */
                            k++;
                            if (2*k >= ptr_in_config->pdo_b_stride)
                            {
                                /* error early terminated */
                                MY_ERR("early terminated");
                                return 1;
                            }
                            if (0x0 == (k & 0x3))
                            {
                                pdo_b_buffer = pdo_b_buffer_0 & 0xFFFF;
                            }
                            else if (0x1 == (k & 0x3))
                            {
                                pdo_b_buffer = (pdo_b_buffer_0>>16) & 0xFFFF;
                                ptr_pdo_b++;
                                pdo_b_buffer_0 = *ptr_pdo_b;
                            }
                            else if (0x2 == (k & 0x3))
                            {
                                pdo_b_buffer = pdo_b_buffer_1 & 0xFFFF;
                            }
                            else
                            {
                                pdo_b_buffer = (pdo_b_buffer_1>>16) & 0xFFFF;
                                ptr_pdo_b++;
                                pdo_b_buffer_1 = *ptr_pdo_b;
                            }
                            if (x_pos <= pdo_a_end)
                            {
                                /* skip pdo_a */
                                pdo_a_found = true;
                            }
                        }
                        x_pos += PD_prd_val[n];
                        n++;
                        if (x_pos >= x_size)
                        {
                            break;/* end of line */
                        }
                    }
                    while (n <= PD_prd_num);
                }
                if (pdo_a_found && pdo_b_found)
                {
                    pdo_y++;
                    ptr_pdo_a = ptr_in_config->ptr_in_pdo_a + pdo_y*((unsigned int)ptr_in_config->pdo_a_stride>>2);
                    ptr_pdo_b = ptr_in_config->ptr_in_pdo_b + pdo_y*((unsigned int)ptr_in_config->pdo_b_stride>>2);
                    /* end of line 0x0, 0x40 */
                    if (2*m >= ptr_in_config->merge_stride)
                    {
                        /* error early terminated */
                        MY_ERR("early terminated");
                        return 1;
                    }
                    if (m & 0x1)
                    {
                        merge_buffer |= 0x4000<<16;
                        *ptr_merge = merge_buffer;
                    }
                    else
                    {
                        *(short *)ptr_merge = 0x4000;
                    }
                    ptr_merge = ptr_in_config->ptr_out_merge + pdo_y*((unsigned int)ptr_in_config->merge_stride>>2);
                    if (pdo_y > ptr_in_config->pdo_ysize)
                    {
                        break;/* end of last line, early terminated */
                    }
                }
                else
                {
                    /* error early terminated */
                    MY_ERR("Error: pdo_a_found: %d, pdo_b_found: %d\n", pdo_a_found, pdo_b_found);
                    MY_ERR("Error: count: %d, bpci_y: %d, x_size: %d, PD_prd_num: %d\n", i, bpci_y, x_size, PD_prd_num);
                    MY_ERR("Error: x_pos: %d, x_size: %d\n", x_pos, x_size);
                    return 1;
                }
            }
            /* update buffer */
            i++;
            if (i >= bpci_count)
            {
                /* early terminated */
                break;
            }
            if (0x0 == (i & 0x3))
            {
                bpc_i_buffer = bpc_i_buffer_0 & 0xFFFF;
            }
            else if (0x1 == (i & 0x3))
            {
                bpc_i_buffer = (bpc_i_buffer_0>>16) & 0xFFFF;
                if (i + 4 < bpci_count)
                {
                    ptr_bpci++;
                    bpc_i_buffer_0 = *ptr_bpci;
                }
                else if (i + 3 < bpci_count)
                {
                    /* end short */
                    ptr_bpci++;
                    bpc_i_buffer_0 = *(unsigned short *)ptr_bpci & 0xFFFF;
                }
            }
            else if (0x2 == (i & 0x3))
            {
                bpc_i_buffer = bpc_i_buffer_1 & 0xFFFF;
            }
            else
            {
                bpc_i_buffer = (bpc_i_buffer_1>>16) & 0xFFFF;
                if (i + 4 < bpci_count)
                {
                    ptr_bpci++;
                    bpc_i_buffer_1 = *ptr_bpci;
                }
                else if (i + 3 < bpci_count)
                {
                    /* end short */
                    ptr_bpci++;
                    bpc_i_buffer_1 = *(unsigned short *)ptr_bpci & 0xFFFF;
                }
            }
        }
        /* end of frame */
        *(short *)ptr_merge = (short)0x8000;
        return 0;
    }
    return 0;
}

MINT32 PDMgr::pdo_merge_simple(PDO_MERGE_IN_CONFIG_T *ptr_in_config)
{
    int ret_code = 0;
    int pdo_y = 0;
    int bpci_count = (unsigned int)(ptr_in_config->bpci_xsize + 1)>>1;/* short */
    int pdo_a_valid = ptr_in_config->bmx_a_end_x + 1;
    int pdo_b_start = pdo_a_valid - ptr_in_config->bmx_b_str_x;
    unsigned short *ptr_pdo_a_start = (unsigned short *)ptr_in_config->ptr_in_pdo_a;
    unsigned short *ptr_pdo_b_start = (unsigned short *)ptr_in_config->ptr_in_pdo_b;
    unsigned short *ptr_merge_start = (unsigned short *)ptr_in_config->ptr_out_merge;
    unsigned short *ptr_bpci = (unsigned short *)ptr_in_config->ptr_in_bpci;

    // line offset
    int pdo_a_ofst = ptr_in_config->pdo_a_stride >> 1;
    int pdo_b_ofst = ptr_in_config->pdo_b_stride >> 1;
    int merge_ofst = ptr_in_config->merge_stride >> 1;

    while (pdo_y <= ptr_in_config->pdo_ysize)
    {
        // get data from bpci table
        unsigned int bpci_value = *ptr_bpci++;

        // set start pointer
        unsigned short *ptr_pdo_a = ptr_pdo_a_start;
        unsigned short *ptr_pdo_b = ptr_pdo_b_start;
        unsigned short *ptr_merge = ptr_merge_start;

        // check first 2 bit of 16-bit data
        if (bpci_value >= 0xC000)
        {
            int x_pos = *ptr_bpci++;
            int x_size = x_pos + *ptr_bpci++;
            int PD_prd_num = *ptr_bpci++;
            int PD_prd_val[16];
            for (int n = 0; n <= PD_prd_num; n++)
            {
                // read data
                bpci_value = *ptr_bpci++;

                // period
                PD_prd_val[n] = bpci_value & 0x3FFF;
            }

            // merge from pdo_b to pdo_a
            while (x_pos < x_size)
            {
                for (int n = 0; n <= PD_prd_num; n++)
                {
                    if (x_pos < pdo_a_valid)
                    {
                        *ptr_merge++ = *ptr_pdo_a++;
                        if (x_pos >= pdo_b_start)
                        {
                            ptr_pdo_b++;
                        }
                    }
                    else
                    {
                        *ptr_merge++ = *ptr_pdo_b++;
                    }
                    x_pos += PD_prd_val[n];
                }
            }

            // file in marker of line's end
            *ptr_merge = 0x4000;

            // change pointer to next line
            pdo_y++;
            ptr_pdo_a_start += pdo_a_ofst;
            ptr_pdo_b_start += pdo_b_ofst;
            ptr_merge_start += merge_ofst;
        }
    }

    // fill in marker of frame's end
    *ptr_merge_start = 0x8000;

    return ret_code;
}

};  //  namespace NS3A
