#include "frame_2/dpe_test_case_00_frame_2.h"
#include "frame_3/dpe_test_case_00_frame_3.h"
#include <common/include/common.h>
#include <iopipe/PostProc/IDpeStream.h>
#include <drv/imem_drv.h>
#include "dpe/dpe_testcommon.h"
using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSIoPipe;
using namespace NSDpe;
extern bool multi_enque_dpe_test_case_00_frame_2_DPE_Config();


extern int g_frame2_bOneRequestHaveManyBuffer;
extern int g_frame2_DveCount;
extern int g_frame2_WmfeCount;

