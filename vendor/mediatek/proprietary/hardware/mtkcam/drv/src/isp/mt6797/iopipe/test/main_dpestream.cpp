/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

///////////////////////////////////////////////////////////////////////////////
// No Warranty
// Except as may be otherwise agreed to in writing, no warranties of any
// kind, whether express or implied, are given by MTK with respect to any MTK
// Deliverables or any use thereof, and MTK Deliverables are provided on an
// "AS IS" basis.  MTK hereby expressly disclaims all such warranties,
// including any implied warranties of merchantability, non-infringement and
// fitness for a particular purpose and any warranties arising out of course
// of performance, course of dealing or usage of trade.  Parties further
// acknowledge that Company may, either presently and/or in the future,
// instruct MTK to assist it in the development and the implementation, in
// accordance with Company's designs, of certain softwares relating to
// Company's product(s) (the "Services").  Except as may be otherwise agreed
// to in writing, no warranties of any kind, whether express or implied, are
// given by MTK with respect to the Services provided, and the Services are
// provided on an "AS IS" basis.  Company further acknowledges that the
// Services may contain errors, that testing is important and Company is
// solely responsible for fully testing the Services and/or derivatives
// thereof before they are used, sublicensed or distributed.  Should there be
// any third party action brought against MTK, arising out of or relating to
// the Services, Company agree to fully indemnify and hold MTK harmless.
// If the parties mutually agree to enter into or continue a business
// relationship or other arrangement, the terms and conditions set forth
// hereunder shall remain effective and, unless explicitly stated otherwise,
// shall prevail in the event of a conflict in the terms in any agreements
// entered into between the parties.
////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008, MediaTek Inc.
// All rights reserved.
//
// Unauthorized use, practice, perform, copy, distribution, reproduction,
// or disclosure of this information in whole or in part is prohibited.

 
#define LOG_TAG "dpestream_test"

#include <vector>

#include <sys/time.h>
#include <sys/stat.h>
#include <sys/prctl.h>

#include <stdio.h>
#include <stdlib.h>
//
#include <errno.h>
#include <fcntl.h>

#include <common/include/common.h>

//
#include <semaphore.h>
#include <pthread.h>
#include <utils/threads.h>
#include <PostProc/IDpeStream.h>
#include <drv/dpeunittest.h>

//#include <mtkcam/iopipe/PostProc/IFeatureStream.h>

//#include <mtkcam/imageio/ispio_pipe_ports.h>
#include <drv/imem_drv.h>
//#include <mtkcam/drv/isp_drv.h>

#include <IImageBuffer.h>
#include <utils/StrongPointer.h>
#include <utils/include/common.h>
#include <utils/include/ImageBufferHeap.h>
//
#include "dpe/dpe_testcommon.h"
#include "dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_setting_00.h"
#include "dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_setting_00.h"
#include "dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_setting_00.h"
#include "dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_setting_00.h"
#include "dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_setting_00.h"
#include "dpe/dpe_test_case_00/MultiEnque_dpe_test_case_00_frame_0_dpe_setting_00.h"
#include "dpe/dpe_test_case_00/MultiEnque_dpe_test_case_00_frame_2_dpe_setting_00.h"
using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSIoPipe;
using namespace NSDpe;

/******************************************************************************
* save the buffer to the file
*******************************************************************************/
MUINT32 comp_roi_mem_with_file(char* pGolden, short mode, MUINTPTR base_addr, int width, int height, MUINT32 stride,
                                int start_x, int start_y, int end_x, int end_y, MUINTPTR mask_base_addr, 
                                MUINT32 mask_size, MUINT32 mask_stride);
static bool
saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size);
/******************************************************************************
* DPE Test Case
*******************************************************************************/
//#include "frame_0_dpe_setting_00.h"
#include <dpe/dpe_default/frame_0/frame_0.h>

extern "C" void getframe_0GoldPointer(
    unsigned long* golden_dpe_dvo_l_frame,
    unsigned long* golden_dpe_dvo_r_frame,
    unsigned long* golden_dpe_confo_l_frame,
    unsigned long* golden_dpe_confo_r_frame,
    unsigned long* golden_dpe_respo_l_frame,
    unsigned long* golden_dpe_respo_r_frame,
    unsigned long* golden_dpe_wmf_dpo_frame_0,
    unsigned long* golden_dpe_wmf_dpo_frame_1,
    unsigned long* golden_dpe_wmf_dpo_frame_2
);


#define DVE_ENABLE 0x1
#define WMFE_ENABLE 0x1
#define WMFE_ENABLE_0 0x1
#define WMFE_ENABLE_1 0x1
#define WMFE_ENABLE_2 0x1
int frame_0_golden_l_start_x = 8;
int frame_0_golden_l_start_y = 8;
int frame_0_golden_l_end_x = 376;
int frame_0_golden_l_end_y = 376;
int frame_0_golden_r_start_x = 18;
int frame_0_golden_r_start_y = 8;
int frame_0_golden_r_end_x = 376;
int frame_0_golden_r_end_y = 376;
MUINT32 frame_0_golden_DVE_HORZ_SV = 0x00A;
MUINT32 frame_0_golden_DVE_VERT_SV =  0x00;

int dpe_default();

MBOOL g_bDVECallback;
MBOOL g_bWMFECallback;

//typedef MVOID                   (*PFN_CALLBACK_T)(DVEParams& rParams);

MVOID DPE_DVECallback(DVEParams& rParams);
MVOID DPE_WMFECallback(WMFEParams& rParams);

pthread_t       DpeUserThread;
sem_t           DpeSem;
volatile bool            g_bDpeThreadTerminated = 0;

/*******************************************************************************
*  Main Function 
*  
********************************************************************************/


void* DpeThreadLoop(void *arg)
{
    int ret = 0; 

    ::pthread_detach(::pthread_self());
    ::sem_wait(&DpeSem);

    while (g_bDpeThreadTerminated) {

        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("Sub Thread do dpe_test_case_00_frame_3_DPE_Config\n");
        ret=dpe_test_case_00_frame_3_DPE_Config();
        printf("Sub Thread do dpe_test_case_00_frame_4_DPE_Config\n");
        ret=dpe_test_case_00_frame_4_DPE_Config();
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
    }
    ::sem_post(&DpeSem);
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    return NULL;
}
void* DpeThreadLoop_OneReqHaveOneBuffer(void *arg)
{
    int ret = 0; 

    ::pthread_detach(::pthread_self());
    ::sem_wait(&DpeSem);

    while (g_bDpeThreadTerminated) {

        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        ret = multi_enque_dpe_test_case_00_frame_2_DPE_Config();
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
    }
    ::sem_post(&DpeSem);
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    return NULL;
}
void* DpeThreadLoop_OneReqHaveTwoBuffer(void *arg)
{
    int ret = 0; 

    ::pthread_detach(::pthread_self());
    ::sem_wait(&DpeSem);

    while (g_bDpeThreadTerminated) {

        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        ret = multi_enque_dpe_test_case_00_frame_2_DPE_Config();
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
    }
    ::sem_post(&DpeSem);
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    return NULL;
}

int test_dpeStream(int argc, char** argv)
{
    int ret = 0; 
    int testCase;
    int i;
    if (argc>=1)
    {
        testCase = atoi(argv[0]);
    }

    switch(testCase)
    {
        case 0:  //Unit Test Case, Only One DVE Request, and One WMFE Request
            g_DPE_UnitTest_Num = DPE_DEFAULT_UT; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            ret=dpe_default();
            break;
        case 1: //Main and DpeUserThread will do the one dve request and one wmfe request at the same time.
            ::sem_init(&DpeSem,0, 0);
            g_DPE_UnitTest_Num = DPE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            pthread_create(&DpeUserThread, NULL, DpeThreadLoop, (void*)NULL);
            g_bDpeThreadTerminated = 1;
            g_bTestBlockingDeque = 0;
            ::sem_post(&DpeSem);
            for (i=0;i<10;i++)
            {
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");

                printf("Main Thread do dpe_test_case_00_frame_0_DPE_Config\n");
                ret=dpe_test_case_00_frame_0_DPE_Config();
                printf("Main Thread do dpe_test_case_00_frame_1_DPE_Config\n");
                ret=dpe_test_case_00_frame_1_DPE_Config();
                printf("Main Thread do dpe_test_case_00_frame_2_DPE_Config\n");
                ret=dpe_test_case_00_frame_2_DPE_Config();
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
            }

            g_bDpeThreadTerminated = 0;
            
            ::sem_wait(&DpeSem);

            break;
        case 2: //Two dve request and Two wmfe request at the same time.
            g_DPE_UnitTest_Num = DPE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            g_bOneRequestHaveManyBuffer = 0;
            g_frame2_bOneRequestHaveManyBuffer = 0;
            ret = multi_enque_dpe_test_case_00_frame_0_DPE_Config();
            ret = multi_enque_dpe_test_case_00_frame_2_DPE_Config();
            break;
        case 3: //Main and Sub thread do Two dve request and Two wmfe request at the same time.
            g_DPE_UnitTest_Num = DPE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            g_bOneRequestHaveManyBuffer = 0;
            g_frame2_bOneRequestHaveManyBuffer = 0;

            ::sem_init(&DpeSem,0, 0);
            pthread_create(&DpeUserThread, NULL, DpeThreadLoop_OneReqHaveOneBuffer, (void*)NULL);
            g_bDpeThreadTerminated = 1;
            ::sem_post(&DpeSem);
            for (i=0;i<10;i++)
            {
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                ret = multi_enque_dpe_test_case_00_frame_0_DPE_Config();
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
            }
            g_bDpeThreadTerminated = 0;
            ::sem_wait(&DpeSem);

            break;

        case 4: //One dve request (two buffer), One wmfe request (two buffer).
            g_DPE_UnitTest_Num = DPE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            g_bOneRequestHaveManyBuffer = 1;
            g_frame2_bOneRequestHaveManyBuffer = 1;
            ret = multi_enque_dpe_test_case_00_frame_0_DPE_Config();
            ret = multi_enque_dpe_test_case_00_frame_2_DPE_Config();
            break;
        case 5: //Main and Sub thread do One dve request (two buffer), One wmfe request (two buffer).
            g_DPE_UnitTest_Num = DPE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            g_bOneRequestHaveManyBuffer = 1;
            g_frame2_bOneRequestHaveManyBuffer = 1;

            ::sem_init(&DpeSem,0, 0);
            pthread_create(&DpeUserThread, NULL, DpeThreadLoop_OneReqHaveTwoBuffer, (void*)NULL);
            g_bDpeThreadTerminated = 1;
            ::sem_post(&DpeSem);
            for (i=0;i<10;i++)
            {
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                ret = multi_enque_dpe_test_case_00_frame_0_DPE_Config();
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
            }
            g_bDpeThreadTerminated = 0;
            ::sem_wait(&DpeSem);
            break;
        case 6: 
            g_DPE_UnitTest_Num = DPE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            g_bTestBlockingDeque = 1;
            ret=dpe_test_case_00_frame_0_DPE_Config();
            break;
        case 7: 
            g_DPE_UnitTest_Num = DPE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            printf("===================Main  Thread===================\n");
            g_bTestBlockingDeque = 0;
            pid_t child_pid;
            child_pid = fork ();

            if (child_pid < 0)
            {
                printf("error in fork!");
            }
            else if (child_pid == 0) {  
                printf("i am the child process, my process id is %d/n",getpid());  
                do{
                    ret=dpe_test_case_00_frame_1_DPE_Config();
                }while(1);
            }  
            else {  
                printf("i am the parent process, my process id is %d/n",getpid());   
                do{

                    ret=dpe_test_case_00_frame_0_DPE_Config();
                }while(1);
            }  
            break;

        default:
            break;
    }
    
    ret = 1;
    return ret; 
}

MVOID DPE_DVECallback(DVEParams& rParams)
{
    MUINT32 DVE_HORZ_SV;
    MUINT32 DVE_VERT_SV;

    printf("--- [DVE callback func]\n");

    vector<DVEConfig>::iterator iter = rParams.mDVEConfigVec.begin();
    for (;iter!= rParams.mDVEConfigVec.end();iter++)
    {
        DVE_VERT_SV = (*iter).Dve_Vert_Sv;
        DVE_HORZ_SV = (*iter).Dve_Horz_Sv;        
        printf("DVE_VERT_SV:%d, DVE_HORZ_SV:%d\n", DVE_VERT_SV, DVE_HORZ_SV);
        if ( (frame_0_golden_DVE_HORZ_SV == DVE_HORZ_SV) &&
             (frame_0_golden_DVE_VERT_SV == DVE_VERT_SV) )
        {
            //Pass
            printf("dpe DVE Statistic Result 0 bit true pass!!!\n");
        }
        else
        {
            //Erro
            printf("dpe DVE Statistic Result 0 bit true fail, DVE_HORZ_SV:(%d), DVE_VERT_SV:(%d)!!!\n", DVE_HORZ_SV, DVE_VERT_SV);
        }
    }
    g_bDVECallback = MTRUE;

}

MVOID DPE_WMFECallback(WMFEParams& rParams)
{
    printf("--- [WMEF callback func]\n");
    g_bWMFECallback = MTRUE;
}


//#include "../../imageio/test/DIP_pics/P2A_FG/imgi_640_480_10.h"
/*********************************************************************************/
int dpe_default()
{
    int ret=0;
    printf("--- [dpe_default]\n");
    
    
    //i need a varaible to switch the difference between test code and dpe_drv.cpp
    
    NSCam::NSIoPipe::NSDpe::IDpeStream* pStream;
    pStream= NSCam::NSIoPipe::NSDpe::IDpeStream::createInstance("test_dpe_default");
    pStream->init();   
    printf("--- [test_dpe_default...DpeStream init done\n");

    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    int golden_l_start_x = frame_0_golden_l_start_x;
    int golden_l_start_y = frame_0_golden_l_start_y;
    int golden_l_end_x = frame_0_golden_l_end_x;
    int golden_l_end_y = frame_0_golden_l_end_y;
    int golden_r_start_x = frame_0_golden_r_start_x;
    int golden_r_start_y = frame_0_golden_r_start_y;
    int golden_r_end_x = frame_0_golden_r_end_x;
    int golden_r_end_y = frame_0_golden_r_end_y;
    MUINT32 golden_DVE_HORZ_SV = frame_0_golden_DVE_HORZ_SV;
    MUINT32 golden_DVE_VERT_SV = frame_0_golden_DVE_VERT_SV;
    //copy the pointer of golden answer of DPE
    unsigned long golden_dpe_dvo_l_frame;
    unsigned long golden_dpe_dvo_r_frame;
    unsigned long golden_dpe_confo_l_frame;
    unsigned long golden_dpe_confo_r_frame;
    unsigned long golden_dpe_respo_l_frame;
    unsigned long golden_dpe_respo_r_frame;
    unsigned long golden_dpe_wmf_dpo_frame_0;
    unsigned long golden_dpe_wmf_dpo_frame_1;
    unsigned long golden_dpe_wmf_dpo_frame_2;
    getframe_0GoldPointer(
    &golden_dpe_dvo_l_frame,
    &golden_dpe_dvo_r_frame,
    &golden_dpe_confo_l_frame,
    &golden_dpe_confo_r_frame,
    &golden_dpe_respo_l_frame,
    &golden_dpe_respo_r_frame,
    &golden_dpe_wmf_dpo_frame_0,
    &golden_dpe_wmf_dpo_frame_1,
    &golden_dpe_wmf_dpo_frame_2
    );

    //input frame pointer
    char* dpe_imgi_l_frame;
    char* dpe_imgi_r_frame;
    char* dpe_dvi_l_frame;
    char* dpe_dvi_r_frame;
    char* dpe_maski_l_frame;
    char* dpe_maski_r_frame;
    char* dpe_wmf_imgi_frame_0;
    char* dpe_wmf_dpi_frame_0;
    char* dpe_wmf_tbli_frame_0;
    char* dpe_wmf_imgi_frame_1;
    char* dpe_wmf_dpi_frame_1;
    char* dpe_wmf_tbli_frame_1;
    char* dpe_wmf_imgi_frame_2;
    char* dpe_wmf_dpi_frame_2;
    char* dpe_wmf_tbli_frame_2;
#if DVE_ENABLE

    printf("--- dve input allocate init\n");

    IMEM_BUF_INFO buf_imgi_l_frame;
    buf_imgi_l_frame.size= frame_0_dpe_imgi_l_frame_00_size;
    printf("buf_imgi_l_frame.size:%d",buf_imgi_l_frame.size);
    mpImemDrv->allocVirtBuf(&buf_imgi_l_frame);
    mpImemDrv->mapPhyAddr(&buf_imgi_l_frame);
    memcpy( (MUINT8*)(buf_imgi_l_frame.virtAddr), (MUINT8*)(frame_0_dpe_imgi_l_frame_00), buf_imgi_l_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_imgi_l_frame);

    IMEM_BUF_INFO buf_imgi_r_frame;
    buf_imgi_r_frame.size=frame_0_dpe_imgi_r_frame_00_size;
    printf("buf_imgi_r_frame.size:%d",buf_imgi_r_frame.size);
    mpImemDrv->allocVirtBuf(&buf_imgi_r_frame);
    mpImemDrv->mapPhyAddr(&buf_imgi_r_frame);
    memcpy( (MUINT8*)(buf_imgi_r_frame.virtAddr), (MUINT8*)(frame_0_dpe_imgi_r_frame_00), buf_imgi_r_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_imgi_r_frame);


    IMEM_BUF_INFO buf_dvi_l_frame;
    buf_dvi_l_frame.size=frame_0_dpe_dvi_l_frame_00_size;
    printf("buf_dvi_l_frame.size:%d",buf_dvi_l_frame.size);
    mpImemDrv->allocVirtBuf(&buf_dvi_l_frame);
    mpImemDrv->mapPhyAddr(&buf_dvi_l_frame);
    memcpy( (MUINT8*)(buf_dvi_l_frame.virtAddr), (MUINT8*)(frame_0_dpe_dvi_l_frame_00), buf_dvi_l_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_dvi_l_frame);

    IMEM_BUF_INFO buf_dvi_r_frame;
    buf_dvi_r_frame.size=frame_0_dpe_dvi_r_frame_00_size;
    printf("buf_dvi_r_frame.size:%d",buf_dvi_r_frame.size);
    mpImemDrv->allocVirtBuf(&buf_dvi_r_frame);
    mpImemDrv->mapPhyAddr(&buf_dvi_r_frame);
    memcpy( (MUINT8*)(buf_dvi_r_frame.virtAddr), (MUINT8*)(frame_0_dpe_dvi_r_frame_00), buf_dvi_r_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_dvi_r_frame);


    IMEM_BUF_INFO buf_maski_l_frame;
    buf_maski_l_frame.size=frame_0_dpe_maski_l_frame_00_size;
    printf("buf_maski_l_frame.size:%d",buf_maski_l_frame.size);
    mpImemDrv->allocVirtBuf(&buf_maski_l_frame);
    mpImemDrv->mapPhyAddr(&buf_maski_l_frame);
    memcpy( (MUINT8*)(buf_maski_l_frame.virtAddr), (MUINT8*)(frame_0_dpe_maski_l_frame_00), buf_maski_l_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_maski_l_frame);

    IMEM_BUF_INFO buf_maski_r_frame;
    buf_maski_r_frame.size=frame_0_dpe_maski_r_frame_00_size;
    printf("buf_maski_r_frame.size:%d",buf_maski_r_frame.size);
    mpImemDrv->allocVirtBuf(&buf_maski_r_frame);
    mpImemDrv->mapPhyAddr(&buf_maski_r_frame);
    memcpy( (MUINT8*)(buf_maski_r_frame.virtAddr), (MUINT8*)(frame_0_dpe_maski_r_frame_00), buf_maski_r_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_maski_r_frame);

    printf("--- dve  input  allocate done\n");

#endif
#if WMFE_ENABLE_0

    printf("--- wmfe0  input  allocate init\n");

    IMEM_BUF_INFO buf_wmf_imgi_frame_0;
    buf_wmf_imgi_frame_0.size=frame_0_dpe_wmf_imgi_frame_00_00_0_size;
    printf("buf_wmf_imgi_frame_0.size:%d",buf_wmf_imgi_frame_0.size);
    mpImemDrv->allocVirtBuf(&buf_wmf_imgi_frame_0);
    mpImemDrv->mapPhyAddr(&buf_wmf_imgi_frame_0);
    memcpy( (MUINT8*)(buf_wmf_imgi_frame_0.virtAddr), (MUINT8*)(frame_0_in_dpe_wmf_imgi_frame_0), buf_wmf_imgi_frame_0.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_imgi_frame_0);

    IMEM_BUF_INFO buf_wmf_dpi_frame_0;
    buf_wmf_dpi_frame_0.size=frame_0_dpe_wmf_dpi_frame_00_00_0_size;
    printf("buf_wmf_dpi_frame_0.size:%d",buf_wmf_dpi_frame_0.size);
    mpImemDrv->allocVirtBuf(&buf_wmf_dpi_frame_0);
    mpImemDrv->mapPhyAddr(&buf_wmf_dpi_frame_0);
    memcpy( (MUINT8*)(buf_wmf_dpi_frame_0.virtAddr), (MUINT8*)(frame_0_in_dpe_wmf_dpi_frame_0), buf_wmf_dpi_frame_0.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_dpi_frame_0);

    IMEM_BUF_INFO buf_wmf_tbli_frame_0;
    buf_wmf_tbli_frame_0.size=frame_0_dpe_wmf_tbli_frame_00_00_0_size;
    printf("buf_wmf_tbli_frame_0.size:%d",buf_wmf_tbli_frame_0.size);
    mpImemDrv->allocVirtBuf(&buf_wmf_tbli_frame_0);
    mpImemDrv->mapPhyAddr(&buf_wmf_tbli_frame_0);
    memcpy( (MUINT8*)(buf_wmf_tbli_frame_0.virtAddr), (MUINT8*)(frame_0_in_dpe_wmf_tbli_frame_0), buf_wmf_tbli_frame_0.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_tbli_frame_0);

    printf("--- wmfe0  input  allocate done\n");

#endif
#if WMFE_ENABLE_1
    printf("--- wmfe1  input allocate init\n");

    IMEM_BUF_INFO buf_wmf_imgi_frame_1;
    buf_wmf_imgi_frame_1.size=frame_0_dpe_wmf_imgi_frame_00_00_1_size;
    printf("buf_wmf_imgi_frame_1.size:%d",buf_wmf_imgi_frame_1.size);
    mpImemDrv->allocVirtBuf(&buf_wmf_imgi_frame_1);
    mpImemDrv->mapPhyAddr(&buf_wmf_imgi_frame_1);
    memcpy( (MUINT8*)(buf_wmf_imgi_frame_1.virtAddr), (MUINT8*)(frame_0_in_dpe_wmf_imgi_frame_1), buf_wmf_imgi_frame_1.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_imgi_frame_1);

    IMEM_BUF_INFO buf_wmf_dpi_frame_1;
    buf_wmf_dpi_frame_1.size=frame_0_dpe_wmf_dpi_frame_00_00_1_size;
    printf("buf_wmf_dpi_frame_1.size:%d",buf_wmf_dpi_frame_1.size);
    mpImemDrv->allocVirtBuf(&buf_wmf_dpi_frame_1);
    mpImemDrv->mapPhyAddr(&buf_wmf_dpi_frame_1);
    memcpy( (MUINT8*)(buf_wmf_dpi_frame_1.virtAddr), (MUINT8*)(frame_0_in_dpe_wmf_dpi_frame_1), buf_wmf_dpi_frame_1.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_dpi_frame_1);

    IMEM_BUF_INFO buf_wmf_tbli_frame_1;
    buf_wmf_tbli_frame_1.size=frame_0_dpe_wmf_tbli_frame_00_00_1_size;
    printf("buf_wmf_tbli_frame_1.size:%d",buf_wmf_tbli_frame_1.size);
    mpImemDrv->allocVirtBuf(&buf_wmf_tbli_frame_1);
    mpImemDrv->mapPhyAddr(&buf_wmf_tbli_frame_1);
    memcpy( (MUINT8*)(buf_wmf_tbli_frame_1.virtAddr), (MUINT8*)(frame_0_in_dpe_wmf_tbli_frame_1), buf_wmf_tbli_frame_1.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_tbli_frame_1);

    printf("--- wmfe1  input  allocate done\n");


#endif
#if WMFE_ENABLE_2
//    dpe_wmf_imgi_frame_2 = &frame_0_in_dpe_wmf_imgi_frame_2[0];
//    dpe_wmf_dpi_frame_2 = &frame_0_in_dpe_wmf_dpi_frame_2[0];
//    dpe_wmf_tbli_frame_2 = &frame_0_in_dpe_wmf_tbli_frame_2[0];

    printf("--- wmfe2  input  allocate init\n");


    IMEM_BUF_INFO buf_wmf_imgi_frame_2;
    buf_wmf_imgi_frame_2.size=frame_0_dpe_wmf_imgi_frame_00_00_2_size;
    printf("buf_wmf_imgi_frame_2.size:%d",buf_wmf_imgi_frame_2.size);
    mpImemDrv->allocVirtBuf(&buf_wmf_imgi_frame_2);
    mpImemDrv->mapPhyAddr(&buf_wmf_imgi_frame_2);
    memcpy( (MUINT8*)(buf_wmf_imgi_frame_2.virtAddr), (MUINT8*)(frame_0_in_dpe_wmf_imgi_frame_2), buf_wmf_imgi_frame_2.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_imgi_frame_2);

    IMEM_BUF_INFO buf_wmf_dpi_frame_2;
    buf_wmf_dpi_frame_2.size=frame_0_dpe_wmf_dpi_frame_00_00_2_size;
    printf("buf_wmf_dpi_frame_2.size:%d",buf_wmf_dpi_frame_2.size);
    mpImemDrv->allocVirtBuf(&buf_wmf_dpi_frame_2);
    mpImemDrv->mapPhyAddr(&buf_wmf_dpi_frame_2);
    memcpy( (MUINT8*)(buf_wmf_dpi_frame_2.virtAddr), (MUINT8*)(frame_0_in_dpe_wmf_dpi_frame_2), buf_wmf_dpi_frame_2.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_dpi_frame_2);

    IMEM_BUF_INFO buf_wmf_tbli_frame_2;
    buf_wmf_tbli_frame_2.size=frame_0_dpe_wmf_tbli_frame_00_00_2_size;
    printf("buf_wmf_tbli_frame_2.size:%d",buf_wmf_tbli_frame_2.size);
    mpImemDrv->allocVirtBuf(&buf_wmf_tbli_frame_2);
    mpImemDrv->mapPhyAddr(&buf_wmf_tbli_frame_2);
    memcpy( (MUINT8*)(buf_wmf_tbli_frame_2.virtAddr), (MUINT8*)(frame_0_in_dpe_wmf_tbli_frame_2), buf_wmf_tbli_frame_2.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_tbli_frame_2);

    printf("--- wmfe2  input allocate done\n");


#endif

    //allocate the memory to be used the Target of DPE
    char* dpe_dvo_l_frame;
    char* dpe_dvo_r_frame;
    char* dpe_confo_l_frame;
    char* dpe_confo_r_frame;
    char* dpe_respo_l_frame;
    char* dpe_respo_r_frame;
    char* dpe_wmf_dpo_frame_0;
    char* dpe_wmf_dpo_frame_1;
    char* dpe_wmf_dpo_frame_2;

    printf("#########################################################\n");
    printf("###########frame_0Start to Test !!!!###########\n");
    printf("#########################################################\n");

#if DVE_ENABLE
    IMEM_BUF_INFO buf_dvo_l_frame;    
    buf_dvo_l_frame.size = frame_0_dpe_dvo_l_frame_00_01_size;
    mpImemDrv->allocVirtBuf(&buf_dvo_l_frame);
    mpImemDrv->mapPhyAddr(&buf_dvo_l_frame);
    memset((MUINT8*)buf_dvo_l_frame.virtAddr, 0xffffffff, buf_dvo_l_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_dvo_l_frame);

    IMEM_BUF_INFO buf_dvo_r_frame;    
    buf_dvo_r_frame.size = frame_0_dpe_dvo_r_frame_00_01_size;
    mpImemDrv->allocVirtBuf(&buf_dvo_r_frame);
    mpImemDrv->mapPhyAddr(&buf_dvo_r_frame);
    memset((MUINT8*)buf_dvo_r_frame.virtAddr, 0xffffffff, buf_dvo_r_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_dvo_r_frame);

    IMEM_BUF_INFO buf_confo_l_frame;    
    buf_confo_l_frame.size = frame_0_dpe_confo_l_frame_00_01_size;
    mpImemDrv->allocVirtBuf(&buf_confo_l_frame);
    mpImemDrv->mapPhyAddr(&buf_confo_l_frame);
    memset((MUINT8*)buf_confo_l_frame.virtAddr, 0xffffffff, buf_confo_l_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_confo_l_frame);

    IMEM_BUF_INFO buf_confo_r_frame;    
    buf_confo_r_frame.size = frame_0_dpe_confo_r_frame_00_01_size;
    mpImemDrv->allocVirtBuf(&buf_confo_r_frame);
    mpImemDrv->mapPhyAddr(&buf_confo_r_frame);
    memset((MUINT8*)buf_confo_r_frame.virtAddr, 0xffffffff, buf_confo_r_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_confo_r_frame);

    IMEM_BUF_INFO buf_respo_l_frame;    
    buf_respo_l_frame.size = frame_0_dpe_respo_l_frame_00_01_size;
    mpImemDrv->allocVirtBuf(&buf_respo_l_frame);
    mpImemDrv->mapPhyAddr(&buf_respo_l_frame);
    memset((MUINT8*)buf_respo_l_frame.virtAddr, 0xffffffff, buf_respo_l_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_respo_l_frame);

    IMEM_BUF_INFO buf_respo_r_frame;    
    buf_respo_r_frame.size = frame_0_dpe_respo_r_frame_00_01_size;
    mpImemDrv->allocVirtBuf(&buf_respo_r_frame);
    mpImemDrv->mapPhyAddr(&buf_respo_r_frame);
    memset((MUINT8*)buf_respo_r_frame.virtAddr, 0xffffffff, buf_respo_r_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_respo_r_frame);

#if WMFE_ENABLE_0
    //dpe_wmf_dpo_frame_0 = (char* )MEM_Allocate_NC( frame_0_golden_dpe_wmf_dpo_0_size, MEM_USER_CAM);
    IMEM_BUF_INFO buf_wmf_dpo_frame_0;    
    buf_wmf_dpo_frame_0.size = frame_0_golden_dpe_wmf_dpo_0_size;
    mpImemDrv->allocVirtBuf(&buf_wmf_dpo_frame_0);
    mpImemDrv->mapPhyAddr(&buf_wmf_dpo_frame_0);
    memset((MUINT8*)buf_wmf_dpo_frame_0.virtAddr, 0xffffffff, buf_wmf_dpo_frame_0.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_dpo_frame_0);
#endif

#if WMFE_ENABLE_1
    //dpe_wmf_dpo_frame_1 = (char* )MEM_Allocate_NC( frame_0_golden_dpe_wmf_dpo_1_size, MEM_USER_CAM);

    IMEM_BUF_INFO buf_wmf_dpo_frame_1;    
    buf_wmf_dpo_frame_1.size = frame_0_golden_dpe_wmf_dpo_1_size;
    mpImemDrv->allocVirtBuf(&buf_wmf_dpo_frame_1);
    mpImemDrv->mapPhyAddr(&buf_wmf_dpo_frame_1);
    memset((MUINT8*)buf_wmf_dpo_frame_1.virtAddr, 0xffffffff, buf_wmf_dpo_frame_1.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_dpo_frame_1);

#endif
#if WMFE_ENABLE_2
    //dpe_wmf_dpo_frame_2 = (char* )MEM_Allocate_NC( frame_0_golden_dpe_wmf_dpo_2_size, MEM_USER_CAM);
    IMEM_BUF_INFO buf_wmf_dpo_frame_2;    
    buf_wmf_dpo_frame_2.size = frame_0_golden_dpe_wmf_dpo_2_size;
    mpImemDrv->allocVirtBuf(&buf_wmf_dpo_frame_2);
    mpImemDrv->mapPhyAddr(&buf_wmf_dpo_frame_2);
    memset((MUINT8*)buf_wmf_dpo_frame_2.virtAddr, 0xffffffff, buf_wmf_dpo_frame_2.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_dpo_frame_2);
#endif



#endif


#if DVE_ENABLE
    printf("golden_dpe_dvo_l_size:%d, golden_dpe_dvo_r_size:%d\n", frame_0_golden_dpe_dvo_l_size, frame_0_golden_dpe_dvo_r_size);
    printf("golden_dpe_confo_l_size:%d, golden_dpe_confo_r_size:%d\n",frame_0_golden_dpe_confo_l_size , frame_0_golden_dpe_confo_r_size);
    printf("golden_dpe_respo_l_size:%d, golden_dpe_respo_r_size:%d\n",frame_0_golden_dpe_respo_l_size , frame_0_golden_dpe_respo_r_size);

#endif

    //buffer operation
    //mpImemDrv->cacheFlushAll();
    //printf("--- [dpe_default...flush done\n]");
    //virtual MBOOL                   DVEenque(
    //                                    DVEParams const& rDveParams
    //                                );

    DVEParams rDveParams;
    DVEConfig dveconfig;
    rDveParams.mpfnCallback = DPE_DVECallback;  

    dveconfig.Dve_Skp_Pre_Dv = false;
    dveconfig.Dve_Mask_En = false;
    dveconfig.Dve_l_Bbox_En = false;
    dveconfig.Dve_r_Bbox_En = false;
    dveconfig.Dve_Horz_Ds_Mode = 0x0;
    dveconfig.Dve_Vert_Ds_Mode = 0x0;
    dveconfig.Dve_Imgi_l_Fmt = DPE_IMGI_Y_FMT;
    dveconfig.Dve_Imgi_r_Fmt = DPE_IMGI_Y_FMT;
    dveconfig.Dve_Org_l_Bbox.DVE_ORG_BBOX_RIGHT = 0x0;
    dveconfig.Dve_Org_l_Bbox.DVE_ORG_BBOX_LEFT = 0x0;
    dveconfig.Dve_Org_l_Bbox.DVE_ORG_BBOX_BOTTOM = 0x0;
    dveconfig.Dve_Org_l_Bbox.DVE_ORG_BBOX_TOP = 0x0;
    dveconfig.Dve_Org_r_Bbox.DVE_ORG_BBOX_RIGHT = 0x0;
    dveconfig.Dve_Org_r_Bbox.DVE_ORG_BBOX_LEFT = 0x0;
    dveconfig.Dve_Org_r_Bbox.DVE_ORG_BBOX_BOTTOM = 0x0;
    dveconfig.Dve_Org_r_Bbox.DVE_ORG_BBOX_TOP = 0x0;
    dveconfig.Dve_Org_Width = 0x180;
    dveconfig.Dve_Org_Height = 0x180;
    dveconfig.Dve_Org_Horz_Sr_1 = 0x80;
    dveconfig.Dve_Org_Horz_Sr_0 = 0x01ff;
    dveconfig.Dve_Org_Vert_Sr_0 = 0x18;
    dveconfig.Dve_Org_Start_Vert_Sv = 0x0;
    dveconfig.Dve_Org_Start_Horz_Sv = 0x0;
    dveconfig.Dve_Cand_Num = 0x8;


    dveconfig.Dve_Cand_0.DVE_CAND_SEL = 0xc;
    dveconfig.Dve_Cand_0.DVE_CAND_TYPE = 0x0;
    dveconfig.Dve_Cand_1.DVE_CAND_SEL = 0xB;
    dveconfig.Dve_Cand_1.DVE_CAND_TYPE = 0x1;
    dveconfig.Dve_Cand_2.DVE_CAND_SEL = 0x12;
    dveconfig.Dve_Cand_2.DVE_CAND_TYPE = 0x2;
    dveconfig.Dve_Cand_3.DVE_CAND_SEL = 0x7;
    dveconfig.Dve_Cand_3.DVE_CAND_TYPE = 0x1;

    dveconfig.Dve_Cand_4.DVE_CAND_SEL = 0x1f;
    dveconfig.Dve_Cand_4.DVE_CAND_TYPE = 0x6;
    dveconfig.Dve_Cand_5.DVE_CAND_SEL = 0x19;
    dveconfig.Dve_Cand_5.DVE_CAND_TYPE = 0x3;
    dveconfig.Dve_Cand_6.DVE_CAND_SEL = 0x1a;
    dveconfig.Dve_Cand_6.DVE_CAND_TYPE = 0x3;
    dveconfig.Dve_Cand_7.DVE_CAND_SEL = 0x1c;
    dveconfig.Dve_Cand_7.DVE_CAND_TYPE = 0x4;
    dveconfig.Dve_Rand_Lut_0 = 0x0B;
    dveconfig.Dve_Rand_Lut_1 = 0x17;
    dveconfig.Dve_Rand_Lut_2 = 0x1f;
    dveconfig.Dve_Rand_Lut_3 = 0x2b;
    dveconfig.DVE_VERT_GMV = 0x0;
    dveconfig.DVE_HORZ_GMV = 0x0;
    dveconfig.Dve_Horz_Dv_Ini = 0x0;
    dveconfig.Dve_Coft_Shift = 0x2;
    dveconfig.Dve_Corner_Th = 0x0010;
    dveconfig.Dve_Smth_Luma_Th_1 = 0x0;
    dveconfig.Dve_Smth_Luma_Th_0 = 0xC;
    dveconfig.Dve_Smth_Luma_Ada_Base = 0x0;
    dveconfig.Dve_Smth_Luma_Horz_Pnlty_Sel = 0x1;

    dveconfig.Dve_Smth_Dv_Mode = 0x1;
    dveconfig.Dve_Smth_Dv_Horz_Pnlty_Sel = 0x2;
    dveconfig.Dve_Smth_Dv_Vert_Pnlty_Sel = 0x1;
    dveconfig.Dve_Smth_Dv_Ada_Base = 0x5;
    dveconfig.Dve_Smth_Dv_Th_0 = 0xC;
    dveconfig.Dve_Smth_Dv_Th_1 = 0x0;
    dveconfig.Dve_Ord_Pnlty_Sel = 0x1;
    dveconfig.Dve_Ord_Coring = 0x4;
    dveconfig.Dve_Ord_Th = 0x100;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_REFINE_PNLTY_SEL = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_GMV_PNLTY_SEL = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_PREV_PNLTY_SEL = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_NBR_PNLTY_SEL = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_RAND_PNLTY_SEL = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_TMPR_PNLTY_SEL = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_SPTL_PNLTY_SEL = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_RAND_COST = 0x20;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_GMV_COST = 0x3;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_PREV_COST = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_NBR_COST = 0x1;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_REFINE_COST = 0x1;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_TMPR_COST = 0x3;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_SPTL_COST = 0x0;
    dveconfig.Dve_Imgi_l.u4BufVA = buf_imgi_l_frame.virtAddr;
    dveconfig.Dve_Imgi_l.u4BufPA = buf_imgi_l_frame.phyAddr;
    dveconfig.Dve_Imgi_l.u4Stride = 0x30;
    dveconfig.Dve_Imgi_r.u4BufVA = buf_imgi_r_frame.virtAddr;
    dveconfig.Dve_Imgi_r.u4BufPA = buf_imgi_r_frame.phyAddr;
    dveconfig.Dve_Imgi_r.u4Stride = 0x30;

    dveconfig.Dve_Dvi_l.u4BufVA = buf_dvi_l_frame.virtAddr;
    dveconfig.Dve_Dvi_l.u4BufPA = buf_dvi_l_frame.phyAddr;
    dveconfig.Dve_Dvi_l.u4Stride = 0x60;
    dveconfig.Dve_Dvi_r.u4BufVA = buf_dvi_r_frame.virtAddr;
    dveconfig.Dve_Dvi_r.u4BufPA = buf_dvi_r_frame.phyAddr;
    dveconfig.Dve_Dvi_r.u4Stride = 0x60;

    dveconfig.Dve_Maski_l.u4BufVA = buf_maski_l_frame.virtAddr;
    dveconfig.Dve_Maski_l.u4BufPA = buf_maski_l_frame.phyAddr;
    dveconfig.Dve_Maski_l.u4Stride = 0x30;
    dveconfig.Dve_Maski_r.u4BufVA = buf_maski_r_frame.virtAddr;
    dveconfig.Dve_Maski_r.u4BufPA = buf_maski_r_frame.phyAddr;
    dveconfig.Dve_Maski_r.u4Stride = 0x30;


    dveconfig.Dve_Dvo_l.u4BufVA = buf_dvo_l_frame.virtAddr;
    dveconfig.Dve_Dvo_l.u4BufPA = buf_dvo_l_frame.phyAddr;
    dveconfig.Dve_Dvo_l.u4Stride = 0x60;
    dveconfig.Dve_Dvo_r.u4BufVA = buf_dvo_r_frame.virtAddr;
    dveconfig.Dve_Dvo_r.u4BufPA = buf_dvo_r_frame.phyAddr;
    dveconfig.Dve_Dvo_r.u4Stride = 0x60;

    dveconfig.Dve_Confo_l.u4BufVA = buf_confo_l_frame.virtAddr;
    dveconfig.Dve_Confo_l.u4BufPA = buf_confo_l_frame.phyAddr;
    dveconfig.Dve_Confo_l.u4Stride = 0x30;
    dveconfig.Dve_Confo_r.u4BufVA = buf_confo_r_frame.virtAddr;
    dveconfig.Dve_Confo_r.u4BufPA = buf_confo_r_frame.phyAddr;
    dveconfig.Dve_Confo_r.u4Stride = 0x30;

    dveconfig.Dve_Respo_l.u4BufVA = buf_respo_l_frame.virtAddr;
    dveconfig.Dve_Respo_l.u4BufPA = buf_respo_l_frame.phyAddr;
    dveconfig.Dve_Respo_l.u4Stride = 0x30;
    dveconfig.Dve_Respo_r.u4BufVA = buf_respo_r_frame.virtAddr;
    dveconfig.Dve_Respo_r.u4BufPA = buf_respo_r_frame.phyAddr;
    dveconfig.Dve_Respo_r.u4Stride = 0x30;



    rDveParams.mDVEConfigVec.push_back(dveconfig);
    g_bDVECallback = MFALSE;


    WMFEParams rWmfeParams;
    WMFEConfig wmfeconfig;
    rWmfeParams.mpfnCallback = DPE_WMFECallback;  
#if WMFE_ENABLE_0
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Enable = true;
    wmfeconfig.Wmfe_Ctrl_0.WmfeFilterSize = WMFE_FILTER_SIZE_7x7;
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Width = 0x30;
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Height = 0x30;
    wmfeconfig.Wmfe_Ctrl_0.WmfeImgiFmt = DPE_IMGI_Y_FMT;
    wmfeconfig.Wmfe_Ctrl_0.WmfeDpiFmt= WMFE_DPI_D_FMT;
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Imgi.u4BufVA = buf_wmf_imgi_frame_0.virtAddr;
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Imgi.u4BufPA = buf_wmf_imgi_frame_0.phyAddr;
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Imgi.u4Stride = 0x30;
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpi.u4BufVA = buf_wmf_dpi_frame_0.virtAddr;
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpi.u4BufPA = buf_wmf_dpi_frame_0.phyAddr;
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpi.u4Stride = 0x30;
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Tbli.u4BufVA = buf_wmf_tbli_frame_0.virtAddr;
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Tbli.u4BufPA = buf_wmf_tbli_frame_0.phyAddr;
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Tbli.u4Stride = 0x100;
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpo.u4BufVA = buf_wmf_dpo_frame_0.virtAddr;
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpo.u4BufPA = buf_wmf_dpo_frame_0.phyAddr;
    wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpo.u4Stride = 0x30;
#endif
#if WMFE_ENABLE_1
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Enable = true;
    wmfeconfig.Wmfe_Ctrl_1.WmfeFilterSize = WMFE_FILTER_SIZE_7x7;
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Width = 0x60;
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Height = 0x60;
    wmfeconfig.Wmfe_Ctrl_1.WmfeImgiFmt = DPE_IMGI_YC_FMT;
    wmfeconfig.Wmfe_Ctrl_1.WmfeDpiFmt= WMFE_DPI_DX_FMT;
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Imgi.u4BufVA = buf_wmf_imgi_frame_1.virtAddr;
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Imgi.u4BufPA = buf_wmf_imgi_frame_1.phyAddr;
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Imgi.u4Stride = 0xc0;
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpi.u4BufVA = buf_wmf_dpi_frame_1.virtAddr;
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpi.u4BufPA = buf_wmf_dpi_frame_1.phyAddr;
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpi.u4Stride = 0xc0;
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Tbli.u4BufVA = buf_wmf_tbli_frame_1.virtAddr;
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Tbli.u4BufPA = buf_wmf_tbli_frame_1.phyAddr;
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Tbli.u4Stride = 0x100;
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpo.u4BufVA = buf_wmf_dpo_frame_1.virtAddr;
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpo.u4BufPA = buf_wmf_dpo_frame_1.phyAddr;
    wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpo.u4Stride = 0x60;

#endif
#if WMFE_ENABLE_2
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Enable = true;
    wmfeconfig.Wmfe_Ctrl_2.WmfeFilterSize = WMFE_FILTER_SIZE_7x7;
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Width = 0x60;
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Height = 0x60;
    wmfeconfig.Wmfe_Ctrl_2.WmfeImgiFmt = DPE_IMGI_Y_FMT;
    wmfeconfig.Wmfe_Ctrl_2.WmfeDpiFmt= WMFE_DPI_D_FMT;
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Imgi.u4BufVA = buf_wmf_imgi_frame_2.virtAddr;
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Imgi.u4BufPA = buf_wmf_imgi_frame_2.phyAddr;
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Imgi.u4Stride = 0x60;
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpi.u4BufVA = buf_wmf_dpi_frame_2.virtAddr;
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpi.u4BufPA = buf_wmf_dpi_frame_2.phyAddr;
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpi.u4Stride = 0x30;
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Tbli.u4BufVA = buf_wmf_tbli_frame_2.virtAddr;
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Tbli.u4BufPA = buf_wmf_tbli_frame_2.phyAddr;
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Tbli.u4Stride = 0x100;
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpo.u4BufVA = buf_wmf_dpo_frame_2.virtAddr;
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpo.u4BufPA = buf_wmf_dpo_frame_2.phyAddr;
    wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpo.u4Stride = 0x60;
#endif

    rWmfeParams.mWMFEConfigVec.push_back(wmfeconfig);
    g_bWMFECallback = MFALSE;


    //enque
    ret=pStream->DVEenque(rDveParams);
    if(!ret)
    {
        printf("---ERRRRRRRRR [dpe_default..dve enque fail\n]");
    }
    else
    {
        printf("---[dpe_default..dve enque done\n]");
    }

    //enque
    ret=pStream->WMFEenque(rWmfeParams);
    if(!ret)
    {
        printf("---ERRRRRRRRR [dpe_default..wmfe enque fail\n]");
    }
    else
    {
        printf("---[dpe_default..wmfe enque done\n]");
    }
#if DVE_ENABLE
    do{
        usleep(100000);
        if (MTRUE == g_bDVECallback)
        {
            break;
        }
    }while(1);
#endif

    MUINT32 DVE_ORG_WDITH = dveconfig.Dve_Org_Width;
    MUINT32 DVE_ORG_HEIGHT = dveconfig.Dve_Org_Height;
    MUINT32 DVE_HORZ_DS_MODE = dveconfig.Dve_Horz_Ds_Mode;
    MUINT32 DVE_VERT_DS_MODE = dveconfig.Dve_Vert_Ds_Mode;
    int int_data_dma_0, int_data_dma_1, int_data_dma_2, int_data_dma_3;
    MUINT32 blk_width;
    MUINT32 blk_height;
    MUINT32 golden_l_start_blk_x;
    int golden_l_end_blk_x;
    MUINT32 golden_l_start_blk_y;
    int golden_l_end_blk_y;

    MUINT32 golden_r_start_blk_x;
    int golden_r_end_blk_x;
    MUINT32 golden_r_start_blk_y;
    int golden_r_end_blk_y;

    int err_cnt_dma;

    MUINT32 REG_DPE_WMFE_SIZE_0 = (wmfeconfig.Wmfe_Ctrl_0.Wmfe_Height << 16) | wmfeconfig.Wmfe_Ctrl_0.Wmfe_Width;
    MUINT32 REG_DPE_WMFE_SIZE_1 = (wmfeconfig.Wmfe_Ctrl_1.Wmfe_Height << 16) | wmfeconfig.Wmfe_Ctrl_1.Wmfe_Width;
    MUINT32 REG_DPE_WMFE_SIZE_2 = (wmfeconfig.Wmfe_Ctrl_2.Wmfe_Height << 16) | wmfeconfig.Wmfe_Ctrl_2.Wmfe_Width;
    MUINT32 wmfe_curr_width;
    MUINT32 wmfe_curr_height;

    if ( 0 == DVE_HORZ_DS_MODE)
    {
        blk_width = (DVE_ORG_WDITH+7) >> 3;
        golden_l_start_blk_x = (golden_l_start_x >> 3);
        if (golden_l_start_x & 0x7)
        {
            golden_l_start_blk_x += 1;
        }
        golden_l_end_blk_x  = (golden_l_end_x >> 3);
        golden_r_start_blk_x = (golden_r_start_x >> 3);
        if (golden_r_start_x & 0x7)
        {
            golden_r_start_blk_x += 1;
        }
        golden_r_end_blk_x  = (golden_r_end_x >> 3);
    }
    else
    {
        blk_width = (DVE_ORG_WDITH+3) >> 2;
        golden_l_start_blk_x = (golden_l_start_x >> 2);
        if (golden_l_start_x & 0x3)
        {
            golden_l_start_blk_x += 1;
        }
        golden_l_end_blk_x  = (golden_l_end_x >> 2);
        golden_r_start_blk_x = (golden_r_start_x >> 2);
        if (golden_r_start_x & 0x3)
        {
            golden_r_start_blk_x += 1;
        }
        golden_r_end_blk_x  = (golden_r_end_x >> 2);
    }

    if ( 0 == DVE_VERT_DS_MODE)
    {
        blk_height = (DVE_ORG_HEIGHT+7) >> 3;
        golden_l_start_blk_y = (golden_l_start_y >> 3);
        if (golden_l_start_y & 0x7)
        {
            golden_l_start_blk_y += 1;
        }
        golden_l_end_blk_y  = (golden_l_end_y >> 3);
        golden_r_start_blk_y = (golden_r_start_y >> 3);
        if (golden_r_start_y & 0x7)
        {
            golden_r_start_blk_y += 1;
        }
        golden_r_end_blk_y  = (golden_r_end_y >> 3);

    }
    else
    {
        blk_height = (DVE_ORG_HEIGHT+3) >> 2;
        golden_l_start_blk_y = (golden_l_start_y >> 2);
        if (golden_l_start_y & 0x3)
        {
            golden_l_start_blk_y += 1;
        }
        golden_l_end_blk_y  = (golden_l_end_y >> 2);
        golden_r_start_blk_y = (golden_r_start_y >> 2);
        if (golden_r_start_y & 0x3)
        {
            golden_r_start_blk_y += 1;
        }
        golden_r_end_blk_y  = (golden_r_end_y >> 2);

    }


    //Compare dpe_confo_l_frame_
#if DVE_ENABLE
    int_data_dma_0 = golden_l_start_blk_x;
    int_data_dma_1 = golden_l_start_blk_y;
    if (golden_l_end_x < 0)
    {
        int_data_dma_2 = -1;
    }
    else
    {
        int_data_dma_2 = golden_l_end_blk_x;
    }
    if (golden_l_end_y < 0)
    {
        int_data_dma_3 = -1;
    }
    else
    {
        int_data_dma_3 = golden_l_end_blk_y;
    }       
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_confo_l_frame); 
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_confo_r_frame);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_respo_l_frame); 
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_respo_r_frame);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_dvo_l_frame); 
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_dvo_r_frame);

    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_confo_l_frame, 
                            1, 
                            dveconfig.Dve_Confo_l.u4BufVA,
                            blk_width,
                            blk_height,
                            dveconfig.Dve_Confo_l.u4Stride,
                            int_data_dma_0,
                            int_data_dma_1,
                            int_data_dma_2,
                            int_data_dma_3,
                            dveconfig.Dve_Maski_l.u4BufVA,
                            1,
                            dveconfig.Dve_Maski_l.u4Stride
                            );  
    if (err_cnt_dma)
    {
        //Error
        printf("dpe left confo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe left confo bit true pass!!!\n");
    }
    

    //Compare the dpe_respo_l_frame_
    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_respo_l_frame, 
                            1, 
                            dveconfig.Dve_Respo_l.u4BufVA,
                            blk_width,
                            blk_height,
                            dveconfig.Dve_Respo_l.u4Stride,
                            int_data_dma_0,
                            int_data_dma_1,
                            int_data_dma_2,
                            int_data_dma_3,
                            dveconfig.Dve_Maski_l.u4BufVA,
                            1,
                            dveconfig.Dve_Maski_l.u4Stride
                            );  
    if (err_cnt_dma)
    {
        //Error
        printf("dpe left respo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe left respo bit true pass!!!\n");
    }



    //Compare dpe_confo_r_frame_
    int_data_dma_0 = golden_r_start_blk_x;
    int_data_dma_1 = golden_r_start_blk_y;
    if (golden_r_end_x < 0)
    {
        int_data_dma_2 = -1;
    }
    else
    {
        int_data_dma_2 = golden_r_end_blk_x;
    }
    if (golden_r_end_y < 0)
    {
        int_data_dma_3 = -1;
    }
    else
    {
        int_data_dma_3 = golden_r_end_blk_y;
    }       
    comp_roi_mem_with_file((char*)golden_dpe_confo_r_frame, 
                            1, 
                            dveconfig.Dve_Confo_r.u4BufVA,
                            blk_width,
                            blk_height,
                            dveconfig.Dve_Confo_r.u4Stride,
                            int_data_dma_0,
                            int_data_dma_1,
                            int_data_dma_2,
                            int_data_dma_3,
                            dveconfig.Dve_Maski_r.u4BufVA,
                            1,
                            dveconfig.Dve_Maski_r.u4Stride
                            );  
    if (err_cnt_dma)
    {
        //Error
        printf("dpe right confo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe right confo bit true pass!!!\n");
    }


    //Compare the dpe_respo_r_frame_
    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_respo_r_frame, 
                            1, 
                            dveconfig.Dve_Respo_r.u4BufVA,
                            blk_width,
                            blk_height,
                            dveconfig.Dve_Respo_r.u4Stride,
                            int_data_dma_0,
                            int_data_dma_1,
                            int_data_dma_2,
                            int_data_dma_3,
                            dveconfig.Dve_Maski_r.u4BufVA,
                            1,
                            dveconfig.Dve_Maski_r.u4Stride
                            );  
    if (err_cnt_dma)
    {
        //Error
        printf("dpe right respo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe right respo bit true pass!!!\n");
    }


    //Compare the dpe_dvo_l_frame_
    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_dvo_l_frame, 
                            0, 
                            dveconfig.Dve_Dvo_l.u4BufVA,
                            (blk_width << 1),
                            blk_height,
                            dveconfig.Dve_Dvo_l.u4Stride,
                            0,
                            0,
                            ((blk_width << 1)-1),
                            blk_height-1,
                            dveconfig.Dve_Maski_l.u4BufVA,
                            2,
                            dveconfig.Dve_Maski_l.u4Stride
                            );  
    if (err_cnt_dma)
    {
        //Error
        printf("dpe left dvo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe left dvo bit true pass!!!\n");
    }


    //Compare the dpe_dvo_r_frame_
    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_dvo_r_frame, 
                            0, 
                            dveconfig.Dve_Dvo_r.u4BufVA,
                            (blk_width << 1),
                            blk_height,
                            dveconfig.Dve_Dvo_r.u4Stride,
                            0,
                            0,
                            ((blk_width << 1)-1),
                            blk_height-1,
                            dveconfig.Dve_Maski_r.u4BufVA,
                            2,
                            dveconfig.Dve_Maski_r.u4Stride
                            );  
    if (err_cnt_dma)
    {
        //Error
        printf("dpe right dvo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe right dvo bit true pass!!!\n");
    }


#endif

#if WMFE_ENABLE
    do{
        usleep(100000);
        if (MTRUE == g_bWMFECallback)
        {
            break;
        }
    }while(1);
#endif
    //Start WMFE DRAM comparison !!
    //Compare dpe_wmf_dpo_frame_ 0
#if WMFE_ENABLE_0
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_wmf_dpo_frame_0);

    int_data_dma_0 = 0;
    int_data_dma_1 = 0;
    wmfe_curr_width = (REG_DPE_WMFE_SIZE_0 & 0xfff);
    wmfe_curr_height = ((REG_DPE_WMFE_SIZE_0 >> 16) & 0xfff);
    int_data_dma_2 = wmfe_curr_width-1;
    int_data_dma_3 = wmfe_curr_height-1;

    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_wmf_dpo_frame_0, 
                            0, 
                            wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpo.u4BufVA,
                            wmfe_curr_width,
                            wmfe_curr_height,
                            wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpo.u4Stride,
                            int_data_dma_0,
                            int_data_dma_1,
                            int_data_dma_2,
                            int_data_dma_3,
                            0,
                            0,
                            0
                            );  
    if (err_cnt_dma)
    {
        //Error
        printf("dpe WMFE DPO Frame0 bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe WMFE DPO Frame0 bit true pass!!!\n");
    }
#endif

           
        //Compare dpe_wmf_dpo_frame_ 1
#if WMFE_ENABLE_1
        mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_wmf_dpo_frame_1);
    
        int_data_dma_0 = 0;
        int_data_dma_1 = 0;
        wmfe_curr_width = (REG_DPE_WMFE_SIZE_1 & 0xfff);
        wmfe_curr_height = ((REG_DPE_WMFE_SIZE_1 >> 16) & 0xfff);
        int_data_dma_2 = wmfe_curr_width-1;
        int_data_dma_3 = wmfe_curr_height-1;
    
        err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_wmf_dpo_frame_1, 
                                0, 
                                wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpo.u4BufVA,
                                wmfe_curr_width,
                                wmfe_curr_height,
                                wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpo.u4Stride,
                                int_data_dma_0,
                                int_data_dma_1,
                                int_data_dma_2,
                                int_data_dma_3,
                                0,
                                0,
                                0
                                );  
        if (err_cnt_dma)
        {
            //Error
            printf("dpe WMFE DPO Frame1 bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
        }
        else
        {
            //Pass
            printf("dpe WMFE DPO Frame1 bit true pass!!!\n");
        }
#endif
            
                //Compare dpe_wmf_dpo_frame_ 2
#if WMFE_ENABLE_2
        mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_wmf_dpo_frame_2);
    
        int_data_dma_0 = 0;
        int_data_dma_1 = 0;
        wmfe_curr_width = (REG_DPE_WMFE_SIZE_2 & 0xfff);
        wmfe_curr_height = ((REG_DPE_WMFE_SIZE_2 >> 16) & 0xfff);
        int_data_dma_2 = wmfe_curr_width-1;
        int_data_dma_3 = wmfe_curr_height-1;
    
        err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_wmf_dpo_frame_2, 
                                0, 
                                wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpo.u4BufVA,
                                wmfe_curr_width,
                                wmfe_curr_height,
                                wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpo.u4Stride,
                                int_data_dma_0,
                                int_data_dma_1,
                                int_data_dma_2,
                                int_data_dma_3,
                                0,
                                0,
                                0
                                );  
        if (err_cnt_dma)
        {
            //Error
            printf("dpe WMFE DPO Frame2 bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
        }
        else
        {
            //Pass
            printf("dpe WMFE DPO Frame2 bit true pass!!!\n");
        }
#endif

#if DVE_ENABLE
    mpImemDrv->freeVirtBuf(&buf_imgi_l_frame);
    mpImemDrv->freeVirtBuf(&buf_imgi_r_frame);
    mpImemDrv->freeVirtBuf(&buf_dvi_l_frame);
    mpImemDrv->freeVirtBuf(&buf_dvi_r_frame);
    mpImemDrv->freeVirtBuf(&buf_maski_l_frame);
    mpImemDrv->freeVirtBuf(&buf_maski_r_frame);

    mpImemDrv->freeVirtBuf(&buf_dvo_l_frame);
    mpImemDrv->freeVirtBuf(&buf_dvo_r_frame);
    mpImemDrv->freeVirtBuf(&buf_confo_l_frame);
    mpImemDrv->freeVirtBuf(&buf_confo_r_frame);
    mpImemDrv->freeVirtBuf(&buf_respo_l_frame);
    mpImemDrv->freeVirtBuf(&buf_respo_r_frame);

#endif
#if WMFE_ENABLE_0
    mpImemDrv->freeVirtBuf(&buf_wmf_imgi_frame_0);
    mpImemDrv->freeVirtBuf(&buf_wmf_dpi_frame_0);
    mpImemDrv->freeVirtBuf(&buf_wmf_tbli_frame_0);
    mpImemDrv->freeVirtBuf(&buf_wmf_dpo_frame_0);
#endif
#if WMFE_ENABLE_1
    mpImemDrv->freeVirtBuf(&buf_wmf_imgi_frame_1);
    mpImemDrv->freeVirtBuf(&buf_wmf_dpi_frame_1);
    mpImemDrv->freeVirtBuf(&buf_wmf_tbli_frame_1);
    mpImemDrv->freeVirtBuf(&buf_wmf_dpo_frame_1);
    
#endif
#if WMFE_ENABLE_2
    mpImemDrv->freeVirtBuf(&buf_wmf_imgi_frame_2);
    mpImemDrv->freeVirtBuf(&buf_wmf_dpi_frame_2);
    mpImemDrv->freeVirtBuf(&buf_wmf_tbli_frame_2);
    mpImemDrv->freeVirtBuf(&buf_wmf_dpo_frame_2);
#endif
    pStream->uninit();   
    printf("--- [DpeStream uninit done\n");

    mpImemDrv->uninit();
    printf("--- [Imem uninit done\n");

    return ret;
}



/******************************************************************************
* save the buffer to the file
*******************************************************************************/
static bool
saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size)
{
    int nw, cnt = 0;
    uint32_t written = 0;

    //LOG_INF("(name, buf, size) = (%s, %x, %d)", fname, buf, size);
    //LOG_INF("opening file [%s]\n", fname);
    int fd = ::open(fname, O_RDWR | O_CREAT, S_IRWXU);
    if (fd < 0) {
        printf(": failed to create file [%s]: %s \n", fname, ::strerror(errno));
        return false;
    }

    //LOG_INF("writing %d bytes to file [%s]\n", size, fname);
    while (written < size) {
        nw = ::write(fd,
                     buf + written,
                     size - written);
        if (nw < 0) {
            printf(": failed to write to file [%s]: %s\n", fname, ::strerror(errno));
            break;
        }
        written += nw;
        cnt++;
    }
    //LOG_INF("done writing %d bytes to file [%s] in %d passes\n", size, fname, cnt);
    ::close(fd);
    return true;
}



