#
# libacdk
#
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
sinclude $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk

#

LOCAL_SRC_FILES += \
    acdk/AcdkBase.cpp \
    acdk/AcdkIF.cpp \
    acdk/AcdkMain.cpp \
    acdk/AcdkMhalBase.cpp \
    acdk/AcdkMhalEng.cpp \
    acdk/AcdkMhalPure.cpp \
    acdk/AcdkUtility.cpp \

LOCAL_SRC_FILES += \
    cctia/AcdkCctBase.cpp \
    cctia/AcdkCctMain.cpp \
    cctia/AcdkCctMhalBase.cpp \
    cctia/AcdkCctMhalEng.cpp \
    cctia/AcdkCctMhalPure.cpp \

LOCAL_SRC_FILES += \
    surfaceview/AcdkSurfaceView.cpp \
    surfaceview/surfaceView.cpp \

ACDK_BUILD_PURE_SMT := yes
ACDK_BUILD_DUMMY_ENG := no

$(info MTK_PATH_SOURCE      : $(MTK_PATH_SOURCE))
$(info MTK_PATH_COMMON      : $(MTK_PATH_COMMON))
$(info MTK_PATH_CUSTOM      : $(MTK_PATH_CUSTOM))
$(info MTK_PATH_CUSTOM_PLATFORM : $(MTK_PATH_CUSTOM_PLATFORM))
$(info PLATFORM_SDK_VERSION      : $(PLATFORM_SDK_VERSION))

# Note: "/bionic" and "/external/stlport/stlport" is for stlport.
LOCAL_C_INCLUDES += \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_VERSION)/acdk \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_VERSION)/drv \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_VERSION)/imageio \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_VERSION)/iopipe \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/$(MTKCAM_DRV_VERSION)/inc/acdk \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/$(MTKCAM_DRV_VERSION)/inc/cct \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/$(MTKCAM_DRV_VERSION)/inc/cctia \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/common/include/acdk \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/ \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/common/include \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/common/include/metadata \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/include/$(MTKCAM_DRV_VERSION) \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/aaa/include/$(MTKCAM_3A_VERSION)/ \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/aaa/include/$(MTKCAM_3A_VERSION)/Hal3/ \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/middleware/v1.4/include/ \
    $(TOP)/$(MTK_PATH_SOURCE)/kernel/drivers/video \
    $(TOP)/$(MTK_PATH_SOURCE)/kernel/include \
    $(TOP)/hardware/libhardware/include/ \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/ldvt/$(PLATFORM)/include \
    $(TOP)/$(MTK_PATH_CUSTOM_PLATFORM)/hal/inc/isp_tuning \
    $(TOP)/$(MTK_PATH_CUSTOM_PLATFORM)/hal/inc/aaa \
    $(TOP)/$(MTK_PATH_CUSTOM_PLATFORM)/hal/inc \
    $(TOP)/$(MTK_PATH_CUSTOM_PLATFORM)/hal/inc/debug_exif/aaa \
    $(TOP)/$(MTK_PATH_COMMON)/kernel/imgsensor/inc \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/common/include/acdk/

LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/m4u/$(PLATFORM)
LOCAL_C_INCLUDES += $(MTK_PATH_SOURCE)/frameworks/av/include
ifeq ($(ACDK_BUILD_DUMMY_ENG),no)
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/jpeg/include
endif

# Add a define value that can be used in the code to indicate that it's using LDVT now.
# For print message function when using LDVT.
# Note: It will be cleared by "CLEAR_VARS", so if it is needed in other module, it
# has to be set in other module again.

LOCAL_WHOLE_STATIC_LIBRARIES := \
    libcct

ifeq ($(BUILD_MTK_LDVT),yes)
    LOCAL_CFLAGS += -DUSING_MTK_LDVT
    LOCAL_WHOLE_STATIC_LIBRARIES += libuvvf
endif

LOCAL_SHARED_LIBRARIES := \
    libcutils \
    libimageio \
    libcam.iopipe \
    libcam_utils \
    libcam.halsensor \
    libcam.metadata \
    libm4u \
    libcamdrv_imem

LOCAL_SHARED_LIBRARIES += libstdc++

LOCAL_SHARED_LIBRARIES += libhardware
LOCAL_SHARED_LIBRARIES += libutils
LOCAL_SHARED_LIBRARIES += libbinder
LOCAL_SHARED_LIBRARIES += libdl
#LOCAL_SHARED_LIBRARIES += libcamera_client libcamera_client_mtk
LOCAL_SHARED_LIBRARIES += libcamera_client libmtkcamera_client
LOCAL_SHARED_LIBRARIES += libcam.hal3a.v3
LOCAL_SHARED_LIBRARIES += libcam.hal3a.v3.nvram
LOCAL_SHARED_LIBRARIES += libcam.hal3a.v3.lsctbl
LOCAL_SHARED_LIBRARIES += libcamalgo

ifeq ($(ACDK_BUILD_PURE_SMT),no)
LOCAL_SHARED_LIBRARIES += libcamalgo
LOCAL_SHARED_LIBRARIES += libcam.exif
LOCAL_SHARED_LIBRARIES += libcameracustom
endif
ifeq ($(ACDK_BUILD_DUMMY_ENG),no)
LOCAL_SHARED_LIBRARIES += libcam.metadataprovider
LOCAL_SHARED_LIBRARIES += libcam.hal3a.v3
LOCAL_SHARED_LIBRARIES += libcam.camshot
LOCAL_SHARED_LIBRARIES += libJpgEncPipe
endif

LOCAL_SHARED_LIBRARIES += liblog
#
LOCAL_PRELINK_MODULE := false

#
LOCAL_MODULE := libacdk

#

#
# Start of common part ------------------------------------
sinclude $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk

#-----------------------------------------------------------
LOCAL_CFLAGS += $(MTKCAM_CFLAGS)

#LOCAL_CFLAGS += -DACDK_CAMERA_3A
#LOCAL_CFLAGS += -DACDK_FAKE_SENSOR
#LOCAL_CFLAGS += -DACDK_BYPASS_P2

ifeq ($(ACDK_BUILD_DUMMY_ENG),yes)
	LOCAL_CFLAGS += -DACDK_DUMMY_ENG
endif
ifeq ($(ACDK_BUILD_PURE_SMT),yes)
    LOCAL_CFLAGS += -DACDK_PURE_SMT
endif

#-----------------------------------------------------------
LOCAL_C_INCLUDES += $(MTKCAM_C_INCLUDES)
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/middleware/common/include
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/gralloc_extra/include
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/ext/include


#-----------------------------------------------------------
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/include
LOCAL_C_INCLUDES += $(TOP)/system/media/camera/include
# End of common part ---------------------------------------
#
include $(BUILD_SHARED_LIBRARY)

#
include $(call all-makefiles-under, $(LOCAL_PATH))
