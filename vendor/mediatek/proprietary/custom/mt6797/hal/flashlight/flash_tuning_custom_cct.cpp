#define LOG_TAG "flash_tuning_custom_cct.cpp"
#define MTK_LOG_ENABLE 1
#include "string.h"
#include "camera_custom_nvram.h"
#include "camera_custom_types.h"
#include "camera_custom_AEPlinetable.h"
#include <cutils/log.h>
#include "flash_feature.h"
#include "flash_param.h"
#include "flash_tuning_custom.h"
#include <kd_camera_feature.h>

//==============================================================================
//
//==============================================================================
int cust_fillDefaultStrobeNVRam_main (void* data)
{
    int i;
    NVRAM_CAMERA_STROBE_STRUCT* p;
    p = (NVRAM_CAMERA_STROBE_STRUCT*)data;

	static short engTab[]={
          -1, 530,1017,1466,1722,1987,2185,2382,2571,2768,2951,3218,3485,3818,4060,4386,4535,4764,5047,5246,5520,5785,6017,6326,6556,6804,7023,
         687,1224,1715,2166,2420,2670,2869,3068,3257,3449,3638,3906,4171,4489,4743,5065,5214,5442,5726,5916,6203,6454,6671,6993,7214,7460,7545,
        1336,1877,2369,2821,3076,3470,3667,3867,4058,4256,4436,4706,4969,5294,5543,5860,6007,6236,6522,6723,6988,7242,7473,7768,7986,8167,8341,
        1932,2474,2967,3421,3673,4058,4257,4456,4640,4837,5025,5292,5556,5891,6131,6417,6568,6781,7101,7284,7540,7816,8014,8249,8417,8537,8584,
        2272,2814,3309,3761,4017,4338,4534,4733,4921,5123,5302,5561,5832,6165,6406,6693,6869,7092,7378,7562,7826,8051,8227,8420,8560,8671,8770,
        2593,3132,3747,4191,4404,4615,4815,5012,5203,5399,5581,5849,6095,6437,6685,6994,7150,7370,7640,7828,8058,8246,8397,8522,8632,8786,8866,
        2858,3397,4010,4455,4667,4881,5073,5271,5466,5664,5834,6113,6373,6686,6948,7230,7377,7620,7879,8049,8238,8403,8527,8679,8774,8820,8943,
        3124,3663,4280,4724,4936,5148,5340,5542,5735,5931,6113,6377,6640,6976,7213,7521,7659,7858,8076,8230,8364,8538,8650,8762,8861,8936,9018,
        3375,3911,4535,4973,5191,5402,5602,5800,5990,6172,6368,6634,6874,7222,7451,7745,7881,8049,8254,8334,8521,8636,8740,8841,8936,8965,9040,
        3632,4173,4790,5237,5449,5660,5847,6057,6247,6436,6622,6890,7128,7483,7699,7966,8073,8222,8387,8498,8597,8729,8788,8895,9005,9081,9154,
        3874,4417,5024,5481,5691,5904,6101,6299,6469,6687,6845,7133,7392,7674,7899,8127,8225,8348,8505,8598,8711,8810,8887,8989,9069,9101,9213,
        4234,4767,5401,5845,6056,6264,6462,6660,6832,7024,7226,7487,7722,7987,8151,8306,8410,8517,8639,8692,8804,8905,8987,9086,9165,9235,9307,
        4592,5136,5757,6202,6389,6622,6820,6997,7210,7394,7564,7768,7990,8208,8340,8468,8556,8648,8750,8827,8897,9004,9081,9189,9249,9341,9409,
        5041,5563,6204,6643,6836,7071,7266,7420,7624,7765,7923,8079,8255,8422,8526,8643,8701,8777,8881,8944,9040,9126,9199,9310,9383,9460,9521,
        5363,5911,6530,6971,7182,7350,7535,7734,7877,7984,8098,8249,8402,8542,8629,8742,8794,8868,8969,9036,9124,9216,9254,9395,9465,9533,9601,
        5793,6331,6948,7383,7571,7740,7891,8020,8134,8214,8328,8427,8533,8653,8733,8857,8910,8987,9084,9150,9210,9325,9408,9508,9574,9643,9697,
        5988,6491,7155,7565,7736,7852,7986,8134,8235,8329,8405,8516,8616,8729,8809,8916,8942,9043,9139,9209,9298,9384,9421,9561,9626,9644,9734,
        6284,6834,7378,7783,7898,8064,8143,8271,8357,8447,8515,8608,8682,8815,8894,8977,9051,9125,9223,9289,9373,9468,9537,9632,9665,9755,9762,
        6683,7215,7734,8027,8142,8230,8342,8422,8496,8565,8627,8718,8807,8923,9002,9105,9156,9233,9330,9391,9488,9572,9638,9731,9779,9833,  -1,
        6944,7447,7909,8166,8268,8332,8438,8512,8578,8647,8707,8798,8866,9000,9080,9174,9234,9303,9398,9473,9557,9612,9710,9793,9843,  -1,  -1,
        7290,7676,8108,8318,8380,8484,8551,8622,8686,8753,8809,8904,8992,9074,9179,9288,9317,9411,9504,9569,9644,9734,9793,9830,9910,  -1,  -1,
        7598,7955,8257,8441,8513,8588,8652,8720,8787,8849,8911,8997,9066,9196,9278,9383,9437,9507,9595,9662,9754,9817,9871,  -1,  -1,  -1,  -1,
        7833,8122,8383,8539,8614,8682,8753,8789,8880,8946,8977,9100,9186,9298,9372,9477,9525,9601,9696,9762,9801,9899,9946,  -1,  -1,  -1,  -1,
        8055,8292,8502,8647,8728,8799,8866,8934,8991,9027,9086,9210,9275,9415,9490,9598,9646,9723,9787,9863,9935,  -1,  -1,  -1,  -1,  -1,  -1,
        8188,8389,8596,8748,8822,8895,8952,9027,9083,9151,9206,9297,9350,9502,9576,9673,9729,9757,9885,9941,9999,  -1,  -1,  -1,  -1,  -1,  -1,
        8288,8473,8679,8827,8898,8935,9024,9101,9172,9236,9293,9382,9474,9573,9660,9764,9818,9884,9963,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
        8377,8557,8771,8919,8941,9061,9121,9185,9259,9318,9358,9468,9552,9669,9745,9837,9889,9959,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
	};    
    //version
    p->u4Version = NVRAM_CAMERA_STROBE_FILE_VERSION;
    //eng tab
    memcpy(p->engTab.yTab, engTab, sizeof(engTab));

    //tuningPara[8];
    for(i=0;i<8;i++)
    {
        p->tuningPara[i].yTarget = 300;
        p->tuningPara[i].fgWIncreaseLevelbySize = 10;
        p->tuningPara[i].fgWIncreaseLevelbyRef = 5;//0;
        p->tuningPara[i].ambientRefAccuracyRatio = 5;
        p->tuningPara[i].flashRefAccuracyRatio = 0;//1;
        p->tuningPara[i].backlightAccuracyRatio = 18;
        p->tuningPara[i].backlightUnderY = 40;
        p->tuningPara[i].backlightWeakRefRatio = 32;
        p->tuningPara[i].safetyExp =33322;//33322;
        p->tuningPara[i].maxUsableISO = 680;//680;
        p->tuningPara[i].yTargetWeight = 0;
        p->tuningPara[i].lowReflectanceThreshold = 13;
        p->tuningPara[i].flashReflectanceWeight = 3;
        p->tuningPara[i].bgSuppressMaxDecreaseEV = 20;
        p->tuningPara[i].bgSuppressMaxOverExpRatio = 6;
        p->tuningPara[i].fgEnhanceMaxIncreaseEV = 50;
        p->tuningPara[i].fgEnhanceMaxOverExpRatio = 6;//6;
        p->tuningPara[i].isFollowCapPline = 0;
        p->tuningPara[i].histStretchMaxFgYTarget = 300;//285;//266;
        p->tuningPara[i].histStretchBrightestYTarget = 480;//404;//328;
        p->tuningPara[i].fgSizeShiftRatio = 0;
        p->tuningPara[i].backlitPreflashTriggerLV = 90;
        p->tuningPara[i].backlitMinYTarget = 90;//100;
        p->tuningPara[i].minstameanpass = 80;
    }

    p->tuningPara[0].isFollowCapPline = 0;

    p->paraIdxForceOn[0] =1;    //default
    p->paraIdxForceOn[1] =0;    //LIB3A_AE_SCENE_OFF
    p->paraIdxForceOn[2] =0;    //LIB3A_AE_SCENE_AUTO
    p->paraIdxForceOn[3] =1;    //LIB3A_AE_SCENE_NIGHT
    p->paraIdxForceOn[4] =1;    //LIB3A_AE_SCENE_ACTION
    p->paraIdxForceOn[5] =1;    //LIB3A_AE_SCENE_BEACH
    p->paraIdxForceOn[6] =1;    //LIB3A_AE_SCENE_CANDLELIGHT
    p->paraIdxForceOn[7] =1;    //LIB3A_AE_SCENE_FIREWORKS
    p->paraIdxForceOn[8] =1;    //LIB3A_AE_SCENE_LANDSCAPE
    p->paraIdxForceOn[9] =1;    //LIB3A_AE_SCENE_PORTRAIT
    p->paraIdxForceOn[10] =1;   //LIB3A_AE_SCENE_NIGHT_PORTRAIT
    p->paraIdxForceOn[11] =1;   //LIB3A_AE_SCENE_PARTY
    p->paraIdxForceOn[12] =1;   //LIB3A_AE_SCENE_SNOW
    p->paraIdxForceOn[13] =1;   //LIB3A_AE_SCENE_SPORTS
    p->paraIdxForceOn[14] =1;   //LIB3A_AE_SCENE_STEADYPHOTO
    p->paraIdxForceOn[15] =1;   //LIB3A_AE_SCENE_SUNSET
    p->paraIdxForceOn[16] =1;   //LIB3A_AE_SCENE_THEATRE
    p->paraIdxForceOn[17] =1;   //LIB3A_AE_SCENE_ISO_ANTI_SHAKE
    p->paraIdxForceOn[18] =1;   //LIB3A_AE_SCENE_BACKLIGHT

    p->paraIdxAuto[0] =1;  //default
    p->paraIdxAuto[1] =0;  //LIB3A_AE_SCENE_OFF
    p->paraIdxAuto[2] =0;  //LIB3A_AE_SCENE_AUTO
    p->paraIdxAuto[3] =1;  //LIB3A_AE_SCENE_NIGHT
    p->paraIdxAuto[4] =1;  //LIB3A_AE_SCENE_ACTION
    p->paraIdxAuto[5] =1;  //LIB3A_AE_SCENE_BEACH
    p->paraIdxAuto[6] =1;  //LIB3A_AE_SCENE_CANDLELIGHT
    p->paraIdxAuto[7] =1;  //LIB3A_AE_SCENE_FIREWORKS
    p->paraIdxAuto[8] =1;  //LIB3A_AE_SCENE_LANDSCAPE
    p->paraIdxAuto[9] =1;  //LIB3A_AE_SCENE_PORTRAIT
    p->paraIdxAuto[10] =1; //LIB3A_AE_SCENE_NIGHT_PORTRAIT
    p->paraIdxAuto[11] =1; //LIB3A_AE_SCENE_PARTY
    p->paraIdxAuto[12] =1; //LIB3A_AE_SCENE_SNOW
    p->paraIdxAuto[13] =1; //LIB3A_AE_SCENE_SPORTS
    p->paraIdxAuto[14] =1; //LIB3A_AE_SCENE_STEADYPHOTO
    p->paraIdxAuto[15] =1; //LIB3A_AE_SCENE_SUNSET
    p->paraIdxAuto[16] =1; //LIB3A_AE_SCENE_THEATRE
    p->paraIdxAuto[17] =1; //LIB3A_AE_SCENE_ISO_ANTI_SHAKE
    p->paraIdxAuto[18] =1; //LIB3A_AE_SCENE_BACKLIGHT



    //--------------------
    //eng level
    //index mode
    //torch
    p->engLevel.torchDuty = 1;
    //af
    p->engLevel.afDuty = 0;
    //pf, mf, normal
    p->engLevel.pfDuty = -1;
    p->engLevel.mfDutyMax = 25;
    p->engLevel.mfDutyMin = -1;
    //low bat
    p->engLevel.IChangeByVBatEn = 1;
    p->engLevel.vBatL = 3600;
    p->engLevel.pfDutyL = 0;
    p->engLevel.mfDutyMaxL = 3;
    p->engLevel.mfDutyMinL = 1;
    //burst setting
    p->engLevel.IChangeByBurstEn = 1;
    p->engLevel.pfDutyB = 0;
    p->engLevel.mfDutyMaxB = 3;
    p->engLevel.mfDutyMinB = 1;
    //high current setting
    p->engLevel.decSysIAtHighEn = 1;
    p->engLevel.dutyH = 15;

    //LT
    p->engLevelLT.torchDuty = 0;
    //af
    p->engLevelLT.afDuty = 0;
    //pf, mf, normal
    p->engLevelLT.pfDuty = 0;
    p->engLevelLT.mfDutyMax = 25;
    p->engLevelLT.mfDutyMin = -1;
    p->engLevelLT.pfDutyL = -1;
    p->engLevelLT.mfDutyMaxL = 3;
    p->engLevelLT.mfDutyMinL = 1;
    p->engLevelLT.pfDutyB = -1;
    p->engLevelLT.mfDutyMaxB = 3;
    p->engLevelLT.mfDutyMinB = 1;

    p->dualTuningPara.toleranceEV_pos = 30; //0.1 EV
    p->dualTuningPara.toleranceEV_neg = 30; //0.1 EV

    p->dualTuningPara.XYWeighting = 64;
    p->dualTuningPara.useAwbPreferenceGain = 1;
    p->dualTuningPara.envOffsetIndex[0] = -150;
    p->dualTuningPara.envOffsetIndex[1] = -50;
    p->dualTuningPara.envOffsetIndex[2] = 0;
    p->dualTuningPara.envOffsetIndex[3] = 100;

    p->dualTuningPara.envXrOffsetValue[0] = 0;
    p->dualTuningPara.envXrOffsetValue[1] = 0;
    p->dualTuningPara.envXrOffsetValue[2] = 0;
    p->dualTuningPara.envXrOffsetValue[3] = 0;

    p->dualTuningPara.envYrOffsetValue[0] = 0;
    p->dualTuningPara.envYrOffsetValue[1] = 0;
    p->dualTuningPara.envYrOffsetValue[2] = 0;
    p->dualTuningPara.envYrOffsetValue[3] = 0;

    p->dualTuningPara.VarianceTolerance = 1;
    p->dualTuningPara.ChooseColdOrWarm = FLASH_CHOOSE_COLD;
    return 0;
}

int cust_fillDefaultFlashCalibrationNVRam_main (void* data)
{

    NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT* d;
    d = (NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT*)data;

    static short engTab[]=
         {1012,1922,2745,3495,4873,6114,7242,8280,9202,9999};
    memcpy(d->yTab, engTab, sizeof(engTab));


    //d->flashWBGain

    return 0;
}
