// Flash AWB tuning parameter
{
    9, //foreground percentage
    95, //background percentage
    2, //FgPercentage_Th1
    5, //FgPercentage_Th2
    10, //FgPercentage_Th3
    15, //FgPercentage_Th4
    250, //FgPercentage_Th1_Val
    250, //FgPercentage_Th2_Val
    250, //FgPercentage_Th3_Val
    250, //FgPercentage_Th4_Val
    10, //location_map_th1
    20, //location_map_th2
    40, //location_map_th3
    50, //location_map_th4
    100, //location_map_val1
    100, //location_map_val2
    100, //location_map_val3
    100, //location_map_val4
    0, //SelfTuningFbBgWeightTbl
    100, //FgBgTbl_Y0
    100, //FgBgTbl_Y1
    100, //FgBgTbl_Y2
    100, //FgBgTbl_Y3
    100, //FgBgTbl_Y4
    100, //FgBgTbl_Y5
    5, //YPrimeWeightTh[0]
    9, //YPrimeWeightTh[1]
    11, //YPrimeWeightTh[2]
    13, //YPrimeWeightTh[3]
    15, //YPrimeWeightTh[4]
    1, //YPrimeWeight[0]
    3, //YPrimeWeight[1]
    5, //YPrimeWeight[2]
    7, //YPrimeWeight[3]
    512, //FlashPreferenceGain R
    512, //FlashPreferenceGain G
    512, //FlashPreferenceGain B
},

// Flash AWB calibration
{{

    {512, 512, 512},
    {1250, 512, 892},
    {1092, 512, 851},
    {1069, 512, 842},
    {1070, 512, 840},
    {1069, 512, 819},
    {1068, 512, 813},
    {1068, 512, 806},
    {1068, 512, 802},
    {1067, 512, 796},
    {1069, 512, 792},
    {1068, 512, 786},
    {1068, 512, 783},
    {1067, 512, 778},
    {1068, 512, 775},
    {1068, 512, 772},
    {1068, 512, 769},
    {1067, 512, 766},
    {1068, 512, 763},
    {1068, 512, 760},
    {1068, 512, 758},
    {1068, 512, 756},
    {1069, 512, 751},
    {1069, 512, 748},
    {1070, 512, 744},
    {1071, 512, 741},
    {1072, 512, 738},
    {572, 512, 3763},
    {781, 512, 1173},
    {856, 512, 1016},
    {907, 512, 962},
    {927, 512, 944},
    {948, 512, 903},
    {965, 512, 883},
    {979, 512, 864},
    {987, 512, 851},
    {994, 512, 840},
    {1000, 512, 832},
    {1006, 512, 823},
    {1009, 512, 817},
    {1012, 512, 811},
    {1016, 512, 805},
    {1018, 512, 800},
    {1020, 512, 796},
    {1022, 512, 792},
    {1024, 512, 787},
    {1026, 512, 784},
    {1027, 512, 781},
    {1029, 512, 778},
    {1031, 512, 772},
    {1033, 512, 768},
    {1035, 512, 763},
    {1037, 512, 759},
    {512, 512, 512},
    {564, 512, 3295},
    {708, 512, 1318},
    {784, 512, 1120},
    {837, 512, 1039},
    {857, 512, 1014},
    {877, 512, 969},
    {900, 512, 940},
    {919, 512, 913},
    {931, 512, 896},
    {942, 512, 881},
    {950, 512, 870},
    {960, 512, 857},
    {965, 512, 847},
    {970, 512, 840},
    {975, 512, 832},
    {979, 512, 826},
    {982, 512, 821},
    {985, 512, 815},
    {990, 512, 810},
    {992, 512, 805},
    {994, 512, 802},
    {997, 512, 798},
    {1000, 512, 791},
    {1003, 512, 786},
    {1007, 512, 780},
    {1009, 512, 776},
    {512, 512, 512},
    {561, 512, 2560},
    {675, 512, 1421},
    {742, 512, 1205},
    {791, 512, 1105},
    {811, 512, 1069},
    {818, 512, 1040},
    {841, 512, 1001},
    {864, 512, 967},
    {880, 512, 946},
    {892, 512, 928},
    {902, 512, 913},
    {913, 512, 897},
    {919, 512, 886},
    {926, 512, 875},
    {934, 512, 864},
    {938, 512, 857},
    {943, 512, 851},
    {948, 512, 844},
    {953, 512, 837},
    {956, 512, 831},
    {959, 512, 827},
    {962, 512, 823},
    {967, 512, 814},
    {971, 512, 808},
    {975, 512, 801},
    {512, 512, 512},
    {512, 512, 512},
    {562, 512, 2381},
    {659, 512, 1467},
    {720, 512, 1262},
    {766, 512, 1150},
    {785, 512, 1109},
    {807, 512, 1054},
    {830, 512, 1014},
    {854, 512, 979},
    {868, 512, 957},
    {881, 512, 938},
    {892, 512, 922},
    {902, 512, 906},
    {910, 512, 893},
    {918, 512, 883},
    {925, 512, 872},
    {930, 512, 864},
    {936, 512, 858},
    {940, 512, 850},
    {944, 512, 843},
    {948, 512, 838},
    {951, 512, 833},
    {954, 512, 828},
    {959, 512, 819},
    {964, 512, 813},
    {967, 512, 806},
    {512, 512, 512},
    {512, 512, 512},
    {560, 512, 2156},
    {647, 512, 1489},
    {718, 512, 1250},
    {757, 512, 1148},
    {773, 512, 1115},
    {789, 512, 1084},
    {812, 512, 1040},
    {835, 512, 1001},
    {850, 512, 977},
    {863, 512, 957},
    {875, 512, 940},
    {886, 512, 923},
    {894, 512, 910},
    {901, 512, 898},
    {910, 512, 887},
    {914, 512, 878},
    {920, 512, 871},
    {925, 512, 863},
    {930, 512, 855},
    {933, 512, 849},
    {937, 512, 844},
    {940, 512, 840},
    {946, 512, 830},
    {951, 512, 823},
    {954, 512, 816},
    {512, 512, 512},
    {512, 512, 512},
    {560, 512, 2129},
    {635, 512, 1545},
    {699, 512, 1301},
    {736, 512, 1194},
    {751, 512, 1157},
    {765, 512, 1124},
    {788, 512, 1075},
    {812, 512, 1032},
    {826, 512, 1005},
    {840, 512, 982},
    {851, 512, 964},
    {864, 512, 945},
    {872, 512, 931},
    {880, 512, 919},
    {889, 512, 907},
    {895, 512, 897},
    {900, 512, 890},
    {905, 512, 880},
    {911, 512, 872},
    {915, 512, 866},
    {919, 512, 860},
    {921, 512, 855},
    {927, 512, 844},
    {932, 512, 836},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {561, 512, 2117},
    {626, 512, 1615},
    {684, 512, 1347},
    {719, 512, 1235},
    {733, 512, 1194},
    {747, 512, 1160},
    {770, 512, 1107},
    {792, 512, 1060},
    {807, 512, 1032},
    {821, 512, 1007},
    {832, 512, 987},
    {845, 512, 966},
    {853, 512, 952},
    {862, 512, 938},
    {870, 512, 925},
    {877, 512, 916},
    {882, 512, 907},
    {888, 512, 897},
    {894, 512, 888},
    {898, 512, 882},
    {901, 512, 876},
    {905, 512, 870},
    {912, 512, 859},
    {915, 512, 849},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {561, 512, 2102},
    {619, 512, 1653},
    {674, 512, 1383},
    {705, 512, 1269},
    {719, 512, 1227},
    {733, 512, 1191},
    {754, 512, 1137},
    {777, 512, 1087},
    {792, 512, 1056},
    {805, 512, 1029},
    {817, 512, 1007},
    {829, 512, 985},
    {838, 512, 970},
    {847, 512, 956},
    {855, 512, 942},
    {861, 512, 931},
    {867, 512, 922},
    {873, 512, 912},
    {878, 512, 903},
    {883, 512, 896},
    {887, 512, 889},
    {890, 512, 883},
    {897, 512, 871},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {560, 512, 2147},
    {612, 512, 1687},
    {661, 512, 1423},
    {691, 512, 1308},
    {704, 512, 1265},
    {717, 512, 1228},
    {738, 512, 1171},
    {759, 512, 1118},
    {774, 512, 1084},
    {788, 512, 1056},
    {799, 512, 1033},
    {812, 512, 1009},
    {823, 512, 991},
    {829, 512, 978},
    {837, 512, 962},
    {844, 512, 951},
    {850, 512, 941},
    {856, 512, 930},
    {862, 512, 920},
    {866, 512, 913},
    {870, 512, 906},
    {874, 512, 900},
    {879, 512, 886},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {560, 512, 2137},
    {610, 512, 1700},
    {654, 512, 1448},
    {683, 512, 1333},
    {695, 512, 1291},
    {708, 512, 1250},
    {728, 512, 1193},
    {749, 512, 1140},
    {763, 512, 1103},
    {776, 512, 1074},
    {788, 512, 1050},
    {801, 512, 1025},
    {809, 512, 1008},
    {818, 512, 992},
    {826, 512, 976},
    {833, 512, 965},
    {839, 512, 955},
    {845, 512, 943},
    {851, 512, 933},
    {855, 512, 925},
    {859, 512, 918},
    {862, 512, 910},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {561, 512, 2128},
    {605, 512, 1721},
    {648, 512, 1471},
    {676, 512, 1357},
    {687, 512, 1313},
    {699, 512, 1273},
    {719, 512, 1213},
    {740, 512, 1157},
    {754, 512, 1122},
    {767, 512, 1091},
    {778, 512, 1067},
    {791, 512, 1041},
    {799, 512, 1023},
    {808, 512, 1007},
    {816, 512, 990},
    {823, 512, 978},
    {829, 512, 967},
    {835, 512, 956},
    {841, 512, 944},
    {845, 512, 936},
    {848, 512, 928},
    {851, 512, 920},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {561, 512, 2118},
    {602, 512, 1737},
    {642, 512, 1494},
    {670, 512, 1376},
    {681, 512, 1335},
    {692, 512, 1293},
    {711, 512, 1233},
    {731, 512, 1175},
    {745, 512, 1140},
    {758, 512, 1109},
    {769, 512, 1083},
    {781, 512, 1056},
    {790, 512, 1038},
    {798, 512, 1021},
    {806, 512, 1004},
    {813, 512, 991},
    {819, 512, 979},
    {825, 512, 967},
    {831, 512, 956},
    {835, 512, 947},
    {838, 512, 938},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {561, 512, 2111},
    {600, 512, 1746},
    {639, 512, 1510},
    {664, 512, 1396},
    {675, 512, 1352},
    {686, 512, 1311},
    {704, 512, 1251},
    {724, 512, 1192},
    {738, 512, 1155},
    {750, 512, 1125},
    {761, 512, 1098},
    {773, 512, 1069},
    {782, 512, 1051},
    {790, 512, 1033},
    {798, 512, 1015},
    {805, 512, 1002},
    {810, 512, 991},
    {817, 512, 978},
    {822, 512, 966},
    {826, 512, 956},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {561, 512, 2099},
    {597, 512, 1760},
    {634, 512, 1531},
    {658, 512, 1417},
    {668, 512, 1374},
    {679, 512, 1334},
    {699, 512, 1266},
    {715, 512, 1213},
    {729, 512, 1175},
    {741, 512, 1142},
    {752, 512, 1115},
    {764, 512, 1086},
    {772, 512, 1067},
    {780, 512, 1050},
    {789, 512, 1030},
    {795, 512, 1017},
    {801, 512, 1005},
    {807, 512, 992},
    {812, 512, 978},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {561, 512, 2089},
    {596, 512, 1767},
    {631, 512, 1545},
    {654, 512, 1432},
    {664, 512, 1390},
    {674, 512, 1348},
    {692, 512, 1286},
    {710, 512, 1226},
    {723, 512, 1188},
    {736, 512, 1154},
    {746, 512, 1127},
    {758, 512, 1099},
    {766, 512, 1078},
    {774, 512, 1060},
    {782, 512, 1041},
    {788, 512, 1027},
    {794, 512, 1014},
    {800, 512, 1000},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {561, 512, 2083},
    {594, 512, 1777},
    {628, 512, 1560},
    {650, 512, 1444},
    {661, 512, 1401},
    {670, 512, 1363},
    {688, 512, 1298},
    {705, 512, 1240},
    {719, 512, 1199},
    {730, 512, 1166},
    {739, 512, 1139},
    {751, 512, 1110},
    {760, 512, 1088},
    {768, 512, 1070},
    {777, 512, 1050},
    {782, 512, 1036},
    {788, 512, 1023},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {561, 512, 2076},
    {593, 512, 1782},
    {626, 512, 1568},
    {648, 512, 1458},
    {657, 512, 1413},
    {667, 512, 1374},
    {683, 512, 1311},
    {701, 512, 1250},
    {713, 512, 1213},
    {725, 512, 1177},
    {735, 512, 1150},
    {747, 512, 1118},
    {755, 512, 1098},
    {763, 512, 1079},
    {771, 512, 1059},
    {777, 512, 1045},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {561, 512, 2073},
    {594, 512, 1777},
    {623, 512, 1580},
    {644, 512, 1473},
    {654, 512, 1428},
    {663, 512, 1388},
    {678, 512, 1327},
    {696, 512, 1264},
    {708, 512, 1226},
    {720, 512, 1190},
    {729, 512, 1162},
    {741, 512, 1132},
    {749, 512, 1110},
    {756, 512, 1091},
    {765, 512, 1070},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {561, 512, 2062},
    {591, 512, 1792},
    {621, 512, 1588},
    {642, 512, 1482},
    {651, 512, 1438},
    {660, 512, 1398},
    {676, 512, 1336},
    {693, 512, 1273},
    {705, 512, 1234},
    {716, 512, 1199},
    {726, 512, 1170},
    {737, 512, 1140},
    {744, 512, 1120},
    {751, 512, 1100},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {562, 512, 2056},
    {591, 512, 1796},
    {620, 512, 1598},
    {640, 512, 1491},
    {649, 512, 1446},
    {658, 512, 1408},
    {673, 512, 1346},
    {689, 512, 1286},
    {701, 512, 1244},
    {711, 512, 1209},
    {722, 512, 1178},
    {732, 512, 1149},
    {741, 512, 1126},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {562, 512, 2048},
    {590, 512, 1801},
    {619, 512, 1603},
    {637, 512, 1501},
    {647, 512, 1454},
    {656, 512, 1415},
    {671, 512, 1353},
    {686, 512, 1293},
    {699, 512, 1251},
    {709, 512, 1216},
    {719, 512, 1186},
    {729, 512, 1158},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512}
}}

