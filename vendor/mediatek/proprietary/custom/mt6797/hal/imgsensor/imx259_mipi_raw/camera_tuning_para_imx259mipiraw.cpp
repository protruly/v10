#include <utils/Log.h>
#include <fcntl.h>
#include <math.h>
#include <cutils/log.h>
#include <string.h>
#include "camera_custom_nvram.h"
#include "camera_custom_sensor.h"
#include "image_sensor.h"
#include "kd_imgsensor_define.h"
#include "camera_AE_PLineTable_imx259mipiraw.h"
#include "camera_info_imx259mipiraw.h"
#include "camera_custom_AEPlinetable.h"
#include "camera_custom_tsf_tbl.h"

#define NVRAM_TUNING_PARAM_NUM  5341001

const NVRAM_CAMERA_ISP_PARAM_STRUCT CAMERA_ISP_DEFAULT_VALUE =
{{
    //Version
    Version: NVRAM_CAMERA_PARA_FILE_VERSION,

    //SensorId
    SensorId: SENSOR_ID,
    ISPComm:{
      {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      }
    },
    ISPPca: {
#include INCLUDE_FILENAME_ISP_PCA_PARAM
    },
    ISPRegs:{
#include INCLUDE_FILENAME_ISP_REGS_PARAM
    },
    ISPMulitCCM:{
      Poly22:{
        88650,      // i4R_AVG
        21153,      // i4R_STD
        104700,      // i4B_AVG
        26882,      // i4B_STD
         0,      // i4R_MAX
        0,      // i4R_MIN
        0,      // i4G_MAX
        0,      // i4G_MIN
        0,      // i4B_MAX
        0,      // i4B_MIN
        {  // i4P00[9]
            9640000, -4597500, 70000, -1385000, 6822500, -312500, -75000, -4212500, 9412500
        },
        {  // i4P10[9]
            5242649, -5342475, 101321, -1297486, 884506, 439148, 8897, 3181972, -3202458
        },
        {  // i4P01[9]
            4456821, -4554346, 99603, -1554595, 640072, 932924, -173436, 1931753, -1765440
        },
        {  // i4P20[9]
            0, 0, 0, 0, 0, 0, 0, 0, 0
        },
        {  // i4P11[9]
            0, 0, 0, 0, 0, 0, 0, 0, 0
        },
        {  // i4P02[9]
            0, 0, 0, 0, 0, 0, 0, 0, 0
        }
      },
      AWBGain:{
        // Strobe
        {
      1137,    // i4R
          512,    // i4G
          684    // i4B
        },
        // A
        {
          625,    // i4R
          512,    // i4G
          1333    // i4B
        },
        // TL84
        {
          853,    // i4R
          512,    // i4G
          1097    // i4B
        },
        // CWF
        {
          931,    // i4R
          512,    // i4G
          1074    // i4B
        },
        // D65
        {
          1137,    // i4R
          512,    // i4G
          684    // i4B
        },
        // Reserved 1
        {
            512,    // i4R
            512,    // i4G
            512    // i4B
        },
        // Reserved 2
        {
            512,    // i4R
            512,    // i4G
            512    // i4B
        },
        // Reserved 3
        {
            512,    // i4R
            512,    // i4G
            512    // i4B
        }
      },
      Weight:{
        1, // Strobe
        1, // A
        1, // TL84
        1, // CWF
        1, // D65
        1, // Reserved 1
        1, // Reserved 2
        1  // Reserved 3
      }
    },
    MDPMulitCCM:{
      Poly22:{
        125225,      // i4R_AVG
        32141,      // i4R_STD
        165675,      // i4B_AVG
        30249,      // i4B_STD
        646,      // i4R_MAX
        527,      // i4R_MIN
        789,      // i4G_MAX
        670,      // i4G_MIN
        856,      // i4B_MAX
        721,      // i4B_MIN
        {  // i4P00[9]
            5995000, -505000, -370000, -1387500, 7165000, -657500, -532500, -2045000, 7697500
        },
        {  // i4P10[9]
            725905, -1081879, 355974, 627096, -389018, -238079, 626246, -86040, -540207
        },
        {  // i4P01[9]
            257857, -576218, 318361, -96902, -138571, 235473, 24959, 193148, -218107
        },
        {  // i4P20[9]
            0, 0, 0, 0, 0, 0, 0, 0, 0
        },
        {  // i4P11[9]
            0, 0, 0, 0, 0, 0, 0, 0, 0
        },
        {  // i4P02[9]
            0, 0, 0, 0, 0, 0, 0, 0, 0
        }
      },

      AWBGain:{
        // Strobe
       {
      1137,    // i4R
          512,    // i4G
          684    // i4B
        },
           // A
        {
          625,    // i4R
          512,    // i4G
          1333    // i4B
        },
        // TL84
        {
          853,    // i4R
          512,    // i4G
          1097    // i4B
        },
        // CWF
        {
          931,    // i4R
          512,    // i4G
          1074    // i4B
        },
        // D65
        {
          1137,    // i4R
          512,    // i4G
          684    // i4B
        },
        // Reserved 1
        {
          512,    // i4R
          512,    // i4G
          512    // i4B
        },
        // Reserved 2
        {
          512,    // i4R
          512,    // i4G
          512    // i4B
        },
        // Reserved 3
        {
          512,    // i4R
          512,    // i4G
          512    // i4B
        }
      },
      Weight:{
        1, // Strobe
        1, // A
        1, // TL84
        1, // CWF
        1, // D65
        1, // Reserved 1
        1, // Reserved 2
        1  // Reserved 3
      }
    },
    isp_ccm_ratio: 0.500000,
    //bInvokeSmoothCCM
    bInvokeSmoothCCM: MTRUE,
    DngMetadata:
    {
        0,  //i4RefereceIlluminant1
        3,  //i4RefereceIlluminant2
      rNoiseProfile:{
        {
          S:{
            0.000004,      // a
            0.000336       // b
          },
          O:{
            0.000000,      // a
            -0.000234       // b
          }
        },
        {
          S:{
            0.000004,      // a
            0.000336       // b
          },
          O:{
            0.000000,      // a
            -0.000234       // b
          }
        },
        {
          S:{
            0.000004,      // a
            0.000336       // b
          },
          O:{
            0.000000,      // a
            -0.000234       // b
          }
        },
        {
          S:{
            0.000004,      // a
            0.000336       // b
          },
          O:{
            0.000000,      // a
            -0.000234       // b
          }
        }
      }
  },
  rGmaParam:
  {
      {   // Normal Mode
            eISP_DYNAMIC_GMA_MODE,  // eGMAMode
          8,
			{
				{   // i4ContrastWeightingTbl
				        0, 0, 33, 66, 100, 100, 100, 100, 100, 100, 100
				},
				{   // i4LVWeightingTbl
				        0, 0, 0, 0, 0, 0, 0, 0, 0, 33, 66, 100, 100, 100, 100, 100, 100, 100, 100, 100
				},
			},
			{
				1,      // i4Enable
				1,      // i4WaitAEStable
				4       // i4Speed
			},
			{
				0,      // i4Enable
				2047,      // i4CenterPt
				50,      // i4LowPercent
				100000,      // i4LowCurve100
				100000,      // i4HighCurve100
				50,      // i4HighPercent
				100,      // i4SlopeH100
				100       // i4SlopeL100
			},
			{
				0       // i4Enable
			},
      },
      {   // HDR Mode
            eISP_DYNAMIC_GMA_MODE,  // eGMAMode
          8,
			{
				{   // i4ContrastWeightingTbl
				        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
				},
				{   // i4LVWeightingTbl
				        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 33, 66, 100, 100, 100, 100, 100, 100, 100, 100
				},
			},
			{
				1,      // i4Enable
				1,      // i4WaitAEStable
				4       // i4Speed
			},
			{
				0,      // i4Enable
				2047,      // i4CenterPt
				50,      // i4LowPercent
				100000,      // i4LowCurve100
				100000,      // i4HighCurve100
				50,      // i4HighPercent
				100,      // i4SlopeH100
				100       // i4SlopeL100
			},
			{
				0       // i4Enable
			},
      },
      {   // Reserve 0
            eISP_DYNAMIC_GMA_MODE,  // eGMAMode
          8,
			{
				{   // i4ContrastWeightingTbl
				        0, 0, 33, 66, 100, 100, 100, 100, 100, 100, 100
				},
				{   // i4LVWeightingTbl
				        0, 0, 0, 0, 0, 0, 0, 0, 0, 33, 66, 100, 100, 100, 100, 100, 100, 100, 100, 100
				},
			},
			{
				1,      // i4Enable
				1,      // i4WaitAEStable
				4       // i4Speed
			},
			{
				0,      // i4Enable
				2047,      // i4CenterPt
				50,      // i4LowPercent
				100000,      // i4LowCurve100
				100000,      // i4HighCurve100
				50,      // i4HighPercent
				100,      // i4SlopeH100
				100       // i4SlopeL100
			},
			{
				0       // i4Enable
			},
      },
      {   // Reserve 1
            eISP_DYNAMIC_GMA_MODE,  // eGMAMode
          8,
			{
				{   // i4ContrastWeightingTbl
				        0, 0, 33, 66, 100, 100, 100, 100, 100, 100, 100
				},
				{   // i4LVWeightingTbl
				        0, 0, 0, 0, 0, 0, 0, 0, 0, 33, 66, 100, 100, 100, 100, 100, 100, 100, 100, 100
				},
			},
			{
				1,      // i4Enable
				1,      // i4WaitAEStable
				4       // i4Speed
			},
			{
				0,      // i4Enable
				2047,      // i4CenterPt
				50,      // i4LowPercent
				100000,      // i4LowCurve100
				100000,      // i4HighCurve100
				50,      // i4HighPercent
				100,      // i4SlopeH100
				100       // i4SlopeL100
			},
			{
				0       // i4Enable
			},
      }
  },
  rLceParam:
  {
      {   // Normal Mode
			1,      // i4AutoLCEEnable
			{   // rDefLCEParam
				0,      // i4LCEBa
				50,      // i4LCEPa
				110       // i4LCEPb
			},
			{   // rAutoLCEParam
				3,      // i4LCESeg
				800,      // i4LCEContrastRatio
				{
					0,      // i4LCEBa
					{   // i4LCEPa
						{70, 70, 70, 70, 70, 70, 70, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{70, 70, 70, 70, 70, 70, 70, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{70, 70, 70, 70, 70, 70, 60, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{70, 70, 70, 70, 70, 70, 60, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{60, 60, 60, 60, 60, 60, 60, 50, 50, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{60, 60, 60, 60, 60, 60, 60, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{60, 60, 60, 60, 60, 60, 60, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50}
					},
					{   // i4LCEPb
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 80, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 80, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 90, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 90, 90, 80, 80, 80, 80, 80},
						{120, 120, 120, 120, 120, 130, 100, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 100, 100, 100, 100, 100, 100, 100, 100},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 100, 110, 110, 110, 110, 110, 110, 110},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 110, 110, 110, 110, 110, 110, 110, 110},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 110, 110, 110, 110, 110, 110, 110, 110}
					},
				},
				{   // rLCESmooth
					1,      // i4Enable
					0,      // i4WaitAEStable
					4      // i4Speed
				},
				{   // rLCEFlare
                    0   // i4Enable
				}
			}
      },
      {   // HDR Mode
			1,      // i4AutoLCEEnable
			{   // rDefLCEParam
				0,      // i4LCEBa
				50,      // i4LCEPa
				110       // i4LCEPb
			},
			{   // rAutoLCEParam
				3,      // i4LCESeg
				800,      // i4LCEContrastRatio
				{
					0,      // i4LCEBa
					{   // i4LCEPa
						{70, 70, 70, 70, 70, 70, 70, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{70, 70, 70, 70, 70, 70, 70, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{70, 70, 70, 70, 70, 70, 60, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{70, 70, 70, 70, 70, 70, 60, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{60, 60, 60, 60, 60, 60, 60, 50, 50, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{60, 60, 60, 60, 60, 60, 60, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{60, 60, 60, 60, 60, 60, 60, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50}
					},
					{   // i4LCEPb
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 80, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 80, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 90, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 90, 90, 80, 80, 80, 80, 80},
						{120, 120, 120, 120, 120, 130, 100, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 100, 100, 100, 100, 100, 100, 100, 100},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 100, 110, 110, 110, 110, 110, 110, 110},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 110, 110, 110, 110, 110, 110, 110, 110},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 110, 110, 110, 110, 110, 110, 110, 110}
					},
				},
				{   // rLCESmooth
					1,      // i4Enable
					0,      // i4WaitAEStable
					4      // i4Speed
				},
				{   // rLCEFlare
					0,      // i4Enable
				}
			}
      },
      {   // Reserve 0
			1,      // i4AutoLCEEnable
			{   // rDefLCEParam
				0,      // i4LCEBa
				50,      // i4LCEPa
				110       // i4LCEPb
			},
			{   // rAutoLCEParam
				3,      // i4LCESeg
				800,      // i4LCEContrastRatio
				{
					0,      // i4LCEBa
					{   // i4LCEPa
						{70, 70, 70, 70, 70, 70, 70, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{70, 70, 70, 70, 70, 70, 70, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{70, 70, 70, 70, 70, 70, 60, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{70, 70, 70, 70, 70, 70, 60, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{60, 60, 60, 60, 60, 60, 60, 50, 50, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{60, 60, 60, 60, 60, 60, 60, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{60, 60, 60, 60, 60, 60, 60, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50}
					},
					{   // i4LCEPb
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 80, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 80, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 90, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 90, 90, 80, 80, 80, 80, 80},
						{120, 120, 120, 120, 120, 130, 100, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 100, 100, 100, 100, 100, 100, 100, 100},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 100, 110, 110, 110, 110, 110, 110, 110},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 110, 110, 110, 110, 110, 110, 110, 110},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 110, 110, 110, 110, 110, 110, 110, 110}
					},
				},
				{   // rLCESmooth
					1,      // i4Enable
					0,      // i4WaitAEStable
					4      // i4Speed
				},
				{   // rLCEFlare
					0,      // i4Enable
				}
			}
      },
      {   // Reserve 1
			1,      // i4AutoLCEEnable
			{   // rDefLCEParam
				0,      // i4LCEBa
				50,      // i4LCEPa
				110       // i4LCEPb
			},
			{   // rAutoLCEParam
				3,      // i4LCESeg
				800,      // i4LCEContrastRatio
				{
					0,      // i4LCEBa
					{   // i4LCEPa
						{70, 70, 70, 70, 70, 70, 70, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{70, 70, 70, 70, 70, 70, 70, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{70, 70, 70, 70, 70, 70, 60, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{70, 70, 70, 70, 70, 70, 60, 60, 60, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{60, 60, 60, 60, 60, 60, 60, 50, 50, 50, 50, 50, 50, 50, 40, 40, 40, 40, 40},
						{60, 60, 60, 60, 60, 60, 60, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{60, 60, 60, 60, 60, 60, 60, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50}
					},
					{   // i4LCEPb
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 80, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 80, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 90, 80, 80, 80, 80, 80, 80},
						{130, 130, 130, 130, 130, 130, 100, 100, 90, 90, 90, 90, 90, 90, 80, 80, 80, 80, 80},
						{120, 120, 120, 120, 120, 130, 100, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 100, 100, 100, 100, 100, 100, 100, 100},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 100, 110, 110, 110, 110, 110, 110, 110},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 110, 110, 110, 110, 110, 110, 110, 110},
						{120, 120, 120, 120, 120, 110, 100, 90, 90, 90, 90, 110, 110, 110, 110, 110, 110, 110, 110}
					},
				},
				{   // rLCESmooth
					1,      // i4Enable
					0,      // i4WaitAEStable
					4      // i4Speed
				},
				{   // rLCEFlare
					0,      // i4Enable
				}
			}
      }
  },
  ANR_TBL:
  {0}
}};

const NVRAM_CAMERA_3A_STRUCT CAMERA_3A_NVRAM_DEFAULT_VALUE =
{
    NVRAM_CAMERA_3A_FILE_VERSION, // u4Version
    SENSOR_ID, // SensorId

    // AE NVRAM
    {
        // rDevicesInfo
        {
            1144,    // u4MinGain, 1024 base = 1x
            16384,    // u4MaxGain, 16x
            100,    // u4MiniISOGain, ISOxx  
            128,    // u4GainStepUnit, 1x/8 
            20649,    // u4PreExpUnit 
            30,    // u4PreMaxFrameRate
            10325,    // u4VideoExpUnit  
            30,    // u4VideoMaxFrameRate 
            1024,    // u4Video2PreRatio, 1024 base = 1x 
            10325,    // u4CapExpUnit 
            30,    // u4CapMaxFrameRate
            1024,    // u4Cap2PreRatio, 1024 base = 1x
            10325,    // u4Video1ExpUnit
            119,    // u4Video1MaxFrameRate
            1024,    // u4Video12PreRatio, 1024 base = 1x
            20649,    // u4Video2ExpUnit
            30,    // u4Video2MaxFrameRate
            1024,    // u4Video22PreRatio, 1024 base = 1x
            20649,    // u4Custom1ExpUnit
            30,    // u4Custom1MaxFrameRate
            1024,    // u4Custom12PreRatio, 1024 base = 1x
            20649,    // u4Custom2ExpUnit
            30,    // u4Custom2MaxFrameRate
            1024,    // u4Custom22PreRatio, 1024 base = 1x
            20649,    // u4Custom3ExpUnit
            30,    // u4Custom3MaxFrameRate
            1024,    // u4Custom32PreRatio, 1024 base = 1x
            20649,    // u4Custom4ExpUnit
            30,    // u4Custom4MaxFrameRate
            1024,    // u4Custom42PreRatio, 1024 base = 1x
            20649,    // u4Custom5ExpUnit
            30,    // u4Custom5MaxFrameRate
            1024,    // u4Custom52PreRatio, 1024 base = 1x
            20,    // u4LensFno, Fno = 2.8
            350    // u4FocusLength_100x
        },
        // rHistConfig
        {
            4, // 2,   // u4HistHighThres
            40,  // u4HistLowThres
            2,   // u4MostBrightRatio
            1,   // u4MostDarkRatio
            160, // u4CentralHighBound
            20,  // u4CentralLowBound
            {240, 230, 220, 210, 200}, // u4OverExpThres[AE_CCT_STRENGTH_NUM]
            {62, 70, 82, 108, 141},  // u4HistStretchThres[AE_CCT_STRENGTH_NUM]
            {18, 22, 26, 30, 34}       // u4BlackLightThres[AE_CCT_STRENGTH_NUM]
        },
        // rCCTConfig
        {
            TRUE,            // bEnableBlackLight
            TRUE,            // bEnableHistStretch
            TRUE,           // bEnableAntiOverExposure
            TRUE,            // bEnableTimeLPF
            TRUE,            // bEnableCaptureThres
            TRUE,            // bEnableVideoThres
            TRUE,            // bEnableVideo1Thres
            TRUE,            // bEnableVideo2Thres
            TRUE,            // bEnableCustom1Thres
            TRUE,            // bEnableCustom2Thres
            TRUE,            // bEnableCustom3Thres
            TRUE,            // bEnableCustom4Thres
            TRUE,            // bEnableCustom5Thres
            TRUE,            // bEnableStrobeThres
            53,    // u4AETarget
            47,                // u4StrobeAETarget

            50,                // u4InitIndex
            4,                 // u4BackLightWeight
            32,                // u4HistStretchWeight
            4,                 // u4AntiOverExpWeight
            2,                 // u4BlackLightStrengthIndex
            2,                 // u4HistStretchStrengthIndex
            2,                 // u4AntiOverExpStrengthIndex
            2,                 // u4TimeLPFStrengthIndex
            {1, 3, 5, 7, 8}, // u4LPFConvergeTable[AE_CCT_STRENGTH_NUM]
            90,                // u4InDoorEV = 9.0, 10 base
            -10,               // i4BVOffset delta BV = -2.3
            64,                 // u4PreviewFlareOffset
            64,                 // u4CaptureFlareOffset
            3,                 // u4CaptureFlareThres
            64,                 // u4VideoFlareOffset
            3,                 // u4VideoFlareThres
            64,               // u4CustomFlareOffset
            3,                 //  u4CustomFlareThres
            64,                 // u4StrobeFlareOffset //12 bits
            3,                 // u4StrobeFlareThres // 0.5%
            130,    // u4PrvMaxFlareThres
            0,                 // u4PrvMinFlareThres
            130,    // u4VideoMaxFlareThres
            0,                 // u4VideoMinFlareThres
            18,                // u4FlatnessThres              // 10 base for flatness condition.
            75,    // u4FlatnessStrength
            //rMeteringSpec
            {
                //rHS_Spec
                {
                    TRUE,//bEnableHistStretch           // enable histogram stretch
                    1024,//u4HistStretchWeight          // Histogram weighting value
                    40,//u4Pcent                      // 
                    230,//u4Thd                        // 0~255
                    100, //74,//u4FlatThd                    // 0~255
                    120,//u4FlatBrightPcent
                    120,//u4FlatDarkPcent
                    //sFlatRatio
                    {
                        1000,  //i4X1
                        1024,  //i4Y1
                        2400, //i4X2
                        0     //i4Y2
                    },
                    TRUE, //bEnableGreyTextEnhance
                    1800, //u4GreyTextFlatStart, > sFlatRatio.i4X1, < sFlatRatio.i4X2
                    {
                        10,     //i4X1
                        1024,   //i4Y1
                        80,     //i4X2
                        0       //i4Y2
                    }
                },
                //rAOE_Spec
                {
                    TRUE, //bEnableAntiOverExposure
                    1024, //u4AntiOverExpWeight
                  20,//38,//35,// 28, //25,//20,    //u4Pcent
                    220,//200 //u4Thd
                    TRUE, //bEnableCOEP
                    1, //u4COEPcent
                    86,//110, //u4COEThd
                    0, //u4BVCompRatio
                    //sCOEYRatio;     // the outer y ratio
                    {
                        23,   //i4X1
                        1024,  //i4Y1
                        47,   //i4X2
                        0     //i4Y2
                    },
                    //sCOEDiffRatio;  // inner/outer y difference ratio
                    {
                        1500, //i4X1
                        0,    //i4Y1
                        2100, //i4X2
                        1024   //i4Y2
                    }
                },
                //rABL_Spec
                {
                    TRUE,//bEnableBlackLigh
                    1024,//u4BackLightWeigh
                   400,//u4Pcent
                    160,//19,//u4Thd
                    255, // center luminance
                    236,//256, // final target limitation, 256/128 = 2x
                    //sFgBgEVRatio
                    {
                        3000,//2200, //i4X1
                        0,    //i4Y1
                        4500,//4000, //i4X2
                        1024   //i4Y2
                    },
                    //sBVRatio
                    {
                        3800,//i4X1
                        0,   //i4Y1
                        5000,//i4X2
                        1024  //i4Y2
                    }
                },
                //rNS_Spec
                {
                    TRUE, // bEnableNightScene
                    10,//u4Pcent
                    180,//u4Thd
                    75,//u4FlatThd
                    180,//u4BrightTonePcent
                    120,//80//40//u4BrightToneThd
                    500,//u4LowBndPcent
                    5,//u4LowBndThd
                    10,//20,//12,//14,//19,//22,//26,//u4LowBndThdLimit
                    50,//u4FlatBrightPcent
                    300,//u4FlatDarkPcent
                    //sFlatRatio
                    {
                        1200, //i4X1
                        1024, //i4Y1
                      2800,//  2400, //i4X2
                        0    //i4Y2
                    },
                    //sBVRatio
                    {
                        -500, //i4X1
                        1024,  //i4Y1
                        3000, //i4X2
                        0     //i4Y2
                    },
                    TRUE, // bEnableNightSkySuppresion
                    //sSkyBVRatio
                    {
                        -4000, //i4X1
                        1024, //i4X2
                        -2000,  //i4Y1
                        0     //i4Y2
                    }
                },
                // rTOUCHFD_Spec
                {
                    40, //uMeteringYLowBound;
                    50, //uMeteringYHighBound;
                    25,//45,//40, //uFaceYLowBound;
                    40,//55,//50, //uFaceYHighBound;
                    3,//4,//3,  //uFaceCentralWeight;
                    120,//u4MeteringStableMax;
                    80, //u4MeteringStableMin;
                }
            }, //End rMeteringSpec
            // rFlareSpec
            {
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, //uPrvFlareWeightArr[16];
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, //uVideoFlareWeightArr[16];
                96,                                               //u4FlareStdThrHigh;
                48,                                               //u4FlareStdThrLow;
                0,                                                //u4PrvCapFlareDiff;
                2,//4,                                                //u4FlareMaxStepGap_Fast;      
                0,                                                //u4FlareMaxStepGap_Slow;
                1800,                                             //u4FlarMaxStepGapLimitBV;
                2,//0,                                                //u4FlareAEStableCount;
            },
            //rAEMoveRatio =
            {
                100, //u4SpeedUpRatio
                100,//120, //u4GlobalRatio
                190, //u4Bright2TargetEnd
                10,//20, //u4Dark2TargetStart
                90, //u4B2TEnd
                85,//70,  //u4B2TStart 70
                70,//60,  //u4D2TEnd 60
                85,  //u4D2TStart
            },

            //rAEVideoMoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAEVideo1MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAEVideo2MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom1MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom2MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom3MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom4MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom5MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAEFaceMoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                190,  //u4Bright2TargetEnd
                10,    //u4Dark2TargetStart
                80, //u4B2TEnd
                30,  //u4B2TStart
                20,  //u4D2TEnd
                60,  //u4D2TStart
            },

            //rAETrackingMoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                190,  //u4Bright2TargetEnd
                10,    //u4Dark2TargetStart
                80, //u4B2TEnd
                30,  //u4B2TStart
                20,  //u4D2TEnd
                60,  //u4D2TStart
            },
            //rAEAOENVRAMParam =
            {
                1,      // i4AOEStrengthIdx: 0 / 1 / 2
                130,    // u4BVCompRatio
                {
                    {
                        47,  //u4Y_Target
                        25,  //u4AOE_OE_percent
                        210,  //u4AOE_OEBound
                        10,    //u4AOE_DarkBound
                        950,    //u4AOE_LowlightPrecent
                        1,    //u4AOE_LowlightBound
                        145,    //u4AOESceneLV_L
                        180,    //u4AOESceneLV_H
                        40,    //u4AOE_SWHdrLE_Bound
                    },
                    {
                        47,  //u4Y_Target
                        25,  //u4AOE_OE_percent
                        210,  //u4AOE_OEBound
                        15, //20,    //u4AOE_DarkBound
                        950,    //u4AOE_LowlightPrecent
                        3, //10,    //u4AOE_LowlightBound
                        145,    //u4AOESceneLV_L
                        180,    //u4AOESceneLV_H
                        40,    //u4AOE_SWHdrLE_Bound
                    },
                    {
                        47,  //u4Y_Target
                        25,  //u4AOE_OE_percent
                        210,  //u4AOE_OEBound
                        25,    //u4AOE_DarkBound
                        950,    //u4AOE_LowlightPrecent
                        8,    //u4AOE_LowlightBound
                        145,    //u4AOESceneLV_L
                        180,    //u4AOESceneLV_H
                        40,    //u4AOE_SWHdrLE_Bound
                    }
                }
            }
        },
        // rHDRAEConfig
        {
            3072,   // i4RMGSeg
            35,     // i4HDRTarget_L;
            40,     // i4HDRTarget_H;
            100,    // i4HDRTargetLV_L;
            120,    // i4HDRTargetLV_H;
            20,     // i4OverExpoRatio;
            212,    // i4OverExpoTarget;
            120,    // i4OverExpoLV_L;
            180,    // i4OverExpoLV_H;
            4,      // i4UnderExpoContrastThr;
            {
             // Contrast:
             //  0   1   2   3   4   5   6   7   8   9  10
                 3,  3,  3,  3,  3,  2,  1,  1,  1,  1,  1    // i4UnderExpoTargetTbl[AE_HDR_UNDEREXPO_CONTRAST_TARGET_TBL_NUM];
            },
            950,    // i4UnderExpoRatio
            1000,    // i4AvgExpoRatio
            8,    // i4AvgExpoTarget
            768,    // i4HDRAESpeed
            2,    // i4HDRConvergeThr
            40,    // i4SWHdrLEThr
            20,    // i4SWHdrSERatio
            180,    // i4SWHdrSETarget
            1024,    // i4SWHdrBaseGain
        },
    },
    // AWB NVRAM
    AWB_NVRAM_START
    {
        {
            {
                // AWB calibration data
                {
                    // rUnitGain (unit gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rGoldenGain (golden sample gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rUnitGain TL84 (unit gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rGoldenGain TL84 (golden sample gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                     // rUnitGain Alight (unit gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rGoldenGain Alight (golden sample gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rTuningUnitGain (Tuning sample unit gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rD65Gain (D65 WB gain: 1.0 = 512)
                    {
                    1178,    // D65Gain_R
			                    512,    // D65Gain_G
                    758    // D65Gain_B
                    }
                },
                // Original XY coordinate of AWB light source
                {
                    // Strobe
                        {
                                0,    // i4X
                                0    // i4Y
                        },
                        // Horizon
                        {
                    -469,    // OriX_Hor
                    -479    // OriY_Hor
		                },
		                // A
		                {
                    -300,    // OriX_A
                    -479    // OriY_A
		                },
		                // TL84
		                {
                    -73,    // OriX_TL84
                    -511    // OriY_TL84
		                },
		                // CWF
		                {
                    -69,    // OriX_CWF
                    -561    // OriY_CWF
		                },
		                // DNP
		                {
                    46,    // OriX_DNP
                    -485    // OriY_DNP
		                },
		                // D65
		                {
                    163,    // OriX_D65
                    -453    // OriY_D65
		                },
		                // DF
		                {
                    54,    // OriX_DF
                    -503    // OriY_DF
                        }
                },
                // Rotated XY coordinate of AWB light source
                {
                    // Strobe
                        {
                                0,    // i4X
                                0    // i4Y
                        },
                        // Horizon
                        {
                    -501,    // RotX_Hor
                    -444    // RotY_Hor
		                },
		                // A
		                {
                    -333,    // RotX_A
                    -456    // RotY_A
		                },
		                // TL84
		                {
                    -109,    // RotX_TL84
                    -504    // RotY_TL84
		                },
		                // CWF
		                {
                    -108,    // RotX_CWF
                    -554    // RotY_CWF
		                },
		                // DNP
		                {
                    12,    // RotX_DNP
                    -486    // RotY_DNP
		                },
		                // D65
		                {
                    131,    // RotX_D65
                    -463    // RotY_D65
		                },
		                // DF
		                {
                    18,    // RotX_DF
                    -505    // RotY_DF
                        }
                },
                // AWB gain of AWB light source
                {
                    // Strobe
                        {
                                512,    // i4R
                                512,    // i4G
                                512    // i4B
                        },
                        // Horizon
                        {
                    519,    // AWBGAIN_HOR_R
                    512,    // AWBGAIN_HOR_G
                    1849    // AWBGAIN_HOR_B
                },
                // A 
                {
                    653,    // AWBGAIN_A_R
                    512,    // AWBGAIN_A_G
                    1471    // AWBGAIN_A_B
                },
                // TL84 
                {
                    926,    // AWBGAIN_TL84_R
                    512,    // AWBGAIN_TL84_G
                    1128    // AWBGAIN_TL84_B
                },
                // CWF 
                {
                    997,    // AWBGAIN_CWF_R
                    512,    // AWBGAIN_CWF_G
                    1201    // AWBGAIN_CWF_B
                },
                // DNP 
                {
                    1051,    // AWBGAIN_DNP_R
                    512,    // AWBGAIN_DNP_G
                    927    // AWBGAIN_DNP_B
                },
                // D65 
                {
                    1178,    // AWBGAIN_D65_R
                    512,    // AWBGAIN_D65_G
                    758    // AWBGAIN_D65_B
                },
                // DF 
                {
                    1088,    // AWBGAIN_DF_R
                    512,    // AWBGAIN_DF_G
                    941    // AWBGAIN_DF_B
                }
            },
            // Rotation matrix parameter
            {
                4,    // RotationAngle
                255,    // Cos
                18    // Sin
            },
            // Daylight locus parameter
            {
                -145,    // i4SlopeNumerator
                128    // i4SlopeDenominator
            },
            // Predictor gain
            {
                100, // i4PrefRatio100
                // DaylightLocus_L
                {
                    1115,    // i4R
                    522,    // i4G
                    790    // i4B
                },
                // DaylightLocus_H
                {
                    925,    // i4R
                    512,    // i4G
                    1005    // i4B
                },
                // Temporal General
                {
                    650,    // i4R
                    512,    // i4G
                    500    // i4B
                },
                // AWB LSC Gain
                {
                    995,        // i4R
                    512,        // i4G
                    923        // i4B
                }
            },
            // AWB light area
            {
                // Strobe:FIXME
                {
                    0,    // i4RightBound
                    0,    // i4LeftBound
                    0,    // i4UpperBound
                    0    // i4LowerBound
                },
                // Tungsten
                {
                    -220,    // TungRightBound
                    -901,    // TungLeftBound
                    -370,    // TungUpperBound
                    -455    // TungLowerBound
                },
                // Warm fluorescent
                {
                    -220,    // WFluoRightBound
                    -901,    // WFluoLeftBound
                    -455,    // WFluoUpperBound
                    -644    // WFluoLowerBound
                },
                // Fluorescent
                {
                    -53,    // FluoRightBound
                    -220,    // FluoLeftBound
                    -370,    // FluoUpperBound
                    -510    // FluoLowerBound
                },
                // CWF
                {
                -40,    // CWFRightBound
                -220,    // CWFLeftBound
                -510,    // CWFUpperBound
                -760    // CWFLowerBound
                },
                // Daylight
                {
                    185,    // DayRightBound
                    -53,    // DayLeftBound
                    -360,    // DayUpperBound
                    -510    // DayLowerBound
                },
                // Shade
                {
                    491,    // ShadeRightBound
                    185,    // ShadeLeftBound
                    -380,    // ShadeUpperBound
                    -510    // ShadeLowerBound
                },
                // Daylight Fluorescent
                {
                    161,    // DFRightBound
                    -40,    // DFLeftBound
                    -510,    // DFUpperBound
                    -640    // DFLowerBound
                }
            },
            // PWB light area
            {
                // Reference area
                {
                    491,    // PRefRightBound
                    -901,    // PRefLeftBound
                    0,    // PRefUpperBound
                    -760    // PRefLowerBound
                },
                // Daylight
                {
                    210,    // PDayRightBound
                    -53,    // PDayLeftBound
                    -360,    // PDayUpperBound
                    -510    // PDayLowerBound
                },
                // Cloudy daylight
                {
                    310,    // PCloudyRightBound
                    135,    // PCloudyLeftBound
                    -360,    // PCloudyUpperBound
                    -510    // PCloudyLowerBound
                },
                // Shade
                {
                    410,    // PShadeRightBound
                    135,    // PShadeLeftBound
                    -360,    // PShadeUpperBound
                    -510    // PShadeLowerBound
                },
                // Twilight
                {
                    -53,    // PTwiRightBound
                    -213,    // PTwiLeftBound
                    -360,    // PTwiUpperBound
                    -510    // PTwiLowerBound
                },
                // Fluorescent
                {
                    181,    // PFluoRightBound
                    -209,    // PFluoLeftBound
                    -413,    // PFluoUpperBound
                    -604    // PFluoLowerBound
                },
                // Warm fluorescent
                {
                    -233,    // PWFluoRightBound
                    -433,    // PWFluoLeftBound
                    -413,    // PWFluoUpperBound
                    -604    // PWFluoLowerBound
                },
                // Incandescent
                {
                    -233,    // PIncaRightBound
                    -433,    // PIncaLeftBound
                    -360,    // PIncaUpperBound
                    -510    // PIncaLowerBound
                },
                // Gray World
                {
                    5000,    // PGWRightBound
                    -5000,    // PGWLeftBound
                    5000,    // PGWUpperBound
                    -5000    // PGWLowerBound
                }
            },
            // PWB default gain	
            {
                // Daylight
                {
                    1061,    // PWB_Day_R
                    512,    // PWB_Day_G
                    790    // PWB_Day_B
                },
                // Cloudy daylight
                {
                    1271,    // PWB_Cloudy_R
                    512,    // PWB_Cloudy_G
                    641    // PWB_Cloudy_B
                },
                // Shade
                {
                    1354,    // PWB_Shade_R
                    512,    // PWB_Shade_G
                    596    // PWB_Shade_B
                },
                // Twilight
                {
                    813,    // PWB_Twi_R
                    512,    // PWB_Twi_G
                    1072    // PWB_Twi_B
                },
                // Fluorescent
                {
                    1050,    // PWB_Fluo_R
                    512,    // PWB_Fluo_G
                    990    // PWB_Fluo_B
                },
                // Warm fluorescent
                {
                    703,    // PWB_WFluo_R
                    512,    // PWB_WFluo_G
                    1571    // PWB_WFluo_B
                },
                // Incandescent
                {
                    632,    // PWB_Inca_R
                    512,    // PWB_Inca_G
                    1433    // PWB_Inca_B
                },
                // Gray World
                {
                    512,    // PWB_GW_R
                    512,    // PWB_GW_G
                    512    // PWB_GW_B
                }
            },
            // AWB preference color	
            {
                // Tungsten
                {
                    45,    // TUNG_SLIDER
                    5400    // TUNG_OFFS
                },
                // Warm fluorescent	
                {
                    45,    // WFluo_SLIDER
                    5000    // WFluo_OFFS
                },
                // Shade
                {
                    50,    // Shade_SLIDER
                    500    // Shade_OFFS
                },
                // Sunset Area
                {
                    -5,   // i4Sunset_BoundXr_Thr
                    -486    // i4Sunset_BoundYr_Thr
                },
                // Shade F Area
                {
                    -160,   // i4BoundXrThr
                    -400    // i4BoundYrThr
                },
                // Shade F Vertex
                {
                    -113,   // i4BoundXrThr
                    -519    // i4BoundYrThr
                },
                // Shade CWF Area
                {
                    -200,   // i4BoundXrThr
                    -560    // i4BoundYrThr
                },
                // Shade CWF Vertex
                {
                    -126,   // i4BoundXrThr
                    -584    // i4BoundYrThr
                },
            },
            // CCT estimation
            {
                // CCT
                {
                    2300,    // i4CCT[0]
                    2850,    // i4CCT[1]
                    3750,    // i4CCT[2]
                    5100,    // i4CCT[3]
                    6500     // i4CCT[4]
                },
                // Rotated X coordinate
                {
                -632,    // i4RotatedXCoordinate[0]
                -464,    // i4RotatedXCoordinate[1]
                -240,    // i4RotatedXCoordinate[2]
                -119,    // i4RotatedXCoordinate[3]
                0    // i4RotatedXCoordinate[4]
                }
            }
        },
        // Algorithm Tuning Parameter
        {
            // AWB Backup Enable
            0,

            // Daylight locus offset LUTs for tungsten
            {
                21, // i4Size: LUT dimension
                {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                {0, 350, 800, 1222, 1444, 1667, 1889, 2111, 2333, 2556, 2778, 3000, 3222, 3444, 3667, 3889, 4111, 4333, 4556, 4778, 5000} // i4LUTOut
            },
            // Daylight locus offset LUTs for WF
            {
                21, // i4Size: LUT dimension
                {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                {0, 350, 700, 1000, 1444, 1667, 1889, 3000, 2333, 2556, 2778, 3000, 3222, 3444, 3667, 3889, 4111, 4333, 4556, 4778, 5000} // i4LUTOut
            },
            // Daylight locus offset LUTs for Shade
            {
                21, // i4Size: LUT dimension
                {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                {0, 350, 800, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6000, 6000, 6500, 8000, 8500, 9000, 9500, 10000} // i4LUTOut
                },
                // Preference gain for each light source
                {
                    //        LV0              LV1              LV2              LV3              LV4              LV5              LV6              LV7              LV8              LV9
                    {
                    {512, 512, 512}, {512, 512, 500}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, 
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                }, // STROBE
                {
                    {495, 512, 470}, {500, 512, 470}, {505, 512, 475}, {505, 512, 485}, {505, 512, 495}, {520, 512, 475}, {520, 512, 470}, {520, 512, 475}, {510, 512, 470}, {520, 512, 470}, 
                    {490, 512, 515}, {495, 512, 512}, {500, 512, 512}, {505, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                }, // TUNGSTEN
                {
                    {515, 512, 485}, {520, 512, 480}, {525, 512, 482}, {515, 512, 480}, {512, 512, 485}, {525, 512, 475}, {525, 512, 470}, {525, 512, 470}, {525, 512, 470}, {522, 512, 472}, 
                    {492, 512, 512}, {492, 512, 512}, {492, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                }, // WARM F
                {
                    {495, 512, 480}, {495, 512, 475}, {500, 512, 480}, {508, 512, 485}, {515, 512, 490}, {525, 512, 490}, {532, 512, 485}, {525, 512, 490}, {520, 512, 495}, {520, 512, 492}, 
                    {512, 512, 485}, {505, 512, 492}, {492, 512, 508}, {490, 512, 515}, {495, 512, 515}, {502, 512, 510}, {505, 512, 505}, {502, 512, 510}, {502, 512, 512}
                }, // F
                {
                    {532, 512, 512}, {532, 512, 512}, {532, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {500, 512, 500}, {500, 512, 500}, {490, 512, 500}, {480, 512, 500}, 
                    {475, 512, 505}, {475, 512, 505}, {475, 512, 505}, {480, 512, 505}, {480, 512, 505}, {495, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                }, // CWF
                {
                    {525, 512, 505}, {525, 512, 505}, {525, 512, 505}, {505, 512, 505}, {512, 512, 505}, {512, 512, 505}, {515, 512, 505}, {505, 512, 500}, {505, 512, 500}, {505, 512, 500}, 
                    {505, 512, 500}, {500, 512, 515}, {500, 512, 515}, {505, 512, 490}, {505, 512, 490}, {508, 512, 495}, {508, 512, 505}, {508, 512, 505}, {508, 512, 505}
                }, // DAYLIGHT
                {
                    {512, 512, 500}, {512, 512, 500}, {512, 512, 500}, {512, 512, 500}, {512, 512, 500}, {512, 512, 500}, {512, 512, 500}, {512, 512, 500}, {512, 512, 500}, {512, 512, 500}, 
                    {512, 512, 500}, {512, 512, 500}, {512, 512, 500}, {512, 512, 500}, {512, 512, 500}, {512, 512, 500}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                }, // SHADE
                {
                    {522, 512, 505}, {522, 512, 500}, {522, 512, 495}, {515, 512, 490}, {520, 512, 485}, {525, 512, 470}, {528, 512, 470}, {520, 512, 480}, {510, 512, 505}, {500, 512, 510}, 
                    {505, 512, 505}, {502, 512, 512}, {505, 512, 512}, {510, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                } // DAYLIGHT F
                },
                // Parent block weight parameter
                {
                    1,      // bEnable
                    6           // i4ScalingFactor: [6] 1~12, [7] 1~6, [8] 1~3, [9] 1~2, [>=10]: 1
                },
                // AWB LV threshold for predictor
                {
                115,    // i4InitLVThr_L
                155,    // i4InitLVThr_H
                100      // i4EnqueueLVThr
                },
                // AWB number threshold for temporal predictor
                {
                        65,     // i4Neutral_ParentBlk_Thr
                    //LV0   1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  50,  25,   2,   2,   2,   2,   2,   2,   2}  // (%) i4CWFDF_LUTThr
                },
                // AWB light neutral noise reduction for outdoor
                {
                    //LV0  1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    // Non neutral
	                { 3,   3,   3,   3,   3,   3,   3,   3,    3,   3,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // Flurescent
                    {   0,   0,   0,   0,   0,   3,   5,   5,   5,   5,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // CWF
                    {   0,   0,   0,   0,   0,   3,   5,   5,   5,   5,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // Daylight
	                { 0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   0,   2,   2,   2,   2,   2,   2,   2,   2},  // (%)
	                // DF
	                { 0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
                },
                // AWB feature detection
                {
                    // Sunset Prop
                    {
                        1,          // i4Enable
                        120,        // i4LVThr_L
                        130,        // i4LVThr_H
                        10,         // i4SunsetCountThr
                        0,          // i4SunsetCountRatio_L
                        171         // i4SunsetCountRatio_H
                    },

                    // Shade F Detection
                    {
                        1,          // i4Enable
                        50,        // i4LVThr_L
                        90,        // i4LVThr_H
                        80         // i4DaylightProb
                    },

                    // Shade CWF Detection
                    {
                        1,          // i4Enable
                        50,        // i4LVThr_L
                        130,        // i4LVThr_H
                        110         // i4DaylightProb
                    }

                },

                // AWB non-neutral probability for spatial and temporal weighting look-up table (Max: 100; Min: 0)
                {
                    //LV0   1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18
                    {   0,  33,  66, 100, 100,  65,  65,  65,  65, 100, 100,  70,  30,  20,  10,   0,   0,   0,   0}
                },

                    // AWB daylight locus probability look-up table (Max: 100; Min: 0)
                    {   //LV0    1     2     3      4     5     6     7     8      9      10     11    12   13     14    15   16    17    18
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  50,  25,   0,   0,   0,   0}, // Strobe
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  75,  50,  25,   0,   0,   0}, // Tungsten
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  75,  50,  25,  25,  25,   0,   0,   0}, // Warm fluorescent
                    { 100, 100, 100, 100, 100, 100, 100,  80,  80,  80,  80,  75,  50,  25,  25,  25,   0,   0,   0}, // Fluorescent
                    {  90,  90,  90,  90,  90,  90,  90,  90,  90,  90,  90,  80,  70,  70,  60,  30,   0,   0,   0}, // CWF
                    { 100, 100, 100, 100, 100, 100, 100, 100,  80,  92,  85,  83,  83,  83,  80,  75,  50,  30,  20}, // Daylight
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  75,  50,  25,   0,   0,   0,   0}, // Shade
                    {  90,  90,  90,  90,  90,  90,  90,  90,  90,  90,  80,  55,  30,  30,  30,  30,   0,   0,   0} // Daylight fluorescent
                },

                // AWB tuning information
                {
                    0,		// project code
                    0,		// model
                    0,		// date
                    0,		// reserved 0
                    0,		// reserved 1
                    0,		// reserved 2
                    0,		// reserved 3
                    0,		// reserved 4
                }
            }
        },
        {
            {
                // AWB calibration data
                {
                    // rUnitGain (unit gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rGoldenGain (golden sample gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rUnitGain TL84 (unit gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rGoldenGain TL84 (golden sample gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                     // rUnitGain Alight (unit gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rGoldenGain Alight (golden sample gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rTuningUnitGain (Tuning sample unit gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rD65Gain (D65 WB gain: 1.0 = 512)
                    {
                    1220,    // D65Gain_R
			                    512,    // D65Gain_G
                    711    // D65Gain_B
                    }
                },
                // Original XY coordinate of AWB light source
                {
                    // Strobe
                        {
                    199,    // i4X
                    -442    // i4Y
                },
                        // Horizon
                        {
                    -421,    // OriX_Hor
                    -409    // OriY_Hor
                },
		                // A
		                {
                    -264,    // OriX_A
                    -422    // OriY_A
		                },
		                // TL84
		                {
                    -56,    // OriX_TL84
                    -478    // OriY_TL84
                },
                // CWF
                {
                    -30,    // OriX_CWF
                    -568    // OriY_CWF
                },
                // DNP
                {
                    88,    // OriX_DNP
                    -453    // OriY_DNP
                },
                // D65
                {
                    199,    // OriX_D65
                    -442    // OriY_D65
                },
                // DF
                {
                    147,    // OriX_DF
                    -531    // OriY_DF
                }
            },
            // Rotated XY coordinate of AWB light source
            {
                // Strobe
                {
                    215,    // i4X
                    -435    // i4Y
                },
                // Horizon
                {
                    -407,    // RotX_Hor
                    -424    // RotY_Hor
                },
                // A
                {
                    -249,    // RotX_A
                    -431    // RotY_A
                },
                // TL84
                {
                    -39,    // RotX_TL84
                    -480    // RotY_TL84
                },
                // CWF
                {
                    -10,    // RotX_CWF
                    -569    // RotY_CWF
                },
                // DNP
                {
                    104,    // RotX_DNP
                    -450    // RotY_DNP
                },
                // D65
                {
                    215,    // RotX_D65
                    -435    // RotY_D65
                },
                // DF
                {
                    166,    // RotX_DF
                    -526    // RotY_DF
                }
            },
            // AWB gain of AWB light source
            {
                // Strobe 
                {
                    1220,    // i4R
                    512,    // i4G
                    711    // i4B
                },
                // Horizon 
                {
                    512,    // AWBGAIN_HOR_R
                    520,    // AWBGAIN_HOR_G
                    1599    // AWBGAIN_HOR_B
                },
                // A 
                {
                    635,    // AWBGAIN_A_R
                    512,    // AWBGAIN_A_G
                    1297    // AWBGAIN_A_B
                },
                // TL84 
                {
                    907,    // AWBGAIN_TL84_R
                    512,    // AWBGAIN_TL84_G
                    1055    // AWBGAIN_TL84_B
                },
                // CWF 
                {
                    1061,    // AWBGAIN_CWF_R
                    512,    // AWBGAIN_CWF_G
                    1150    // AWBGAIN_CWF_B
                },
                // DNP 
                {
                    1065,    // AWBGAIN_DNP_R
                    512,    // AWBGAIN_DNP_G
                    840    // AWBGAIN_DNP_B
                },
                // D65 
                {
                    1220,    // AWBGAIN_D65_R
                    512,    // AWBGAIN_D65_G
                    711    // AWBGAIN_D65_B
                },
                // DF 
                {
                    1281,    // AWBGAIN_DF_R
                    512,    // AWBGAIN_DF_G
                    861    // AWBGAIN_DF_B
                }
            },
            // Rotation matrix parameter
            {
                0,    // RotationAngle
                256,    // Cos
                -9    // Sin
            },
            // Daylight locus parameter
            {
                -120,    // i4SlopeNumerator
                128    // i4SlopeDenominator
            },
            // Predictor gain
            {
                101, // i4PrefRatio100
                // DaylightLocus_L
                {
                    1190,    // i4R
                    530,    // i4G
                    731    // i4B
                },
                // DaylightLocus_H
                {
                    1066,    // i4R
                    512,    // i4G
                    805    // i4B
                },
                // Temporal General
                {
                    1220,    // i4R
                    512,    // i4G
                    711    // i4B
                },
                // AWB LSC Gain
                {
                    1022,        // i4R
                    512,        // i4G
                    839        // i4B
                }
            },
            // AWB light area
            {
                // Strobe:FIXME
                {
                    265,    // i4RightBound
                    165,    // i4LeftBound
                    -385,    // i4UpperBound
                    -485    // i4LowerBound
                },
                // Tungsten
                {
                    -123,    // TungRightBound
                    -807,    // TungLeftBound
                    -369,    // TungUpperBound
                    -456    // TungLowerBound
                },
                // Warm fluorescent
                {
                    -123,    // WFluoRightBound
                    -807,    // WFluoLeftBound
                    -456,    // WFluoUpperBound
                    -659    // WFluoLowerBound
                },
                // Fluorescent
                {
                    58,    // FluoRightBound
                    -123,    // FluoLeftBound
                    -375,    // FluoUpperBound
                    -525    // FluoLowerBound
                },
                // CWF
                {
                67,    // CWFRightBound
                -123,    // CWFLeftBound
                -525,    // CWFUpperBound
                -624    // CWFLowerBound
                },
                // Daylight
                {
                    245,    // DayRightBound
                    58,    // DayLeftBound
                    -375,    // DayUpperBound
                    -525    // DayLowerBound
                },
                // Shade
                {
                    575,    // ShadeRightBound
                    245,    // ShadeLeftBound
                    -375,    // ShadeUpperBound
                    -489    // ShadeLowerBound
                },
                // Daylight Fluorescent
                {
                    245,    // DFRightBound
                    67,    // DFLeftBound
                    -525,    // DFUpperBound
                    -624    // DFLowerBound
                }
            },
            // PWB light area
            {
                // Reference area
                {
                    575,    // PRefRightBound
                    -807,    // PRefLeftBound
                    -344,    // PRefUpperBound
                    -659    // PRefLowerBound
                },
                // Daylight
                {
                    270,    // PDayRightBound
                    58,    // PDayLeftBound
                    -375,    // PDayUpperBound
                    -525    // PDayLowerBound
                },
                // Cloudy daylight
                {
                    370,    // PCloudyRightBound
                    195,    // PCloudyLeftBound
                    -375,    // PCloudyUpperBound
                    -525    // PCloudyLowerBound
                },
                // Shade
                {
                    470,    // PShadeRightBound
                    195,    // PShadeLeftBound
                    -375,    // PShadeUpperBound
                    -525    // PShadeLowerBound
                },
                // Twilight
                {
                    58,    // PTwiRightBound
                    -102,    // PTwiLeftBound
                    -375,    // PTwiUpperBound
                    -525    // PTwiLowerBound
                },
                // Fluorescent
                {
                    265,    // PFluoRightBound
                    -139,    // PFluoLeftBound
                    -385,    // PFluoUpperBound
                    -619    // PFluoLowerBound
                },
                // Warm fluorescent
                {
                    -149,    // PWFluoRightBound
                    -349,    // PWFluoLeftBound
                    -385,    // PWFluoUpperBound
                    -619    // PWFluoLowerBound
                },
                // Incandescent
                {
                    -149,    // PIncaRightBound
                    -349,    // PIncaLeftBound
                    -375,    // PIncaUpperBound
                    -525    // PIncaLowerBound
                },
                // Gray World
                {
                    5000,    // PGWRightBound
                    -5000,    // PGWLeftBound
                    5000,    // PGWUpperBound
                    -5000    // PGWLowerBound
                }
            },
            // PWB default gain	
            {
                // Daylight
                {
                    1159,    // PWB_Day_R
                    512,    // PWB_Day_G
                    776    // PWB_Day_B
                },
                // Cloudy daylight
                {
                    1368,    // PWB_Cloudy_R
                    512,    // PWB_Cloudy_G
                    665    // PWB_Cloudy_B
                },
                // Shade
                {
                    1467,    // PWB_Shade_R
                    512,    // PWB_Shade_G
                    623    // PWB_Shade_B
                },
                // Twilight
                {
                    893,    // PWB_Twi_R
                    512,    // PWB_Twi_G
                    989    // PWB_Twi_B
                },
                // Fluorescent
                {
                    1076,    // PWB_Fluo_R
                    512,    // PWB_Fluo_G
                    952    // PWB_Fluo_B
                },
                // Warm fluorescent
                {
                    696,    // PWB_WFluo_R
                    512,    // PWB_WFluo_G
                    1431    // PWB_WFluo_B
                },
                // Incandescent
                {
                    650,    // PWB_Inca_R
                    512,    // PWB_Inca_G
                    1330    // PWB_Inca_B
                },
                // Gray World
                {
                    512,    // PWB_GW_R
                    512,    // PWB_GW_G
                    512    // PWB_GW_B
                }
            },
            // AWB preference color	
            {
                // Tungsten
                {
                    40,    // TUNG_SLIDER
                    4807    // TUNG_OFFS
                },
                // Warm fluorescent	
                {
                    40,    // WFluo_SLIDER
                    4807    // WFluo_OFFS
                },
                // Shade
                {
                    50,    // Shade_SLIDER
                    909    // Shade_OFFS
                },
                // Sunset Area
                {
                    140,   // i4Sunset_BoundXr_Thr
                    -450    // i4Sunset_BoundYr_Thr
                },
                // Shade F Area
                {
                    -123,   // i4BoundXrThr
                    -484    // i4BoundYrThr
                },
                // Shade F Vertex
                {
                    -33,   // i4BoundXrThr
                    -505    // i4BoundYrThr
                },
                // Shade CWF Area
                {
                    -123,   // i4BoundXrThr
                    -573    // i4BoundYrThr
                },
                // Shade CWF Vertex
                {
                    -28,   // i4BoundXrThr
                    -599    // i4BoundYrThr
                },
            },
            // CCT estimation
            {
                // CCT
                {
                    2300,    // i4CCT[0]
                    2850,    // i4CCT[1]
                    3750,    // i4CCT[2]
                    5100,    // i4CCT[3]
                    6500     // i4CCT[4]
                },
                // Rotated X coordinate
                {
                -622,    // i4RotatedXCoordinate[0]
                -464,    // i4RotatedXCoordinate[1]
                -254,    // i4RotatedXCoordinate[2]
                -111,    // i4RotatedXCoordinate[3]
                0    // i4RotatedXCoordinate[4]
                }
            }
        },
        // Algorithm Tuning Parameter
        {
            // AWB Backup Enable
            0,

                // Daylight locus offset LUTs for tungsten
                {
                    21, // i4Size: LUT dimension
                    {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                    {0, 350,  800, 1222, 1444, 1667, 1889, 2111, 2333, 2556, 2778, 3000, 3222, 3444, 3667, 3889, 4111, 4333, 4556, 4778,  5000} // i4LUTOut
                },

                // Daylight locus offset LUTs for WF
                {
                    21, // i4Size: LUT dimension
                    {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                {0, 350, 700, 1000, 1444, 1667, 1889, 2111, 2333, 2556, 2778, 3000, 3222, 3444, 3667, 3889, 4111, 4333, 4556, 4778, 5000} // i4LUTOut
                },

                // Daylight locus offset LUTs for shade
                {
                    21, // i4Size: LUT dimension
                    {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000} // i4LUTOut
                },
                // Preference gain for each light source
                {
                    //        LV0              LV1              LV2              LV3              LV4              LV5              LV6              LV7              LV8              LV9
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                    //        LV10             LV11             LV12             LV13             LV14             LV15             LV16             LV17             LV18
          	            {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
        	        }, // STROBE
        	        {
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
           	            {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
        	        }, // TUNGSTEN
        	        {
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
        	        }, // WARM F
        	        {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}
                    }, // F
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                    }, // CWF
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}
                    }, // DAYLIGHT
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                    }, // SHADE
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                    } // DAYLIGHT F
                },
                // Parent block weight parameter
                {
                    1,      // bEnable
                    6           // i4ScalingFactor: [6] 1~12, [7] 1~6, [8] 1~3, [9] 1~2, [>=10]: 1
                },
                // AWB LV threshold for predictor
                {
                115,    // i4InitLVThr_L
                155,    // i4InitLVThr_H
                100      // i4EnqueueLVThr
                },
                // AWB number threshold for temporal predictor
                {
                        65,     // i4Neutral_ParentBlk_Thr
                    //LV0   1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  50,  25,   2,   2,   2,   2,   2,   2,   2}  // (%) i4CWFDF_LUTThr
                },
                // AWB light neutral noise reduction for outdoor
                {
                    //LV0  1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    // Non neutral
	                { 3,   3,   3,   3,   3,   3,   3,   3,    3,   3,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // Flurescent
                    {   0,   0,   0,   0,   0,   3,   5,   5,   5,   5,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // CWF
                    {   0,   0,   0,   0,   0,   3,   5,   5,   5,   5,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // Daylight
	                { 0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   0,   2,   2,   2,   2,   2,   2,   2,   2},  // (%)
	                // DF
	                { 0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
                },
                // AWB feature detection
                {
                    // Sunset Prop
                    {
                        1,          // i4Enable
                        120,        // i4LVThr_L
                        130,        // i4LVThr_H
                        10,         // i4SunsetCountThr
                        0,          // i4SunsetCountRatio_L
                        171         // i4SunsetCountRatio_H
                    },
                    // Shade F Detection
                    {
                        1,          // i4Enable
                        50,        // i4LVThr_L
                        90,        // i4LVThr_H
                        128         // i4DaylightProb
                    },
                    // Shade CWF Detection
                    {
                        1,          // i4Enable
                        50,        // i4LVThr_L
                        90,        // i4LVThr_H
                        192         // i4DaylightProb
                    }

                },

                // AWB non-neutral probability for spatial and temporal weighting look-up table (Max: 100; Min: 0)
                {
                    //LV0   1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    {   0,  33,  66, 100, 100, 100, 100, 100, 100, 100, 100,  70,  30,  20,  10,   0,   0,   0,   0}
                },

                    // AWB daylight locus probability look-up table (Max: 100; Min: 0)
                    {   //LV0    1     2     3      4     5     6     7     8      9      10     11    12   13     14    15   16    17    18
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  50,  25,   0,   0,   0,   0}, // Strobe
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  75,  50,  25,   0,   0,   0}, // Tungsten
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  75,  50,  25,  25,  25,   0,   0,   0}, // Warm fluorescent
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  95,  75,  50,  25,  25,  25,   0,   0,   0}, // Fluorescent
                    {  90,  90,  90,  90,  90,  90,  90,  90,  90,  90,  80,  55,  30,  30,  30,  30,   0,   0,   0}, // CWF
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  75,  50,  30,  20,  10,  10,   0}, // Daylight
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  75,  50,  25,   0,   0,   0,   0}, // Shade
                    {  90,  90,  90,  90,  90,  90,  90,  90,  90,  90,  80,  55,  30,  30,  30,  30,   0,   0,   0} // Daylight fluorescent
                },

                // AWB tuning information
                {
                    6735,       // project code
                    5588,       // model
                    20150624,   // date
                    0,          // reserved 0
                    1,          // reserved 1
                    2,          // reserved 2
                    3,          // reserved 3
                    4,          // reserved 4
                }
            }
        }
    },

    // Flash AWB NVRAM
    {
#include INCLUDE_FILENAME_FLASH_AWB_PARA
    },

    {0}
};

#include INCLUDE_FILENAME_ISP_LSC_PARAM
//};  //  namespace

const CAMERA_TSF_TBL_STRUCT CAMERA_TSF_DEFAULT_VALUE =
{
    {
        1,  // isTsfEn
        2,  // tsfCtIdx
        {20, 2000, -110, -110, 512, 512, 512, 0}    // rAWBInput[8]
    },

#include INCLUDE_FILENAME_TSF_PARA
#include INCLUDE_FILENAME_TSF_DATA
};

const NVRAM_CAMERA_FEATURE_STRUCT CAMERA_FEATURE_DEFAULT_VALUE =
{
#include INCLUDE_FILENAME_FEATURE_PARA
};

typedef NSFeature::RAWSensorInfo<SENSOR_ID> SensorInfoSingleton_T;


namespace NSFeature {
  template <>
  UINT32
  SensorInfoSingleton_T::
  impGetDefaultData(CAMERA_DATA_TYPE_ENUM const CameraDataType, VOID*const pDataBuf, UINT32 const size) const
  {
    UINT32 dataSize[CAMERA_DATA_TYPE_NUM] = {sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT),
        sizeof(NVRAM_CAMERA_3A_STRUCT),
        sizeof(NVRAM_CAMERA_SHADING_STRUCT),
        sizeof(NVRAM_LENS_PARA_STRUCT),
        sizeof(AE_PLINETABLE_T),
        0,
        sizeof(CAMERA_TSF_TBL_STRUCT),
        0,
        sizeof(NVRAM_CAMERA_FEATURE_STRUCT)
    };

    if (CameraDataType > CAMERA_NVRAM_DATA_FEATURE || NULL == pDataBuf || (size < dataSize[CameraDataType]))
    {
      return 1;
    }

    switch(CameraDataType)
    {
      case CAMERA_NVRAM_DATA_ISP:
        memcpy(pDataBuf,&CAMERA_ISP_DEFAULT_VALUE,sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT));
        break;
      case CAMERA_NVRAM_DATA_3A:
        memcpy(pDataBuf,&CAMERA_3A_NVRAM_DEFAULT_VALUE,sizeof(NVRAM_CAMERA_3A_STRUCT));
        break;
      case CAMERA_NVRAM_DATA_SHADING:
        memcpy(pDataBuf,&CAMERA_SHADING_DEFAULT_VALUE,sizeof(NVRAM_CAMERA_SHADING_STRUCT));
        break;
      case CAMERA_DATA_AE_PLINETABLE:
        memcpy(pDataBuf,&g_PlineTableMapping,sizeof(AE_PLINETABLE_T));
        break;
      case CAMERA_DATA_TSF_TABLE:
        memcpy(pDataBuf,&CAMERA_TSF_DEFAULT_VALUE,sizeof(CAMERA_TSF_TBL_STRUCT));
        break;
      case CAMERA_NVRAM_DATA_FEATURE:
        memcpy(pDataBuf,&CAMERA_FEATURE_DEFAULT_VALUE,sizeof(NVRAM_CAMERA_FEATURE_STRUCT));
        break;
      default:
        break;
    }
    return 0;
  }};  //  NSFeature


