/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#include <utils/Log.h>
#include <fcntl.h>
#include <math.h>
#include <cutils/log.h>
#include <string.h>
#include "camera_custom_nvram.h"
#include "camera_custom_sensor.h"
#include "image_sensor.h"
#include "kd_imgsensor_define.h"
#include "camera_AE_PLineTable_s5k3l8mipiraw.h"
#include "camera_info_s5k3l8mipiraw.h"
#include "camera_custom_AEPlinetable.h"
#include "camera_custom_tsf_tbl.h"



const NVRAM_CAMERA_ISP_PARAM_STRUCT CAMERA_ISP_DEFAULT_VALUE =
{{
    //Version
    Version: NVRAM_CAMERA_PARA_FILE_VERSION,

    //SensorId
    SensorId: SENSOR_ID,
    ISPComm:{
      {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      }
    },
    ISPPca: {
#include INCLUDE_FILENAME_ISP_PCA_PARAM
    },
    ISPRegs:{
#include INCLUDE_FILENAME_ISP_REGS_PARAM
    },
    ISPMulitCCM:{
      Poly22:{
        80425,      // i4R_AVG
        16456,      // i4R_STD
        115550,      // i4B_AVG
        29001,      // i4B_STD
				  0, // i4R_MAX
				   0, // i4R_MIN
				  0, // i4G_MAX
				   0, // i4G_MIN
				  0, // i4B_MAX
				   0, // i4B_MIN
                { // i4P00[9]
            9777500, -4332500, -312500, -1925000, 7447500, -400000, 162500, -4110000, 9065000
                },
                { // i4P10[9]
            2425996, -2808951, 399905, -141567, -404539, 537271, -18169, 1327825, -1344534
                },
                { // i4P01[9]
            1945251, -2184887, 248331, -559972, -338981, 902354, -145328, 26251, 96372
                },
                { // i4P20[9]
                0, 0, 0, 0, 0, 0, 0, 0, 0
                },
                { // i4P11[9]
                0, 0, 0, 0, 0, 0, 0, 0, 0
                },
                { // i4P02[9]
                0, 0, 0, 0, 0, 0, 0, 0, 0
                }

      },
      AWBGain:{
        // Strobe
        {
          512,    // i4R
          512,    // i4G
          512    // i4B
        },
        // A
        {
          606,    // i4R
            512,    // i4G
          1469    // i4B
        },
        // TL84
        {
          763,    // i4R
            512,    // i4G
          1143    // i4B
        },
        // CWF
        {
          848,    // i4R
            512,    // i4G
          1238    // i4B
        },
        // D65
        {
          1000,    // i4R
            512,    // i4G
          772    // i4B
        },
        // Reserved 1
        {
            512,    // i4R
            512,    // i4G
            512    // i4B
        },
        // Reserved 2
        {
            512,    // i4R
            512,    // i4G
            512    // i4B
        },
        // Reserved 3
        {
            512,    // i4R
            512,    // i4G
            512    // i4B
        }
      },
      Weight:{
        1, // Strobe
        1, // A
        1, // TL84
        1, // CWF
        1, // D65
        1, // Reserved 1
        1, // Reserved 2
        1  // Reserved 3
      }
    },

          MDPMulitCCM:{
                Poly22:{
                  125225, // i4R_AVG
                   32141, // i4R_STD
                  165675, // i4B_AVG
                   30249, // i4B_STD
                     646, // i4R_MAX
                     527, // i4R_MIN
                     789, // i4G_MAX
                     670, // i4G_MIN
                     856, // i4B_MAX
                     721, // i4B_MIN
                  {  // i4P00[9]
                     5995000,   -505000,   -370000,  -1387500,   7165000,   -657500,   -532500,  -2045000,   7697500
                  },
                  {  // i4P10[9]
                      725905,  -1081879,    355974,    627096,   -389018,   -238079,    626246,    -86040,   -540207
                  },
                  {  // i4P01[9]
                      257857,   -576218,    318361,    -96902,   -138571,    235473,     24959,    193148,   -218107
                  },
                  {  // i4P20[9]
                      0, 0, 0, 0, 0, 0, 0, 0, 0
                          },
                          { // i4P11[9]
                      0, 0, 0, 0, 0, 0, 0, 0, 0
                          },
                          { // i4P02[9]
                      0, 0, 0, 0, 0, 0, 0, 0, 0
                          }

                },
                AWBGain:{
                  // Strobe
                  {
                    1016,    // i4R
                    512,    // i4G
                    1101    // i4B
                  },
                  // A
                  {
                    878,    // i4R
                      512,    // i4G
                    1971    // i4B
                  },
                  // TL84
                  {
                    1192,    // i4R
                      512,    // i4G
                    1628    // i4B
                  },
                  // CWF
                  {
                    1280,    // i4R
                      512,    // i4G
                    1773    // i4B
                  },
                  // D65
                  {
                    1659,    // i4R
                      512,    // i4G
                    1255    // i4B
                  },
                  // Reserved 1
                  {
                      512,    // i4R
                      512,    // i4G
                      512    // i4B
                  },
                  // Reserved 2
                  {
                      512,    // i4R
                      512,    // i4G
                      512    // i4B
                  },
                  // Reserved 3
                  {
                      512,    // i4R
                      512,    // i4G
                      512    // i4B
                  }
                },
                Weight:{
                  1, // Strobe
                  1, // A
                  1, // TL84
                  1, // CWF
                  1, // D65
                  1, // Reserved 1
                  1, // Reserved 2
                  1  // Reserved 3
                }
              },
    isp_ccm_ratio: 1.0,
    //bInvokeSmoothCCM
    bInvokeSmoothCCM: MFALSE,
    DngMetadata:
    {
        0,  //i4RefereceIlluminant1
        3,  //i4RefereceIlluminant2
        {
            // rNoiseProfile[4]
            {
                {3.530980e-6, 3.357493e-4},
                {8.098436e-8, -2.336529e-4},
            },
            {
                {3.530980e-6, 3.357493e-4},
                {8.098436e-8, -2.336529e-4},
            },
            {
                {3.530980e-6, 3.357493e-4},
                {8.098436e-8, -2.336529e-4},
            },
            {
                {3.530980e-6, 3.357493e-4},
                {8.098436e-8, -2.336529e-4},
            },
        },
    },
    rGmaParam:
    {
        {   // Normal Mode
            eISP_DYNAMIC_GMA_MODE,  // eGMAMode
            8,                  // i4LowContrastThr
            {
                {   // i4ContrastWeightingTbl
                    //  0   1   2    3    4    5    6    7    8    9    10
                         0, 0, 33, 66, 100, 100, 100, 100, 100, 100, 100
                },
                {   // i4LVWeightingTbl
                    //LV0   1   2   3   4   5   6   7   8   9   10   11   12   13   14   15   16   17   18   19
                        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  33,  66, 100, 100, 100, 100, 100, 100, 100, 100
				},
            },
            {
                1,      // i4Enable
                1,      // i4WaitAEStable
                4       // i4Speed
            },
            {
                0,      // i4Enable
                2047,   // i4CenterPt
                50,     // i4LowPercent
                100000, // i4LowCurve100
                100000, // i4HighCurve100
                50,     // i4HighPercent
                100,    // i4SlopeH100
                100     // i4SlopeL100
            },
            {
                0       // rGMAFlare.i4Enable
			},
        },
        {   // HDR Mode
            eISP_DYNAMIC_GMA_MODE,  // eGMAMode
            8,                  // i4LowContrastThr
            {
                {   // i4ContrastWeightingTbl
                    //  0   1   2    3    4    5    6    7    8    9    10
                        0,  0,  0,   0,   0,   0,   0,   0,   0,   0,    0
                },
                {   // i4LVWeightingTbl
                    //LV0   1   2   3   4   5   6   7   8   9   10   11   12   13   14   15   16   17   18   19
                        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  33,  66, 100, 100, 100, 100, 100, 100, 100, 100
                }
            },
            {
                1,      // i4Enable
                1,      // i4WaitAEStable
                4       // i4Speed
            },
            {
                0,      // i4Enable
                2047,   // i4CenterPt
                50,     // i4LowPercent
                100000, // i4LowCurve100
                100000, // i4HighCurve100
                50,     // i4HighPercent
                100,    // i4SlopeH100
                100     // i4SlopeL100
            },
            {
                0       // rGMAFlare.i4Enable
			},
        },
        {   // Reserve 0
            eISP_DYNAMIC_GMA_MODE,  // eGMAMode
            8,                  // i4LowContrastThr
            {
                {   // i4ContrastWeightingTbl
                    //  0   1   2    3    4    5    6    7    8    9    10
                        0,  0, 33,  66,  100, 100, 100,  100, 100, 100, 100
                },
                {   // i4LVWeightingTbl
                    //LV0   1   2   3   4   5   6   7   8   9   10   11   12   13   14   15   16   17   18   19
                        0,  0,  0,  0,  0,  0,  0,  0,  0, 33,  66, 100, 100, 100, 100, 100, 100, 100, 100, 100
				},
            },
            {
                1,      // i4Enable
                1,      // i4WaitAEStable
                4       // i4Speed
            },
            {
                0,      // i4Enable
                2047,   // i4CenterPt
                50,     // i4LowPercent
                100000, // i4LowCurve100
                100000, // i4HighCurve100
                50,     // i4HighPercent
                100,    // i4SlopeH100
                100     // i4SlopeL100
            },
            {
                0       // rGMAFlare.i4Enable
			},
        },
        {   // Reserve 1
            eISP_DYNAMIC_GMA_MODE,  // eGMAMode
            8,                  // i4LowContrastThr
            {
                {   // i4ContrastWeightingTbl
                    //  0   1   2    3    4    5    6    7    8    9    10
                        0,  0, 33,  66,  100, 100, 100,  100, 100, 100, 100
                },
                {   // i4LVWeightingTbl
                    //LV0   1   2   3   4   5   6   7   8   9   10   11   12   13   14   15   16   17   18   19
                        0,  0,  0,  0,  0,  0,  0,  0,  0, 33,  66, 100, 100, 100, 100, 100, 100, 100, 100, 100
				},
            },
            {
                1,      // i4Enable
                1,      // i4WaitAEStable
                4       // i4Speed
            },
            {
                0,      // i4Enable
                2047,   // i4CenterPt
                50,     // i4LowPercent
                100000, // i4LowCurve100
                100000, // i4HighCurve100
                50,     // i4HighPercent
                100,    // i4SlopeH100
                100     // i4SlopeL100
            },
            {
                0       // rGMAFlare.i4Enable
			},
        }
    },
    rLceParam:
    {
        //   Normal Mode
        {
            1,  // i4AutoLCEEnable
            // rDefLCEParam
            {
                 0,  //  i4LCEBa
                50, //  i4LCEPa
               110  //  i4LCEPb
            },
            //  rAutoLCEParam
            {
                3,          //  i4LCESeg
                800,        //  i4LCEContrastRatio
                {
                    0, //2,      // i4LCEBa
                    {
                        // i4LCEPa
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {70,  70,  70,  70,  70,  70,  70,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  0 * N
                        {70,  70,  70,  70,  70,  70,  70,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  1
                        {70,  70,  70,  70,  70,  70,  60,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  2
                        {70,  70,  70,  70,  70,  70,  60,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  3
                        {60,  60,  60,  60,  60,  60,  60,  50,  50,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  4
                        {60,  60,  60,  60,  60,  60,  60,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  5
                        {60,  60,  60,  60,  60,  60,  60,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  6
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  7
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  8
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  9
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50}    // 10

                    },
                    {
                    // i4LCEPb
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  80,  80,   80,   80,   80,   80,   80,   80,   80,   80},   //  0 * N
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   80,   80,   80,   80,   80,   80,   80},   //  1
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   80,   80,   80,   80,   80,   80,   80},   //  2
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   90,   80,   80,   80,   80,   80,   80},   //  3
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   90,   90,   80,   80,   80,   80,   80},   //  4
                        {120, 120, 120, 120, 120, 130, 100,  90,  90,  90,  90,   90,   90,   90,   90,   90,   90,   90,   90},   //  5
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,   90,   90,   90,   90,   90,   90,   90,   90},   //  6
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  100,  100,  100,  100,  100,  100,  100,  100},   //  7
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  100,  110,  110,  110,  110,  110,  110,  110},   //  8
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  110,  110,  110,  110,  110,  110,  110,  110},   //  9
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  110,  110,  110,  110,  110,  110,  110,  110}    // 10
					},
                },
                {           // rLCESmooth
                    1,      // i4Enable
                    0,      // i4WaitAEStable
                    4       // i4Speed
                },
                {
                    // rLCEFlare
					0,      // i4Enable
                }
            }
        },
        {
            1,  // i4AutoLCEEnable
            // rDefLCEParam
            {
                 0,  //  i4LCEBa
                50, //  i4LCEPa
               110  //  i4LCEPb
            },
            //  rAutoLCEParam
            {
                3,          //  i4LCESeg
                800,        //  i4LCEContrastRatio
                {
                    0, //2,      // i4LCEBa
                    {
                        // i4LCEPa
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {70,  70,  70,  70,  70,  70,  70,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  0 * N
                        {70,  70,  70,  70,  70,  70,  70,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  1
                        {70,  70,  70,  70,  70,  70,  60,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  2
                        {70,  70,  70,  70,  70,  70,  60,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  3
                        {60,  60,  60,  60,  60,  60,  60,  50,  50,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  4
                        {60,  60,  60,  60,  60,  60,  60,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  5
                        {60,  60,  60,  60,  60,  60,  60,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  6
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  7
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  8
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  9
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50}    // 10

                    },
                    {
                    // i4LCEPb
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  80,  80,   80,   80,   80,   80,   80,   80,   80,   80},   //  0 * N
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   80,   80,   80,   80,   80,   80,   80},   //  1
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   80,   80,   80,   80,   80,   80,   80},   //  2
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   90,   80,   80,   80,   80,   80,   80},   //  3
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   90,   90,   80,   80,   80,   80,   80},   //  4
                        {120, 120, 120, 120, 120, 130, 100,  90,  90,  90,  90,   90,   90,   90,   90,   90,   90,   90,   90},   //  5
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,   90,   90,   90,   90,   90,   90,   90,   90},   //  6
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  100,  100,  100,  100,  100,  100,  100,  100},   //  7
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  100,  110,  110,  110,  110,  110,  110,  110},   //  8
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  110,  110,  110,  110,  110,  110,  110,  110},   //  9
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  110,  110,  110,  110,  110,  110,  110,  110}    // 10
					},
                },
                {           // rLCESmooth
                    1,      // i4Enable
                    0,      // i4WaitAEStable
                    4       // i4Speed
                },
                {
                    // rLCEFlare
					0,      // i4Enable
                }
            }
        },
        //   Reserve0
        {
            1,  // i4AutoLCEEnable
            // rDefLCEParam
            {
                 0,  //  i4LCEBa
                50, //  i4LCEPa
               110  //  i4LCEPb
            },
            //  rAutoLCEParam
            {
                3,          //  i4LCESeg
                800,        //  i4LCEContrastRatio
                {
                    0, //2,      // i4LCEBa
                    {
                        // i4LCEPa
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {70,  70,  70,  70,  70,  70,  70,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  0 * N
                        {70,  70,  70,  70,  70,  70,  70,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  1
                        {70,  70,  70,  70,  70,  70,  60,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  2
                        {70,  70,  70,  70,  70,  70,  60,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  3
                        {60,  60,  60,  60,  60,  60,  60,  50,  50,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  4
                        {60,  60,  60,  60,  60,  60,  60,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  5
                        {60,  60,  60,  60,  60,  60,  60,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  6
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  7
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  8
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  9
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50}    // 10

                    },
                    {
                    // i4LCEPb
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  80,  80,   80,   80,   80,   80,   80,   80,   80,   80},   //  0 * N
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   80,   80,   80,   80,   80,   80,   80},   //  1
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   80,   80,   80,   80,   80,   80,   80},   //  2
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   90,   80,   80,   80,   80,   80,   80},   //  3
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   90,   90,   80,   80,   80,   80,   80},   //  4
                        {120, 120, 120, 120, 120, 130, 100,  90,  90,  90,  90,   90,   90,   90,   90,   90,   90,   90,   90},   //  5
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,   90,   90,   90,   90,   90,   90,   90,   90},   //  6
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  100,  100,  100,  100,  100,  100,  100,  100},   //  7
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  100,  110,  110,  110,  110,  110,  110,  110},   //  8
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  110,  110,  110,  110,  110,  110,  110,  110},   //  9
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  110,  110,  110,  110,  110,  110,  110,  110}    // 10
					},
                },
                {           // rLCESmooth
                    1,      // i4Enable
                    0,      // i4WaitAEStable
                    4       // i4Speed
                },
                {
                    // rLCEFlare
					0,      // i4Enable
                }
            }
        },
        //   Reserve1
        {
            1,  // i4AutoLCEEnable
            // rDefLCEParam
            {
                 0,  //  i4LCEBa
                50, //  i4LCEPa
               110  //  i4LCEPb
            },
            //  rAutoLCEParam
            {
                3,          //  i4LCESeg
                800,        //  i4LCEContrastRatio
                {
                    0, //2,      // i4LCEBa
                    {
                        // i4LCEPa
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {70,  70,  70,  70,  70,  70,  70,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  0 * N
                        {70,  70,  70,  70,  70,  70,  70,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  1
                        {70,  70,  70,  70,  70,  70,  60,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  2
                        {70,  70,  70,  70,  70,  70,  60,  60,  60,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  3
                        {60,  60,  60,  60,  60,  60,  60,  50,  50,  50,  50,   50,   50,   50,   40,   40,   40,   40,   40},   //  4
                        {60,  60,  60,  60,  60,  60,  60,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  5
                        {60,  60,  60,  60,  60,  60,  60,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  6
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  7
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  8
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50},   //  9
                        {50,  50,  50,  50,  50,  50,  50,  50,  50,  50,  50,   50,   50,   50,   50,   50,   50,   50,   50}    // 10

                    },
                    {
                    // i4LCEPb
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  80,  80,   80,   80,   80,   80,   80,   80,   80,   80},   //  0 * N
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   80,   80,   80,   80,   80,   80,   80},   //  1
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   80,   80,   80,   80,   80,   80,   80},   //  2
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   90,   80,   80,   80,   80,   80,   80},   //  3
                        {130, 130, 130, 130, 130, 130, 100, 100,  90,  90,  90,   90,   90,   90,   80,   80,   80,   80,   80},   //  4
                        {120, 120, 120, 120, 120, 130, 100,  90,  90,  90,  90,   90,   90,   90,   90,   90,   90,   90,   90},   //  5
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,   90,   90,   90,   90,   90,   90,   90,   90},   //  6
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  100,  100,  100,  100,  100,  100,  100,  100},   //  7
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  100,  110,  110,  110,  110,  110,  110,  110},   //  8
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  110,  110,  110,  110,  110,  110,  110,  110},   //  9
                        {120, 120, 120, 120, 120, 110, 100,  90,  90,  90,  90,  110,  110,  110,  110,  110,  110,  110,  110}    // 10
					},
                },
                {           // rLCESmooth
                    1,      // i4Enable
                    0,      // i4WaitAEStable
                    4       // i4Speed
                },
                {
                    // rLCEFlare
					0,      // i4Enable
                }
            }
        }
    },
    ANR_TBL:
  {0}

	//CT_0
	
		//ISO_0
		
		//ISO_1
		
		//ISO_2
	
	    //ISO_3
		
		//ISO_4
		
		//ISO_5
	
	
	//CT_1
	
		//ISO_0
		
		//ISO_1
		
		//ISO_2
        
	    //ISO_3
		
		//ISO_4
		
		//ISO_5
	
}};

const NVRAM_CAMERA_3A_STRUCT CAMERA_3A_NVRAM_DEFAULT_VALUE =
{
    NVRAM_CAMERA_3A_FILE_VERSION, // u4Version
    SENSOR_ID, // SensorId

    // AE NVRAM
    {
        // rDevicesInfo
        {
            1144,    // u4MinGain, 1024 base = 1x
            8192,    // u4MaxGain, 16x
            76,    // u4MiniISOGain, ISOxx  
            128,    // u4GainStepUnit, 1x/8
            19770,    // u4PreExpUnit 
            30,     // u4PreMaxFrameRate
            19770,    // u4VideoExpUnit  
            30,     // u4VideoMaxFrameRate
            1024,   // u4Video2PreRatio, 1024 base = 1x
            19770,    // u4CapExpUnit 
            30,    // u4CapMaxFrameRate
            1024,   // u4Cap2PreRatio, 1024 base = 1x
            19770,    // u4Video1ExpUnit
            120,    // u4Video1MaxFrameRate
            1024,   // u4Video12PreRatio, 1024 base = 1x
            19770,    // u4Video2ExpUnit
            30,     // u4Video2MaxFrameRate
            1024,   // u4Video22PreRatio, 1024 base = 1x
            19770,    // u4Custom1ExpUnit
            30,     // u4Custom1MaxFrameRate
            1024,   // u4Custom12PreRatio, 1024 base = 1x
            19770,    // u4Custom2ExpUnit
            30,     // u4Custom2MaxFrameRate
            1024,   // u4Custom22PreRatio, 1024 base = 1x
            19770,    // u4Custom3ExpUnit
            30,     // u4Custom3MaxFrameRate
            1024,   // u4Custom32PreRatio, 1024 base = 1x
            19770,    // u4Custom4ExpUnit
            30,     // u4Custom4MaxFrameRate
            1024,   // u4Custom42PreRatio, 1024 base = 1x
            19770,    // u4Custom5ExpUnit
            30,     // u4Custom5MaxFrameRate
            1024,   // u4Custom52PreRatio, 1024 base = 1x
            20,      // u4LensFno, Fno = 2.8
            350     // u4FocusLength_100x
        },
        // rHistConfig
        {
            4, // 2,   // u4HistHighThres
            40,  // u4HistLowThres
            2,   // u4MostBrightRatio
            1,   // u4MostDarkRatio
            160, // u4CentralHighBound
            20,  // u4CentralLowBound
            {240, 230, 220, 210, 200}, // u4OverExpThres[AE_CCT_STRENGTH_NUM]
            {62, 70, 82, 108, 141},  // u4HistStretchThres[AE_CCT_STRENGTH_NUM]
            {18, 22, 26, 30, 34}       // u4BlackLightThres[AE_CCT_STRENGTH_NUM]
        },
        // rCCTConfig
        {
            TRUE,            // bEnableBlackLight
            TRUE,            // bEnableHistStretch
            TRUE,           // bEnableAntiOverExposure
            TRUE,            // bEnableTimeLPF
            TRUE,            // bEnableCaptureThres
            TRUE,            // bEnableVideoThres
            TRUE,            // bEnableVideo1Thres
            TRUE,            // bEnableVideo2Thres
            TRUE,            // bEnableCustom1Thres
            TRUE,            // bEnableCustom2Thres
            TRUE,            // bEnableCustom3Thres
            TRUE,            // bEnableCustom4Thres
            TRUE,            // bEnableCustom5Thres
            TRUE,            // bEnableStrobeThres
            47,                // u4AETarget
            47,                // u4StrobeAETarget

            50,                // u4InitIndex
            4,                 // u4BackLightWeight
            32,                // u4HistStretchWeight
            4,                 // u4AntiOverExpWeight
            2,                 // u4BlackLightStrengthIndex
            2,                 // u4HistStretchStrengthIndex
            2,                 // u4AntiOverExpStrengthIndex
            2,                 // u4TimeLPFStrengthIndex
            {1, 3, 5, 7, 8}, // u4LPFConvergeTable[AE_CCT_STRENGTH_NUM]
            90,                // u4InDoorEV = 9.0, 10 base
            -6,    // i4BVOffset delta BV = value/10 
            64,                 // u4PreviewFlareOffset
            64,                 // u4CaptureFlareOffset
            3,                 // u4CaptureFlareThres
            64,                 // u4VideoFlareOffset
            3,                 // u4VideoFlareThres
            64,               // u4CustomFlareOffset
            3,                 //  u4CustomFlareThres
            64,                 // u4StrobeFlareOffset //12 bits
            3,                 // u4StrobeFlareThres // 0.5%
            160,                 // u4PrvMaxFlareThres //12 bit
            24,                 // u4PrvMinFlareThres
            160,                 // u4VideoMaxFlareThres // 12 bit
            24,                 // u4VideoMinFlareThres
            18,                // u4FlatnessThres              // 10 base for flatness condition.
            75,    // u4FlatnessStrength
            //rMeteringSpec
            {
                //rHS_Spec
                {
                    TRUE,//bEnableHistStretch           // enable histogram stretch
                    1024,//u4HistStretchWeight          // Histogram weighting value
                    40, //50, //20,//u4Pcent                      // 1%=10, 0~1000
                    160, //166,//176,//u4Thd                        // 0~255
                    75, //54, //74,//u4FlatThd                    // 0~255

                    120,//u4FlatBrightPcent
                    120,//u4FlatDarkPcent
                    //sFlatRatio
                    {
                        1000,  //i4X1
                        1024,  //i4Y1
                        2400, //i4X2
                        0     //i4Y2
                    },
                    TRUE, //bEnableGreyTextEnhance
                    1800, //u4GreyTextFlatStart, > sFlatRatio.i4X1, < sFlatRatio.i4X2
                    {
                        10,     //i4X1
                        1024,   //i4Y1
                        80,     //i4X2
                        0       //i4Y2
                    }
                },
                //rAOE_Spec
                {
                    TRUE, //bEnableAntiOverExposure
                    1024, //u4AntiOverExpWeight
                    10,    //u4Pcent
                  220,//  200,  //u4Thd

                    TRUE, //bEnableCOEP
                    1,    //u4COEPcent
                    106,  //u4COEThd
                    0,  // u4BVCompRatio
                    //sCOEYRatio;     // the outer y ratio
                    {
                        23,   //i4X1
                        1024,  //i4Y1
                        47,   //i4X2
                        0     //i4Y2
                    },
                    //sCOEDiffRatio;  // inner/outer y difference ratio
                    {
                        1500, //i4X1
                        0,    //i4Y1
                        2100, //i4X2
                        1024   //i4Y2
                    }
                },
                //rABL_Spec
                {
                    TRUE,//bEnableBlackLigh
                    1024,//u4BackLightWeigh
                    400,//u4Pcent
                    22,//u4Thd,
                    255, // center luminance
                    246, //256, // final target limitation, 256/128 = 2x
                    //sFgBgEVRatio
                    {
                        2100,//2200, //i4X1
                        0,    //i4Y1
                        4000, //i4X2
                        1024   //i4Y2
                    },
                    //sBVRatio
                    {
                        3800,//i4X1
                        0,   //i4Y1
                        5000,//i4X2
                        1024  //i4Y2
                    }
                },
                //rNS_Spec
                {
                    TRUE, // bEnableNightScene
                    10, //5,    //u4Pcent
                   150, // 170,  //u4Thd
                    72, //52,   //u4FlatThd

					          180, //  200,  //u4BrightTonePcent
				            80, //85,//	82, //  95, //u4BrightToneThd

                    500,  //u4LowBndPcent
                    5,    //u4LowBndThd
                    26,    //u4LowBndThdLimit

                    50,  //u4FlatBrightPcent;
                    300,   //u4FlatDarkPcent;
                    //sFlatRatio
                    {
                        1200, //i4X1
                        1024, //i4Y1
                      2800,//  2400, //i4X2
                        0    //i4Y2
                    },
                    //sBVRatio
                    {
                        -500, //i4X1
                        1024,  //i4Y1
                        3000, //i4X2
                        0     //i4Y2
                    },
                    TRUE, // bEnableNightSkySuppresion
                    //sSkyBVRatio
                    {
                        -4000, //i4X1
                        1024, //i4X2
                        -2000,  //i4Y1
                        0     //i4Y2
                    }
                },
                // rTOUCHFD_Spec
                {
                    40, //uMeteringYLowBound;
                    50, //uMeteringYHighBound;
                    40, //uFaceYLowBound;
                    50, //uFaceYHighBound;
                    3,  //uFaceCentralWeight;
                    120,//u4MeteringStableMax;
                    80, //u4MeteringStableMin;
                }
            }, //End rMeteringSpec
            // rFlareSpec
            {
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, //uPrvFlareWeightArr[16];
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, //uVideoFlareWeightArr[16];
                96,                                               //u4FlareStdThrHigh;
                48,                                               //u4FlareStdThrLow;
                0,                                                //u4PrvCapFlareDiff;
                2,                                                //u4FlareMaxStepGap_Fast;
                0,//0,                                                //u4FlareMaxStepGap_Slow;
                1800,                                             //u4FlarMaxStepGapLimitBV;
                2,//0,                                                //u4FlareAEStableCount;
            },
            //rAEMoveRatio =
            {
                100,//100, //u4SpeedUpRatio 500
                100, //100, //u4GlobalRatio
                190, //u4Bright2TargetEnd  190 150
                10,//20,   //u4Dark2TargetStart
                90, //u4B2TEnd
                85,//70,  //u4B2TStart 70
                70,//60,  //u4D2TEnd 60
                85,  //u4D2TStart
            },

            //rAEVideoMoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAEVideo1MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAEVideo2MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom1MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom2MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom3MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom4MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom5MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAEFaceMoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                190,  //u4Bright2TargetEnd
                10,    //u4Dark2TargetStart
                80, //u4B2TEnd
                30,  //u4B2TStart
                20,  //u4D2TEnd
                60,  //u4D2TStart
            },

            //rAETrackingMoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                190,  //u4Bright2TargetEnd
                10,    //u4Dark2TargetStart
                80, //u4B2TEnd
                30,  //u4B2TStart
                20,  //u4D2TEnd
                60,  //u4D2TStart
            },
            //rAEAOENVRAMParam =
            {
                1,      // i4AOEStrengthIdx: 0 / 1 / 2
                130,    // u4BVCompRatio
                {
                    {
                        47,  //u4Y_Target
                        25,  //u4AOE_OE_percent
                        210,  //u4AOE_OEBound
                        10,    //u4AOE_DarkBound
                        950,    //u4AOE_LowlightPrecent
                        1,    //u4AOE_LowlightBound
                        145,    //u4AOESceneLV_L
                        180,    //u4AOESceneLV_H
                        40,    //u4AOE_SWHdrLE_Bound
                    },
                    {
                        47,  //u4Y_Target
                        25,  //u4AOE_OE_percent
                        210,  //u4AOE_OEBound
                        15, //20,    //u4AOE_DarkBound
                        950,    //u4AOE_LowlightPrecent
                        3, //10,    //u4AOE_LowlightBound
                        145,    //u4AOESceneLV_L
                        180,    //u4AOESceneLV_H
                        40,    //u4AOE_SWHdrLE_Bound
                    },
                    {
                        47,  //u4Y_Target
                        25,  //u4AOE_OE_percent
                        210,  //u4AOE_OEBound
                        25,    //u4AOE_DarkBound
                        950,    //u4AOE_LowlightPrecent
                        8,    //u4AOE_LowlightBound
                        145,    //u4AOESceneLV_L
                        180,    //u4AOESceneLV_H
                        40,    //u4AOE_SWHdrLE_Bound
                    }
                }
            }
        },
        // rAEHDRConfig
        {
            3072,   // i4RMGSeg
            35,     // i4HDRTarget_L;
            40,     // i4HDRTarget_H;
            100,    // i4HDRTargetLV_L;
            120,    // i4HDRTargetLV_H;
            20,     // i4OverExpoRatio;
            212,    // i4OverExpoTarget;
            120,    // i4OverExpoLV_L;
            180,    // i4OverExpoLV_H;
            4,      // i4UnderExpoContrastThr;
             // Contrast:
             //  0   1   2   3   4   5   6   7   8   9  10
            {    3,  3,  3,  3,  3,  2,  1,  1,  1,  1,  1            },    // i4UnderExpoTargetTbl[AE_HDR_UNDEREXPO_CONTRAST_TARGET_TBL_NUM]
            950,        // i4UnderExpoRatio;
            1000,       // i4AvgExpoRatio;
            8, //10,       // i4AvgExpoTarget;
            768,  // i4HDRAESpeed
            2,          // i4HDRAEConvergeThr;
            40,         // i4SWHdrLEThr
            20,         // i4SWHdrSERatio
            180,        // i4SWHdrSETarget
            1024,    // i4SWHdrBaseGain
        },
    },

        // AWB NVRAM
    AWB_NVRAM_START
    {
        {
            {
                // AWB calibration data
                {
                    // rUnitGain (unit gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rGoldenGain (golden sample gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rUnitGain TL84 (unit gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rGoldenGain TL84 (golden sample gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                     // rUnitGain Alight (unit gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rGoldenGain Alight (golden sample gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rTuningUnitGain (Tuning sample unit gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rD65Gain (D65 WB gain: 1.0 = 512)
                    {
                    971,    // D65Gain_R
                    512,    // D65Gain_G
                    800    // D65Gain_B
                    }
                },
                // Original XY coordinate of AWB light source
                {
                    // Strobe
                        {
                    0,    // i4X
                    0    // i4Y
                        },
                        // Horizon
                        {
                    -491,    // OriX_Hor
                    -474    // OriY_Hor
                        },
                        // A
                        {
                    -359,    // OriX_A
                    -461    // OriY_A
                        },
                        // TL84
                        {
                    -189,    // OriX_TL84
                    -460    // OriY_TL84
                        },
                        // CWF
                        {
                    -159,    // OriX_CWF
                    -520    // OriY_CWF
                        },
                        // DNP
                        {
                    -61,    // OriX_DNP
                    -454    // OriY_DNP
                        },
                        // D65
                        {
                    71,    // OriX_D65
                    -401    // OriY_D65
                        },
                        // DF
                        {
                    0,    // OriX_DF
                    0    // OriY_DF
                        }
                },
                // Rotated XY coordinate of AWB light source
                {
                    // Strobe
                        {
                    0,    // i4X
                    0    // i4Y
                        },
                // Horizon
                {
                    -554,    // RotX_Hor
                    -401    // RotY_Hor
                },
                // A
                {
                    -421,    // RotX_A
                    -407    // RotY_A
                },
                // TL84
                {
                    -252,    // RotX_TL84
                    -430    // RotY_TL84
                },
                // CWF
                {
                    -231,    // RotX_CWF
                    -494    // RotY_CWF
                },
                // DNP
                {
                    -124,    // RotX_DNP
                    -442    // RotY_DNP
                },
                // D65
                {
                    14,    // RotX_D65
                    -408    // RotY_D65
                },
                // DF
                {
                    -16,    // RotX_DF
                    -484    // RotY_DF
                }
            },
            // AWB gain of AWB light source
            {
                // Strobe 
                {
                    512,    // i4R
                    512,    // i4G
                    512    // i4B
                },
                // Horizon 
                {
                    512,    // AWBGAIN_HOR_R
                    524,    // AWBGAIN_HOR_G
                    1937    // AWBGAIN_HOR_B
                },
                // A 
                {
                    588,    // AWBGAIN_A_R
                    512,    // AWBGAIN_A_G
                    1554    // AWBGAIN_A_B
                },
                // TL84 
                {
                    739,    // AWBGAIN_TL84_R
                    512,    // AWBGAIN_TL84_G
                    1233    // AWBGAIN_TL84_B
                },
                // CWF 
                {
                    835,    // AWBGAIN_CWF_R
                    512,    // AWBGAIN_CWF_G
                    1284    // AWBGAIN_CWF_B
                },
                // DNP 
                {
                    872,    // AWBGAIN_DNP_R
                    512,    // AWBGAIN_DNP_G
                    1029    // AWBGAIN_DNP_B
                },
                // D65 
                {
                    971,    // AWBGAIN_D65_R
                    512,    // AWBGAIN_D65_G
                    800    // AWBGAIN_D65_B
                },
                // DF 
                {
                    512,    // AWBGAIN_DF_R
                    512,    // AWBGAIN_DF_G
                    512    // AWBGAIN_DF_B
                }
            },
            // Rotation matrix parameter
            {
                8,    // RotationAngle
                254,    // Cos
                36    // Sin
            },
            // Daylight locus parameter
            {
                -171,    // i4SlopeNumerator
                128    // i4SlopeDenominator
            },
            // Predictor gain
            {
                101, // i4PrefRatio100
                // DaylightLocus_L
                {
                    941,    // i4R
                    530,    // i4G
                    820    // i4B
                },
                // DaylightLocus_H
                {
                    703,    // i4R
                    512,    // i4G
                    1228    // i4B
                },
                // Temporal General
                {
                    971,    // i4R
                    512,    // i4G
                    800    // i4B
                },
                // AWB LSC Gain
                {
                    810,        // i4R
                    512,        // i4G
                    1019        // i4B
                }
            },
            // AWB light area
            {
                // Strobe:FIXME
                {
                    0,    // i4RightBound
                    0,    // i4LeftBound
                    0,    // i4UpperBound
                    0    // i4LowerBound
                },
                // Tungsten
                {
                    -310,    // TungRightBound
                    -954,    // TungLeftBound
                    -346,    // TungUpperBound
                    -440    // TungLowerBound
                },
                // Warm fluorescent
                {
                    -310,    // WFluoRightBound
                    -954,    // WFluoLeftBound
                    -440,    // WFluoUpperBound
                    -584    // WFluoLowerBound
                },
                // Fluorescent
                {
                    -166,    // FluoRightBound
                    -310,    // FluoLeftBound
                    -348,    // FluoUpperBound
                    -462    // FluoLowerBound
                },
                // CWF
                {
                -136,    // CWFRightBound
                -310,    // CWFLeftBound
                -462,    // CWFUpperBound
                -549    // CWFLowerBound
                },
                // Daylight
                {
                    44,    // DayRightBound
                    -166,    // DayLeftBound
                    -348,    // DayUpperBound
                    -462    // DayLowerBound
                },
                // Shade
                {
                    374,    // ShadeRightBound
                    44,    // ShadeLeftBound
                    -348,    // ShadeUpperBound
                    -442    // ShadeLowerBound
                },
                // Daylight Fluorescent
                {
                    44,    // DFRightBound
                    -136,    // DFLeftBound
                    -462,    // DFUpperBound
                    -549    // DFLowerBound
                }
            },
            // PWB light area
            {
                // Reference area
                {
                    374,    // PRefRightBound
                    -954,    // PRefLeftBound
                    0,    // PRefUpperBound
                    -584    // PRefLowerBound
                },
                // Daylight
                {
                    69,    // PDayRightBound
                    -166,    // PDayLeftBound
                    -348,    // PDayUpperBound
                    -462    // PDayLowerBound
                },
                // Cloudy daylight
                {
                    169,    // PCloudyRightBound
                    -6,    // PCloudyLeftBound
                    -348,    // PCloudyUpperBound
                    -462    // PCloudyLowerBound
                },
                // Shade
                {
                    269,    // PShadeRightBound
                    -6,    // PShadeLeftBound
                    -348,    // PShadeUpperBound
                    -462    // PShadeLowerBound
                },
                // Twilight
                {
                    -166,    // PTwiRightBound
                    -326,    // PTwiLeftBound
                    -348,    // PTwiUpperBound
                    -462    // PTwiLowerBound
                },
                // Fluorescent
                {
                    64,    // PFluoRightBound
                    -352,    // PFluoLeftBound
                    -358,    // PFluoUpperBound
                    -544    // PFluoLowerBound
                },
                // Warm fluorescent
                {
                    -321,    // PWFluoRightBound
                    -521,    // PWFluoLeftBound
                    -358,    // PWFluoUpperBound
                    -544    // PWFluoLowerBound
                },
                // Incandescent
                {
                    -321,    // PIncaRightBound
                    -521,    // PIncaLeftBound
                    -348,    // PIncaUpperBound
                    -462    // PIncaLowerBound
                },
                // Gray World
                {
                    5000,    // PGWRightBound
                    -5000,    // PGWLeftBound
                    5000,    // PGWUpperBound
                    -5000    // PGWLowerBound
                }
            },
            // PWB default gain	
            {
                // Daylight
                {
                    899,    // PWB_Day_R
                    512,    // PWB_Day_G
                    878    // PWB_Day_B
                },
                // Cloudy daylight
                {
                    1044,    // PWB_Cloudy_R
                    512,    // PWB_Cloudy_G
                    720    // PWB_Cloudy_B
                },
                // Shade
                {
                    1105,    // PWB_Shade_R
                    512,    // PWB_Shade_G
                    667    // PWB_Shade_B
                },
                // Twilight
                {
                    717,    // PWB_Twi_R
                    512,    // PWB_Twi_G
                    1187    // PWB_Twi_B
                },
                // Fluorescent
                {
                    864,    // PWB_Fluo_R
                    512,    // PWB_Fluo_G
                    1071    // PWB_Fluo_B
                },
                // Warm fluorescent
                {
                    629,    // PWB_WFluo_R
                    512,    // PWB_WFluo_G
                    1634    // PWB_WFluo_B
                },
                // Incandescent
                {
                    586,    // PWB_Inca_R
                    512,    // PWB_Inca_G
                    1550    // PWB_Inca_B
                },
                // Gray World
                {
                    512,    // PWB_GW_R
                    512,    // PWB_GW_G
                    512    // PWB_GW_B
                }
            },
            // AWB preference color	
            {
                // Tungsten
                {
                    40,    // TUNG_SLIDER
                    5780    // TUNG_OFFS
                },
                // Warm fluorescent	
                {
                    40,    // WFluo_SLIDER
                    5630    // WFluo_OFFS
                },
                // Shade
                {
                    50,    // Shade_SLIDER
                    431    // Shade_OFFS
                },
                // Sunset Area
                {
                    -92,   // i4Sunset_BoundXr_Thr
                    -442    // i4Sunset_BoundYr_Thr
                },
                // Shade F Area
                {
                    -310,   // i4BoundXrThr
                    -434    // i4BoundYrThr
                },
                // Shade F Vertex
                {
                    -243,   // i4BoundXrThr
                    -448    // i4BoundYrThr
                },
                // Shade CWF Area
                {
                    -310,   // i4BoundXrThr
                    -498    // i4BoundYrThr
                },
                // Shade CWF Vertex
                {
                    -228,   // i4BoundXrThr
                    -524    // i4BoundYrThr
                },
            },
            // CCT estimation
            {
                // CCT
                {
                    2300,    // i4CCT[0]
                    2850,    // i4CCT[1]
                    3750,    // i4CCT[2]
                    5100,    // i4CCT[3]
                    6500     // i4CCT[4]
                },
                // Rotated X coordinate
                {
                -330,    // i4RotatedXCoordinate[0]
                -276,    // i4RotatedXCoordinate[1]
                -266,    // i4RotatedXCoordinate[2]
                -138,    // i4RotatedXCoordinate[3]
                0    // i4RotatedXCoordinate[4]
                }
            }
        },
        // Algorithm Tuning Parameter
        {
            // AWB Backup Enable
            0,

            // Daylight locus offset LUTs for tungsten
            {
                21, // i4Size: LUT dimension
                {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                {0, 350, 800, 1222, 1444, 1667, 1889, 2111, 2333, 2556, 2778, 3000, 3222, 3444, 3667, 3889, 4111, 4333, 4556, 4778, 5000} // i4LUTOut
            },
            // Daylight locus offset LUTs for WF
            {
                21, // i4Size: LUT dimension
                {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                {0, 350, 700, 1000, 1444, 1667, 1889, 2111, 2333, 2556, 2778, 3000, 3222, 3444, 3667, 3889, 4111, 4333, 4556, 4778, 5000} // i4LUTOut
            },
            // Daylight locus offset LUTs for Shade
            {
                21, // i4Size: LUT dimension
                {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000} // i4LUTOut
            },
            // Preference gain for each light source
            {
                {
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, 
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                }, // STROBE
                {
                    {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, 
                    {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}
                }, // TUNGSTEN
                {
                    {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, 
                    {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 530}, {488, 512, 512}, {488, 512, 530}, {488, 512, 530}
                }, // WARM F
                {
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, 
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}
                    }, // F
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                    }, // CWF
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}
                    }, // DAYLIGHT
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                    }, // SHADE
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                    } // DAYLIGHT F
                },
                // Parent block weight parameter
                {
                    1,      // bEnable
                    6           // i4ScalingFactor: [6] 1~12, [7] 1~6, [8] 1~3, [9] 1~2, [>=10]: 1
                },
                // AWB LV threshold for predictor
                {
                115,    // i4InitLVThr_L
                155,    // i4InitLVThr_H
                100      // i4EnqueueLVThr
                },
                // AWB number threshold for temporal predictor
                {
                        65,     // i4Neutral_ParentBlk_Thr
                    //LV0   1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  50,  25,   2,   2,   2,   2,   2,   2,   2}  // (%) i4CWFDF_LUTThr
                },
                // AWB light neutral noise reduction for outdoor
                {
                    //LV0  1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    // Non neutral
	                { 3,   3,   3,   3,   3,   3,   3,   3,    3,   3,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // Flurescent
                    {   0,   0,   0,   0,   0,   3,   5,   5,   5,   5,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // CWF
                    {   0,   0,   0,   0,   0,   3,   5,   5,   5,   5,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // Daylight
	                { 0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   0,   2,   2,   2,   2,   2,   2,   2,   2},  // (%)
	                // DF
	                { 0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
                },
                // AWB feature detection
                {
                    // Sunset Prop
                    {
                        1,          // i4Enable
                        120,        // i4LVThr_L
                        130,        // i4LVThr_H
                        10,         // i4SunsetCountThr
                        0,          // i4SunsetCountRatio_L
                        171         // i4SunsetCountRatio_H
                    },

                    // Shade F Detection
                    {
                        1,          // i4Enable
                        50,        // i4LVThr_L
                        90,        // i4LVThr_H
                        128         // i4DaylightProb
                    },

                    // Shade CWF Detection
                    {
                        1,          // i4Enable
                        50,        // i4LVThr_L
                        90,        // i4LVThr_H
                        192         // i4DaylightProb
                    },

                },

                // AWB non-neutral probability for spatial and temporal weighting look-up table (Max: 100; Min: 0)
                {
                    //LV0   1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    {   0,  33,  66, 100, 100, 100, 100, 100, 100, 100, 100,  70,  30,  20,  10,   0,   0,   0,   0}
                },

                // AWB daylight locus probability look-up table (Max: 100; Min: 0)
                {
                    //LV0  1    2    3    4    5    6    7    8    9     10    11   12   13   14   15  16   17   18
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,  100, 100,  50,  25,   0,  0,   0,   0}, // Strobe
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  75,  50,  25,   0,   0,   0}, // Tungsten
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,   75,  50,  25,  25,  25,  0,   0,   0}, // Warm fluorescent
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  95,  75,  50,  25,  25,  25,   0,   0,   0}, // Fluorescent
                    { 90,  90,  90,  90,  90,  90,  90,  90,  90,  90,   80,   55,  30,  30,  30,  30,  0,   0,   0}, // CWF
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,  100, 100, 100,  75,  50, 40,  30,  20}, // Daylight
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,  100,  75,  50,  25,   0,  0,   0,   0}, // Shade
                    { 90,  90,  90,  90,  90,  90,  90,  90,  90,  90,   80,   55,  30,  30,  30,  30,  0,   0,   0}  // Daylight fluorescent
                },

                // AWB tuning information
                {
                    0,		// project code
                    0,		// model
                    0,		// date
                    0,          // reserved 0
                    0,		// reserved 1
                    0,		// reserved 2
                    0,		// reserved 3
                    0,		// reserved 4
                }
            }
        },
        {
            {
                // AWB calibration data
                {
                    // rUnitGain (unit gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rGoldenGain (golden sample gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rUnitGain TL84 (unit gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rGoldenGain TL84 (golden sample gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                     // rUnitGain Alight (unit gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rGoldenGain Alight (golden sample gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rTuningUnitGain (Tuning sample unit gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                // rD65Gain (D65 WB gain: 1.0 = 512)
                {
                    972,    // D65Gain_R
                    512,    // D65Gain_G
                    802    // D65Gain_B
                }
            },
            // Original XY coordinate of AWB light source
            {
                // Strobe
                {
                    0,    // i4X
                    0    // i4Y
                },
                // Horizon
                {
                    -486,    // OriX_Hor
                    -469    // OriY_Hor
                },
                // A
                {
                    -355,    // OriX_A
                    -457    // OriY_A
                },
                // TL84
                {
                    -186,    // OriX_TL84
                    -457    // OriY_TL84
                },
                // CWF
                {
                    -158,    // OriX_CWF
                    -520    // OriY_CWF
                },
                // DNP
                {
                    -61,    // OriX_DNP
                    -455    // OriY_DNP
                },
                // D65
                {
                    71,    // OriX_D65
                    -403    // OriY_D65
                },
                // DF
                {
                    0,    // OriX_DF
                    0    // OriY_DF
                }
            },
            // Rotated XY coordinate of AWB light source
            {
                // Strobe
                {
                    0,    // i4X
                    0    // i4Y
                },
                // Horizon
                {
                    -548,    // RotX_Hor
                    -397    // RotY_Hor
                },
                // A
                {
                    -416,    // RotX_A
                    -404    // RotY_A
                },
                // TL84
                {
                    -249,    // RotX_TL84
                    -427    // RotY_TL84
                },
                // CWF
                {
                    -230,    // RotX_CWF
                    -494    // RotY_CWF
                },
                // DNP
                {
                    -125,    // RotX_DNP
                    -443    // RotY_DNP
                },
                // D65
                {
                    14,    // RotX_D65
                    -410    // RotY_D65
                },
                // DF
                {
                    -17,    // RotX_DF
                    -484    // RotY_DF
                }
            },
            // AWB gain of AWB light source
            {
                // Strobe 
                {
                    512,    // i4R
                    512,    // i4G
                    512    // i4B
                },
                // Horizon 
                {
                    512,    // AWBGAIN_HOR_R
                    523,    // AWBGAIN_HOR_G
                    1907    // AWBGAIN_HOR_B
                },
                // A 
                {
                    588,    // AWBGAIN_A_R
                    512,    // AWBGAIN_A_G
                    1537    // AWBGAIN_A_B
                },
                // TL84 
                {
                    739,    // AWBGAIN_TL84_R
                    512,    // AWBGAIN_TL84_G
                    1223    // AWBGAIN_TL84_B
                },
                // CWF 
                {
                    835,    // AWBGAIN_CWF_R
                    512,    // AWBGAIN_CWF_G
                    1283    // AWBGAIN_CWF_B
                },
                // DNP 
                {
                    873,    // AWBGAIN_DNP_R
                    512,    // AWBGAIN_DNP_G
                    1029    // AWBGAIN_DNP_B
                },
                // D65 
                {
                    972,    // AWBGAIN_D65_R
                    512,    // AWBGAIN_D65_G
                    802    // AWBGAIN_D65_B
                },
                // DF 
                {
                    512,    // AWBGAIN_DF_R
                    512,    // AWBGAIN_DF_G
                    512    // AWBGAIN_DF_B
                }
            },
            // Rotation matrix parameter
            {
                8,    // RotationAngle
                254,    // Cos
                36    // Sin
            },
            // Daylight locus parameter
            {
                -167,    // i4SlopeNumerator
                128    // i4SlopeDenominator
            },
            // Predictor gain
            {
                101, // i4PrefRatio100
                // DaylightLocus_L
                {
                    942,    // i4R
                    530,    // i4G
                    822    // i4B
                },
                // DaylightLocus_H
                {
                    704,    // i4R
                    512,    // i4G
                    1233    // i4B
                },
                // Temporal General
                {
                    972,    // i4R
                    512,    // i4G
                    802    // i4B
                },
                // AWB LSC Gain
                {
                    811,        // i4R
                    512,        // i4G
                    1023        // i4B
                }
            },
            // AWB light area
            {
                // Strobe:FIXME
                {
                    0,    // i4RightBound
                    0,    // i4LeftBound
                    0,    // i4UpperBound
                    0    // i4LowerBound
                },
                // Tungsten
                {
                    -316,    // TungRightBound
                    -948,    // TungLeftBound
                    -342,    // TungUpperBound
                    -440    // TungLowerBound
                },
                // Warm fluorescent
                {
                    -316,    // WFluoRightBound
                    -948,    // WFluoLeftBound
                    -440,    // WFluoUpperBound
                    -584    // WFluoLowerBound
                },
                // Fluorescent
                {
                    -165,    // FluoRightBound
                    -316,    // FluoLeftBound
                    -350,    // FluoUpperBound
                    -450    // FluoLowerBound
                },
                // CWF
                {
                -136,    // CWFRightBound
                -316,    // CWFLeftBound
                -450,    // CWFUpperBound
                -549    // CWFLowerBound
                },
                // Daylight
                {
                    44,    // DayRightBound
                    -165,    // DayLeftBound
                    -350,    // DayUpperBound
                    -461    // DayLowerBound
                },
                // Shade
                {
                    374,    // ShadeRightBound
                    44,    // ShadeLeftBound
                    -350,    // ShadeUpperBound
                    -444    // ShadeLowerBound
                },
                // Daylight Fluorescent
                {
                    44,    // DFRightBound
                    -136,    // DFLeftBound
                    -461,    // DFUpperBound
                    -549    // DFLowerBound
                }
            },
            // PWB light area
            {
                // Reference area
                {
                    374,    // PRefRightBound
                    -948,    // PRefLeftBound
                    0,    // PRefUpperBound
                    -584    // PRefLowerBound
                },
                // Daylight
                {
                    69,    // PDayRightBound
                    -165,    // PDayLeftBound
                    -350,    // PDayUpperBound
                    -461    // PDayLowerBound
                },
                // Cloudy daylight
                {
                    169,    // PCloudyRightBound
                    -6,    // PCloudyLeftBound
                    -350,    // PCloudyUpperBound
                    -461    // PCloudyLowerBound
                },
                // Shade
                {
                    269,    // PShadeRightBound
                    -6,    // PShadeLeftBound
                    -350,    // PShadeUpperBound
                    -461    // PShadeLowerBound
                },
                // Twilight
                {
                    -165,    // PTwiRightBound
                    -325,    // PTwiLeftBound
                    -350,    // PTwiUpperBound
                    -461    // PTwiLowerBound
                },
                // Fluorescent
                {
                    64,    // PFluoRightBound
                    -349,    // PFluoLeftBound
                    -360,    // PFluoUpperBound
                    -544    // PFluoLowerBound
                },
                // Warm fluorescent
                {
                    -316,    // PWFluoRightBound
                    -516,    // PWFluoLeftBound
                    -360,    // PWFluoUpperBound
                    -544    // PWFluoLowerBound
                },
                // Incandescent
                {
                    -316,    // PIncaRightBound
                    -516,    // PIncaLeftBound
                    -350,    // PIncaUpperBound
                    -461    // PIncaLowerBound
                },
                // Gray World
                {
                    5000,    // PGWRightBound
                    -5000,    // PGWLeftBound
                    5000,    // PGWUpperBound
                    -5000    // PGWLowerBound
                }
            },
            // PWB default gain	
            {
                // Daylight
                {
                    900,    // PWB_Day_R
                    512,    // PWB_Day_G
                    878    // PWB_Day_B
                },
                // Cloudy daylight
                {
                    1044,    // PWB_Cloudy_R
                    512,    // PWB_Cloudy_G
                    720    // PWB_Cloudy_B
                },
                // Shade
                {
                    1106,    // PWB_Shade_R
                    512,    // PWB_Shade_G
                    667    // PWB_Shade_B
                },
                // Twilight
                {
                    718,    // PWB_Twi_R
                    512,    // PWB_Twi_G
                    1186    // PWB_Twi_B
                },
                // Fluorescent
                {
                    867,    // PWB_Fluo_R
                    512,    // PWB_Fluo_G
                    1069    // PWB_Fluo_B
                },
                // Warm fluorescent
                {
                    633,    // PWB_WFluo_R
                    512,    // PWB_WFluo_G
                    1624    // PWB_WFluo_B
                },
                // Incandescent
                {
                    590,    // PWB_Inca_R
                    512,    // PWB_Inca_G
                    1539    // PWB_Inca_B
                },
                // Gray World
                {
                    512,    // PWB_GW_R
                    512,    // PWB_GW_G
                    512    // PWB_GW_B
                }
            },
            // AWB preference color	
            {
                // Tungsten
                {
                    40,    // TUNG_SLIDER
                    4993    // TUNG_OFFS
                },
                // Warm fluorescent	
                {
                    40,    // WFluo_SLIDER
                    4735    // WFluo_OFFS
                },
                // Shade
                {
                    50,    // Shade_SLIDER
                    412    // Shade_OFFS
                },
                // Sunset Area
                {
                    -94,   // i4Sunset_BoundXr_Thr
                    -443    // i4Sunset_BoundYr_Thr
                },
                // Shade F Area
                {
                    -316,   // i4BoundXrThr
                    -431    // i4BoundYrThr
                },
                // Shade F Vertex
                {
                    -241,   // i4BoundXrThr
                    -446    // i4BoundYrThr
                },
                // Shade CWF Area
                {
                    -316,   // i4BoundXrThr
                    -498    // i4BoundYrThr
                },
                // Shade CWF Vertex
                {
                    -226,   // i4BoundXrThr
                    -524    // i4BoundYrThr
                },
            },
            // CCT estimation
            {
                // CCT
                {
                    2300,    // i4CCT[0]
                    2850,    // i4CCT[1]
                    3750,    // i4CCT[2]
                    5100,    // i4CCT[3]
                    6500     // i4CCT[4]
                },
                // Rotated X coordinate
                {
                -562,    // i4RotatedXCoordinate[0]
                -430,    // i4RotatedXCoordinate[1]
                -263,    // i4RotatedXCoordinate[2]
                -139,    // i4RotatedXCoordinate[3]
                0    // i4RotatedXCoordinate[4]
                }
            }
            },

            // Algorithm Tuning Paramter
            {
                // AWB Backup Enable
                0,

                // Daylight locus offset LUTs for tungsten
                {
                    21, // i4Size: LUT dimension
                    {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                    {0, 350,  800, 1222, 1444, 1667, 1889, 2111, 2333, 2556, 2778, 3000, 3222, 3444, 3667, 3889, 4111, 4333, 4556, 4778,  5000} // i4LUTOut
                    //{0, 500, 1000, 1333, 1667, 2000, 2333, 2667, 3000, 3333, 3667, 4000, 4333, 4667, 5000, 5333, 5667, 6000, 6333, 6667,  7000} // i4LUTOut
                    //{0, 500, 1000, 1500, 2000, 2313, 2625, 2938, 3250, 3563, 3875, 4188, 4500, 4813, 5125, 5438, 5750, 6063, 6375, 6688,  7000} // i4LUTOut
                },

                // Daylight locus offset LUTs for WF
                {
                    21, // i4Size: LUT dimension
                    {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                {0, 350, 700, 1000, 1444, 1667, 1889, 2111, 2333, 2556, 2778, 3000, 3222, 3444, 3667, 3889, 4111, 4333, 4556, 4778, 5000} // i4LUTOut
                },

                // Daylight locus offset LUTs for shade
                {
                    21, // i4Size: LUT dimension
                    {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000} // i4LUTOut
                },
                // Preference gain for each light source
                {
                    //        LV0              LV1              LV2              LV3              LV4              LV5              LV6              LV7              LV8              LV9
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                    //        LV10             LV11             LV12             LV13             LV14             LV15             LV16             LV17             LV18
          	            {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
        	        }, // STROBE
        	        {
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, 
           	            {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
        	        }, // TUNGSTEN
        	        {
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, 
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
        	        }, // WARM F
        	        {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}
                    }, // F
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                    }, // CWF
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}
                    }, // DAYLIGHT
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                    }, // SHADE
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                    } // DAYLIGHT F
                },
                // Parent block weight parameter
                {
                    1,      // bEnable
                    6           // i4ScalingFactor: [6] 1~12, [7] 1~6, [8] 1~3, [9] 1~2, [>=10]: 1
                },
                // AWB LV threshold for predictor
                {
                    115, //100,    // i4InitLVThr_L
                    155, //140,    // i4InitLVThr_H
                    100 //80      // i4EnqueueLVThr
                },
                // AWB number threshold for temporal predictor
                {
                        65,     // i4Neutral_ParentBlk_Thr
                    //LV0   1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  50,  25,   2,   2,   2,   2,   2,   2,   2}  // (%) i4CWFDF_LUTThr
                },
                // AWB light neutral noise reduction for outdoor
                {
                    //LV0  1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    // Non neutral
	                { 3,   3,   3,   3,   3,   3,   3,   3,    3,   3,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // Flurescent
                    {   0,   0,   0,   0,   0,   3,   5,   5,   5,   5,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // CWF
                    {   0,   0,   0,   0,   0,   3,   5,   5,   5,   5,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // Daylight
	                { 0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   0,   2,   2,   2,   2,   2,   2,   2,   2},  // (%)
	                // DF
	                { 0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
                },
                // AWB feature detection
                {
                    // Sunset Prop
                    {
                        1,          // i4Enable
                        120,        // i4LVThr_L
                        130,        // i4LVThr_H
                        10,         // i4SunsetCountThr
                        0,          // i4SunsetCountRatio_L
                        171         // i4SunsetCountRatio_H
                    },

                    // Shade F Detection
                    {
                        1,          // i4Enable
                        50,        // i4LVThr_L
                        90,        // i4LVThr
                        128         // i4DaylightProb
                    },

                    // Shade CWF Detection
                    {
                        1,          // i4Enable
                        50,        // i4LVThr_L
                        90,        // i4LVThr
                        192         // i4DaylightProb
                    },

                },

                // AWB non-neutral probability for spatial and temporal weighting look-up table (Max: 100; Min: 0)
                {
                    //LV0   1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    {   0,  33,  66, 100, 100, 100, 100, 100, 100, 100, 100,  70,  30,  20,  10,   0,   0,   0,   0}
                },

                // AWB daylight locus probability look-up table (Max: 100; Min: 0)
                {
                    //LV0  1    2    3    4    5    6    7    8    9     10    11   12   13   14   15  16   17   18
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,  100, 100,  50,  25,   0,  0,   0,   0}, // Strobe
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  75,  50,  25,   0,   0,   0}, // Tungsten
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,   75,  50,  25,  25,  25,  0,   0,   0}, // Warm fluorescent
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  95,  75,  50,  25,  25,  25,   0,   0,   0}, // Fluorescent
                    { 90,  90,  90,  90,  90,  90,  90,  90,  90,  90,   80,   55,  30,  30,  30,  30,  0,   0,   0}, // CWF
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,  100, 100, 100,  75,  50, 40,  30,  20}, // Daylight
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,  100,  75,  50,  25,   0,  0,   0,   0}, // Shade
                    { 90,  90,  90,  90,  90,  90,  90,  90,  90,  90,   80,   55,  30,  30,  30,  30,  0,   0,   0}  // Daylight fluorescent
                },

                // AWB tuning information
                {
                    0,		// project code
                    0,		// model
                    0,		// date
                    0,          // reserved 0
                    0,		// reserved 1
                    0,		// reserved 2
                    0,		// reserved 3
                    0,		// reserved 4
                }
            }
        }
    },

    // Flash AWB NVRAM
    {
#include INCLUDE_FILENAME_FLASH_AWB_PARA
    },

    {0}
};

#include INCLUDE_FILENAME_ISP_LSC_PARAM
//};  //  namespace

const CAMERA_TSF_TBL_STRUCT CAMERA_TSF_DEFAULT_VALUE =
{
    {
        1,  // isTsfEn
        2,  // tsfCtIdx
        {20, 2000, -110, -110, 512, 512, 512, 0}    // rAWBInput[8]
    },

#include INCLUDE_FILENAME_TSF_PARA
#include INCLUDE_FILENAME_TSF_DATA
};

const NVRAM_CAMERA_FEATURE_STRUCT CAMERA_FEATURE_DEFAULT_VALUE =
{
#include INCLUDE_FILENAME_FEATURE_PARA
};

typedef NSFeature::RAWSensorInfo<SENSOR_ID> SensorInfoSingleton_T;


namespace NSFeature {
  template <>
  UINT32
  SensorInfoSingleton_T::
  impGetDefaultData(CAMERA_DATA_TYPE_ENUM const CameraDataType, VOID*const pDataBuf, UINT32 const size) const
  {
    UINT32 dataSize[CAMERA_DATA_TYPE_NUM] = {sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT),
        sizeof(NVRAM_CAMERA_3A_STRUCT),
        sizeof(NVRAM_CAMERA_SHADING_STRUCT),
        sizeof(NVRAM_LENS_PARA_STRUCT),
        sizeof(AE_PLINETABLE_T),
        0,
        sizeof(CAMERA_TSF_TBL_STRUCT),
        0,
        sizeof(NVRAM_CAMERA_FEATURE_STRUCT)
    };

    if (CameraDataType > CAMERA_NVRAM_DATA_FEATURE || NULL == pDataBuf || (size < dataSize[CameraDataType]))
    {
      return 1;
    }

    switch(CameraDataType)
    {
      case CAMERA_NVRAM_DATA_ISP:
        memcpy(pDataBuf,&CAMERA_ISP_DEFAULT_VALUE,sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT));
        break;
      case CAMERA_NVRAM_DATA_3A:
        memcpy(pDataBuf,&CAMERA_3A_NVRAM_DEFAULT_VALUE,sizeof(NVRAM_CAMERA_3A_STRUCT));
        break;
      case CAMERA_NVRAM_DATA_SHADING:
        memcpy(pDataBuf,&CAMERA_SHADING_DEFAULT_VALUE,sizeof(NVRAM_CAMERA_SHADING_STRUCT));
        break;
      case CAMERA_DATA_AE_PLINETABLE:
        memcpy(pDataBuf,&g_PlineTableMapping,sizeof(AE_PLINETABLE_T));
        break;
      case CAMERA_DATA_TSF_TABLE:
        memcpy(pDataBuf,&CAMERA_TSF_DEFAULT_VALUE,sizeof(CAMERA_TSF_TBL_STRUCT));
        break;
      case CAMERA_NVRAM_DATA_FEATURE:
        memcpy(pDataBuf,&CAMERA_FEATURE_DEFAULT_VALUE,sizeof(NVRAM_CAMERA_FEATURE_STRUCT));
        break;
      default:
        break;
    }
    return 0;
  }};  //  NSFeature

