/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/

// Flash AWB tuning parameter
{
    9,  //foreground percentage
    95, //background percentage
     2, //FgPercentage_Th1 
     5, //FgPercentage_Th2
    10, //FgPercentage_Th3 
    15, //FgPercentage_Th4 
   250, //FgPercentage_Th1_Val 
   250, //FgPercentage_Th2_Val 
   250, //FgPercentage_Th3_Val 
   250, //FgPercentage_Th4_Val 
    10, //location_map_th1 
    20, //location_map_th2
    40, //location_map_th3 
    50, //location_map_th4 
   100, //location_map_val1 
   100, //location_map_val2 
   100, //location_map_val3
   100, //location_map_val4
     0, //SelfTuningFbBgWeightTbl
    100, //FgBgTbl_Y0
    100, //FgBgTbl_Y1
    100, //FgBgTbl_Y2
    100, //FgBgTbl_Y3
    100, //FgBgTbl_Y4
    100, //FgBgTbl_Y5
      5, //YPrimeWeightTh[0]
      9, //YPrimeWeightTh[1]
		 11, //YPrimeWeightTh[2]
		 13, //YPrimeWeightTh[3]
		 15, //YPrimeWeightTh[4]
			1, //YPrimeWeight[0]
			3, //YPrimeWeight[1]
			5, //YPrimeWeight[2]
			7, //YPrimeWeight[3]
	// FlashPreferenceGain
	{
		512,	// FlashPreferenceGain.i4R
		512,	// FlashPreferenceGain.i4G
		512	  // FlashPreferenceGain.i4B
	},
},

// Flash AWB calibration
{{
   { 512, 512, 512},  //duty=-1, dutyLt=-1
   {1170, 512, 960},  //duty=0, dutyLt=-1
   {1165, 512, 952},  //duty=1, dutyLt=-1
   {1164, 512, 950},  //duty=2, dutyLt=-1
   {1167, 512, 943},  //duty=3, dutyLt=-1
   {1168, 512, 936},  //duty=4, dutyLt=-1
   {1165, 512, 932},  //duty=5, dutyLt=-1
   {1157, 512, 932},  //duty=6, dutyLt=-1
   {1161, 512, 922},  //duty=7, dutyLt=-1
   {1155, 512, 910},  //duty=8, dutyLt=-1
   {1153, 512, 901},  //duty=9, dutyLt=-1
   {1157, 512, 896},  //duty=10, dutyLt=-1
   {1155, 512, 893},  //duty=11, dutyLt=-1
   {1154, 512, 889},  //duty=12, dutyLt=-1
   {1153, 512, 885},  //duty=13, dutyLt=-1
   {1153, 512, 881},  //duty=14, dutyLt=-1
   {1152, 512, 878},  //duty=15, dutyLt=-1
   {1152, 512, 874},  //duty=16, dutyLt=-1
   {1152, 512, 871},  //duty=17, dutyLt=-1
   {1152, 512, 868},  //duty=18, dutyLt=-1
   {1152, 512, 867},  //duty=19, dutyLt=-1
   {1152, 512, 864},  //duty=20, dutyLt=-1
   {1152, 512, 863},  //duty=21, dutyLt=-1
   {1152, 512, 861},  //duty=22, dutyLt=-1
   {1152, 512, 859},  //duty=23, dutyLt=-1
   {1152, 512, 857},  //duty=24, dutyLt=-1
   {1152, 512, 856},  //duty=25, dutyLt=-1
   { 584, 512,2244},  //duty=-1, dutyLt=0
   { 776, 512,1353},  //duty=0, dutyLt=0
   { 880, 512,1178},  //duty=1, dutyLt=0
   { 939, 512,1107},  //duty=2, dutyLt=0
   { 976, 512,1067},  //duty=3, dutyLt=0
   { 999, 512,1041},  //duty=4, dutyLt=0
   {1023, 512,1014},  //duty=5, dutyLt=0
   {1036, 512, 998},  //duty=6, dutyLt=0
   {1048, 512, 980},  //duty=7, dutyLt=0
   {1061, 512, 961},  //duty=8, dutyLt=0
   {1075, 512, 947},  //duty=9, dutyLt=0
   {1082, 512, 937},  //duty=10, dutyLt=0
   {1089, 512, 928},  //duty=11, dutyLt=0
   {1095, 512, 921},  //duty=12, dutyLt=0
   {1100, 512, 913},  //duty=13, dutyLt=0
   {1102, 512, 910},  //duty=14, dutyLt=0
   {1107, 512, 904},  //duty=15, dutyLt=0
   {1108, 512, 898},  //duty=16, dutyLt=0
   {1112, 512, 895},  //duty=17, dutyLt=0
   {1113, 512, 892},  //duty=18, dutyLt=0
   {1115, 512, 889},  //duty=19, dutyLt=0
   {1116, 512, 884},  //duty=20, dutyLt=0
   {1117, 512, 882},  //duty=21, dutyLt=0
   {1118, 512, 880},  //duty=22, dutyLt=0
   {1119, 512, 877},  //duty=23, dutyLt=0
   {1120, 512, 875},  //duty=24, dutyLt=0
   { 512, 512, 512},  //duty=25, dutyLt=0
   { 583, 512,2229},  //duty=-1, dutyLt=1
   { 696, 512,1566},  //duty=0, dutyLt=1
   { 779, 512,1340},  //duty=1, dutyLt=1
   { 835, 512,1235},  //duty=2, dutyLt=1
   { 875, 512,1168},  //duty=3, dutyLt=1
   { 904, 512,1128},  //duty=4, dutyLt=1
   { 932, 512,1088},  //duty=5, dutyLt=1
   { 946, 512,1063},  //duty=6, dutyLt=1
   { 966, 512,1040},  //duty=7, dutyLt=1
   { 990, 512,1012},  //duty=8, dutyLt=1
   {1013, 512, 988},  //duty=9, dutyLt=1
   {1025, 512, 976},  //duty=10, dutyLt=1
   {1036, 512, 964},  //duty=11, dutyLt=1
   {1047, 512, 951},  //duty=12, dutyLt=1
   {1052, 512, 944},  //duty=13, dutyLt=1
   {1059, 512, 936},  //duty=14, dutyLt=1
   {1064, 512, 929},  //duty=15, dutyLt=1
   {1070, 512, 921},  //duty=16, dutyLt=1
   {1075, 512, 916},  //duty=17, dutyLt=1
   {1079, 512, 912},  //duty=18, dutyLt=1
   {1080, 512, 909},  //duty=19, dutyLt=1
   {1083, 512, 905},  //duty=20, dutyLt=1
   {1086, 512, 901},  //duty=21, dutyLt=1
   {1088, 512, 898},  //duty=22, dutyLt=1
   {1090, 512, 895},  //duty=23, dutyLt=1
   {1092, 512, 891},  //duty=24, dutyLt=1
   { 512, 512, 512},  //duty=25, dutyLt=1
   { 582, 512,2229},  //duty=-1, dutyLt=2
   { 663, 512,1695},  //duty=0, dutyLt=2
   { 729, 512,1453},  //duty=1, dutyLt=2
   { 778, 512,1328},  //duty=2, dutyLt=2
   { 817, 512,1249},  //duty=3, dutyLt=2
   { 845, 512,1198},  //duty=4, dutyLt=2
   { 871, 512,1145},  //duty=5, dutyLt=2
   { 891, 512,1124},  //duty=6, dutyLt=2
   { 914, 512,1091},  //duty=7, dutyLt=2
   { 941, 512,1055},  //duty=8, dutyLt=2
   { 967, 512,1024},  //duty=9, dutyLt=2
   { 984, 512,1007},  //duty=10, dutyLt=2
   { 994, 512, 993},  //duty=11, dutyLt=2
   {1005, 512, 982},  //duty=12, dutyLt=2
   {1016, 512, 968},  //duty=13, dutyLt=2
   {1024, 512, 959},  //duty=14, dutyLt=2
   {1031, 512, 951},  //duty=15, dutyLt=2
   {1038, 512, 942},  //duty=16, dutyLt=2
   {1044, 512, 936},  //duty=17, dutyLt=2
   {1048, 512, 931},  //duty=18, dutyLt=2
   {1051, 512, 927},  //duty=19, dutyLt=2
   {1055, 512, 922},  //duty=20, dutyLt=2
   {1059, 512, 917},  //duty=21, dutyLt=2
   {1062, 512, 913},  //duty=22, dutyLt=2
   {1064, 512, 910},  //duty=23, dutyLt=2
   { 512, 512, 512},  //duty=24, dutyLt=2
   { 512, 512, 512},  //duty=25, dutyLt=2
   { 582, 512,2218},  //duty=-1, dutyLt=3
   { 645, 512,1769},  //duty=0, dutyLt=3
   { 700, 512,1535},  //duty=1, dutyLt=3
   { 743, 512,1402},  //duty=2, dutyLt=3
   { 780, 512,1312},  //duty=3, dutyLt=3
   { 808, 512,1245},  //duty=4, dutyLt=3
   { 831, 512,1201},  //duty=5, dutyLt=3
   { 851, 512,1166},  //duty=6, dutyLt=3
   { 876, 512,1134},  //duty=7, dutyLt=3
   { 905, 512,1093},  //duty=8, dutyLt=3
   { 932, 512,1057},  //duty=9, dutyLt=3
   { 950, 512,1036},  //duty=10, dutyLt=3
   { 964, 512,1019},  //duty=11, dutyLt=3
   { 976, 512,1004},  //duty=12, dutyLt=3
   { 988, 512, 990},  //duty=13, dutyLt=3
   { 997, 512, 979},  //duty=14, dutyLt=3
   {1004, 512, 971},  //duty=15, dutyLt=3
   {1012, 512, 961},  //duty=16, dutyLt=3
   {1017, 512, 953},  //duty=17, dutyLt=3
   {1022, 512, 949},  //duty=18, dutyLt=3
   {1025, 512, 945},  //duty=19, dutyLt=3
   {1032, 512, 937},  //duty=20, dutyLt=3
   {1036, 512, 932},  //duty=21, dutyLt=3
   {1040, 512, 928},  //duty=22, dutyLt=3
   {1042, 512, 924},  //duty=23, dutyLt=3
   { 512, 512, 512},  //duty=24, dutyLt=3
   { 512, 512, 512},  //duty=25, dutyLt=3
   { 581, 512,2207},  //duty=-1, dutyLt=4
   { 636, 512,1810},  //duty=0, dutyLt=4
   { 681, 512,1592},  //duty=1, dutyLt=4
   { 721, 512,1455},  //duty=2, dutyLt=4
   { 754, 512,1352},  //duty=3, dutyLt=4
   { 779, 512,1293},  //duty=4, dutyLt=4
   { 802, 512,1245},  //duty=5, dutyLt=4
   { 823, 512,1213},  //duty=6, dutyLt=4
   { 846, 512,1171},  //duty=7, dutyLt=4
   { 875, 512,1127},  //duty=8, dutyLt=4
   { 901, 512,1089},  //duty=9, dutyLt=4
   { 921, 512,1063},  //duty=10, dutyLt=4
   { 936, 512,1045},  //duty=11, dutyLt=4
   { 949, 512,1028},  //duty=12, dutyLt=4
   { 962, 512,1011},  //duty=13, dutyLt=4
   { 971, 512,1000},  //duty=14, dutyLt=4
   { 979, 512, 990},  //duty=15, dutyLt=4
   { 988, 512, 980},  //duty=16, dutyLt=4
   { 993, 512, 973},  //duty=17, dutyLt=4
   {1000, 512, 964},  //duty=18, dutyLt=4
   {1004, 512, 961},  //duty=19, dutyLt=4
   {1010, 512, 953},  //duty=20, dutyLt=4
   {1014, 512, 948},  //duty=21, dutyLt=4
   {1020, 512, 942},  //duty=22, dutyLt=4
   { 512, 512, 512},  //duty=23, dutyLt=4
   { 512, 512, 512},  //duty=24, dutyLt=4
   { 512, 512, 512},  //duty=25, dutyLt=4
   { 582, 512,2189},  //duty=-1, dutyLt=5
   { 627, 512,1845},  //duty=0, dutyLt=5
   { 670, 512,1628},  //duty=1, dutyLt=5
   { 703, 512,1489},  //duty=2, dutyLt=5
   { 734, 512,1401},  //duty=3, dutyLt=5
   { 757, 512,1335},  //duty=4, dutyLt=5
   { 779, 512,1286},  //duty=5, dutyLt=5
   { 798, 512,1246},  //duty=6, dutyLt=5
   { 822, 512,1204},  //duty=7, dutyLt=5
   { 851, 512,1159},  //duty=8, dutyLt=5
   { 879, 512,1115},  //duty=9, dutyLt=5
   { 895, 512,1092},  //duty=10, dutyLt=5
   { 910, 512,1071},  //duty=11, dutyLt=5
   { 924, 512,1052},  //duty=12, dutyLt=5
   { 939, 512,1032},  //duty=13, dutyLt=5
   { 949, 512,1019},  //duty=14, dutyLt=5
   { 957, 512,1008},  //duty=15, dutyLt=5
   { 967, 512, 996},  //duty=16, dutyLt=5
   { 974, 512, 988},  //duty=17, dutyLt=5
   { 980, 512, 981},  //duty=18, dutyLt=5
   { 984, 512, 976},  //duty=19, dutyLt=5
   { 991, 512, 967},  //duty=20, dutyLt=5
   { 997, 512, 960},  //duty=21, dutyLt=5
   {1002, 512, 956},  //duty=22, dutyLt=5
   { 512, 512, 512},  //duty=23, dutyLt=5
   { 512, 512, 512},  //duty=24, dutyLt=5
   { 512, 512, 512},  //duty=25, dutyLt=5
   { 581, 512,2186},  //duty=-1, dutyLt=6
   { 621, 512,1871},  //duty=0, dutyLt=6
   { 659, 512,1658},  //duty=1, dutyLt=6
   { 691, 512,1529},  //duty=2, dutyLt=6
   { 717, 512,1437},  //duty=3, dutyLt=6
   { 740, 512,1374},  //duty=4, dutyLt=6
   { 761, 512,1323},  //duty=5, dutyLt=6
   { 779, 512,1280},  //duty=6, dutyLt=6
   { 802, 512,1237},  //duty=7, dutyLt=6
   { 829, 512,1188},  //duty=8, dutyLt=6
   { 858, 512,1141},  //duty=9, dutyLt=6
   { 877, 512,1113},  //duty=10, dutyLt=6
   { 892, 512,1090},  //duty=11, dutyLt=6
   { 905, 512,1070},  //duty=12, dutyLt=6
   { 919, 512,1052},  //duty=13, dutyLt=6
   { 930, 512,1038},  //duty=14, dutyLt=6
   { 938, 512,1025},  //duty=15, dutyLt=6
   { 949, 512,1012},  //duty=16, dutyLt=6
   { 957, 512,1002},  //duty=17, dutyLt=6
   { 963, 512, 994},  //duty=18, dutyLt=6
   { 967, 512, 989},  //duty=19, dutyLt=6
   { 976, 512, 979},  //duty=20, dutyLt=6
   { 981, 512, 973},  //duty=21, dutyLt=6
   { 512, 512, 512},  //duty=22, dutyLt=6
   { 512, 512, 512},  //duty=23, dutyLt=6
   { 512, 512, 512},  //duty=24, dutyLt=6
   { 512, 512, 512},  //duty=25, dutyLt=6
   { 582, 512,2157},  //duty=-1, dutyLt=7
   { 616, 512,1883},  //duty=0, dutyLt=7
   { 647, 512,1705},  //duty=1, dutyLt=7
   { 674, 512,1585},  //duty=2, dutyLt=7
   { 699, 512,1502},  //duty=3, dutyLt=7
   { 720, 512,1426},  //duty=4, dutyLt=7
   { 738, 512,1373},  //duty=5, dutyLt=7
   { 756, 512,1330},  //duty=6, dutyLt=7
   { 779, 512,1280},  //duty=7, dutyLt=7
   { 805, 512,1226},  //duty=8, dutyLt=7
   { 831, 512,1178},  //duty=9, dutyLt=7
   { 849, 512,1147},  //duty=10, dutyLt=7
   { 866, 512,1119},  //duty=11, dutyLt=7
   { 879, 512,1100},  //duty=12, dutyLt=7
   { 895, 512,1078},  //duty=13, dutyLt=7
   { 905, 512,1062},  //duty=14, dutyLt=7
   { 915, 512,1048},  //duty=15, dutyLt=7
   { 926, 512,1033},  //duty=16, dutyLt=7
   { 933, 512,1023},  //duty=17, dutyLt=7
   { 941, 512,1014},  //duty=18, dutyLt=7
   { 946, 512,1008},  //duty=19, dutyLt=7
   { 953, 512, 998},  //duty=20, dutyLt=7
   { 959, 512, 991},  //duty=21, dutyLt=7
   { 512, 512, 512},  //duty=22, dutyLt=7
   { 512, 512, 512},  //duty=23, dutyLt=7
   { 512, 512, 512},  //duty=24, dutyLt=7
   { 512, 512, 512},  //duty=25, dutyLt=7
   { 583, 512,2124},  //duty=-1, dutyLt=8
   { 612, 512,1915},  //duty=0, dutyLt=8
   { 638, 512,1757},  //duty=1, dutyLt=8
   { 662, 512,1640},  //duty=2, dutyLt=8
   { 683, 512,1553},  //duty=3, dutyLt=8
   { 701, 512,1477},  //duty=4, dutyLt=8
   { 720, 512,1429},  //duty=5, dutyLt=8
   { 735, 512,1381},  //duty=6, dutyLt=8
   { 757, 512,1326},  //duty=7, dutyLt=8
   { 781, 512,1268},  //duty=8, dutyLt=8
   { 806, 512,1215},  //duty=9, dutyLt=8
   { 823, 512,1182},  //duty=10, dutyLt=8
   { 838, 512,1154},  //duty=11, dutyLt=8
   { 852, 512,1132},  //duty=12, dutyLt=8
   { 867, 512,1108},  //duty=13, dutyLt=8
   { 879, 512,1091},  //duty=14, dutyLt=8
   { 889, 512,1077},  //duty=15, dutyLt=8
   { 899, 512,1060},  //duty=16, dutyLt=8
   { 907, 512,1049},  //duty=17, dutyLt=8
   { 914, 512,1039},  //duty=18, dutyLt=8
   { 920, 512,1033},  //duty=19, dutyLt=8
   { 928, 512,1022},  //duty=20, dutyLt=8
   { 512, 512, 512},  //duty=21, dutyLt=8
   { 512, 512, 512},  //duty=22, dutyLt=8
   { 512, 512, 512},  //duty=23, dutyLt=8
   { 512, 512, 512},  //duty=24, dutyLt=8
   { 512, 512, 512},  //duty=25, dutyLt=8
   { 583, 512,2124},  //duty=-1, dutyLt=9
   { 607, 512,1941},  //duty=0, dutyLt=9
   { 630, 512,1792},  //duty=1, dutyLt=9
   { 650, 512,1687},  //duty=2, dutyLt=9
   { 667, 512,1593},  //duty=3, dutyLt=9
   { 684, 512,1530},  //duty=4, dutyLt=9
   { 700, 512,1475},  //duty=5, dutyLt=9
   { 714, 512,1430},  //duty=6, dutyLt=9
   { 733, 512,1374},  //duty=7, dutyLt=9
   { 756, 512,1314},  //duty=8, dutyLt=9
   { 780, 512,1258},  //duty=9, dutyLt=9
   { 797, 512,1222},  //duty=10, dutyLt=9
   { 812, 512,1193},  //duty=11, dutyLt=9
   { 826, 512,1170},  //duty=12, dutyLt=9
   { 841, 512,1142},  //duty=13, dutyLt=9
   { 852, 512,1124},  //duty=14, dutyLt=9
   { 862, 512,1108},  //duty=15, dutyLt=9
   { 873, 512,1090},  //duty=16, dutyLt=9
   { 881, 512,1078},  //duty=17, dutyLt=9
   { 888, 512,1067},  //duty=18, dutyLt=9
   { 894, 512,1061},  //duty=19, dutyLt=9
   { 512, 512, 512},  //duty=20, dutyLt=9
   { 512, 512, 512},  //duty=21, dutyLt=9
   { 512, 512, 512},  //duty=22, dutyLt=9
   { 512, 512, 512},  //duty=23, dutyLt=9
   { 512, 512, 512},  //duty=24, dutyLt=9
   { 512, 512, 512},  //duty=25, dutyLt=9
   { 583, 512,2115},  //duty=-1, dutyLt=10
   { 604, 512,1946},  //duty=0, dutyLt=10
   { 624, 512,1816},  //duty=1, dutyLt=10
   { 643, 512,1714},  //duty=2, dutyLt=10
   { 659, 512,1625},  //duty=3, dutyLt=10
   { 675, 512,1566},  //duty=4, dutyLt=10
   { 688, 512,1509},  //duty=5, dutyLt=10
   { 702, 512,1463},  //duty=6, dutyLt=10
   { 720, 512,1407},  //duty=7, dutyLt=10
   { 742, 512,1349},  //duty=8, dutyLt=10
   { 764, 512,1287},  //duty=9, dutyLt=10
   { 781, 512,1250},  //duty=10, dutyLt=10
   { 795, 512,1220},  //duty=11, dutyLt=10
   { 809, 512,1194},  //duty=12, dutyLt=10
   { 824, 512,1167},  //duty=13, dutyLt=10
   { 834, 512,1147},  //duty=14, dutyLt=10
   { 844, 512,1131},  //duty=15, dutyLt=10
   { 855, 512,1113},  //duty=16, dutyLt=10
   { 864, 512,1100},  //duty=17, dutyLt=10
   { 871, 512,1089},  //duty=18, dutyLt=10
   { 512, 512, 512},  //duty=19, dutyLt=10
   { 512, 512, 512},  //duty=20, dutyLt=10
   { 512, 512, 512},  //duty=21, dutyLt=10
   { 512, 512, 512},  //duty=22, dutyLt=10
   { 512, 512, 512},  //duty=23, dutyLt=10
   { 512, 512, 512},  //duty=24, dutyLt=10
   { 512, 512, 512},  //duty=25, dutyLt=10
   { 583, 512,2105},  //duty=-1, dutyLt=11
   { 601, 512,1953},  //duty=0, dutyLt=11
   { 620, 512,1822},  //duty=1, dutyLt=11
   { 636, 512,1735},  //duty=2, dutyLt=11
   { 651, 512,1652},  //duty=3, dutyLt=11
   { 666, 512,1589},  //duty=4, dutyLt=11
   { 680, 512,1538},  //duty=5, dutyLt=11
   { 692, 512,1491},  //duty=6, dutyLt=11
   { 709, 512,1435},  //duty=7, dutyLt=11
   { 730, 512,1375},  //duty=8, dutyLt=11
   { 752, 512,1317},  //duty=9, dutyLt=11
   { 768, 512,1279},  //duty=10, dutyLt=11
   { 782, 512,1245},  //duty=11, dutyLt=11
   { 795, 512,1217},  //duty=12, dutyLt=11
   { 809, 512,1189},  //duty=13, dutyLt=11
   { 820, 512,1169},  //duty=14, dutyLt=11
   { 829, 512,1151},  //duty=15, dutyLt=11
   { 840, 512,1131},  //duty=16, dutyLt=11
   { 848, 512,1118},  //duty=17, dutyLt=11
   { 512, 512, 512},  //duty=18, dutyLt=11
   { 512, 512, 512},  //duty=19, dutyLt=11
   { 512, 512, 512},  //duty=20, dutyLt=11
   { 512, 512, 512},  //duty=21, dutyLt=11
   { 512, 512, 512},  //duty=22, dutyLt=11
   { 512, 512, 512},  //duty=23, dutyLt=11
   { 512, 512, 512},  //duty=24, dutyLt=11
   { 512, 512, 512},  //duty=25, dutyLt=11
   { 582, 512,2082},  //duty=-1, dutyLt=12
   { 599, 512,1949},  //duty=0, dutyLt=12
   { 616, 512,1837},  //duty=1, dutyLt=12
   { 631, 512,1748},  //duty=2, dutyLt=12
   { 646, 512,1675},  //duty=3, dutyLt=12
   { 660, 512,1613},  //duty=4, dutyLt=12
   { 673, 512,1562},  //duty=5, dutyLt=12
   { 684, 512,1516},  //duty=6, dutyLt=12
   { 701, 512,1461},  //duty=7, dutyLt=12
   { 719, 512,1396},  //duty=8, dutyLt=12
   { 741, 512,1337},  //duty=9, dutyLt=12
   { 756, 512,1299},  //duty=10, dutyLt=12
   { 770, 512,1267},  //duty=11, dutyLt=12
   { 783, 512,1238},  //duty=12, dutyLt=12
   { 796, 512,1209},  //duty=13, dutyLt=12
   { 807, 512,1188},  //duty=14, dutyLt=12
   { 816, 512,1170},  //duty=15, dutyLt=12
   { 827, 512,1150},  //duty=16, dutyLt=12
   { 512, 512, 512},  //duty=17, dutyLt=12
   { 512, 512, 512},  //duty=18, dutyLt=12
   { 512, 512, 512},  //duty=19, dutyLt=12
   { 512, 512, 512},  //duty=20, dutyLt=12
   { 512, 512, 512},  //duty=21, dutyLt=12
   { 512, 512, 512},  //duty=22, dutyLt=12
   { 512, 512, 512},  //duty=23, dutyLt=12
   { 512, 512, 512},  //duty=24, dutyLt=12
   { 512, 512, 512},  //duty=25, dutyLt=12
   { 582, 512,2072},  //duty=-1, dutyLt=13
   { 597, 512,1953},  //duty=0, dutyLt=13
   { 613, 512,1851},  //duty=1, dutyLt=13
   { 628, 512,1765},  //duty=2, dutyLt=13
   { 641, 512,1695},  //duty=3, dutyLt=13
   { 654, 512,1635},  //duty=4, dutyLt=13
   { 665, 512,1584},  //duty=5, dutyLt=13
   { 677, 512,1536},  //duty=6, dutyLt=13
   { 691, 512,1485},  //duty=7, dutyLt=13
   { 710, 512,1423},  //duty=8, dutyLt=13
   { 730, 512,1363},  //duty=9, dutyLt=13
   { 745, 512,1324},  //duty=10, dutyLt=13
   { 757, 512,1291},  //duty=11, dutyLt=13
   { 770, 512,1262},  //duty=12, dutyLt=13
   { 783, 512,1231},  //duty=13, dutyLt=13
   { 794, 512,1211},  //duty=14, dutyLt=13
   { 803, 512,1193},  //duty=15, dutyLt=13
   { 512, 512, 512},  //duty=16, dutyLt=13
   { 512, 512, 512},  //duty=17, dutyLt=13
   { 512, 512, 512},  //duty=18, dutyLt=13
   { 512, 512, 512},  //duty=19, dutyLt=13
   { 512, 512, 512},  //duty=20, dutyLt=13
   { 512, 512, 512},  //duty=21, dutyLt=13
   { 512, 512, 512},  //duty=22, dutyLt=13
   { 512, 512, 512},  //duty=23, dutyLt=13
   { 512, 512, 512},  //duty=24, dutyLt=13
   { 512, 512, 512},  //duty=25, dutyLt=13
   { 582, 512,2078},  //duty=-1, dutyLt=14
   { 596, 512,1964},  //duty=0, dutyLt=14
   { 611, 512,1862},  //duty=1, dutyLt=14
   { 625, 512,1777},  //duty=2, dutyLt=14
   { 637, 512,1713},  //duty=3, dutyLt=14
   { 649, 512,1653},  //duty=4, dutyLt=14
   { 659, 512,1602},  //duty=5, dutyLt=14
   { 670, 512,1558},  //duty=6, dutyLt=14
   { 685, 512,1506},  //duty=7, dutyLt=14
   { 703, 512,1440},  //duty=8, dutyLt=14
   { 722, 512,1382},  //duty=9, dutyLt=14
   { 736, 512,1343},  //duty=10, dutyLt=14
   { 749, 512,1309},  //duty=11, dutyLt=14
   { 761, 512,1280},  //duty=12, dutyLt=14
   { 775, 512,1249},  //duty=13, dutyLt=14
   { 785, 512,1228},  //duty=14, dutyLt=14
   { 512, 512, 512},  //duty=15, dutyLt=14
   { 512, 512, 512},  //duty=16, dutyLt=14
   { 512, 512, 512},  //duty=17, dutyLt=14
   { 512, 512, 512},  //duty=18, dutyLt=14
   { 512, 512, 512},  //duty=19, dutyLt=14
   { 512, 512, 512},  //duty=20, dutyLt=14
   { 512, 512, 512},  //duty=21, dutyLt=14
   { 512, 512, 512},  //duty=22, dutyLt=14
   { 512, 512, 512},  //duty=23, dutyLt=14
   { 512, 512, 512},  //duty=24, dutyLt=14
   { 512, 512, 512},  //duty=25, dutyLt=14
   { 581, 512,2073},  //duty=-1, dutyLt=15
   { 595, 512,1964},  //duty=0, dutyLt=15
   { 610, 512,1863},  //duty=1, dutyLt=15
   { 623, 512,1788},  //duty=2, dutyLt=15
   { 635, 512,1724},  //duty=3, dutyLt=15
   { 646, 512,1666},  //duty=4, dutyLt=15
   { 656, 512,1619},  //duty=5, dutyLt=15
   { 666, 512,1578},  //duty=6, dutyLt=15
   { 680, 512,1522},  //duty=7, dutyLt=15
   { 697, 512,1463},  //duty=8, dutyLt=15
   { 716, 512,1401},  //duty=9, dutyLt=15
   { 729, 512,1361},  //duty=10, dutyLt=15
   { 741, 512,1325},  //duty=11, dutyLt=15
   { 754, 512,1295},  //duty=12, dutyLt=15
   { 767, 512,1265},  //duty=13, dutyLt=15
   { 512, 512, 512},  //duty=14, dutyLt=15
   { 512, 512, 512},  //duty=15, dutyLt=15
   { 512, 512, 512},  //duty=16, dutyLt=15
   { 512, 512, 512},  //duty=17, dutyLt=15
   { 512, 512, 512},  //duty=18, dutyLt=15
   { 512, 512, 512},  //duty=19, dutyLt=15
   { 512, 512, 512},  //duty=20, dutyLt=15
   { 512, 512, 512},  //duty=21, dutyLt=15
   { 512, 512, 512},  //duty=22, dutyLt=15
   { 512, 512, 512},  //duty=23, dutyLt=15
   { 512, 512, 512},  //duty=24, dutyLt=15
   { 512, 512, 512},  //duty=25, dutyLt=15
   { 582, 512,2062},  //duty=-1, dutyLt=16
   { 595, 512,1957},  //duty=0, dutyLt=16
   { 608, 512,1871},  //duty=1, dutyLt=16
   { 620, 512,1800},  //duty=2, dutyLt=16
   { 631, 512,1737},  //duty=3, dutyLt=16
   { 642, 512,1683},  //duty=4, dutyLt=16
   { 652, 512,1637},  //duty=5, dutyLt=16
   { 661, 512,1591},  //duty=6, dutyLt=16
   { 674, 512,1541},  //duty=7, dutyLt=16
   { 690, 512,1482},  //duty=8, dutyLt=16
   { 708, 512,1420},  //duty=9, dutyLt=16
   { 721, 512,1379},  //duty=10, dutyLt=16
   { 734, 512,1344},  //duty=11, dutyLt=16
   { 746, 512,1314},  //duty=12, dutyLt=16
   { 512, 512, 512},  //duty=13, dutyLt=16
   { 512, 512, 512},  //duty=14, dutyLt=16
   { 512, 512, 512},  //duty=15, dutyLt=16
   { 512, 512, 512},  //duty=16, dutyLt=16
   { 512, 512, 512},  //duty=17, dutyLt=16
   { 512, 512, 512},  //duty=18, dutyLt=16
   { 512, 512, 512},  //duty=19, dutyLt=16
   { 512, 512, 512},  //duty=20, dutyLt=16
   { 512, 512, 512},  //duty=21, dutyLt=16
   { 512, 512, 512},  //duty=22, dutyLt=16
   { 512, 512, 512},  //duty=23, dutyLt=16
   { 512, 512, 512},  //duty=24, dutyLt=16
   { 512, 512, 512},  //duty=25, dutyLt=16
   { 583, 512,2049},  //duty=-1, dutyLt=17
   { 595, 512,1957},  //duty=0, dutyLt=17
   { 607, 512,1875},  //duty=1, dutyLt=17
   { 618, 512,1806},  //duty=2, dutyLt=17
   { 629, 512,1746},  //duty=3, dutyLt=17
   { 640, 512,1687},  //duty=4, dutyLt=17
   { 650, 512,1645},  //duty=5, dutyLt=17
   { 658, 512,1603},  //duty=6, dutyLt=17
   { 671, 512,1549},  //duty=7, dutyLt=17
   { 687, 512,1490},  //duty=8, dutyLt=17
   { 704, 512,1430},  //duty=9, dutyLt=17
   { 717, 512,1391},  //duty=10, dutyLt=17
   { 728, 512,1358},  //duty=11, dutyLt=17
   { 512, 512, 512},  //duty=12, dutyLt=17
   { 512, 512, 512},  //duty=13, dutyLt=17
   { 512, 512, 512},  //duty=14, dutyLt=17
   { 512, 512, 512},  //duty=15, dutyLt=17
   { 512, 512, 512},  //duty=16, dutyLt=17
   { 512, 512, 512},  //duty=17, dutyLt=17
   { 512, 512, 512},  //duty=18, dutyLt=17
   { 512, 512, 512},  //duty=19, dutyLt=17
   { 512, 512, 512},  //duty=20, dutyLt=17
   { 512, 512, 512},  //duty=21, dutyLt=17
   { 512, 512, 512},  //duty=22, dutyLt=17
   { 512, 512, 512},  //duty=23, dutyLt=17
   { 512, 512, 512},  //duty=24, dutyLt=17
   { 512, 512, 512},  //duty=25, dutyLt=17
   { 583, 512,2051},  //duty=-1, dutyLt=18
   { 594, 512,1961},  //duty=0, dutyLt=18
   { 606, 512,1881},  //duty=1, dutyLt=18
   { 617, 512,1812},  //duty=2, dutyLt=18
   { 628, 512,1750},  //duty=3, dutyLt=18
   { 638, 512,1699},  //duty=4, dutyLt=18
   { 646, 512,1658},  //duty=5, dutyLt=18
   { 655, 512,1614},  //duty=6, dutyLt=18
   { 668, 512,1561},  //duty=7, dutyLt=18
   { 683, 512,1502},  //duty=8, dutyLt=18
   { 700, 512,1444},  //duty=9, dutyLt=18
   { 713, 512,1403},  //duty=10, dutyLt=18
   { 512, 512, 512},  //duty=11, dutyLt=18
   { 512, 512, 512},  //duty=12, dutyLt=18
   { 512, 512, 512},  //duty=13, dutyLt=18
   { 512, 512, 512},  //duty=14, dutyLt=18
   { 512, 512, 512},  //duty=15, dutyLt=18
   { 512, 512, 512},  //duty=16, dutyLt=18
   { 512, 512, 512},  //duty=17, dutyLt=18
   { 512, 512, 512},  //duty=18, dutyLt=18
   { 512, 512, 512},  //duty=19, dutyLt=18
   { 512, 512, 512},  //duty=20, dutyLt=18
   { 512, 512, 512},  //duty=21, dutyLt=18
   { 512, 512, 512},  //duty=22, dutyLt=18
   { 512, 512, 512},  //duty=23, dutyLt=18
   { 512, 512, 512},  //duty=24, dutyLt=18
   { 512, 512, 512},  //duty=25, dutyLt=18
   { 583, 512,2046},  //duty=-1, dutyLt=19
   { 595, 512,1956},  //duty=0, dutyLt=19
   { 606, 512,1882},  //duty=1, dutyLt=19
   { 616, 512,1815},  //duty=2, dutyLt=19
   { 626, 512,1760},  //duty=3, dutyLt=19
   { 636, 512,1709},  //duty=4, dutyLt=19
   { 645, 512,1665},  //duty=5, dutyLt=19
   { 653, 512,1625},  //duty=6, dutyLt=19
   { 665, 512,1574},  //duty=7, dutyLt=19
   { 680, 512,1515},  //duty=8, dutyLt=19
   { 696, 512,1455},  //duty=9, dutyLt=19
   { 512, 512, 512},  //duty=10, dutyLt=19
   { 512, 512, 512},  //duty=11, dutyLt=19
   { 512, 512, 512},  //duty=12, dutyLt=19
   { 512, 512, 512},  //duty=13, dutyLt=19
   { 512, 512, 512},  //duty=14, dutyLt=19
   { 512, 512, 512},  //duty=15, dutyLt=19
   { 512, 512, 512},  //duty=16, dutyLt=19
   { 512, 512, 512},  //duty=17, dutyLt=19
   { 512, 512, 512},  //duty=18, dutyLt=19
   { 512, 512, 512},  //duty=19, dutyLt=19
   { 512, 512, 512},  //duty=20, dutyLt=19
   { 512, 512, 512},  //duty=21, dutyLt=19
   { 512, 512, 512},  //duty=22, dutyLt=19
   { 512, 512, 512},  //duty=23, dutyLt=19
   { 512, 512, 512},  //duty=24, dutyLt=19
   { 512, 512, 512},  //duty=25, dutyLt=19
   { 584, 512,2035},  //duty=-1, dutyLt=20
   { 594, 512,1958},  //duty=0, dutyLt=20
   { 605, 512,1885},  //duty=1, dutyLt=20
   { 615, 512,1821},  //duty=2, dutyLt=20
   { 625, 512,1767},  //duty=3, dutyLt=20
   { 634, 512,1718},  //duty=4, dutyLt=20
   { 642, 512,1675},  //duty=5, dutyLt=20
   { 650, 512,1637},  //duty=6, dutyLt=20
   { 662, 512,1582},  //duty=7, dutyLt=20
   { 675, 512,1528},  //duty=8, dutyLt=20
   { 512, 512, 512},  //duty=9, dutyLt=20
   { 512, 512, 512},  //duty=10, dutyLt=20
   { 512, 512, 512},  //duty=11, dutyLt=20
   { 512, 512, 512},  //duty=12, dutyLt=20
   { 512, 512, 512},  //duty=13, dutyLt=20
   { 512, 512, 512},  //duty=14, dutyLt=20
   { 512, 512, 512},  //duty=15, dutyLt=20
   { 512, 512, 512},  //duty=16, dutyLt=20
   { 512, 512, 512},  //duty=17, dutyLt=20
   { 512, 512, 512},  //duty=18, dutyLt=20
   { 512, 512, 512},  //duty=19, dutyLt=20
   { 512, 512, 512},  //duty=20, dutyLt=20
   { 512, 512, 512},  //duty=21, dutyLt=20
   { 512, 512, 512},  //duty=22, dutyLt=20
   { 512, 512, 512},  //duty=23, dutyLt=20
   { 512, 512, 512},  //duty=24, dutyLt=20
   { 512, 512, 512},  //duty=25, dutyLt=20
   { 584, 512,2033},  //duty=-1, dutyLt=21
   { 597, 512,1948},  //duty=0, dutyLt=21
   { 605, 512,1886},  //duty=1, dutyLt=21
   { 614, 512,1826},  //duty=2, dutyLt=21
   { 624, 512,1774},  //duty=3, dutyLt=21
   { 632, 512,1726},  //duty=4, dutyLt=21
   { 640, 512,1684},  //duty=5, dutyLt=21
   { 649, 512,1645},  //duty=6, dutyLt=21
   { 661, 512,1590},  //duty=7, dutyLt=21
   { 512, 512, 512},  //duty=8, dutyLt=21
   { 512, 512, 512},  //duty=9, dutyLt=21
   { 512, 512, 512},  //duty=10, dutyLt=21
   { 512, 512, 512},  //duty=11, dutyLt=21
   { 512, 512, 512},  //duty=12, dutyLt=21
   { 512, 512, 512},  //duty=13, dutyLt=21
   { 512, 512, 512},  //duty=14, dutyLt=21
   { 512, 512, 512},  //duty=15, dutyLt=21
   { 512, 512, 512},  //duty=16, dutyLt=21
   { 512, 512, 512},  //duty=17, dutyLt=21
   { 512, 512, 512},  //duty=18, dutyLt=21
   { 512, 512, 512},  //duty=19, dutyLt=21
   { 512, 512, 512},  //duty=20, dutyLt=21
   { 512, 512, 512},  //duty=21, dutyLt=21
   { 512, 512, 512},  //duty=22, dutyLt=21
   { 512, 512, 512},  //duty=23, dutyLt=21
   { 512, 512, 512},  //duty=24, dutyLt=21
   { 512, 512, 512},  //duty=25, dutyLt=21
   { 585, 512,2027},  //duty=-1, dutyLt=22
   { 595, 512,1954},  //duty=0, dutyLt=22
   { 605, 512,1884},  //duty=1, dutyLt=22
   { 615, 512,1825},  //duty=2, dutyLt=22
   { 623, 512,1777},  //duty=3, dutyLt=22
   { 630, 512,1730},  //duty=4, dutyLt=22
   { 640, 512,1689},  //duty=5, dutyLt=22
   { 512, 512, 512},  //duty=6, dutyLt=22
   { 512, 512, 512},  //duty=7, dutyLt=22
   { 512, 512, 512},  //duty=8, dutyLt=22
   { 512, 512, 512},  //duty=9, dutyLt=22
   { 512, 512, 512},  //duty=10, dutyLt=22
   { 512, 512, 512},  //duty=11, dutyLt=22
   { 512, 512, 512},  //duty=12, dutyLt=22
   { 512, 512, 512},  //duty=13, dutyLt=22
   { 512, 512, 512},  //duty=14, dutyLt=22
   { 512, 512, 512},  //duty=15, dutyLt=22
   { 512, 512, 512},  //duty=16, dutyLt=22
   { 512, 512, 512},  //duty=17, dutyLt=22
   { 512, 512, 512},  //duty=18, dutyLt=22
   { 512, 512, 512},  //duty=19, dutyLt=22
   { 512, 512, 512},  //duty=20, dutyLt=22
   { 512, 512, 512},  //duty=21, dutyLt=22
   { 512, 512, 512},  //duty=22, dutyLt=22
   { 512, 512, 512},  //duty=23, dutyLt=22
   { 512, 512, 512},  //duty=24, dutyLt=22
   { 512, 512, 512},  //duty=25, dutyLt=22
   { 586, 512,2022},  //duty=-1, dutyLt=23
   { 595, 512,1951},  //duty=0, dutyLt=23
   { 605, 512,1885},  //duty=1, dutyLt=23
   { 614, 512,1831},  //duty=2, dutyLt=23
   { 622, 512,1782},  //duty=3, dutyLt=23
   { 512, 512, 512},  //duty=4, dutyLt=23
   { 512, 512, 512},  //duty=5, dutyLt=23
   { 512, 512, 512},  //duty=6, dutyLt=23
   { 512, 512, 512},  //duty=7, dutyLt=23
   { 512, 512, 512},  //duty=8, dutyLt=23
   { 512, 512, 512},  //duty=9, dutyLt=23
   { 512, 512, 512},  //duty=10, dutyLt=23
   { 512, 512, 512},  //duty=11, dutyLt=23
   { 512, 512, 512},  //duty=12, dutyLt=23
   { 512, 512, 512},  //duty=13, dutyLt=23
   { 512, 512, 512},  //duty=14, dutyLt=23
   { 512, 512, 512},  //duty=15, dutyLt=23
   { 512, 512, 512},  //duty=16, dutyLt=23
   { 512, 512, 512},  //duty=17, dutyLt=23
   { 512, 512, 512},  //duty=18, dutyLt=23
   { 512, 512, 512},  //duty=19, dutyLt=23
   { 512, 512, 512},  //duty=20, dutyLt=23
   { 512, 512, 512},  //duty=21, dutyLt=23
   { 512, 512, 512},  //duty=22, dutyLt=23
   { 512, 512, 512},  //duty=23, dutyLt=23
   { 512, 512, 512},  //duty=24, dutyLt=23
   { 512, 512, 512},  //duty=25, dutyLt=23
   { 588, 512,2012},  //duty=-1, dutyLt=24
   { 595, 512,1949},  //duty=0, dutyLt=24
   { 605, 512,1885},  //duty=1, dutyLt=24
   { 512, 512, 512},  //duty=2, dutyLt=24
   { 512, 512, 512},  //duty=3, dutyLt=24
   { 512, 512, 512},  //duty=4, dutyLt=24
   { 512, 512, 512},  //duty=5, dutyLt=24
   { 512, 512, 512},  //duty=6, dutyLt=24
   { 512, 512, 512},  //duty=7, dutyLt=24
   { 512, 512, 512},  //duty=8, dutyLt=24
   { 512, 512, 512},  //duty=9, dutyLt=24
   { 512, 512, 512},  //duty=10, dutyLt=24
   { 512, 512, 512},  //duty=11, dutyLt=24
   { 512, 512, 512},  //duty=12, dutyLt=24
   { 512, 512, 512},  //duty=13, dutyLt=24
   { 512, 512, 512},  //duty=14, dutyLt=24
   { 512, 512, 512},  //duty=15, dutyLt=24
   { 512, 512, 512},  //duty=16, dutyLt=24
   { 512, 512, 512},  //duty=17, dutyLt=24
   { 512, 512, 512},  //duty=18, dutyLt=24
   { 512, 512, 512},  //duty=19, dutyLt=24
   { 512, 512, 512},  //duty=20, dutyLt=24
   { 512, 512, 512},  //duty=21, dutyLt=24
   { 512, 512, 512},  //duty=22, dutyLt=24
   { 512, 512, 512},  //duty=23, dutyLt=24
   { 512, 512, 512},  //duty=24, dutyLt=24
   { 512, 512, 512},  //duty=25, dutyLt=24
   { 586, 512,2013},  //duty=-1, dutyLt=25
   { 512, 512, 512},  //duty=0, dutyLt=25
   { 512, 512, 512},  //duty=1, dutyLt=25
   { 512, 512, 512},  //duty=2, dutyLt=25
   { 512, 512, 512},  //duty=3, dutyLt=25
   { 512, 512, 512},  //duty=4, dutyLt=25
   { 512, 512, 512},  //duty=5, dutyLt=25
   { 512, 512, 512},  //duty=6, dutyLt=25
   { 512, 512, 512},  //duty=7, dutyLt=25
   { 512, 512, 512},  //duty=8, dutyLt=25
   { 512, 512, 512},  //duty=9, dutyLt=25
   { 512, 512, 512},  //duty=10, dutyLt=25
   { 512, 512, 512},  //duty=11, dutyLt=25
   { 512, 512, 512},  //duty=12, dutyLt=25
   { 512, 512, 512},  //duty=13, dutyLt=25
   { 512, 512, 512},  //duty=14, dutyLt=25
   { 512, 512, 512},  //duty=15, dutyLt=25
   { 512, 512, 512},  //duty=16, dutyLt=25
   { 512, 512, 512},  //duty=17, dutyLt=25
   { 512, 512, 512},  //duty=18, dutyLt=25
   { 512, 512, 512},  //duty=19, dutyLt=25
   { 512, 512, 512},  //duty=20, dutyLt=25
   { 512, 512, 512},  //duty=21, dutyLt=25
   { 512, 512, 512},  //duty=22, dutyLt=25
   { 512, 512, 512},  //duty=23, dutyLt=25
   { 512, 512, 512},  //duty=24, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512},  //duty=25, dutyLt=25
   { 512, 512, 512}
}}
