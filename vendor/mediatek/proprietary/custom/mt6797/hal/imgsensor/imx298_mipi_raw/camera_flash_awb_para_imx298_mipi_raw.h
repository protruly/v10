/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/

// Flash AWB tuning parameter
{
    9,  //foreground percentage
    95, //background percentage
     2, //FgPercentage_Th1 
     5, //FgPercentage_Th2
    10, //FgPercentage_Th3 
    15, //FgPercentage_Th4 
   250, //FgPercentage_Th1_Val 
   250, //FgPercentage_Th2_Val 
   250, //FgPercentage_Th3_Val 
   250, //FgPercentage_Th4_Val 
    10, //location_map_th1 
    20, //location_map_th2
    40, //location_map_th3 
    50, //location_map_th4 
   100, //location_map_val1 
   100, //location_map_val2 
   100, //location_map_val3
   100, //location_map_val4
     0, //SelfTuningFbBgWeightTbl
    100, //FgBgTbl_Y0
    100, //FgBgTbl_Y1
    100, //FgBgTbl_Y2
    100, //FgBgTbl_Y3
    100, //FgBgTbl_Y4
    100, //FgBgTbl_Y5
      5, //YPrimeWeightTh[0]
      9, //YPrimeWeightTh[1]
		 11, //YPrimeWeightTh[2]
		 13, //YPrimeWeightTh[3]
		 15, //YPrimeWeightTh[4]
			1, //YPrimeWeight[0]
			3, //YPrimeWeight[1]
			5, //YPrimeWeight[2]
			7, //YPrimeWeight[3]
	// FlashPreferenceGain
	{
		512,	// FlashPreferenceGain.i4R
		512,	// FlashPreferenceGain.i4G
		512	  // FlashPreferenceGain.i4B
	},
},

// Flash AWB calibration
{{
   { 512, 480, 512},  //duty=-1, dutyLt=-1  
   { 622, 480,2766},  //duty=0, dutyLt=-1  
   { 621, 480,2721},  //duty=1, dutyLt=-1  
   { 621, 480,2681},  //duty=2, dutyLt=-1  
   { 621, 480,2665},  //duty=3, dutyLt=-1  
   { 618, 480,2621},  //duty=4, dutyLt=-1  
   { 618, 480,2614},  //duty=5, dutyLt=-1  
   { 618, 480,2609},  //duty=6, dutyLt=-1  
   { 618, 480,2613},  //duty=7, dutyLt=-1  
   { 617, 480,2613},  //duty=8, dutyLt=-1  
   { 618, 480,2602},  //duty=9, dutyLt=-1  
   { 618, 480,2602},  //duty=10, dutyLt=-1  
   { 617, 480,2595},  //duty=11, dutyLt=-1  
   { 618, 480,2591},  //duty=12, dutyLt=-1  
   { 618, 480,2581},  //duty=13, dutyLt=-1  
   { 618, 480,2578},  //duty=14, dutyLt=-1  
   { 618, 480,2574},  //duty=15, dutyLt=-1  
   { 618, 480,2570},  //duty=16, dutyLt=-1  
   { 618, 480,2566},  //duty=17, dutyLt=-1  
   { 618, 480,2563},  //duty=18, dutyLt=-1  
   { 619, 480,2558},  //duty=19, dutyLt=-1  
   { 619, 480,2555},  //duty=20, dutyLt=-1  
   { 619, 480,2551},  //duty=21, dutyLt=-1  
   { 619, 480,2545},  //duty=22, dutyLt=-1  
   { 619, 480,2541},  //duty=23, dutyLt=-1  
   { 619, 480,2536},  //duty=24, dutyLt=-1  
   { 619, 480,2532},  //duty=25, dutyLt=-1  
   {1245, 480, 913},  //duty=-1, dutyLt=0  
   { 880, 480,1254},  //duty=0, dutyLt=0  
   { 789, 480,1466},  //duty=1, dutyLt=0  
   { 747, 480,1609},  //duty=2, dutyLt=0  
   { 732, 480,1675},  //duty=3, dutyLt=0  
   { 717, 480,1746},  //duty=4, dutyLt=0  
   { 709, 480,1786},  //duty=5, dutyLt=0  
   { 702, 480,1820},  //duty=6, dutyLt=0  
   { 697, 480,1850},  //duty=7, dutyLt=0  
   { 692, 480,1881},  //duty=8, dutyLt=0  
   { 688, 480,1906},  //duty=9, dutyLt=0  
   { 682, 480,1938},  //duty=10, dutyLt=0  
   { 678, 480,1969},  //duty=11, dutyLt=0  
   { 674, 480,2000},  //duty=12, dutyLt=0  
   { 670, 480,2022},  //duty=13, dutyLt=0  
   { 667, 480,2046},  //duty=14, dutyLt=0  
   { 665, 480,2058},  //duty=15, dutyLt=0  
   { 663, 480,2072},  //duty=16, dutyLt=0  
   { 661, 480,2089},  //duty=17, dutyLt=0  
   { 659, 480,2102},  //duty=18, dutyLt=0  
   { 658, 480,2116},  //duty=19, dutyLt=0  
   { 656, 480,2125},  //duty=20, dutyLt=0  
   { 655, 480,2135},  //duty=21, dutyLt=0  
   { 654, 480,2148},  //duty=22, dutyLt=0  
   { 652, 480,2157},  //duty=23, dutyLt=0  
   { 652, 480,2163},  //duty=24, dutyLt=0  
   { 650, 480,2168},  //duty=25, dutyLt=0  
   {1236, 480, 900},  //duty=-1, dutyLt=1  
   { 978, 480,1096},  //duty=0, dutyLt=1  
   { 879, 480,1240},  //duty=1, dutyLt=1  
   { 826, 480,1349},  //duty=2, dutyLt=1  
   { 805, 480,1403},  //duty=3, dutyLt=1  
   { 797, 480,1420},  //duty=4, dutyLt=1  
   { 786, 480,1454},  //duty=5, dutyLt=1  
   { 775, 480,1488},  //duty=6, dutyLt=1  
   { 766, 480,1516},  //duty=7, dutyLt=1  
   { 758, 480,1544},  //duty=8, dutyLt=1  
   { 752, 480,1569},  //duty=9, dutyLt=1  
   { 742, 480,1603},  //duty=10, dutyLt=1  
   { 735, 480,1635},  //duty=11, dutyLt=1  
   { 726, 480,1672},  //duty=12, dutyLt=1  
   { 721, 480,1696},  //duty=13, dutyLt=1  
   { 715, 480,1725},  //duty=14, dutyLt=1  
   { 712, 480,1739},  //duty=15, dutyLt=1  
   { 708, 480,1757},  //duty=16, dutyLt=1  
   { 704, 480,1780},  //duty=17, dutyLt=1  
   { 701, 480,1795},  //duty=18, dutyLt=1  
   { 697, 480,1813},  //duty=19, dutyLt=1  
   { 695, 480,1828},  //duty=20, dutyLt=1  
   { 692, 480,1842},  //duty=21, dutyLt=1  
   { 687, 480,1858},  //duty=22, dutyLt=1  
   { 684, 480,1863},  //duty=23, dutyLt=1  
   { 678, 480,1862},  //duty=24, dutyLt=1  
   { 669, 480,1854},  //duty=25, dutyLt=1  
   {1232, 480, 894},  //duty=-1, dutyLt=2  
   {1031, 480,1035},  //duty=0, dutyLt=2  
   { 935, 480,1143},  //duty=1, dutyLt=2  
   { 879, 480,1231},  //duty=2, dutyLt=2  
   { 855, 480,1274},  //duty=3, dutyLt=2  
   { 842, 480,1300},  //duty=4, dutyLt=2  
   { 829, 480,1330},  //duty=5, dutyLt=2  
   { 817, 480,1358},  //duty=6, dutyLt=2  
   { 807, 480,1381},  //duty=7, dutyLt=2  
   { 798, 480,1407},  //duty=8, dutyLt=2  
   { 789, 480,1432},  //duty=9, dutyLt=2  
   { 779, 480,1462},  //duty=10, dutyLt=2  
   { 770, 480,1492},  //duty=11, dutyLt=2  
   { 759, 480,1526},  //duty=12, dutyLt=2  
   { 753, 480,1550},  //duty=13, dutyLt=2  
   { 747, 480,1573},  //duty=14, dutyLt=2  
   { 743, 480,1587},  //duty=15, dutyLt=2  
   { 739, 480,1605},  //duty=16, dutyLt=2  
   { 731, 480,1632},  //duty=17, dutyLt=2  
   { 728, 480,1646},  //duty=18, dutyLt=2  
   { 723, 480,1666},  //duty=19, dutyLt=2  
   { 718, 480,1679},  //duty=20, dutyLt=2  
   { 711, 480,1684},  //duty=21, dutyLt=2  
   { 699, 480,1682},  //duty=22, dutyLt=2  
   { 686, 480,1673},  //duty=23, dutyLt=2  
   { 675, 480,1659},  //duty=24, dutyLt=2  
   { 671, 480,1648},  //duty=25, dutyLt=2  
   {1232, 480, 889},  //duty=-1, dutyLt=3  
   {1051, 480,1011},  //duty=0, dutyLt=3  
   { 959, 480,1107},  //duty=1, dutyLt=3  
   { 903, 480,1185},  //duty=2, dutyLt=3  
   { 878, 480,1225},  //duty=3, dutyLt=3  
   { 860, 480,1259},  //duty=4, dutyLt=3  
   { 846, 480,1283},  //duty=5, dutyLt=3  
   { 834, 480,1312},  //duty=6, dutyLt=3  
   { 824, 480,1337},  //duty=7, dutyLt=3  
   { 814, 480,1360},  //duty=8, dutyLt=3  
   { 805, 480,1382},  //duty=9, dutyLt=3  
   { 795, 480,1409},  //duty=10, dutyLt=3  
   { 784, 480,1440},  //duty=11, dutyLt=3  
   { 774, 480,1472},  //duty=12, dutyLt=3  
   { 766, 480,1495},  //duty=13, dutyLt=3  
   { 760, 480,1519},  //duty=14, dutyLt=3  
   { 755, 480,1536},  //duty=15, dutyLt=3  
   { 750, 480,1554},  //duty=16, dutyLt=3  
   { 743, 480,1576},  //duty=17, dutyLt=3  
   { 739, 480,1589},  //duty=18, dutyLt=3  
   { 733, 480,1604},  //duty=19, dutyLt=3  
   { 723, 480,1611},  //duty=20, dutyLt=3  
   { 713, 480,1610},  //duty=21, dutyLt=3  
   { 697, 480,1598},  //duty=22, dutyLt=3  
   { 681, 480,1585},  //duty=23, dutyLt=3  
   { 666, 480,1567},  //duty=24, dutyLt=3  
   { 653, 480,1550},  //duty=25, dutyLt=3  
   {1232, 480, 886},  //duty=-1, dutyLt=4  
   {1068, 480, 992},  //duty=0, dutyLt=4  
   { 962, 480,1097},  //duty=1, dutyLt=4  
   { 912, 480,1165},  //duty=2, dutyLt=4  
   { 893, 480,1194},  //duty=3, dutyLt=4  
   { 876, 480,1223},  //duty=4, dutyLt=4  
   { 862, 480,1249},  //duty=5, dutyLt=4  
   { 849, 480,1274},  //duty=6, dutyLt=4  
   { 839, 480,1297},  //duty=7, dutyLt=4  
   { 829, 480,1320},  //duty=8, dutyLt=4  
   { 820, 480,1340},  //duty=9, dutyLt=4  
   { 808, 480,1369},  //duty=10, dutyLt=4  
   { 799, 480,1392},  //duty=11, dutyLt=4  
   { 786, 480,1428},  //duty=12, dutyLt=4  
   { 779, 480,1450},  //duty=13, dutyLt=4  
   { 770, 480,1476},  //duty=14, dutyLt=4  
   { 766, 480,1489},  //duty=15, dutyLt=4  
   { 761, 480,1506},  //duty=16, dutyLt=4  
   { 754, 480,1526},  //duty=17, dutyLt=4  
   { 748, 480,1537},  //duty=18, dutyLt=4  
   { 737, 480,1543},  //duty=19, dutyLt=4  
   { 724, 480,1541},  //duty=20, dutyLt=4  
   { 709, 480,1534},  //duty=21, dutyLt=4  
   { 697, 480,1519},  //duty=22, dutyLt=4  
   { 673, 480,1500},  //duty=23, dutyLt=4  
   { 658, 480,1482},  //duty=24, dutyLt=4  
   { 642, 480,1462},  //duty=25, dutyLt=4  
   {1231, 480, 883},  //duty=-1, dutyLt=5  
   {1079, 480, 980},  //duty=0, dutyLt=5  
   { 976, 480,1075},  //duty=1, dutyLt=5  
   { 926, 480,1138},  //duty=2, dutyLt=5  
   { 907, 480,1167},  //duty=3, dutyLt=5  
   { 891, 480,1195},  //duty=4, dutyLt=5  
   { 876, 480,1217},  //duty=5, dutyLt=5  
   { 864, 480,1241},  //duty=6, dutyLt=5  
   { 852, 480,1264},  //duty=7, dutyLt=5  
   { 842, 480,1286},  //duty=8, dutyLt=5  
   { 833, 480,1303},  //duty=9, dutyLt=5  
   { 821, 480,1332},  //duty=10, dutyLt=5  
   { 810, 480,1358},  //duty=11, dutyLt=5  
   { 799, 480,1386},  //duty=12, dutyLt=5  
   { 791, 480,1410},  //duty=13, dutyLt=5  
   { 783, 480,1432},  //duty=14, dutyLt=5  
   { 779, 480,1444},  //duty=15, dutyLt=5  
   { 771, 480,1463},  //duty=16, dutyLt=5  
   { 761, 480,1478},  //duty=17, dutyLt=5  
   { 751, 480,1482},  //duty=18, dutyLt=5  
   { 737, 480,1481},  //duty=19, dutyLt=5  
   { 720, 480,1474},  //duty=20, dutyLt=5  
   { 705, 480,1462},  //duty=21, dutyLt=5  
   { 683, 480,1442},  //duty=22, dutyLt=5  
   { 666, 480,1425},  //duty=23, dutyLt=5  
   { 649, 480,1410},  //duty=24, dutyLt=5  
   { 632, 480,1386},  //duty=25, dutyLt=5  
   {1230, 480, 880},  //duty=-1, dutyLt=6  
   {1088, 480, 969},  //duty=0, dutyLt=6  
   { 989, 480,1059},  //duty=1, dutyLt=6  
   { 939, 480,1118},  //duty=2, dutyLt=6  
   { 921, 480,1144},  //duty=3, dutyLt=6  
   { 904, 480,1170},  //duty=4, dutyLt=6  
   { 889, 480,1190},  //duty=5, dutyLt=6  
   { 876, 480,1215},  //duty=6, dutyLt=6  
   { 865, 480,1236},  //duty=7, dutyLt=6  
   { 854, 480,1256},  //duty=8, dutyLt=6  
   { 845, 480,1274},  //duty=9, dutyLt=6  
   { 833, 480,1300},  //duty=10, dutyLt=6  
   { 822, 480,1325},  //duty=11, dutyLt=6  
   { 809, 480,1355},  //duty=12, dutyLt=6  
   { 801, 480,1375},  //duty=13, dutyLt=6  
   { 792, 480,1399},  //duty=14, dutyLt=6  
   { 786, 480,1409},  //duty=15, dutyLt=6  
   { 777, 480,1420},  //duty=16, dutyLt=6  
   { 763, 480,1426},  //duty=17, dutyLt=6  
   { 750, 480,1425},  //duty=18, dutyLt=6  
   { 731, 480,1418},  //duty=19, dutyLt=6  
   { 713, 480,1405},  //duty=20, dutyLt=6  
   { 695, 480,1392},  //duty=21, dutyLt=6  
   { 672, 480,1371},  //duty=22, dutyLt=6  
   { 656, 480,1353},  //duty=23, dutyLt=6  
   { 638, 480,1335},  //duty=24, dutyLt=6  
   { 623, 480,1315},  //duty=25, dutyLt=6  
   {1229, 480, 876},  //duty=-1, dutyLt=7  
   {1095, 480, 957},  //duty=0, dutyLt=7  
   {1000, 480,1044},  //duty=1, dutyLt=7  
   { 951, 480,1097},  //duty=2, dutyLt=7  
   { 932, 480,1124},  //duty=3, dutyLt=7  
   { 915, 480,1148},  //duty=4, dutyLt=7  
   { 901, 480,1171},  //duty=5, dutyLt=7  
   { 888, 480,1191},  //duty=6, dutyLt=7  
   { 876, 480,1211},  //duty=7, dutyLt=7  
   { 866, 480,1228},  //duty=8, dutyLt=7  
   { 856, 480,1248},  //duty=9, dutyLt=7  
   { 844, 480,1274},  //duty=10, dutyLt=7  
   { 833, 480,1293},  //duty=11, dutyLt=7  
   { 820, 480,1325},  //duty=12, dutyLt=7  
   { 811, 480,1343},  //duty=13, dutyLt=7  
   { 799, 480,1363},  //duty=14, dutyLt=7  
   { 791, 480,1370},  //duty=15, dutyLt=7  
   { 779, 480,1374},  //duty=16, dutyLt=7  
   { 760, 480,1374},  //duty=17, dutyLt=7  
   { 751, 480,1369},  //duty=18, dutyLt=7  
   { 725, 480,1357},  //duty=19, dutyLt=7  
   { 706, 480,1343},  //duty=20, dutyLt=7  
   { 687, 480,1328},  //duty=21, dutyLt=7  
   { 662, 480,1307},  //duty=22, dutyLt=7  
   { 645, 480,1288},  //duty=23, dutyLt=7  
   { 639, 480,1277},  //duty=24, dutyLt=7  
   { 623, 480,1260},  //duty=25, dutyLt=7  
   {1229, 480, 874},  //duty=-1, dutyLt=8  
   {1103, 480, 951},  //duty=0, dutyLt=8  
   {1010, 480,1030},  //duty=1, dutyLt=8  
   { 962, 480,1083},  //duty=2, dutyLt=8  
   { 943, 480,1106},  //duty=3, dutyLt=8  
   { 926, 480,1129},  //duty=4, dutyLt=8  
   { 911, 480,1147},  //duty=5, dutyLt=8  
   { 898, 480,1170},  //duty=6, dutyLt=8  
   { 887, 480,1189},  //duty=7, dutyLt=8  
   { 876, 480,1208},  //duty=8, dutyLt=8  
   { 867, 480,1224},  //duty=9, dutyLt=8  
   { 854, 480,1248},  //duty=10, dutyLt=8  
   { 843, 480,1267},  //duty=11, dutyLt=8  
   { 828, 480,1298},  //duty=12, dutyLt=8  
   { 818, 480,1312},  //duty=13, dutyLt=8  
   { 801, 480,1324},  //duty=14, dutyLt=8  
   { 792, 480,1327},  //duty=15, dutyLt=8  
   { 776, 480,1326},  //duty=16, dutyLt=8  
   { 755, 480,1320},  //duty=17, dutyLt=8  
   { 738, 480,1312},  //duty=18, dutyLt=8  
   { 721, 480,1300},  //duty=19, dutyLt=8  
   { 694, 480,1282},  //duty=20, dutyLt=8  
   { 677, 480,1270},  //duty=21, dutyLt=8  
   { 654, 480,1251},  //duty=22, dutyLt=8  
   { 634, 480,1228},  //duty=23, dutyLt=8  
   { 618, 480,1211},  //duty=24, dutyLt=8  
   { 603, 480,1196},  //duty=25, dutyLt=8  
   {1228, 480, 871},  //duty=-1, dutyLt=9  
   {1109, 480, 943},  //duty=0, dutyLt=9  
   {1018, 480,1016},  //duty=1, dutyLt=9  
   { 971, 480,1068},  //duty=2, dutyLt=9  
   { 953, 480,1091},  //duty=3, dutyLt=9  
   { 936, 480,1113},  //duty=4, dutyLt=9  
   { 921, 480,1132},  //duty=5, dutyLt=9  
   { 908, 480,1151},  //duty=6, dutyLt=9  
   { 896, 480,1166},  //duty=7, dutyLt=9  
   { 885, 480,1187},  //duty=8, dutyLt=9  
   { 876, 480,1201},  //duty=9, dutyLt=9  
   { 863, 480,1227},  //duty=10, dutyLt=9  
   { 851, 480,1248},  //duty=11, dutyLt=9  
   { 836, 480,1267},  //duty=12, dutyLt=9  
   { 821, 480,1279},  //duty=13, dutyLt=9  
   { 800, 480,1284},  //duty=14, dutyLt=9  
   { 788, 480,1283},  //duty=15, dutyLt=9  
   { 771, 480,1279},  //duty=16, dutyLt=9  
   { 746, 480,1269},  //duty=17, dutyLt=9  
   { 729, 480,1259},  //duty=18, dutyLt=9  
   { 706, 480,1244},  //duty=19, dutyLt=9  
   { 684, 480,1226},  //duty=20, dutyLt=9  
   { 666, 480,1211},  //duty=21, dutyLt=9  
   { 642, 480,1191},  //duty=22, dutyLt=9  
   { 625, 480,1176},  //duty=23, dutyLt=9  
   { 618, 480,1166},  //duty=24, dutyLt=9  
   { 596, 480,1147},  //duty=25, dutyLt=9  
   {1227, 480, 867},  //duty=-1, dutyLt=10  
   {1116, 480, 931},  //duty=0, dutyLt=10  
   {1030, 480,1004},  //duty=1, dutyLt=10  
   { 984, 480,1050},  //duty=2, dutyLt=10  
   { 966, 480,1071},  //duty=3, dutyLt=10  
   { 949, 480,1091},  //duty=4, dutyLt=10  
   { 934, 480,1109},  //duty=5, dutyLt=10  
   { 921, 480,1127},  //duty=6, dutyLt=10  
   { 910, 480,1141},  //duty=7, dutyLt=10  
   { 898, 480,1157},  //duty=8, dutyLt=10  
   { 889, 480,1176},  //duty=9, dutyLt=10  
   { 874, 480,1196},  //duty=10, dutyLt=10  
   { 859, 480,1212},  //duty=11, dutyLt=10  
   { 836, 480,1223},  //duty=12, dutyLt=10  
   { 817, 480,1225},  //duty=13, dutyLt=10  
   { 795, 480,1220},  //duty=14, dutyLt=10  
   { 778, 480,1216},  //duty=15, dutyLt=10  
   { 758, 480,1208},  //duty=16, dutyLt=10  
   { 733, 480,1196},  //duty=17, dutyLt=10  
   { 721, 480,1187},  //duty=18, dutyLt=10  
   { 691, 480,1170},  //duty=19, dutyLt=10  
   { 668, 480,1153},  //duty=20, dutyLt=10  
   { 650, 480,1137},  //duty=21, dutyLt=10  
   { 628, 480,1119},  //duty=22, dutyLt=10  
   { 611, 480,1105},  //duty=23, dutyLt=10  
   { 597, 480,1095},  //duty=24, dutyLt=10  
   { 584, 480,1080},  //duty=25, dutyLt=10  
   {1227, 480, 864},  //duty=-1, dutyLt=11  
   {1123, 480, 925},  //duty=0, dutyLt=11  
   {1040, 480, 990},  //duty=1, dutyLt=11  
   { 995, 480,1033},  //duty=2, dutyLt=11  
   { 977, 480,1049},  //duty=3, dutyLt=11  
   { 960, 480,1071},  //duty=4, dutyLt=11  
   { 946, 480,1089},  //duty=5, dutyLt=11  
   { 932, 480,1103},  //duty=6, dutyLt=11  
   { 921, 480,1122},  //duty=7, dutyLt=11  
   { 909, 480,1136},  //duty=8, dutyLt=11  
   { 898, 480,1148},  //duty=9, dutyLt=11  
   { 880, 480,1159},  //duty=10, dutyLt=11  
   { 858, 480,1169},  //duty=11, dutyLt=11  
   { 829, 480,1170},  //duty=12, dutyLt=11  
   { 807, 480,1166},  //duty=13, dutyLt=11  
   { 781, 480,1158},  //duty=14, dutyLt=11  
   { 763, 480,1152},  //duty=15, dutyLt=11  
   { 742, 480,1141},  //duty=16, dutyLt=11  
   { 715, 480,1126},  //duty=17, dutyLt=11  
   { 696, 480,1113},  //duty=18, dutyLt=11  
   { 678, 480,1101},  //duty=19, dutyLt=11  
   { 652, 480,1085},  //duty=20, dutyLt=11  
   { 635, 480,1071},  //duty=21, dutyLt=11  
   { 612, 480,1054},  //duty=22, dutyLt=11  
   { 600, 480,1048},  //duty=23, dutyLt=11  
   { 583, 480,1030},  //duty=24, dutyLt=11  
   { 571, 480,1019},  //duty=25, dutyLt=11  
   {1226, 480, 859},  //duty=-1, dutyLt=12  
   {1129, 480, 912},  //duty=0, dutyLt=12  
   {1051, 480, 974},  //duty=1, dutyLt=12  
   {1008, 480,1015},  //duty=2, dutyLt=12  
   { 989, 480,1029},  //duty=3, dutyLt=12  
   { 974, 480,1050},  //duty=4, dutyLt=12  
   { 959, 480,1066},  //duty=5, dutyLt=12  
   { 945, 480,1077},  //duty=6, dutyLt=12  
   { 930, 480,1091},  //duty=7, dutyLt=12  
   { 913, 480,1100},  //duty=8, dutyLt=12  
   { 897, 480,1105},  //duty=9, dutyLt=12  
   { 874, 480,1107},  //duty=10, dutyLt=12  
   { 845, 480,1106},  //duty=11, dutyLt=12  
   { 810, 480,1099},  //duty=12, dutyLt=12  
   { 787, 480,1092},  //duty=13, dutyLt=12  
   { 756, 480,1080},  //duty=14, dutyLt=12  
   { 741, 480,1072},  //duty=15, dutyLt=12  
   { 720, 480,1061},  //duty=16, dutyLt=12  
   { 692, 480,1045},  //duty=17, dutyLt=12  
   { 676, 480,1037},  //duty=18, dutyLt=12  
   { 653, 480,1021},  //duty=19, dutyLt=12  
   { 634, 480,1009},  //duty=20, dutyLt=12  
   { 618, 480, 998},  //duty=21, dutyLt=12  
   { 596, 480, 982},  //duty=22, dutyLt=12  
   { 582, 480, 973},  //duty=23, dutyLt=12  
   { 568, 480, 961},  //duty=24, dutyLt=12  
   { 558, 480, 952},  //duty=25, dutyLt=12  
   {1225, 480, 855},  //duty=-1, dutyLt=13  
   {1134, 480, 909},  //duty=0, dutyLt=13  
   {1058, 480, 965},  //duty=1, dutyLt=13  
   {1016, 480,1002},  //duty=2, dutyLt=13  
   { 998, 480,1019},  //duty=3, dutyLt=13  
   { 981, 480,1031},  //duty=4, dutyLt=13  
   { 964, 480,1044},  //duty=5, dutyLt=13  
   { 946, 480,1056},  //duty=6, dutyLt=13  
   { 927, 480,1062},  //duty=7, dutyLt=13  
   { 909, 480,1065},  //duty=8, dutyLt=13  
   { 891, 480,1066},  //duty=9, dutyLt=13  
   { 863, 480,1065},  //duty=10, dutyLt=13  
   { 830, 480,1059},  //duty=11, dutyLt=13  
   { 795, 480,1049},  //duty=12, dutyLt=13  
   { 770, 480,1041},  //duty=13, dutyLt=13  
   { 738, 480,1026},  //duty=14, dutyLt=13  
   { 723, 480,1019},  //duty=15, dutyLt=13  
   { 703, 480,1008},  //duty=16, dutyLt=13  
   { 677, 480, 994},  //duty=17, dutyLt=13  
   { 660, 480, 985},  //duty=18, dutyLt=13  
   { 640, 480, 973},  //duty=19, dutyLt=13  
   { 620, 480, 960},  //duty=20, dutyLt=13  
   { 612, 480, 954},  //duty=21, dutyLt=13  
   { 585, 480, 937},  //duty=22, dutyLt=13  
   { 572, 480, 928},  //duty=23, dutyLt=13  
   { 560, 480, 920},  //duty=24, dutyLt=13  
   { 549, 480, 909},  //duty=25, dutyLt=13  
   {1225, 480, 852},  //duty=-1, dutyLt=14  
   {1139, 480, 901},  //duty=0, dutyLt=14  
   {1066, 480, 952},  //duty=1, dutyLt=14  
   {1023, 480, 986},  //duty=2, dutyLt=14  
   {1002, 480, 998},  //duty=3, dutyLt=14  
   { 980, 480,1007},  //duty=4, dutyLt=14  
   { 958, 480,1013},  //duty=5, dutyLt=14  
   { 936, 480,1016},  //duty=6, dutyLt=14  
   { 913, 480,1017},  //duty=7, dutyLt=14  
   { 894, 480,1016},  //duty=8, dutyLt=14  
   { 869, 480,1013},  //duty=9, dutyLt=14  
   { 842, 480,1007},  //duty=10, dutyLt=14  
   { 808, 480, 999},  //duty=11, dutyLt=14  
   { 776, 480, 988},  //duty=12, dutyLt=14  
   { 752, 480, 978},  //duty=13, dutyLt=14  
   { 716, 480, 962},  //duty=14, dutyLt=14  
   { 701, 480, 956},  //duty=15, dutyLt=14  
   { 681, 480, 946},  //duty=16, dutyLt=14  
   { 657, 480, 934},  //duty=17, dutyLt=14  
   { 642, 480, 925},  //duty=18, dutyLt=14  
   { 629, 480, 918},  //duty=19, dutyLt=14  
   { 605, 480, 904},  //duty=20, dutyLt=14  
   { 589, 480, 894},  //duty=21, dutyLt=14  
   { 571, 480, 883},  //duty=22, dutyLt=14  
   { 559, 480, 875},  //duty=23, dutyLt=14  
   { 548, 480, 866},  //duty=24, dutyLt=14  
   { 540, 480, 858},  //duty=25, dutyLt=14  
   {1224, 480, 849},  //duty=-1, dutyLt=15  
   {1141, 480, 893},  //duty=0, dutyLt=15  
   {1070, 480, 947},  //duty=1, dutyLt=15  
   {1023, 480, 975},  //duty=2, dutyLt=15  
   { 999, 480, 984},  //duty=3, dutyLt=15  
   { 977, 480, 989},  //duty=4, dutyLt=15  
   { 954, 480, 992},  //duty=5, dutyLt=15  
   { 927, 480, 993},  //duty=6, dutyLt=15  
   { 903, 480, 992},  //duty=7, dutyLt=15  
   { 879, 480, 989},  //duty=8, dutyLt=15  
   { 858, 480, 986},  //duty=9, dutyLt=15  
   { 825, 480, 978},  //duty=10, dutyLt=15  
   { 794, 480, 967},  //duty=11, dutyLt=15  
   { 759, 480, 956},  //duty=12, dutyLt=15  
   { 735, 480, 946},  //duty=13, dutyLt=15  
   { 704, 480, 933},  //duty=14, dutyLt=15  
   { 697, 480, 931},  //duty=15, dutyLt=15  
   { 671, 480, 918},  //duty=16, dutyLt=15  
   { 649, 480, 906},  //duty=17, dutyLt=15  
   { 633, 480, 898},  //duty=18, dutyLt=15  
   { 614, 480, 888},  //duty=19, dutyLt=15  
   { 597, 480, 878},  //duty=20, dutyLt=15  
   { 590, 480, 874},  //duty=21, dutyLt=15  
   { 565, 480, 858},  //duty=22, dutyLt=15  
   { 554, 480, 851},  //duty=23, dutyLt=15  
   { 550, 480, 853},  //duty=24, dutyLt=15  
   { 537, 480, 836},  //duty=25, dutyLt=15  
   {1224, 480, 846},  //duty=-1, dutyLt=16  
   {1144, 480, 892},  //duty=0, dutyLt=16  
   {1071, 480, 933},  //duty=1, dutyLt=16  
   {1018, 480, 956},  //duty=2, dutyLt=16  
   { 993, 480, 959},  //duty=3, dutyLt=16  
   { 963, 480, 962},  //duty=4, dutyLt=16  
   { 941, 480, 963},  //duty=5, dutyLt=16  
   { 911, 480, 960},  //duty=6, dutyLt=16  
   { 886, 480, 957},  //duty=7, dutyLt=16  
   { 860, 480, 950},  //duty=8, dutyLt=16  
   { 838, 480, 946},  //duty=9, dutyLt=16  
   { 807, 480, 937},  //duty=10, dutyLt=16  
   { 782, 480, 930},  //duty=11, dutyLt=16  
   { 741, 480, 914},  //duty=12, dutyLt=16  
   { 718, 480, 905},  //duty=13, dutyLt=16  
   { 694, 480, 901},  //duty=14, dutyLt=16  
   { 676, 480, 888},  //duty=15, dutyLt=16  
   { 658, 480, 880},  //duty=16, dutyLt=16  
   { 636, 480, 869},  //duty=17, dutyLt=16  
   { 621, 480, 863},  //duty=18, dutyLt=16  
   { 604, 480, 854},  //duty=19, dutyLt=16  
   { 586, 480, 844},  //duty=20, dutyLt=16  
   { 573, 480, 836},  //duty=21, dutyLt=16  
   { 557, 480, 826},  //duty=22, dutyLt=16  
   { 551, 480, 826},  //duty=23, dutyLt=16  
   { 538, 480, 811},  //duty=24, dutyLt=16  
   { 537, 480, 809},  //duty=25, dutyLt=16  
   {1223, 480, 844},  //duty=-1, dutyLt=17  
   {1146, 480, 885},  //duty=0, dutyLt=17  
   {1063, 480, 916},  //duty=1, dutyLt=17  
   {1000, 480, 924},  //duty=2, dutyLt=17  
   { 971, 480, 924},  //duty=3, dutyLt=17  
   { 939, 480, 920},  //duty=4, dutyLt=17  
   { 912, 480, 917},  //duty=5, dutyLt=17  
   { 885, 480, 913},  //duty=6, dutyLt=17  
   { 860, 480, 908},  //duty=7, dutyLt=17  
   { 835, 480, 902},  //duty=8, dutyLt=17  
   { 813, 480, 896},  //duty=9, dutyLt=17  
   { 782, 480, 887},  //duty=10, dutyLt=17  
   { 754, 480, 878},  //duty=11, dutyLt=17  
   { 719, 480, 865},  //duty=12, dutyLt=17  
   { 697, 480, 858},  //duty=13, dutyLt=17  
   { 671, 480, 848},  //duty=14, dutyLt=17  
   { 659, 480, 843},  //duty=15, dutyLt=17  
   { 641, 480, 835},  //duty=16, dutyLt=17  
   { 620, 480, 826},  //duty=17, dutyLt=17  
   { 607, 480, 821},  //duty=18, dutyLt=17  
   { 588, 480, 811},  //duty=19, dutyLt=17  
   { 573, 480, 803},  //duty=20, dutyLt=17  
   { 561, 480, 797},  //duty=21, dutyLt=17  
   { 546, 480, 786},  //duty=22, dutyLt=17  
   { 539, 480, 780},  //duty=23, dutyLt=17  
   { 531, 480, 773},  //duty=24, dutyLt=17  
   { 512, 480, 512},  //duty=25, dutyLt=17  
   {1222, 480, 840},  //duty=-1, dutyLt=18  
   {1143, 480, 876},  //duty=0, dutyLt=18  
   {1051, 480, 895},  //duty=1, dutyLt=18  
   { 983, 480, 897},  //duty=2, dutyLt=18  
   { 951, 480, 894},  //duty=3, dutyLt=18  
   { 927, 480, 893},  //duty=4, dutyLt=18  
   { 893, 480, 885},  //duty=5, dutyLt=18  
   { 865, 480, 879},  //duty=6, dutyLt=18  
   { 841, 480, 874},  //duty=7, dutyLt=18  
   { 815, 480, 867},  //duty=8, dutyLt=18  
   { 794, 480, 862},  //duty=9, dutyLt=18  
   { 764, 480, 853},  //duty=10, dutyLt=18  
   { 743, 480, 848},  //duty=11, dutyLt=18  
   { 705, 480, 834},  //duty=12, dutyLt=18  
   { 683, 480, 826},  //duty=13, dutyLt=18  
   { 660, 480, 820},  //duty=14, dutyLt=18  
   { 646, 480, 812},  //duty=15, dutyLt=18  
   { 631, 480, 806},  //duty=16, dutyLt=18  
   { 611, 480, 798},  //duty=17, dutyLt=18  
   { 596, 480, 791},  //duty=18, dutyLt=18  
   { 580, 480, 784},  //duty=19, dutyLt=18  
   { 569, 480, 783},  //duty=20, dutyLt=18  
   { 554, 480, 769},  //duty=21, dutyLt=18  
   { 540, 480, 760},  //duty=22, dutyLt=18  
   { 533, 480, 754},  //duty=23, dutyLt=18  
   { 512, 480, 512},  //duty=24, dutyLt=18  
   { 512, 480, 512},  //duty=25, dutyLt=18  
   {1218, 480, 834},  //duty=-1, dutyLt=19  
   {1133, 480, 857},  //duty=0, dutyLt=19  
   {1027, 480, 865},  //duty=1, dutyLt=19  
   { 955, 480, 859},  //duty=2, dutyLt=19  
   { 930, 480, 857},  //duty=3, dutyLt=19  
   { 892, 480, 848},  //duty=4, dutyLt=19  
   { 865, 480, 843},  //duty=5, dutyLt=19  
   { 838, 480, 837},  //duty=6, dutyLt=19  
   { 814, 480, 831},  //duty=7, dutyLt=19  
   { 790, 480, 825},  //duty=8, dutyLt=19  
   { 771, 480, 821},  //duty=9, dutyLt=19  
   { 742, 480, 811},  //duty=10, dutyLt=19  
   { 716, 480, 804},  //duty=11, dutyLt=19  
   { 693, 480, 799},  //duty=12, dutyLt=19  
   { 667, 480, 789},  //duty=13, dutyLt=19  
   { 641, 480, 779},  //duty=14, dutyLt=19  
   { 634, 480, 780},  //duty=15, dutyLt=19  
   { 615, 480, 769},  //duty=16, dutyLt=19  
   { 596, 480, 763},  //duty=17, dutyLt=19  
   { 584, 480, 757},  //duty=18, dutyLt=19  
   { 570, 480, 753},  //duty=19, dutyLt=19  
   { 555, 480, 743},  //duty=20, dutyLt=19  
   { 545, 480, 737},  //duty=21, dutyLt=19  
   { 539, 480, 734},  //duty=22, dutyLt=19  
   { 528, 480, 723},  //duty=23, dutyLt=19  
   { 512, 480, 512},  //duty=24, dutyLt=19  
   { 512, 480, 512},  //duty=25, dutyLt=19  
   {1203, 480, 823},  //duty=-1, dutyLt=20  
   {1106, 480, 835},  //duty=0, dutyLt=20  
   { 999, 480, 831},  //duty=1, dutyLt=20  
   { 925, 480, 821},  //duty=2, dutyLt=20  
   { 894, 480, 816},  //duty=3, dutyLt=20  
   { 864, 480, 809},  //duty=4, dutyLt=20  
   { 838, 480, 805},  //duty=5, dutyLt=20  
   { 813, 480, 799},  //duty=6, dutyLt=20  
   { 790, 480, 793},  //duty=7, dutyLt=20  
   { 769, 480, 789},  //duty=8, dutyLt=20  
   { 749, 480, 783},  //duty=9, dutyLt=20  
   { 724, 480, 777},  //duty=10, dutyLt=20  
   { 704, 480, 772},  //duty=11, dutyLt=20  
   { 671, 480, 761},  //duty=12, dutyLt=20  
   { 651, 480, 755},  //duty=13, dutyLt=20  
   { 627, 480, 747},  //duty=14, dutyLt=20  
   { 616, 480, 743},  //duty=15, dutyLt=20  
   { 602, 480, 738},  //duty=16, dutyLt=20  
   { 585, 480, 731},  //duty=17, dutyLt=20  
   { 572, 480, 726},  //duty=18, dutyLt=20  
   { 557, 480, 719},  //duty=19, dutyLt=20  
   { 546, 480, 714},  //duty=20, dutyLt=20  
   { 538, 480, 708},  //duty=21, dutyLt=20  
   { 512, 480, 512},  //duty=22, dutyLt=20  
   { 512, 480, 512},  //duty=23, dutyLt=20  
   { 512, 480, 512},  //duty=24, dutyLt=20  
   { 512, 480, 512},  //duty=25, dutyLt=20  
   {1180, 480, 804},  //duty=-1, dutyLt=21  
   {1077, 480, 807},  //duty=0, dutyLt=21  
   { 968, 480, 797},  //duty=1, dutyLt=21  
   { 898, 480, 786},  //duty=2, dutyLt=21  
   { 866, 480, 780},  //duty=3, dutyLt=21  
   { 839, 480, 775},  //duty=4, dutyLt=21  
   { 813, 480, 769},  //duty=5, dutyLt=21  
   { 799, 480, 769},  //duty=6, dutyLt=21  
   { 769, 480, 760},  //duty=7, dutyLt=21  
   { 748, 480, 755},  //duty=8, dutyLt=21  
   { 738, 480, 755},  //duty=9, dutyLt=21  
   { 704, 480, 743},  //duty=10, dutyLt=21  
   { 681, 480, 737},  //duty=11, dutyLt=21  
   { 654, 480, 729},  //duty=12, dutyLt=21  
   { 637, 480, 724},  //duty=13, dutyLt=21  
   { 614, 480, 717},  //duty=14, dutyLt=21  
   { 604, 480, 714},  //duty=15, dutyLt=21  
   { 590, 480, 709},  //duty=16, dutyLt=21  
   { 572, 480, 702},  //duty=17, dutyLt=21  
   { 560, 480, 697},  //duty=18, dutyLt=21  
   { 553, 480, 698},  //duty=19, dutyLt=21  
   { 538, 480, 686},  //duty=20, dutyLt=21  
   { 531, 480, 681},  //duty=21, dutyLt=21  
   { 512, 480, 512},  //duty=22, dutyLt=21  
   { 512, 480, 512},  //duty=23, dutyLt=21  
   { 512, 480, 512},  //duty=24, dutyLt=21  
   { 512, 480, 512},  //duty=25, dutyLt=21  
   {1143, 480, 775},  //duty=-1, dutyLt=22  
   {1035, 480, 770},  //duty=0, dutyLt=22  
   { 932, 480, 758},  //duty=1, dutyLt=22  
   { 867, 480, 749},  //duty=2, dutyLt=22  
   { 836, 480, 741},  //duty=3, dutyLt=22  
   { 810, 480, 736},  //duty=4, dutyLt=22  
   { 787, 480, 732},  //duty=5, dutyLt=22  
   { 764, 480, 727},  //duty=6, dutyLt=22  
   { 746, 480, 724},  //duty=7, dutyLt=22  
   { 734, 480, 724},  //duty=8, dutyLt=22  
   { 717, 480, 720},  //duty=9, dutyLt=22  
   { 684, 480, 709},  //duty=10, dutyLt=22  
   { 667, 480, 707},  //duty=11, dutyLt=22  
   { 636, 480, 695},  //duty=12, dutyLt=22  
   { 619, 480, 691},  //duty=13, dutyLt=22  
   { 597, 480, 684},  //duty=14, dutyLt=22  
   { 588, 480, 681},  //duty=15, dutyLt=22  
   { 574, 480, 676},  //duty=16, dutyLt=22  
   { 562, 480, 674},  //duty=17, dutyLt=22  
   { 550, 480, 667},  //duty=18, dutyLt=22  
   { 539, 480, 661},  //duty=19, dutyLt=22  
   { 512, 480, 512},  //duty=20, dutyLt=22  
   { 512, 480, 512},  //duty=21, dutyLt=22  
   { 512, 480, 512},  //duty=22, dutyLt=22  
   { 512, 480, 512},  //duty=23, dutyLt=22  
   { 512, 480, 512},  //duty=24, dutyLt=22  
   { 512, 480, 512},  //duty=25, dutyLt=22  
   {1108, 480, 750},  //duty=-1, dutyLt=23  
   {1004, 480, 742},  //duty=0, dutyLt=23  
   { 903, 480, 729},  //duty=1, dutyLt=23  
   { 840, 480, 718},  //duty=2, dutyLt=23  
   { 813, 480, 713},  //duty=3, dutyLt=23  
   { 787, 480, 708},  //duty=4, dutyLt=23  
   { 767, 480, 706},  //duty=5, dutyLt=23  
   { 744, 480, 700},  //duty=6, dutyLt=23  
   { 727, 480, 697},  //duty=7, dutyLt=23  
   { 708, 480, 693},  //duty=8, dutyLt=23  
   { 693, 480, 689},  //duty=9, dutyLt=23  
   { 670, 480, 684},  //duty=10, dutyLt=23  
   { 656, 480, 683},  //duty=11, dutyLt=23  
   { 623, 480, 671},  //duty=12, dutyLt=23  
   { 608, 480, 667},  //duty=13, dutyLt=23  
   { 588, 480, 662},  //duty=14, dutyLt=23  
   { 578, 480, 658},  //duty=15, dutyLt=23  
   { 572, 480, 659},  //duty=16, dutyLt=23  
   { 551, 480, 648},  //duty=17, dutyLt=23  
   { 542, 480, 644},  //duty=18, dutyLt=23  
   { 533, 480, 640},  //duty=19, dutyLt=23  
   { 512, 480, 512},  //duty=20, dutyLt=23  
   { 512, 480, 512},  //duty=21, dutyLt=23  
   { 512, 480, 512},  //duty=22, dutyLt=23  
   { 512, 480, 512},  //duty=23, dutyLt=23  
   { 512, 480, 512},  //duty=24, dutyLt=23  
   { 512, 480, 512},  //duty=25, dutyLt=23  
   {1077, 480, 726},  //duty=-1, dutyLt=24  
   { 976, 480, 717},  //duty=0, dutyLt=24  
   { 879, 480, 704},  //duty=1, dutyLt=24  
   { 820, 480, 694},  //duty=2, dutyLt=24  
   { 795, 480, 690},  //duty=3, dutyLt=24  
   { 781, 480, 691},  //duty=4, dutyLt=24  
   { 752, 480, 685},  //duty=5, dutyLt=24  
   { 730, 480, 678},  //duty=6, dutyLt=24  
   { 710, 480, 673},  //duty=7, dutyLt=24  
   { 693, 480, 669},  //duty=8, dutyLt=24  
   { 678, 480, 666},  //duty=9, dutyLt=24  
   { 656, 480, 661},  //duty=10, dutyLt=24  
   { 635, 480, 655},  //duty=11, dutyLt=24  
   { 613, 480, 652},  //duty=12, dutyLt=24  
   { 596, 480, 646},  //duty=13, dutyLt=24  
   { 577, 480, 640},  //duty=14, dutyLt=24  
   { 567, 480, 636},  //duty=15, dutyLt=24  
   { 556, 480, 633},  //duty=16, dutyLt=24  
   { 543, 480, 627},  //duty=17, dutyLt=24  
   { 512, 480, 512},  //duty=18, dutyLt=24  
   { 512, 480, 512},  //duty=19, dutyLt=24  
   { 512, 480, 512},  //duty=20, dutyLt=24  
   { 512, 480, 512},  //duty=21, dutyLt=24  
   { 512, 480, 512},  //duty=22, dutyLt=24  
   { 512, 480, 512},  //duty=23, dutyLt=24  
   { 512, 480, 512},  //duty=24, dutyLt=24  
   { 512, 480, 512},  //duty=25, dutyLt=24  
   {1045, 480, 702},  //duty=-1, dutyLt=25  
   { 948, 480, 692},  //duty=0, dutyLt=25  
   { 854, 480, 679},  //duty=1, dutyLt=25  
   { 798, 480, 670},  //duty=2, dutyLt=25  
   { 788, 480, 674},  //duty=3, dutyLt=25  
   { 751, 480, 662},  //duty=4, dutyLt=25  
   { 732, 480, 660},  //duty=5, dutyLt=25  
   { 714, 480, 657},  //duty=6, dutyLt=25  
   { 694, 480, 651},  //duty=7, dutyLt=25  
   { 679, 480, 648},  //duty=8, dutyLt=25  
   { 667, 480, 649},  //duty=9, dutyLt=25  
   { 643, 480, 640},  //duty=10, dutyLt=25  
   { 624, 480, 636},  //duty=11, dutyLt=25  
   { 600, 480, 629},  //duty=12, dutyLt=25  
   { 585, 480, 626},  //duty=13, dutyLt=25  
   { 568, 480, 621},  //duty=14, dutyLt=25  
   { 559, 480, 618},  //duty=15, dutyLt=25  
   { 548, 480, 613},  //duty=16, dutyLt=25  
   { 512, 512, 512},  //duty=17, dutyLt=25  
   { 512, 512, 512},  //duty=18, dutyLt=25  
   { 512, 512, 512},  //duty=19, dutyLt=25  
   { 512, 512, 512},  //duty=20, dutyLt=25  
   { 512, 512, 512},  //duty=21, dutyLt=25  
   { 512, 512, 512},  //duty=22, dutyLt=25  
   { 512, 512, 512},  //duty=23, dutyLt=25  
   { 512, 512, 512},  //duty=24, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512},  //duty=25, dutyLt=25  
   { 512, 512, 512}
}}


