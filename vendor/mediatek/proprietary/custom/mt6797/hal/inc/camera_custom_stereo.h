/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
#ifndef _CAMERA_CUSTOM_STEREO_IF_
#define _CAMERA_CUSTOM_STEREO_IF_

#include "camera_custom_types.h"
#include "common/include/UITypes.h" //For MSize
#include <vector>
#include <map>

/*******************************************************************************
* Return enable/disable flag of STEREO to ISP
*******************************************************************************/
enum ENUM_STEREO_SENSOR_PROFILE
{
    STEREO_SENSOR_PROFILE_UNKNOWN,
    STEREO_SENSOR_PROFILE_REAR_REAR,
    STEREO_SENSOR_PROFILE_FRONT_FRONT,
    STEREO_SENSOR_PROFILE_REAR_FRONT
};

enum ENUM_STEREO_SENSOR_RELATIVE_POSITION
{
    //Position definition: let phone screen face you and hold it vertically
    STEREO_SENSOR_REAR_MAIN_TOP    = 0,
    STEREO_SENSOR_REAR_MAIN_BOTTOM = 1,
    STEREO_SENSOR_FRONT_MAIN_RIGHT = 0,
    STEREO_SENSOR_FRONT_MAIN_LEFT  = 1
};

enum ENUM_ROTATION
{
    //Rotation is defined as clock-wised
    eRotate_0   = 0,
    eRotate_90  = 90,
    eRotate_180 = 180,
    eRotate_270 = 270
};

enum ENUN_DEPTHMAP_SIZE
{
    STEREO_DEPTHMAP_1X = 1, //240x136
    STEREO_DEPTHMAP_2X = 2, //480x272
    STEREO_DEPTHMAP_4X = 4  //960x544
};

enum ENUM_STEREO_CAM_SCENARIO
{
    eStereoCamPreview,
    eStereoCamRecord,
    eStereoCamCapture
};

enum ENUM_STEREO_CAM_FEATURE
{
    eStereoCamVsdof,
    eStereoCamStereo_Capture,
    eStereoBMDnoise,
};

struct BMDeNoiseISODependentParam
{
    int ISO            = 1;
    int BW_SingleRange = 1;
    int BW_OccRange    = 1;
    int BW_Range       = 1;
    int BW_Kernel      = 4;
    int B_Range        = 1;
    int B_Kernel       = 1;
    int W_Range        = 1;
    int W_Kernel       = 1;
    int VSL            = 2598;
    int VOFT           = 28635;
    int VGAIN          = 8192;

    BMDeNoiseISODependentParam(
        int iso,
        int bw_SingleRange, int bw_OccRange, int bw_Range, int bw_Kernel,
        int b_Range, int b_Kernel, int w_Range, int w_Kernel,
        int vsl, int voft, int vgain
    )
    : ISO(iso)
    , BW_SingleRange(bw_SingleRange)
    , BW_OccRange(bw_OccRange)
    , BW_Range(bw_Range)
    , BW_Kernel(bw_Kernel)
    , B_Range(b_Range)
    , B_Kernel(b_Kernel)
    , W_Range(w_Range)
    , W_Kernel(w_Kernel)
    , VSL(vsl)
    , VOFT(voft)
    , VGAIN(vgain)
    {}
};

struct BMDeNoiseSensorDependentParam
{
    float v_scale = 1.2685f;
    float v_offset = 55.927f;
};

struct BMDeNoiseQualityPerformanceParam
{
    int dsH = 4;
    int dsV = 4;
    int FPREPROC = 0;
    int FSSTEP = 4;
};

enum DeNoiseMode
{
    eDeNoiseMode_NONE = 0,
    eDeNoiseMode_BM,
    eDeNoiseMode_BM_MNR,
    eDeNoiseMode_BM_SWNR,
};

struct SensorFOV
{
    SensorFOV() {
        fov_horizontal = 0.0f;
        fov_vertical   = 0.0f;
    }

    SensorFOV(float h, float v)
        : fov_horizontal(h)
        , fov_vertical(v)
    {
    }

    float fov_horizontal;
    float fov_vertical;
};

/**
 * Ratio of width:height
 */
enum ENUM_STEREO_RATIO
{
    eRatio_Unknown
    , eRatio_16_9   // 16:9, default 1920x1080
    , eRatio_4_3    //  4:3, default 1600x1200
    , eRatio_Default = eRatio_16_9
    , eRatio_Sensor  = eRatio_4_3
};

typedef std::vector<NSCam::MSize> SIZE_LIST_T;
typedef std::map<ENUM_STEREO_RATIO, SIZE_LIST_T> CAPTURE_SIZE_T;

extern const CAPTURE_SIZE_T CAPTURE_SIZES;

//This order must align sensors' id
extern const std::vector<SensorFOV> FOV_LIST;

//This order must align sensors' id
extern const std::vector<SensorFOV> TARGET_FOV_LIST;

//This order must align sensors' id
extern const std::vector<ENUM_ROTATION> MODULE_ROTATION;

extern const float LENS_INFO_REAR[];
extern const float LENS_INFO_FRONT[];

extern const ENUN_DEPTHMAP_SIZE CUSTOM_DEPTHMAP_SIZE;

//TODO: query from sensor instead of hard-coding
extern const std::vector<bool> SENSOR_AF;

//This order must align ENUM_STEREO_SENSOR_PROFILE
extern const std::vector<bool> ENABLE_LDC;

float getStereoBaseline(ENUM_STEREO_SENSOR_PROFILE profile);

bool getStereoSensorID(ENUM_STEREO_SENSOR_PROFILE profile, int &main1Id, int &main2Id);

ENUM_STEREO_SENSOR_RELATIVE_POSITION getSensorRelation(ENUM_STEREO_SENSOR_PROFILE stereoProfile);


/*******************************************************************************
* Stereo ZSD cap buffer size
*******************************************************************************/
int   get_stereo_zsd_cap_stored_frame_cnt(void);

/*******************************************************************************
* Stereo BM-DeNoise mode
*******************************************************************************/
DeNoiseMode getBMDeNoiseMode(int ISO);

int   get_stereo_bmdenoise_zsd_cap_stored_frame_cnt(void);

int   get_stereo_bmdenoise_capture_buffer_cnt(void);

/*******************************************************************************
* Stereo BM-DeNoise tuning params
*******************************************************************************/
BMDeNoiseISODependentParam getBMDeNoiseISODePendentParams(int ISO);

BMDeNoiseSensorDependentParam getBMDeNoiseSensorDependentParam();

BMDeNoiseQualityPerformanceParam getBMDeNoiseQualityPerformanceParam();
/*******************************************************************************
* Stereo scenario control for cpu and frequency usage
*******************************************************************************/
std::vector<int>
get_stereo_cam_cpu_num(
    ENUM_STEREO_CAM_FEATURE featureId,
    ENUM_STEREO_CAM_SCENARIO scenario
);

bool get_stereo_cam_cpu_frequency(
    ENUM_STEREO_CAM_FEATURE featureId,
    ENUM_STEREO_CAM_SCENARIO scenario,
    std::vector<int>& min,
    std::vector<int>& max
);

enum ENUM_DENOISE_MODE
{
    E_DENOISE_MODE_NORMAL,
    E_DENOISE_MODE_PERFORMANCE
};

extern const ENUM_DENOISE_MODE DENOISE_MODE;

#endif  //  _CAMERA_CUSTOM_STEREO_IF_