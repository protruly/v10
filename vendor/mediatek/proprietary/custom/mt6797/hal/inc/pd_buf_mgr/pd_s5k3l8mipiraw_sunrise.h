#include <utils/Log.h>
#include <fcntl.h>
#include <math.h>

#include "pd_buf_mgr.h"



class PD_S5K3L8MIPIRAW_SUNRISE : protected PDBufMgr
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Ctor/Dtor.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
private:    ////    Disallowed.
	MUINT32  m_PDBufSz;
	MUINT16 *m_PDBuf;
    MUINT32  m_PDXSz; //pixels in two byte.
    MUINT32  m_PDYSz; //lines

protected :
	/**
	* @brief checking current sensor is supported or not. 
	*/
    MBOOL IsSupport( SPDProfile_t &iPdProfile); 
    void seprate( int stride, unsigned char *ptr, int pd_x_num, int pd_y_num, unsigned short *ptrLROut);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:
    PD_S5K3L8MIPIRAW_SUNRISE();
    ~PD_S5K3L8MIPIRAW_SUNRISE();

	static PDBufMgr* getInstance();

	

 	/**
	* @brief get PD calibration data size.
	*/
	MINT32 GetPDCalSz();
	/**
	* @brief convert PD data buffer format.
	*/
    MUINT16* ConvertPDBufFormat( MUINT32 i4Size, MUINT32 i4Stride, MUINT8 *ptrBufAddr, MUINT32 i4FrmCnt);
    MBOOL GetPDOPDdataInfo(MUINT32 &PDXsz, MUINT32 &PDYsz, MUINT32 &PDBufSz);
    MBOOL GetPDOHWInfo( MINT32 i4CurSensorMode, SPDOHWINFO_T &oPDOhwInfo);
};


