/*
	Here is an interface of connection to hide implementation. 
	A general connection gives an informal program two models to communicate to a formal context by events. 
	For synchronous communications, a program can invoke send() and recv() to transceive events. 
	On the other hand, asynchronous communications can catch unsolicited events by another receipt handler. 
	It is a common for send(), but the recv() is prior to the receipt handler when a mixed use. 

	The init function initializes the connection with adaptive communication interfaces. 
	The communication interfaces for send(), recv(), and the receipt handler can be individaul. 
	The exit function uninitializes the connection. 
	The send function is an abstract interface to send one event statelessly. 
	The recv function is an abstract interface to conditionally receive one event statelessly. 

	An ATI connection applies one-to-one communications through mailboxes. 
	Thus, a dedicated mailbox is sufficient to send and receive events with a context due to a single thread. 
	Conditional receipt are disabled due to unnecessity. 
	Asynchronous communications are disabled due to unnecessity. 
*/

#ifndef __CONN_H__
#define __CONN_H__

#include "sys_info.h"
#include "compiler.h"
#include "context.h"
#include "event.h"
#include "io/mailbox.h"
#include <stddef.h>

#include "context/task.h"
#include "io/chnl.h"
#include <stdlib.h>

// Compiler flags, NEED_TO_BE NOTICED, set by compiler
// N/A

// Type definitions
typedef struct conn conn_t;
typedef struct conn* conn_ptr_t;
typedef struct conn** conn_ptr2_t;
typedef struct conn_init_arg conn_init_arg_t;
typedef struct conn_init_arg* conn_init_arg_ptr_t;
typedef context_id_t conn_id_t;
typedef void* (*conn_hdl_t) (conn_ptr_t conn_ptr, event_ptr_t event_ptr, void *arg);
typedef bool (*conn_cond_fp_t) (conn_ptr_t conn_ptr, event_ptr_t event_ptr, void *cond_arg);
typedef bool (*rx_cond_hdl_t) (event_ptr_t event_ptr, void *arg);

// Macros
#define CONN_INVAL_ID	CONTEXT_INVAL_ID
// => MFI connection
// N/A
// => ATI connection
#define ATI_CONN_INVAL_FD   (-1)
// ==== Legacy ====
#define CONN_TBL_SIZE	(1024)
#define CONN_PATH_LEN	(64)
#define CONN_PATH_PREFIX	"tmp/conn_"
// ==== Legacy ====

// Functions
#define conn_init(conn_ptr, arg_ptr)	(unlikely((conn_ptr) == NULL || (conn_ptr)->init_fp == NULL) ? NULL : (conn_ptr)->init_fp((conn_ptr), (conn_init_arg_ptr_t)(arg_ptr)))
#define conn_exit(conn_ptr)	(unlikely((conn_ptr) == NULL || (conn_ptr)->exit_fp == NULL) ? NULL : (conn_ptr)->exit_fp((conn_ptr)))
#define conn_send(conn_ptr, event_ptr)	(unlikely((conn_ptr) == NULL || (conn_ptr)->send_fp == NULL) ? SYS_FAIL : (conn_ptr)->send_fp((conn_ptr), (event_ptr)))
#define conn_recv(conn_ptr)	(unlikely((conn_ptr) == NULL || (conn_ptr)->recv_fp == NULL) ? NULL : (conn_ptr)->recv_fp((conn_ptr)))
#define conn_set_cond(conn_ptr, cond_fp, cond_arg, cond_arg_len)	(unlikely((conn_ptr) == NULL || (conn_ptr)->set_cond_fp == NULL) ? SYS_FAIL : (conn_ptr)->set_cond_fp((conn_ptr), (cond_fp), (cond_arg), (cond_arg_len)))
#define conn_get_id(conn_ptr)	(unlikely((conn_ptr) == NULL) ? CONN_INVAL_ID : (conn_ptr)->id)

// Interface-implementation binding, NEED_TO_BE_NOTICED, if the srv_ptr were given as a value, a compile-time error would be given
#if 0
// => Default connection
#define default_conn_employ(conn_ptr)	mfi_conn_employ((conn_ptr))
#define default_conn_dismiss(conn_ptr)	mfi_conn_dismiss((conn_ptr))
// => MFI connection
#define mfi_conn_employ(conn_ptr)	(unlikely((conn_ptr) == NULL) ? NULL : \
                                 	(((conn_ptr)->init_fp = mfi_conn_init), \
                                 	((conn_ptr)->exit_fp = mfi_conn_exit), \
                                 	((conn_ptr)->send_fp = mfi_conn_send), \
                                 	((conn_ptr)->recv_fp = mfi_conn_recv), \
                                 	((conn_ptr)->set_cond_fp = mfi_conn_set_cond), \
                                 	(conn_ptr)))
#define mfi_conn_dismiss(conn_ptr)	(unlikely((conn_ptr) == NULL) ? NULL : \
                                  	(((conn_ptr)->init_fp = NULL), \
                                  	((conn_ptr)->exit_fp = NULL), \
                                  	((conn_ptr)->send_fp = NULL), \
                                  	((conn_ptr)->recv_fp = NULL), \
                                  	((conn_ptr)->set_cond_fp = NULL), \
                                  	(conn_ptr)))
#endif
// => ATI connection
#define ati_conn_employ(conn_ptr)	(unlikely((conn_ptr) == NULL) ? NULL : \
                                 	(((conn_ptr)->init_fp = ati_conn_init), \
                                 	((conn_ptr)->exit_fp = ati_conn_exit), \
                                 	((conn_ptr)->send_fp = ati_conn_send), \
                                 	((conn_ptr)->recv_fp = ati_conn_recv), \
                                 	((conn_ptr)->set_cond_fp = ati_conn_set_cond), \
                                 	(conn_ptr)))
#define ati_conn_dismiss(conn_ptr)	(unlikely((conn_ptr) == NULL) ? NULL : \
                                  	(((conn_ptr)->init_fp = NULL), \
                                  	((conn_ptr)->exit_fp = NULL), \
                                  	((conn_ptr)->send_fp = NULL), \
                                  	((conn_ptr)->recv_fp = NULL), \
                                  	((conn_ptr)->set_cond_fp = NULL), \
                                  	(conn_ptr)))

// Implementation
// => Initialization arguments for connections
struct conn_init_arg
{
	// General variables
	// => Public
// ==== Legacy ====
	conn_hdl_t hdl;
	void *hdl_arg;
// ==== Legacy ====
	// => Private
	// N/A
	// Individual variables which must be private
	union
	{
		// MFI connection
		struct
		{
			// N/A
		}mfi;

		// ATI connection
		struct
		{
			conn_id_t id;
			mailbox_addr_ptr_t peer_addr_ptr;
		}ati;
	}idv;
};

// => Abstract data type
struct conn
{
	// General variables
	// => Public
	conn_ptr_t (*init_fp) (conn_ptr_t conn_ptr, const conn_init_arg_ptr_t arg_ptr);
	conn_ptr_t (*exit_fp) (conn_ptr_t conn_ptr);
	int (*send_fp) (conn_ptr_t conn_ptr, event_ptr_t event_ptr);
	event_ptr_t (*recv_fp) (conn_ptr_t conn_ptr);
	int (*set_cond_fp) (conn_ptr_t conn_ptr, conn_cond_fp_t cond_fp, const void *cond_arg, size_t cond_arg_len);
	// => Private
	conn_id_t id;
	event_seq_t seq;
	event_seq_t ext_seq;
// ==== Legacy ====
	chnl_t chnl;
	bool is_set_task; // does the task for unsolicited events exist?
	task_t task;	// for unsolicited events
	struct conn_data *data_ptr;	// a linked list for send_recv()
	thrd_lock_t mutex;	// for linked list data_ptr
	thrd_lock_t sem;	// for MFIA
	mailbox_t mailbox;	// for send_recv() from MFIA
// ==== Legacy ====
	// Individual variables which must be private
	union
	{
		// MFI connection
		struct
		{
			bool is_fd_active;
		}mfi;

		// ATI connection
		struct
		{
			mailbox_t mailbox;
			mailbox_addr_t peer_addr;
		}ati;
	}idv;
};

#if 0
// => MFI connection
extern conn_ptr_t mfi_conn_init (conn_ptr_t conn_ptr, const conn_init_arg_ptr_t arg_ptr);
extern conn_ptr_t mfi_conn_exit (conn_ptr_t conn_ptr);
extern int mfi_conn_send (conn_ptr_t conn_ptr, event_ptr_t event_ptr);
extern event_ptr_t mfi_conn_recv (conn_ptr_t conn_ptr);
extern int mfi_conn_set_cond (conn_ptr_t conn_ptr, conn_cond_fp_t cond_fp, const void *cond_arg, size_t cond_arg_len);
#endif
// => ATI connection
extern conn_ptr_t ati_conn_init (conn_ptr_t conn_ptr, const conn_init_arg_ptr_t arg_ptr);
extern conn_ptr_t ati_conn_exit (conn_ptr_t conn_ptr);
extern int ati_conn_send (conn_ptr_t conn_ptr, event_ptr_t event_ptr);
extern event_ptr_t ati_conn_recv (conn_ptr_t conn_ptr);
extern int ati_conn_set_cond (conn_ptr_t conn_ptr, conn_cond_fp_t cond_fp, const void *cond_arg, size_t cond_arg_len);

#endif
