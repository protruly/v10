#ifndef __MFI_EPDGA_H__
#define __MFI_EPDGA_H__

#include <stddef.h>
#include <stdbool.h>

typedef struct mfi_ea_sim_info_t mfi_ea_sim_info_t;

typedef enum mfi_ea_sim_type_t {
    MFI_EA_SIM_TYPE_USIM = 0,
    MFI_EA_SIM_TYPE_ISIM = 1,
} mfi_ea_sim_type_t;


mfi_ea_sim_info_t* mfi_ea_sim_info_alloc(mfi_ea_sim_info_t* sim_info);

void mfi_ea_sim_info_free(mfi_ea_sim_info_t* sim_info);

void mfi_ea_sim_info_set_id(mfi_ea_sim_info_t *sim_info, unsigned id);

void mfi_ea_sim_info_set_ps_capability(mfi_ea_sim_info_t *sim_info,
    bool has_ps_capability);

void mfi_ea_sim_info_set_operator(mfi_ea_sim_info_t *sim_info,
    const char *operator, size_t str_len);

void mfi_ea_sim_info_set_mcc_mnc(mfi_ea_sim_info_t *sim_info, const char *mcc,
    size_t mcc_str_len, const char *mnc, size_t mnc_str_len);

void mfi_ea_sim_info_set_mcc(mfi_ea_sim_info_t *sim_info, const char *mcc,
    size_t str_len);

void mfi_ea_sim_info_set_mnc(mfi_ea_sim_info_t *sim_info, const char *mnc,
    size_t str_len);

void mfi_ea_sim_info_set_imsi(mfi_ea_sim_info_t *sim_info, const char *imsi,
    size_t str_len);

void mfi_ea_sim_info_set_imei(mfi_ea_sim_info_t *sim_info, const char *imei,
    size_t str_len);

void mfi_ea_sim_info_set_impi(mfi_ea_sim_info_t *sim_info, const char *impi,
    size_t str_len);

void mfi_ea_sim_info_set_sim_type(mfi_ea_sim_info_t *sim_info,
    mfi_ea_sim_type_t sim_type);

bool mfi_ea_sim_ready_notify(mfi_ea_sim_info_t *sim_info);

bool mfi_ea_sim_rejected_notify(unsigned sim_id);

#endif
