#ifndef __LIBMDFX_ENTITY_NWMNGR_H__
#define __LIBMDFX_ENTITY_NWMNGR_H__

/* ------------------------------------ */
/*  Network Manager                     */
/* ------------------------------------ */

typedef struct _nwmngr_ps_reg_state {
    unsigned int            cmd_result;
    unsigned int            n;
    unsigned int            state;
    char                    lac[8];
    char                    ci[12];
    unsigned int            Act;
    char                    rac[4];
    unsigned int            cause_type;
    unsigned int            reject_cause;

    unsigned int            wifi_state;
    unsigned int            data_state;
    int                         wfc_support;

    char                    raw_data[128];
} nwmngr_ps_reg_state_t;

typedef struct _nwmngr_data_reg_state_ {
	int status;
	int state;
	char lac[8];
	char ci[12];
	int act;
} nwmngr_data_reg_state_resp_t;

typedef struct {
    unsigned int rat_type;
    unsigned int serial_no;
} nw_trigger_get_cell_info_t;

typedef struct {
    int status;
    unsigned int rat_type;
    unsigned int serial_no;

    /*3GPP*/
    unsigned char   plmn[8]; /* in string format */
    unsigned char   lac [8];  /* in string format, lac or tac */
    unsigned char   ci  [12];   /* in string format */
    unsigned char   cell_id[64]; /* ignore 3gpp or non-3gpp */
    unsigned char   is_ems_support;

    /*   C2K  */
    unsigned char   sid[8]; /* in string format */
    unsigned char   nid[8]; /* in string format */
    unsigned char   pzid[4]; /* in string format */
    unsigned char   base_id[8]; /* in string format */
    unsigned char   sector_id[36]; /* in string format */
    unsigned char   subnet_length[4]; /* in string format */
    unsigned char   carrier_id[8]; /* in string format */

    /* iwlan */
    char mac_addr[64];
} nw_rat_cell_info_t;

#endif /* __LIBMDFX_ENTITY_NWMNGR_H__ */
