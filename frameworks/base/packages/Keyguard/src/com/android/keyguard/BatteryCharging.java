package com.android.keyguard;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.PixelFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.util.Log;

public class BatteryCharging {
	private static final String TAG = "lq_BatteryCharging";
	private static final boolean DBG = false;

	private View				mWindowContent = null;
	private RelativeLayout 	mRelativeLayout = null;
	private ImageView			mBatteryBgImg = null;
	private ImageView			mBatteryChargedImg = null;
	private TextView 			mChargedText = null;

	private Context mContext = null;
	private final WindowManager mWindowManager;
	private WindowManager.LayoutParams mLp;
	private AnimatorSet mAnimationSet = null;
	private boolean mFinish = false;

	private int mStatus;
	private int mPlugged;
	private int mLevel;
	private int mHealth;
	private void setBatteryData(int status, int plugged, int level, int health) {
		mStatus = status;
		mPlugged = plugged;
		mLevel = level;
		mHealth = health;
		Log.d(TAG, "---> mStatus = " + mStatus + "; mPlugged = " + mPlugged + "; mLevel = " + mLevel + "; mHealth = " + mHealth);
	}

	public BatteryCharging(Context context, int status, int plugged, int level, int health) {
		mContext = context;
		mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

		setBatteryData(status, plugged, level, health);
		createWindow(context);
	}

	private void createWindow(Context context) {
		LayoutInflater inflater = LayoutInflater.from(context);
		mWindowContent = inflater.inflate(R.layout.battery_charge_view, null);
		initViews(mWindowContent);

		mLp = new WindowManager.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT,
				WindowManager.LayoutParams.TYPE_BOOT_PROGRESS,
				getModeFlag(),
				PixelFormat.TRANSLUCENT);
		mLp.flags |= WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED;
		mLp.setTitle("BatteryChargeService");
	}

	private void initViews(View view) {
		mRelativeLayout = (RelativeLayout) view.findViewById(R.id.window_bg);
		mRelativeLayout.setVisibility(View.INVISIBLE);

		mBatteryBgImg = (ImageView) view.findViewById(R.id.battery_bg_img);
		if (mLevel <= 40) {
			mBatteryBgImg.setBackgroundResource(R.drawable.battery_bg_flash);
		} else {
			mBatteryBgImg.setBackgroundResource(R.drawable.battery_bg);
		}

		mBatteryChargedImg = (ImageView) view.findViewById(R.id.battery_charge_img);
		if (mLevel <= 10) {
			mBatteryChargedImg.setBackgroundResource(R.drawable.battery_red);
		} else {
			mBatteryChargedImg.setBackgroundResource(R.drawable.battery_green);
		}
		mBatteryChargedImg.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				mBatteryChargedImg.getViewTreeObserver().removeOnGlobalLayoutListener(this);
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mBatteryChargedImg.getLayoutParams();
				params.height = (int)(((float)mLevel/100)*(mBatteryBgImg.getHeight() - 54));
				if (DBG) Log.d(TAG, "----------> params.width = " + params.width + "; params.height = " + params.height + "; mBatteryBgImg.getHeight() = " + mBatteryBgImg.getHeight());
				mBatteryChargedImg.setLayoutParams(params);
				mBatteryChargedImg.requestFocus();
				mBatteryChargedImg.invalidate();
			}
		});

		mChargedText = (TextView) view.findViewById(R.id.battery_charge_text);
		String levelStr = Integer.toString(mLevel) + "%";
		mChargedText.setText(levelStr);
	}

	private int getModeFlag() {
		int windowFlag = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
				| WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
				| WindowManager.LayoutParams.FLAG_FULLSCREEN
				//| WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
				| WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED
				//| WindowManager.LayoutParams.FLAG_DIM_BEHIND
				//| WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
				;

		Log.i(TAG, "getModeFlag windowFlag = " + Integer.toHexString(windowFlag));
		return windowFlag;
	}

	public void updateWindowParams() {
		mWindowContent.requestFocus();
		mWindowManager.addView(mWindowContent, mLp);
		StartAnimation();
	}

	private void StartAnimation()
	{
		mFinish = false;
		if (mAnimationSet != null) {
			mAnimationSet.end();
			mAnimationSet.removeAllListeners();
		}
		ValueAnimator FadeInAnim = createFadeInAnimation();
		ValueAnimator FadeOutAnim = createFadeOutAnimation();
		mAnimationSet = new AnimatorSet();
		mAnimationSet.playSequentially(FadeInAnim, FadeOutAnim);
		mAnimationSet.addListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				super.onAnimationEnd(animation);

				mFinish = true;
				mRelativeLayout.setVisibility(View.GONE);
				mWindowManager.removeView(mWindowContent);
			}
		});
		mWindowContent.post(new Runnable() {
			@Override
			public void run() {
				mAnimationSet.start();
			}
		});
	}

	public void Destory() {
		if (!mFinish) {
			mFinish = true;
			mRelativeLayout.setVisibility(View.GONE);
			if (mAnimationSet != null) {
				mAnimationSet.end();
				mAnimationSet.removeAllListeners();
			}
			mWindowManager.removeView(mWindowContent);
		}
	}

	private ValueAnimator createFadeInAnimation() {
		ValueAnimator anim = ValueAnimator.ofFloat(0f, 1f);
		anim.setDuration(800);
		anim.addListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationStart(Animator animation) {
				mRelativeLayout.setAlpha(0f);
				mRelativeLayout.setVisibility(View.VISIBLE);
				mBatteryBgImg.setAlpha(0f);
				mBatteryBgImg.setVisibility(View.VISIBLE);
				mBatteryChargedImg.setAlpha(0f);
				mBatteryChargedImg.setVisibility(View.VISIBLE);
				mChargedText.setAlpha(0f);
				mChargedText.setVisibility(View.VISIBLE);
			}
			@Override
			public void onAnimationEnd(android.animation.Animator animation) {
			}
		});
		anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				float t = (Float) animation.getAnimatedValue();
				mRelativeLayout.setAlpha(t);
				mBatteryBgImg.setAlpha(t);
				mBatteryChargedImg.setAlpha(t);
				mChargedText.setAlpha(t);
			}
		});
		return anim;
	}

	private ValueAnimator createFadeOutAnimation() {
		ValueAnimator anim = ValueAnimator.ofFloat(0f, 1f);
		anim.setStartDelay(2500);
		anim.setDuration(800);
		anim.addListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				mRelativeLayout.setVisibility(View.GONE);
				mBatteryBgImg.setVisibility(View.GONE);
				mBatteryChargedImg.setVisibility(View.GONE);
				mChargedText.setVisibility(View.GONE);
			}
		});
		anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				float t = (Float) animation.getAnimatedValue();
				mRelativeLayout.setAlpha(1f - t);
				mBatteryBgImg.setAlpha(1f - t);
				mBatteryChargedImg.setAlpha(1f - t);
				mChargedText.setAlpha(1f - t);
			}
		});
		return anim;
	}
}
