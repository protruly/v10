package com.android.server.policy;

import com.android.internal.app.AlertController;
import com.android.internal.app.AlertController.AlertParams;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManagerPolicy.WindowManagerFuncs;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.R;


class PowerOffDialog extends Dialog implements DialogInterface, OnClickListener {
    private final Context mContext;
    private final int mWindowTouchSlop;
    private View rootview;

    private EnableAccessibilityController mEnableAccessibilityController;
    private final WindowManagerFuncs mWindowManagerFuncs;
        
    private boolean mIntercepted;
    private boolean mCancelOnUp;
        
    private ImageView mPoweroff;
    private ImageView mRestart;

    private TextView mTextPowerOff;
    private TextView mTextReboot;

    private static Handler sHandler;

    private Animation mRestartEnterAnimation;
    private ImageView mBg;
    public PowerOffDialog(Context context, WindowManagerFuncs windowManagerFuncs) {
        super(context,  com.hb.R.style.poweroffDialog);
        mContext = getContext();
        mWindowManagerFuncs = windowManagerFuncs;
        mWindowTouchSlop = ViewConfiguration.get(context).getScaledWindowTouchSlop();
            
        rootview = LayoutInflater.from(context).inflate(com.hb.R.layout.poweroff, null, false);
        mTextPowerOff = (TextView)rootview.findViewById(com.hb.R.id.poweroff_dialog_poweroff_text);
        mTextReboot = (TextView)rootview.findViewById(com.hb.R.id.poweroff_dialog_restart_text);
        mBg = (ImageView)rootview.findViewById(com.hb.R.id.poweroff_dialog_grey_bg);
        mBg.setOnClickListener(this);
        mPoweroff = (ImageView)rootview.findViewById(com.hb.R.id.poweroff_dialog_poweroff);
        mPoweroff.setOnClickListener(this);
        mRestart = (ImageView)rootview.findViewById(com.hb.R.id.poweroff_dialog_restart);
        mRestart.setOnClickListener(this);
	    mRestartEnterAnimation = AnimationUtils.loadAnimation(getContext(), com.hb.R.anim.anim_restart_enter);
        mRestartEnterAnimation.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				mTextReboot.animate()
				.alpha(1.0f).setDuration(500).setInterpolator(new LinearInterpolator()).start();
				mTextPowerOff.animate()
				.alpha(1.0f).setDuration(500).setInterpolator(new LinearInterpolator()).start();
			}
		});
    }
    

    private static Boolean bPoweroff = false;
    private static Boolean bRestart = false;

    private Boolean bAnimationStart = false; // add by huliang for bug 2930
    
	@Override
	public void onClick(View v) {
        if (true == bAnimationStart) { // add by huliang for bug 2930
            return;
        }
		if (v.equals(mBg )) {
			this.cancel();
			return;
		} else if (v.equals(mPoweroff)) {
			bPoweroff = true;
		} else if (v.equals(mRestart)) {
			bRestart =  true;
		}
		
		mTextReboot.animate()
		.alpha(0.0f).setDuration(500).setInterpolator(new LinearInterpolator()).start();
		mTextPowerOff.animate()
		.alpha(0.0f).setDuration(500).setInterpolator(new LinearInterpolator())
		.setListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator animation) {
				// TODO Auto-generated method stub
                bAnimationStart = true; // add by huliang for bug 2930
			}
			
			@Override
			public void onAnimationRepeat(Animator animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animator animation) {
				// TODO Auto-generated method stub
				mRestart.animate().translationY(-(50*3 + mRestart.getHeight() / 2)).alpha(0.0f).setDuration(500).setInterpolator(new LinearInterpolator())
				.start();
				mPoweroff.animate().translationY(50*3+mPoweroff.getHeight() / 2).alpha(0.0f).setDuration(500).setInterpolator(new LinearInterpolator())
				.start();
				mBg.animate().alpha(1.0f).setDuration(500).setInterpolator(new LinearInterpolator()).setListener(new AnimatorListener(){
					@Override
					public void onAnimationStart(Animator animation) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onAnimationRepeat(Animator animation) {
						// TODO Auto-generated/system/framework/ method stub
						
					}
					@Override
					public void onAnimationEnd(Animator animation) {
						if (true == bPoweroff) {
			    			mWindowManagerFuncs.shutdown(); // modify by huliang for remove poweroff progress dialog
						} else if (true == bRestart) {
                            // modify by huliang for remove poweroff progress dialog
//                            mWindowManagerFuncs.reboot();
                            Intent intent = new Intent(Intent.ACTION_REBOOT);
                            intent.setAction(Intent.ACTION_REBOOT);
                            intent.putExtra("nowait", 1);
                            intent.putExtra("interval", 1);
                            intent.putExtra("window", 0);
                            mContext.sendBroadcast(intent);
                        }
					}
					@Override
					public void onAnimationCancel(Animator animation) {
						// TODO Auto-generated method stub
						
					}
				} ).start();
			}
			
			@Override
			public void onAnimationCancel(Animator animation) {
				// TODO Auto-generated method stub
				
			}
		}).start();
	}		
        @Override
        protected void onStart() {
            // If global accessibility gesture can be performed, we will take care
            // of dismissing the dialog on touch outside. This is because the dialog
            // is dismissed on the first down while the global gesture is a long press
            // with two fingers anywhere on the screen.
            if (EnableAccessibilityController.canEnableAccessibilityViaGesture(mContext)) {
                mEnableAccessibilityController = new EnableAccessibilityController(mContext,
                        new Runnable() {
                    @Override
                    public void run() {
                        dismiss();
                    }
                });
                super.setCanceledOnTouchOutside(false);
            } else {
                mEnableAccessibilityController = null;
                super.setCanceledOnTouchOutside(true);
            }

            super.onStart();
        }

    @Override
    protected void onStop() {
        super.onStop();
        Intent intent = new Intent("com_hb_hide_navagationbar_action");
        intent.putExtra("state",1);
        getContext().sendBroadcast(intent);
    }
    
    @Override
    public void show() {
    	super.show();
    	mPoweroff.startAnimation(AnimationUtils.loadAnimation(getContext(), com.hb.R.anim.anim_power_off_enter));
    	mRestart.startAnimation(mRestartEnterAnimation);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
       
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(rootview);

        sHandler = new Handler();

        Intent intent = new Intent("com_hb_hide_navagationbar_action");
        intent.putExtra("state",0);
        intent.putExtra("from",1);
        getContext().sendBroadcast(intent);
    }
}