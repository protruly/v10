package com.android.server.policy;

import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.content.Context;
import android.os.SystemProperties;
import android.content.res.Configuration;
import android.view.WindowManager;
import android.graphics.Point;
import android.util.Log;
/**
 *
 * @author tangjun
 *  add for three finger screenshot
 *
 */
public class HbThreeFingerScreenShotPolicy {

	private final static String TAG = "HbThreeFingerScreenShotPolicy";
	private final static boolean DEBUG = false;
	private HbITakeScreenShot mScreenShot;
	private static final int FINGER_COUNT = 3;
	private float distance = 360f;// need to change for diff screen size
	private Handler mHandler = new Handler();
	private ConsumeState mConsumeState = new ConsumeState();

	private Runnable mTakeScreenShot = new Runnable() {
		@Override
		public void run() {
			if(DEBUG){
				Log.i(TAG, "start screenshot");
			}
			mScreenShot.takeScreenShot();
		}
	};

	float[] startPYS = { -1, -1, -1 };
	float[] endPYS = { -1, -1, -1 };
	boolean isFirst = true;
	boolean mShouldTake = false;
	int mPointCount = 0;
	boolean isComeIn = false;

	private void resetPointState(){
		startPYS[0] = startPYS[1] = startPYS[2] = -1;
		endPYS[0] = endPYS[1] = endPYS[2] = -1;
		isFirst = true;
		isComeIn = false;
	}

	public HbThreeFingerScreenShotPolicy(Context mContext){
		mScreenShot = new HbScreenShotHelper(mContext);
		mConfiguration = mContext.getResources().getConfiguration();
		initDistance(mContext);
	}

	public boolean handleEvent(MotionEvent event) {

		int action = event.getActionMasked();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			if(DEBUG){
				Log.i(TAG, "ACTION_DOWN:  " + event.getPointerCount());
			}
			break;
		case MotionEvent.ACTION_POINTER_DOWN:
			mPointCount = event.getPointerCount();
			if(mPointCount == FINGER_COUNT){
				if(DEBUG){
					Log.e(TAG, " ACTION_POINTER_DOWN:  Three Points");
				}
				isComeIn = true;
			}
			break;

		case MotionEvent.ACTION_MOVE:
			if (!mShouldTake && isComeIn) {
				if (event.getPointerCount() == FINGER_COUNT) {
					if (isFirst) {
						startPYS[0] = event.getY(0);
						startPYS[1] = event.getY(1);
						startPYS[2] = event.getY(2);
						mConsumeState.update(true);
						if(DEBUG){
							Log.e(TAG, "start point: " + startPYS[0] + " "+ startPYS[1] + " "+ startPYS[2]);
						}
						isFirst = false;
					} else {
						endPYS[0] = event.getY(0);
						endPYS[1] = event.getY(1);
						endPYS[2] = event.getY(2);
						if(DEBUG){
							Log.e(TAG, "start point: " + endPYS[0] + " "+ endPYS[1] + " "+ endPYS[2]);
						}
					}
				} else {
					if(!isFirst){
						mShouldTake = isTakeScreenShot(startPYS, endPYS);
					}
					resetPointState();
				}
			}

			break;
		case MotionEvent.ACTION_UP:
			if(DEBUG){
				Log.e(TAG, " ACTION_up=> isComeIn:  " + isComeIn + "  isFirst:  " + isFirst + "  mShouldTake:  " + mShouldTake);
			}
			if (mShouldTake) {
				takeScreenShot();
			}
			// as three point up, red-mi three point dispear at same time and do not come in ACTION_MOVE.
			// usually, point disapear at different time.
			else {
				if(!isFirst){
					mShouldTake = isTakeScreenShot(startPYS, endPYS);
				}
				if (mShouldTake) {
					takeScreenShot();
				}
			}
			resetPointState();
			mShouldTake = false;
			mConsumeState.update(false);
			break;
		case MotionEvent.ACTION_CANCEL:
			resetPointState();
			mShouldTake = false;
			mConsumeState.update(false);
			if(DEBUG){
				Log.e(TAG, " ACTION_Cancel: ");
			}
			break;
		}
		return true;
	}

	private void takeScreenShot() {
		mHandler.removeCallbacks(mTakeScreenShot);
		mHandler.post(mTakeScreenShot);
	}

	private boolean isTakeScreenShot(float[] startPYs, float[] endPYs) {
		int direction = 0;// 0 means down, 1 means up
		distance = getDistance();
		if(DEBUG){
			Log.e(TAG, "the distance is: " + distance);
		}
		if (startPYs.length != FINGER_COUNT
				|| endPYs.length != FINGER_COUNT
				|| startPYs[0] == -1 || endPYs[0] == -1)
			return false;
		for (int i = 0; i < FINGER_COUNT; i++) {
			if ((i == 0) && (startPYs[i] > endPYs[i])) {
				direction = 1;
			}
			if (direction == 0) {
				if (endPYs[i] - startPYs[i] > distance)
					continue;
				else
					return false;
			} else {
				//暂时只允许三指下滑截屏
				return false;
				/*
				if (startPYs[i] - endPYs[i] > distance)
					continue;
				else
					return false;
					*/
			}
		}
		return true;
	}

    private class ConsumeState {
        private boolean mConsumed;

        public ConsumeState() {
            mConsumed = false;
        }

        public void update(boolean newState){
            mConsumed = newState;
            if (mConsumed){
				SystemProperties.set("sys.hb.input.intercept", "1");
			}else{
                SystemProperties.set("sys.hb.input.intercept", "0");
			}
			if(true){
				Log.e(TAG, "update() " + mConsumed + "  " + SystemProperties.getBoolean("sys.monster.input.intercept", false));
			}
        }

        public boolean getState() {
            return mConsumed;
        }
    }


	private int distanceLand = 200;
	private int distancePort = 400;
	private Configuration mConfiguration ;

	private void initDistance(Context context) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Point p = new Point();
		wm.getDefaultDisplay().getSize(p);
		int ori = mConfiguration.orientation;
		if (ori == Configuration.ORIENTATION_PORTRAIT) {
			distancePort = p.y / 6;
			distanceLand = p.x / 6;
		} else {
			distancePort = p.x / 6;
			distanceLand = p.y / 6;
		}
		if(DEBUG){
			Log.e(TAG, "the distancePort is: " + distancePort + "   the distanceLand is: " + distanceLand);
		}
	}

	private int getDistance(){
		int ori = mConfiguration.orientation;
		if (ori == Configuration.ORIENTATION_PORTRAIT) {
			return distancePort;
		} else {
			return distanceLand;
		}
	}
}
