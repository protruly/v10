package com.android.server.policy;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.IWindowManager;
import android.view.WindowManager;
import android.os.ServiceManager;
import android.database.ContentObserver;
import android.graphics.Point;
import static android.view.WindowManagerPolicy.WindowManagerFuncs;
import android.provider.Settings;
import android.view.WindowManagerPolicy.PointerEventListener;
import android.widget.Toast;

/**
 *
 * @author tangjun
 *  add for three finger screenshot
 *
 */

public class HbPointerInterceptor {
    private static final String TAG = "HbPointerInterceptor";
    private static final boolean DEBUG = true;
    private static final boolean THREEFINGERS_CAPTURE_SUPPORT = true;
    private Context mContext;
    private WindowManagerFuncs mWindowManagerFuncs;
	private HbThreeFingerScreenShotPolicy mScreenShot;
	private PointerEventListener mCaptureEventListener;
    private Handler mHandler = new Handler();
    private final static String three_finger_screenshot_enable = "three_finger_screenshot_enable";
    public static final String THREE_FINGER_CAPTURE = "three_finger_capture";
    private final String THREE_FINGER_GESTURES_ENABLE = "sys.ssshot.threePointer";

    public HbPointerInterceptor(Context context, WindowManagerFuncs windowManagerFuncs) {
        mContext = context;
        mWindowManagerFuncs = windowManagerFuncs;
        enable();
        mContext.getContentResolver().registerContentObserver(
                Settings.Global.getUriFor(three_finger_screenshot_enable), true,
                mThreeFingerScreenshotSettingObserver);

    }
    private final class ThreeFingerCaptureEventListener implements PointerEventListener {
        @Override
        public void onPointerEvent(MotionEvent motionEvent) {
            onMotionEvent(motionEvent);
        }
    }

    public void enable() {
    	log("enable");

		if(THREEFINGERS_CAPTURE_SUPPORT){
			if (mScreenShot == null){
				mScreenShot = new HbThreeFingerScreenShotPolicy(mContext);
			}
		}

		if(mCaptureEventListener == null){
			mCaptureEventListener = new ThreeFingerCaptureEventListener();
			mWindowManagerFuncs.registerPointerEventListener(mCaptureEventListener);
		}
    }

    public void disable(){
    	log("disable");
        if(THREEFINGERS_CAPTURE_SUPPORT){
			if(mCaptureEventListener != null) {
				mWindowManagerFuncs.unregisterPointerEventListener(mCaptureEventListener);
				mCaptureEventListener = null;
			}
		}
    }

    private void onMotionEvent(MotionEvent event) {
		if(THREEFINGERS_CAPTURE_SUPPORT && isThreeFingerScreenShotEnable()){
			mScreenShot.handleEvent(event);
		}
    }

	private boolean isThreeFingerScreenShotEnable(){
		//return Settings.System.getInt(mContext.getContentResolver(), three_finger_screenshot_enable, 1) == 1;
		//Log.d("111111", "---111isThreeFingerScreenShotEnable = " + SystemProperties.getBoolean(THREE_FINGER_GESTURES_ENABLE, true));
		//Log.d("111111", "---222isThreeFingerScreenShotEnable = " + SystemProperties.getBoolean(THREE_FINGER_GESTURES_ENABLE, false));
		//return SystemProperties.getBoolean(THREE_FINGER_GESTURES_ENABLE, true);
		return Settings.Secure.getInt(mContext.getContentResolver(), THREE_FINGER_CAPTURE, 1) != 0;
	}

	protected final ContentObserver mThreeFingerScreenshotSettingObserver = new ContentObserver(mHandler) {
        @Override
        public void onChange(boolean selfChange) {
        	/*
            final int three_finger_setting_value = Settings.System.getInt(mContext.getContentResolver(), "three_finger_screenshot_enable", 1);
            if(isThreeFingerScreenShotEnable()){
				enable();
			}else{
				disable();
			}
			*/
        }
    };

    private void log(String msg) {
    	if (DEBUG) {
    		Log.e(TAG, "HbPointerInterceptor: " + msg);
    	}
    }
}
