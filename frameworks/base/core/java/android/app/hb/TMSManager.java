/* Copyright (C) 2016 Tcl Corporation Limited */
package android.app.hb;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;
import android.app.hb.CodeNameInfo;
import android.app.hb.ITrafficCorrectListener;
import java.util.List;
/**
 * hummmingbird luolaigang add for TMSManager 20170330
 * @author luolaigang
 * @hide
 */
public class TMSManager
{
	public static final String TMS_SERVICE_ACTION = "android.intent.action.TMSService";
    	public static final String TMS_SERVICE = "tms_service";
        public static final String TAG = "TMSManager";
        ITMSManager mService;

        public TMSManager(ITMSManager service) {
                mService = service;
        }

        public String getLocation(String number){
            try {
                return mService.getLocation(number);
            } catch (RemoteException e) {
                Log.e(TAG, "[getValue] RemoteException");
            }
            return null;
        }

    /**
     * add by zhaolaichao
     * @return
     */
    public List<CodeNameInfo> getAllProvinces() {
        try {
            return mService.getAllProvinces();
        } catch (RemoteException e) {
                Log.e(TAG, "[getValue] RemoteException");
        }
        return null;
    }
    /**
     *add by zhaolaichao
     * @return
     */
    public List<CodeNameInfo> getCities(String provinceCode) {
        try {
            return mService.getCities(provinceCode);
        } catch (RemoteException e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return null;
    }
    /**
     *add by zhaolaichao
     * @return
     */
    public List<CodeNameInfo> getBrands(String carryId) {
        try {
            return mService.getBrands(carryId);
        } catch (RemoteException e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return null;
    }
    /**
     *add by zhaolaichao
     * @return
     */
    public List<CodeNameInfo> getCarries() {
        try {
            return mService.getCarries();
        } catch (RemoteException e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return null;
    }
    /**
     *add by zhaolaichao
     * @return
     */
    public int setConfig(int simIndex, String provinceId, String cityId, String carryId, String brandId, int closingDay) {
        try {
            return mService.setConfig(simIndex, provinceId, cityId, carryId, brandId, closingDay);
        } catch (RemoteException e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return -1;
    }
    /**
     *add by zhaolaichao
     * @return
     */
    public int startCorrection(int simIndex) {
        try {
            return mService.startCorrection(simIndex);
        } catch (RemoteException e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return -1;
    }
    /**
     *add by zhaolaichao
     * @return
     */
    public int analysisSMS(int simIndex, String queryCode, String queryPort, String smsBody) {
        try {
            return mService.analysisSMS(simIndex, queryCode, queryPort, smsBody);
        } catch (RemoteException e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return -1;
    }
    /**
     *add by zhaolaichao
     * @return
     */
    public int[] getTrafficInfo(int simIndex) {
        try {
            return mService.getTrafficInfo(simIndex);
        } catch (RemoteException e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return null;
    }

    /**
     *add by zhaolaichao
     * @return
     */
    public void trafficCorrectListener(ITrafficCorrectListener listener) {
        try {
            mService.trafficCorrectListener(listener);
        } catch (RemoteException e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
    }
}