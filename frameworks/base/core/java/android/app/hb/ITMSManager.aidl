package android.app.hb;
import android.app.hb.CodeNameInfo;
import android.app.hb.ITrafficCorrectListener;
import java.util.List;
/**
 * hummmingbird luolaigang add for TMSManager 20170330
 * {@hide}
 */
interface ITMSManager  
{  
    String getLocation(String number);
    //zhalaichao add for HMBNetManager 20170519 start
    List<CodeNameInfo> getAllProvinces();
    List<CodeNameInfo> getCities(String provinceCode);
    List<CodeNameInfo> getCarries();
    List<CodeNameInfo> getBrands(String carryId);
    int setConfig(int simIndex, String provinceId, String cityId, String carryId, String brandId, int closingDay);
    int startCorrection(int simIndex);
    int analysisSMS(int simIndex, String queryCode, String queryPort, String smsBody);
    int[] getTrafficInfo(int simIndex);
    void trafficCorrectListener(ITrafficCorrectListener listener);
    //zhalaichao add for HMBNetManager 20170519 end
}
