package android.content.res;

import android.util.Log;
import android.util.TypedValue;

public class HbTypedArray extends TypedArray{
	private HbThemeResourceInner mResources;
	HbTypedArray(Resources resources, int[] data, int[] indices, int len) {
		super(resources, data, indices, len);
		// TODO Auto-generated constructor stub
		if(resources instanceof HbThemeResourceInner){
			mResources = (HbThemeResourceInner)resources;
		}
	}
	
	@Override
	public int getDimensionPixelSize(int index, int defValue) {
		// TODO Auto-generated method stub
//		Integer themeDimen = getThemeDimensionPixelSize(index);
//		if(themeDimen != null){
//			return themeDimen.intValue();
//		}
		return super.getDimensionPixelSize(index, defValue);
	}
	
	private Integer getThemeDimensionPixelSize(int index){
		if(mResources == null){
			return null;
		}
		final TypedValue value = mValue;
		getValueAt(index*AssetManager.STYLE_NUM_ENTRIES, value);
		if(value.resourceId != 0){
			return mResources.getThemeDimensionPixelSize(value.resourceId);
		}
		return null;
	}

}
