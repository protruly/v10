package com.android.providers.media;

import android.view.MenuItem;
import android.view.View;
import hb.app.HbActivity;
import hb.widget.toolbar.Toolbar;
/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class FullActivityBase extends HbActivity{
	private boolean mDisplayHomeAsUpEnabled = true;

    @Override
    public void setContentView(int layoutResID) {
    	super.setHbContentView(layoutResID);
    	initToolbar(null);
    }
    
    public void setContentView(int layoutResID, String title) {
    	super.setHbContentView(layoutResID);
    	initToolbar(title);
    }
    
    @Override
    public void setContentView(View view) {
    	super.setHbContentView(view);
    	initToolbar(null);
    }
    
    private void initToolbar(String title) {
    	try{
    		Toolbar toolbar = getToolbar();
			if (toolbar != null) {
				toolbar.setTitle(null == title?getTitle():title);
				showBackIcon(mDisplayHomeAsUpEnabled);
				setActionBar(toolbar);
				toolbar.setNavigationOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						onNavigationClicked(view);
					}
				});
			}

    	}catch(Exception e){
    		e.printStackTrace();
    	}        
	}

	public void onNavigationClicked(View view){
		if (mDisplayHomeAsUpEnabled) {
			finish();
		}
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		return getWindow().getCallback().onMenuItemSelected(0,item);
	}

	public void setDisplayHomeAsUpEnabled(boolean mDisplayHomeAsUpEnabled) {
		this.mDisplayHomeAsUpEnabled = mDisplayHomeAsUpEnabled;
	}
}
