/*
* Copyright (C) 2014 MediaTek Inc.
* Modification based on code covered by the mentioned copyright
* and/or permission notice(s).
*/
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.providers.media;

import android.app.ListActivity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.CharArrayBuffer;
import android.database.Cursor;
import android.media.AudioAttributes;
import android.media.Ringtone;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import hb.app.HbActivity;
import hb.app.HbListActivity;
import android.app.hb.HBRingtoneManager;

import java.io.File;

import com.android.internal.app.AlertActivity;
import com.android.internal.app.AlertController;
import com.android.providers.media.R;
import com.android.providers.media.music.HbMusicPickerActivity;
//hummmingbird liuqin add for bugfix #2215 20170617 start
import android.content.Context;
import java.io.IOException;
import android.media.AudioManager;
import android.media.MediaPlayer;
//hummmingbird liuqin add for bugfix #2215 20170617 end

/**
 * The {@link HbRingtonePickerActivity} allows the user to choose one from all
 * of the available ringtones. The chosen ringtone's URI will be persisted as a
 * string.
 *
 * @see HBRingtoneManager#ACTION_RINGTONE_PICKER
 */
public final class HbRingtonePickerActivity extends FullActivityBase implements Runnable, OnItemClickListener {

	private static final int POS_UNKNOWN = -1;

	private static final String TAG = "RingtonePickerActivity";

	private static final int DELAY_MS_SELECTION_PLAYED = 300;

	private static final String SAVE_CLICKED_POS = "clicked_pos";
	/// M: Request codes to MusicPicker for add more ringtone
	private static final int ADD_MORE_RINGTONES = 1;

	private HBRingtoneManager mRingtoneManager;
	// Give a init value for ringtone type.
	// private int mType;

	private Cursor mCursor;
	private Handler mHandler;

	/** The position in the list of the 'Silent' item. */
	private int mSilentPos = POS_UNKNOWN;

	/** The position in the list of the 'Default' item. */
	private int mDefaultRingtonePos = POS_UNKNOWN;

	/** The position in the list of the last clicked item. */
	private int mClickedPos = POS_UNKNOWN;

	/** The position in the list of the ringtone to sample. */
	private int mSampleRingtonePos = POS_UNKNOWN;

	/** Whether this list has the 'Silent' item. */
	private boolean mHasSilentItem;

	/** The Uri to place a checkmark next to. */
	private Uri mExistingUri;

	/** The number of static items in the list. */
	private int mStaticItemCount;

	/** Whether this list has the 'Default' item. */
	private boolean mHasDefaultItem;

	/** The Uri to play when the 'Default' item is clicked. */
	private Uri mUriForDefaultItem;

	/** M: Whether this list has the 'More Ringtongs' item. */
	private boolean mHasMoreRingtonesItem = true;

	/** M: The position in the list of the 'More Ringtongs' item. */
	private int mMoreRingtonesPos = POS_UNKNOWN;

	/** M: The ringtone type to show and add in the list. */
	private int mType = -1;

	/** M: Whether need to refresh listview after activity on resume. */
	private boolean mNeedRefreshOnResume = true;

	/**
	 * A Ringtone for the default ringtone. In most cases, the RingtoneManager
	 * will stop the previous ringtone. However, the RingtoneManager doesn't
	 * manage the default ringtone for us, so we should stop this one manually.
	 */
	private Ringtone mDefaultRingtone;

	/**
	 * The ringtone that's currently playing, unless the currently playing one
	 * is the default ringtone.
	 */
	private Ringtone mCurrentRingtone;

	private int mAttributesFlags;

	/**
	 * Keep the currently playing ringtone around when changing orientation, so
	 * that it can be stopped later, after the activity is recreated.
	 */
	private static Ringtone sPlayingRingtone;

	private ListView listView;
	
	private RingToneAdapter ringToneAdapter;

	int oldIndex = -1;
	
	private Ringtone mFirstRingtone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		CharSequence mTitle = intent.getCharSequenceExtra(HBRingtoneManager.EXTRA_RINGTONE_TITLE);
        if (mTitle == null) {
        	mTitle = getString(com.android.internal.R.string.ringtone_picker_title);
        }
        
		setContentView(R.layout.ringtone_picker, mTitle.toString());
		listView = (ListView)findViewById(android.R.id.list);
		listView.setOnItemClickListener(this);
		
		mHandler = new Handler();

		
		/*
		 * Get whether to show the 'Default' item, and the URI to play when the
		 * default is clicked
		 */
		mHasDefaultItem = intent.getBooleanExtra(HBRingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
		mUriForDefaultItem = intent.getParcelableExtra(HBRingtoneManager.EXTRA_RINGTONE_DEFAULT_URI);
		if (mUriForDefaultItem == null) {
			mUriForDefaultItem = Settings.System.DEFAULT_RINGTONE_URI;
		}

		if (savedInstanceState != null) {
			mClickedPos = savedInstanceState.getInt(SAVE_CLICKED_POS, POS_UNKNOWN);
		}
		// Get whether to show the 'Silent' item
		mHasSilentItem = intent.getBooleanExtra(HBRingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true);
		// AudioAttributes flags
		mAttributesFlags |= intent.getIntExtra(HBRingtoneManager.EXTRA_RINGTONE_AUDIO_ATTRIBUTES_FLAGS,
				0 /* defaultValue == no flags */);

		/// M: Get whether to show the 'More Ringtones' item
		/*
		 * mHasMoreRingtonesItem = intent.getBooleanExtra(
		 * RingtoneManager.EXTRA_RINGTONE_SHOW_MORE_RINGTONES, false);
		 */

		// Give the Activity so it can do managed queries
		mRingtoneManager = new HBRingtoneManager(this);

		// Get whether to include DRM ringtones
		final boolean includeDrm = intent.getBooleanExtra(HBRingtoneManager.EXTRA_RINGTONE_INCLUDE_DRM, true);
		mRingtoneManager.setIncludeDrm(includeDrm);

		// Get the types of ringtones to show
		mType = intent.getIntExtra(HBRingtoneManager.EXTRA_RINGTONE_TYPE, -1);
		if (mType != -1) {
			mRingtoneManager.setType(mType);
		}

		mCursor = mRingtoneManager.getCursor();

		// The volume keys will control the stream that we are choosing a
		// ringtone for
		setVolumeControlStream(mRingtoneManager.inferStreamType());

		// Get the URI whose list item should have a checkmark
		mExistingUri = intent.getParcelableExtra(HBRingtoneManager.EXTRA_RINGTONE_EXISTING_URI);

		onPrepareListView();
		
		ringToneAdapter = new RingToneAdapter();
		listView.setAdapter(ringToneAdapter);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		MtkLog.d(TAG, "onSaveInstanceState: mClickedPos = " + mClickedPos);
		outState.putInt(SAVE_CLICKED_POS, mClickedPos);
	}

	int addItemCount = -1;

	public void onPrepareListView() {
		MtkLog.d(TAG, "onPrepareListView>>>: mClickedPos = " + mClickedPos);
		/// M: Add "More Ringtone" to the top of listview to let user choose
		/// more ringtone
		if (mHasMoreRingtonesItem) {
			addItemCount++;
			mMoreRingtonesPos = addItemCount;
		}

		if (mHasDefaultItem) {
			addItemCount++;
			mDefaultRingtonePos = addItemCount;

			if (HBRingtoneManager.isDefault(mExistingUri)) {
				mClickedPos = mDefaultRingtonePos;
			}
		}

		if (mHasSilentItem) {
			addItemCount++;
			mSilentPos = addItemCount;

			// The 'Silent' item should use a null Uri
			if (mExistingUri == null) {
				mClickedPos = mSilentPos;
			}
		}

		mStaticItemCount = addItemCount+1;

		if (mClickedPos == POS_UNKNOWN) {
			/// M: if the given uri not exist, show default ringtone.
			if (HBRingtoneManager.isRingtoneExist(getApplicationContext(), mExistingUri)) {
				mClickedPos = getListPosition(mRingtoneManager.getRingtonePosition(mExistingUri));
				MtkLog.d(TAG, "mClickedPos1" + mClickedPos + ", mExistingUri = " + mExistingUri);
				if(mClickedPos == POS_UNKNOWN){
					//选中外部铃声处理
					mClickedPos = 0;
				}
			} else {
				if (mHasDefaultItem) {
					mClickedPos = mDefaultRingtonePos;
				} else {

					Uri uri = HBRingtoneManager.getDefaultRingtoneUri(getApplicationContext(), mType);
					if(HBRingtoneManager.isRingtoneExist(getApplicationContext(), uri)){
						mClickedPos = getListPosition(mRingtoneManager.getRingtonePosition(uri));	
					}else{
						mExistingUri = HBRingtoneManager.getInternalDefaultRingtone(getApplicationContext(), mType);
						mClickedPos = getListPosition(mRingtoneManager.getRingtonePosition(mExistingUri));
					}					
				}
				MtkLog.d(TAG, "mClickedPos2" + mClickedPos + ", mExistingUri = " + mExistingUri);
			}
		}

		// Put a checkmark next to an item.
		MtkLog.d(TAG, "onPrepareListView<<<: mClickedPos = " + mClickedPos + ", mExistingUri = " + mExistingUri);
	}
	
	/*
	 * On item selected via keys
	 */
	public void onItemSelected(AdapterView parent, View view, int position, long id) {
		playRingtone(position, DELAY_MS_SELECTION_PLAYED);
	}

	public void onNothingSelected(AdapterView parent) {
	}

	private void playRingtone(int position, int delayMs) {
		mHandler.removeCallbacks(this);
		mSampleRingtonePos = position;
		mHandler.postDelayed(this, delayMs);
	}

	public void run() {
		stopAnyPlayingRingtone();
		if (mSampleRingtonePos == mSilentPos) {
			return;
		}

		/*
		 * Stop the default ringtone, if it's playing (other ringtones will be
		 * stopped by the RingtoneManager when we get another Ringtone from it.
		 */
		if (mDefaultRingtone != null && mDefaultRingtone.isPlaying()) {
			mDefaultRingtone.stop();
			mDefaultRingtone = null;
		}

		Ringtone ringtone;
		if (mSampleRingtonePos == mDefaultRingtonePos) {
			/// M: if default URI is "", it means silent if default item
			/// selected. @{
			if (mUriForDefaultItem.toString().isEmpty()) {
				return;
			}
			/// @}

			if (mDefaultRingtone == null) {
				mDefaultRingtone = HBRingtoneManager.getRingtone(this, mUriForDefaultItem);
			}
			/*
			 * Stream type of mDefaultRingtone is not set explicitly here. It
			 * should be set in accordance with mRingtoneManager of this
			 * Activity.
			 */
			if (mDefaultRingtone != null) {
				mDefaultRingtone.setStreamType(mRingtoneManager.inferStreamType());
			}
			ringtone = mDefaultRingtone;
			mCurrentRingtone = null;
		} else {
			ringtone = mRingtoneManager.getRingtone(getRingtoneManagerPosition(mSampleRingtonePos));
			mCurrentRingtone = ringtone;
		}

		if (ringtone != null) {
			if (mAttributesFlags != 0) {
				ringtone.setAudioAttributes(
						new AudioAttributes.Builder(ringtone.getAudioAttributes()).setFlags(mAttributesFlags).build());
			}
            //hummmingbird liuqin add for bugfix #2215 20170617 start
			if (!IS_USE_CUSTOM_MEDIAPLAY) {
				ringtone.play();
			} else {
				playRingtone(ringtone);
			}
            //hummmingbird liuqin add for bugfix #2215 20170617 end
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (!isChangingConfigurations()) {
			stopAnyPlayingRingtone();
		} else {
			saveAnyPlayingRingtone();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (!isChangingConfigurations()) {
			stopAnyPlayingRingtone();
		}
		mNeedRefreshOnResume = true;
	}

	private void saveAnyPlayingRingtone() {
		if (mDefaultRingtone != null && mDefaultRingtone.isPlaying()) {
			MtkLog.d(TAG, "saveAnyPlayingRingtone>>>:  mDefaultRingtone " + mDefaultRingtone);
			sPlayingRingtone = mDefaultRingtone;
		} else if (mCurrentRingtone != null && mCurrentRingtone.isPlaying()) {
			MtkLog.d(TAG, "saveAnyPlayingRingtone>>>:   mCurrentRingtone " + mCurrentRingtone);
			sPlayingRingtone = mCurrentRingtone;
		}
	}

	private void stopAnyPlayingRingtone() {
        //hummmingbird liuqin add for bugfix #2215 20170617 start
		if (!IS_USE_CUSTOM_MEDIAPLAY) {
			if (sPlayingRingtone != null && sPlayingRingtone.isPlaying()) {
				sPlayingRingtone.stop();
			}
			sPlayingRingtone = null;

			if (mDefaultRingtone != null && mDefaultRingtone.isPlaying()) {
				mDefaultRingtone.stop();
			}

			if (mRingtoneManager != null) {
				mRingtoneManager.stopPreviousRingtone();
			}
		} else {
			stopMediaPlayer();
		}
        //hummmingbird liuqin add for bugfix #2215 20170617 end
	}

	private int getRingtoneManagerPosition(int listPos) {
		return listPos - mStaticItemCount;
	}

	private int getListPosition(int ringtoneManagerPos) {

		// If the manager position is -1 (for not found), return that
		if (ringtoneManagerPos < 0)
			return ringtoneManagerPos;

		return ringtoneManagerPos + mStaticItemCount;
	}

	/// M: Add to restore user's choice after activity has been killed.
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		MtkLog.d(TAG, "onRestoreInstanceState: savedInstanceState = " + savedInstanceState + ",mClickedPos = "
				+ mClickedPos + ",this = " + this);
		super.onRestoreInstanceState(savedInstanceState);
		mClickedPos = savedInstanceState.getInt(SAVE_CLICKED_POS, mClickedPos);
	}

	/// M: Add to refresh activity because some new ringtones will insert to
	/// listview.
	@Override
	protected void onResume() {
		super.onResume();
		MtkLog.d(TAG, "onResume>>>: mNeedRefreshOnResume = " + mNeedRefreshOnResume);
		/// When activity first start, just return. Only when restart we need to
		/// refresh in resume.
		if (!mNeedRefreshOnResume) {
			return;
		}
		MtkLog.d(TAG, "onResume>>>: mClickedPos = " + mClickedPos);
		if (null == listView) {
			MtkLog.e(TAG, "onResume: listview is null, return!");
			return;
		}
		/// Refresh the checked position after activity resume,
		/// because maybe there are new ringtone insert to listview.
		ListAdapter adapter = listView.getAdapter();
		ListAdapter headAdapter = adapter;
		if (null != headAdapter && (headAdapter instanceof HeaderViewListAdapter)) {
			/// Get the cursor adapter with the listview
			adapter = ((HeaderViewListAdapter) headAdapter).getWrappedAdapter();
			mCursor = mRingtoneManager.getNewCursor();
			((SimpleCursorAdapter) adapter).changeCursor(mCursor);
			MtkLog.d(TAG, "onResume: notify adapter update listview with new cursor!");
		} else {
			MtkLog.e(TAG, "onResume: cursor adapter is null!");
		}
		/// Get position from ringtone list with this uri, if the return
		/// position is
		/// valid value, set it to be current clicked position
		if ((mClickedPos >= mStaticItemCount || (mHasSilentItem && mClickedPos == 1)) && (null != mExistingUri)) {
			/// M: TODO avoid cursor out of bound, so move cursor position.
			if (null != mCursor && mCursor.moveToFirst()) {
				mClickedPos = getListPosition(mRingtoneManager.getRingtonePosition(mExistingUri));

				if(HBRingtoneManager.isRingtoneExist(getApplicationContext(), mExistingUri)){
					mClickedPos = getListPosition(mRingtoneManager.getRingtonePosition(mExistingUri));	
				}else{
					mExistingUri = HBRingtoneManager.getInternalDefaultRingtone(getApplicationContext(), mType);
					mClickedPos = getListPosition(mRingtoneManager.getRingtonePosition(mExistingUri));
				}
				
				MtkLog.d(TAG, "onResume: get the position of uri = " + mExistingUri + ", position = " + mClickedPos);
				if (POS_UNKNOWN != mClickedPos) {
					 //mCheckedItem = mClickedPos;
				} else {
					MtkLog.w(TAG, "onResume: get position is invalid!");
				}
			}
		}else if(POS_UNKNOWN == mClickedPos && HBRingtoneManager.isRingtoneExist(getApplicationContext(), mExistingUri)){
			mClickedPos = 0;
		}else if (POS_UNKNOWN == mClickedPos) {
			MtkLog.w(TAG, "onResume: no ringtone checked, show default instead!");
			if (mHasDefaultItem) {
				mClickedPos = mDefaultRingtonePos;
			} else {
				if (null != mCursor && mCursor.moveToFirst()) {
					mClickedPos = getListPosition(mRingtoneManager.getRingtonePosition(
							HBRingtoneManager.getDefaultRingtoneUri(getApplicationContext(), mType)));
				}
			}
		}
		ringToneAdapter.notifyDataSetChanged();
		//listView.setItemChecked(mClickedPos, true);
		//listView.setSelection(mClickedPos);
		mNeedRefreshOnResume = false;
		MtkLog.d(TAG, "onResume<<<: set position to be checked: mClickedPos = " + mClickedPos);
	}

	/// M: Add to close cursor if the cursor is not close
	/// when activity destroy and remove messages
	@Override
	protected void onDestroy() {
		mHandler.removeCallbacksAndMessages(null);
		if (mCursor != null && !mCursor.isClosed()) {
			mCursor.close();
		}
		super.onDestroy();
	}

	/// M: Add to handle user choose a ringtone from MusicPicker
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		switch (requestCode) {
		case ADD_MORE_RINGTONES:
			if (resultCode == RESULT_OK) {
				Uri uri = (null == intent ? null : intent.getData());
				if (uri != null) {
					setRingtone(this.getContentResolver(), uri);
					MtkLog.v(TAG, "onActivityResult: RESULT_OK, so set to be ringtone! " + uri);

                    //hummmingbird liuqin add for bugfix #2182 20170615 start
                    mExistingUri = uri;
                    onClick();
                    //hummmingbird liuqin add for bugfix #2182 20170615 end
				}
				oldIndex = -1;
			} else {
				MtkLog.v(TAG, "onActivityResult: Cancel to choose more ringtones, " + "so do nothing!");
				mClickedPos = oldIndex;
				oldIndex = -1;
				listView.invalidate();
			}
			break;
		}
	}

	/**
	 * M: Set the given uri to be ringtone
	 *
	 * @param resolver
	 *            content resolver
	 * @param uri
	 *            the given uri to set to be ringtones
	 */
	private void setRingtone(ContentResolver resolver, Uri uri) {
		/// Set the flag in the database to mark this as a ringtone
		try {
			ContentValues values = new ContentValues(1);
			if ((HBRingtoneManager.TYPE_RINGTONE == mType) || (HBRingtoneManager.TYPE_VIDEO_CALL == mType)
					|| (HBRingtoneManager.TYPE_SIP_CALL == mType)) {
				values.put(MediaStore.Audio.Media.IS_RINGTONE, "1");
			} else if (HBRingtoneManager.TYPE_ALARM == mType) {
				values.put(MediaStore.Audio.Media.IS_ALARM, "1");
			} else if (HBRingtoneManager.TYPE_NOTIFICATION == mType) {
				values.put(MediaStore.Audio.Media.IS_NOTIFICATION, "1");
			} else {
				MtkLog.e(TAG, "Unsupport ringtone type =  " + mType);
				return;
			}
			resolver.update(uri, values, null, null);
			/// Restore the new uri and set it to be checked after resume
			mExistingUri = uri;
		} catch (UnsupportedOperationException ex) {
			/// most likely the card just got unmounted
			MtkLog.e(TAG, "couldn't set ringtone flag for uri " + uri);
		}
        //hummmingbird liuqin modify for bugfix #2182 20170615 start
        // onClick();
        //hummmingbird liuqin modify for bugfix #2182 20170615 end
	}

	private View oldCheckedTextView;
	
	private Handler setNameHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			mFirstRingtone = HBRingtoneManager.getRingtone(HbRingtonePickerActivity.this, mExistingUri);
			if(msg.obj!=null && msg.obj instanceof TextView){
				TextView textView = (TextView)msg.obj;
				if(Integer.parseInt(textView.getTag().toString())==mMoreRingtonesPos){
					textView.setVisibility(View.VISIBLE);
					textView.setText(mFirstRingtone.getTitle(HbRingtonePickerActivity.this));
				}				
			}						
		};
	};
	class RingToneAdapter extends BaseAdapter {

		int textIndex = 0;

		public RingToneAdapter() {
			textIndex = mCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
		}

		@Override
		public int getCount() {
			return mCursor.getCount()+mStaticItemCount;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.music_picker_item, parent, false);
				convertView.setTag(ViewHolder.getViewHodel(convertView));
			}
			ViewHolder viewHodel = (ViewHolder)convertView.getTag();	
			if(position == 0){
				viewHodel.title.setText(R.string.self_ringtone);
				viewHodel.title.setVisibility(View.VISIBLE);
			}else if(position == 1){
				viewHodel.title.setText(R.string.system_ringtone);
				viewHodel.title.setVisibility(View.VISIBLE);
			}else{
				viewHodel.title.setVisibility(View.GONE);
			}
			viewHodel.nameTextView.setText("");	
			viewHodel.nameTextView.setVisibility(View.GONE);
			viewHodel.nextPage.setImageDrawable(null);
			viewHodel.radioButton.setVisibility(View.VISIBLE);
			viewHodel.nameTextView.setTag(position);
			if (mHasMoreRingtonesItem && mMoreRingtonesPos == position) {
				
				viewHodel.textView.setText(R.string.local_audio);
				viewHodel.nextPage.setImageResource(com.hb.R.drawable.ic_next_page);
				viewHodel.radioButton.setVisibility(View.GONE);
				if(mClickedPos == mMoreRingtonesPos){
					Message msg = new Message();
					msg.obj = viewHodel.nameTextView;
					setNameHandler.sendMessage(msg );					
				}
			} else if (mHasDefaultItem && mDefaultRingtonePos == position) {
				viewHodel.textView.setText(com.android.internal.R.string.ringtone_default);
			} else if (mHasSilentItem && mSilentPos == position) {
				viewHodel.textView.setText(com.android.internal.R.string.ringtone_silent);
			} else {
				int nowPosition = getRingtoneManagerPosition(position);
				if (mCursor.moveToPosition(nowPosition)) {
					viewHodel.textView.setText(mCursor.getString(textIndex));
				}
			}
			
			if(mClickedPos == position){
				oldCheckedTextView = convertView;
			}
			viewHodel.radioButton.setChecked(mClickedPos == position);
			return convertView;
		}

	}
	
	public static class ViewHolder{
		public TextView title;
		public TextView textView;
		public RadioButton radioButton;
		public TextView nameTextView;
		public ImageView nextPage;
		public CharArrayBuffer buffer1;
		
		public static ViewHolder getViewHodel(View convertView){
			ViewHolder viewHodel = new ViewHolder();
			viewHodel.title = (TextView) convertView.findViewById(R.id.title);
			viewHodel.textView = (TextView) convertView.findViewById(android.R.id.text1);
			viewHodel.radioButton = (RadioButton) convertView.findViewById(R.id.checked_mark);
			viewHodel.nameTextView = (TextView) convertView.findViewById(R.id.audio_name);
			viewHodel.nextPage = (ImageView) convertView.findViewById(R.id.next_page);
			
			viewHodel.radioButton.setClickable(false);
			viewHodel.radioButton.setFocusable(false);
			return viewHodel;
		}		
	}

	public void onClick() {
		// Stop playing the previous ringtone
		mRingtoneManager.stopPreviousRingtone();
		//switch (v.getId()) {
		//case R.id.cancel:
			//setResult(RESULT_CANCELED);
			//break;
		//case R.id.ok:
			Intent resultIntent = new Intent();
			Uri uri = null;

			if (mClickedPos == mDefaultRingtonePos) {
				// Set it to the default Uri that they originally gave us
				uri = mUriForDefaultItem;
				/// M: When the ringtone list has not init,
				/// we should set the ringtone to be null instead
				/// set to to be mUriForDefaultItem,
				/// because we should not set value to ringtone before
				/// MediaScanner init
				/// these ringtone uri. {@
				if (mDefaultRingtonePos == POS_UNKNOWN) {
					uri = null;
					MtkLog.w(TAG, "onClick with no list item, " + "set uri to be null! mDefaultRingtonePos = "
							+ mDefaultRingtonePos);
				}
				/// @}
			} else if (mClickedPos == mSilentPos) {
				// A null Uri is for the 'Silent' item
				uri = null;
			} else {
				uri = mExistingUri;//mRingtoneManager.getRingtoneUri(getRingtoneManagerPosition(mClickedPos));
			}

			resultIntent.putExtra(HBRingtoneManager.EXTRA_RINGTONE_PICKED_URI, uri);
			/// M: return the picked position to caller to distinguish default
			/// item
			/// and the same ringtone item.
			resultIntent.putExtra(HBRingtoneManager.EXTRA_RINGTONE_PICKED_POSITION, mClickedPos);
			setResult(RESULT_OK, resultIntent);
			//break;
		//}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		/// M: Show MusicPicker activity to let user choose song to be ringtone
		/// @{
		if (position == mMoreRingtonesPos) {
			oldIndex = mClickedPos;
			Intent intent = new Intent(this, HbMusicPickerActivity.class);
			intent.addCategory(Intent.CATEGORY_DEFAULT);
			intent.addCategory(Intent.CATEGORY_OPENABLE);
			intent.setType("vnd.android.cursor.dir/audio");
			intent.setData(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
			intent.putExtra(HBRingtoneManager.EXTRA_RINGTONE_EXISTING_URI, mExistingUri);
			intent.putExtra(HBRingtoneManager.EXTRA_RINGTONE_TYPE, mType);
            // hummingbird liuqin add to show all local music 20170713 start
            intent.putExtra(com.mediatek.drm.OmaDrmStore.DrmExtra.EXTRA_DRM_LEVEL, com.mediatek.drm.OmaDrmStore.DrmExtra.LEVEL_ALL);
            // hummingbird liuqin add to show all local music 20170713 end
			startActivityForResult(intent, ADD_MORE_RINGTONES);		
			/// @}
		} else {
			// Save the position of most recently clicked item
			// Play clip
			playRingtone(position, 0);
			/// M: save the uri of current position
			
			mExistingUri = mRingtoneManager.getRingtoneUri(getRingtoneManagerPosition(position));
		}
		mClickedPos = position;
		if(oldCheckedTextView!=null){
			RadioButton textView = (RadioButton) oldCheckedTextView.findViewById(R.id.checked_mark);
			TextView nameTextView = (TextView) oldCheckedTextView.findViewById(R.id.audio_name);
			if(oldCheckedTextView!=null && oldCheckedTextView != v){
				textView.setChecked(false);
			}
			nameTextView.setText("");
		}		
		oldCheckedTextView = v;
		RadioButton newTextView = (RadioButton) v.findViewById(R.id.checked_mark);
		newTextView.setChecked(true);
		onClick();
	
	}

	//hummmingbird liuqin add for bugfix #2215 20170617 start
	private static final boolean IS_USE_CUSTOM_MEDIAPLAY = true;
	private MediaPlayer mMediaPlayer;
	private AudioManager mAudioManager;

	private void playRingtone(Ringtone ringtone) {
		// do not play ringtones if stream volume is 0
		// (typically because ringer mode is silent).
//		if (getAudioManager().getStreamVolume(
//				android.media.AudioAttributes.toLegacyStreamType(ringtone.getAudioAttributes())) == 0) {
//			return;
//		}
		if (getAudioManager().getStreamVolume(AudioManager.STREAM_MUSIC) <= 0) {
			return;
		}

		stopMediaPlayer();
		if (mMediaPlayer == null) {
            mMediaPlayer = new MediaPlayer();
            try {
                mMediaPlayer.setDataSource(this, ringtone.getUri());
//				mMediaPlayer.setAudioAttributes(ringtone.getAudioAttributes());
				mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					@Override
					public void onCompletion(MediaPlayer mp) {
						mp.stop();
						mp.release();
						if (mMediaPlayer == mp) {
							mMediaPlayer = null;

							HbMusicPickerActivity.abandonAudioFocus(getAudioManager());
						}
					}
				});
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.prepare();
                mMediaPlayer.start();

                HbMusicPickerActivity.requestAudioFocus(getAudioManager());
            } catch (IOException e) {
				e.printStackTrace();
            }
        }
	}

	void stopMediaPlayer() {
		if (mMediaPlayer != null) {
			mMediaPlayer.stop();
			mMediaPlayer.release();
			mMediaPlayer = null;

			HbMusicPickerActivity.abandonAudioFocus(getAudioManager());
		}
	}

    private AudioManager getAudioManager() {
		if (mAudioManager == null) {
			mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		}
		return mAudioManager;
    }
    //hummmingbird liuqin add for bugfix #2215 20170617 end
}
