package com.eastaeon.agingtest;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

public class ReceiverTest extends Activity implements View.OnClickListener {
    private AudioManager mAudioManager = null;
    private MediaPlayer mp = null;
    private final static int MESSAGE_START_PLAY = 1;
    private final static int MESSAGE_STOP_PLAY = 2;
    private final static int TEST_FAIL = 99;
    Button mStop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_receiver_test);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), AudioManager.FLAG_PLAY_SOUND);
        mStop = (Button) findViewById(R.id.id_stop_receiver);
        mStop.setOnClickListener(this);
    }

    private void InitMediaPlayer() {
        mp = new MediaPlayer();
        int max = mAudioManager.getStreamMaxVolume( AudioManager.STREAM_VOICE_CALL );
        mp.setVolume(max, max);
        try {
            mp.setDataSource(this,
                    RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_ALARM));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    Handler mPlayerHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_START_PLAY:
                    InitMediaPlayer();
                    mAudioManager.setMode(2);
                    try {
                        Thread.sleep(500);
                    } catch (java.lang.InterruptedException e) {
                    }

                    mp.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
                    mp.setLooping(true);
                    try {
                        mp.prepare();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mp.start();
                    break;
                case MESSAGE_STOP_PLAY:
                    mp.stop();
                    mp.release();
                    mAudioManager.setMode(0);
                    break;
                default:
                    break;
            }
        };
    };
    @Override
    protected void onPause() {
        super.onPause();
        mPlayerHandler.sendEmptyMessage(MESSAGE_STOP_PLAY);
        mAudioManager.setMode(0);
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPlayerHandler.sendEmptyMessage(MESSAGE_START_PLAY);
        handler.postDelayed(runnable, 1800000);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_stop_receiver:
                setResult(TEST_FAIL);
                finish();
        }
    }

    Handler handler=new Handler();
    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            setResult(Activity.RESULT_OK);
            handler.postDelayed(this, 2000);
            finish();
        }
    };

    private long mExitTime;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                Object mHelperUtils;
                Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                mExitTime = System.currentTimeMillis();

            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
