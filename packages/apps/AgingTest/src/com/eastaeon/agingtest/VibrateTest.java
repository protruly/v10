package com.eastaeon.agingtest;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class VibrateTest extends Activity implements View.OnClickListener{
    private final static String LOGTAG = "VibrateTest";
    private final static int TEST_FAIL = 99;
    // Set Vibrate frequency, vibrate 500ms, stop 500ms
    // long[] mVibFreq = {500,500};
    long[] mVibFreq = {
            10, 500, 500, 500
    }; // zhangle add this one line code for movego while delete the above code
    private Vibrator mVibrator;
    private NotificationManager mLed;
    // Notification for LED test
    private Notification mNotification;
    // timer handle event
    private static final int TIMER_EVENT_TICK = 1;
    // Notificatin ID
    private static final int NOTIFY_LED = 0x1010;

    /*
     * The method id for restart vibrate & LED
     */
    private void changeVibratorLedStatus() {
        // Stop the predecessor LED test
        mLed.cancel(NOTIFY_LED);
        // change LED color
        if (Color.RED == mNotification.ledARGB) {
            mNotification.ledARGB = Color.GREEN;
        } else if (Color.GREEN == mNotification.ledARGB) {
            mNotification.ledARGB = Color.RED;
        }
        // Set new LED config
        mLed.notify(NOTIFY_LED, mNotification);

        // start new vibrator
        // mVibrator.vibrate(this.mVibFreq, -1);
    }

    // TIMER_EVENT_TICK handler
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case TIMER_EVENT_TICK:
                    changeVibratorLedStatus();
                    // send new TIMER_EVENT_TICK message
                    sendEmptyMessageDelayed(TIMER_EVENT_TICK, 1000);
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // hide title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_vibrate_test);

        ((Button) findViewById(R.id.id_start_vibrate)).setOnClickListener(this);
        ((Button) findViewById(R.id.id_stop_vibrate)).setOnClickListener(this);

        mVibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

        mLed = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotification = new Notification.Builder(this).setSmallIcon(R.drawable.icon)
                .getNotification();
        mNotification.flags |= Notification.FLAG_SHOW_LIGHTS;
        mNotification.ledARGB = Color.RED;
            // mNotification.setSmallIcon(R.drawable.icon);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeMessages(TIMER_EVENT_TICK);
        mVibrator.cancel();
        mLed.cancel(NOTIFY_LED);
        handler.removeCallbacks(runnable);
    }
    Handler handler=new Handler();
    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            setResult(Activity.RESULT_OK);
            handler.postDelayed(this, 2000);
            finish();
        }
    };
    @Override
    protected void onResume() {
        super.onResume();
        mLed.notify(NOTIFY_LED, mNotification);
        mVibrator.vibrate(mVibFreq, 2);

        mHandler.sendEmptyMessageDelayed(TIMER_EVENT_TICK, 1000);
        //handler.postDelayed(runnable, 1800000);
        handler.postDelayed(runnable, 1800000);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_stop_vibrate:
                setResult(TEST_FAIL);
                finish();
        }
    }

    private long mExitTime;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                Object mHelperUtils;
                Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                mExitTime = System.currentTimeMillis();

            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
