#!/bin/bash

echo ""
echo "# begin fota properties"
echo "ro.fota.platform=MTK6797T_6.0"
#type info: phone, pad ,box, tv
echo "ro.fota.type=phone"
echo "ro.fota.app=5"
#oem info
echo "ro.fota.oem=protruly6797T_6.0"
#model info, Settings->About phone->Model number
FotaDevice=$(grep "ro.product.model=" "$1"|awk -F "=" '{print $NF}' )
echo "ro.fota.device=$FotaDevice" | sed 's/\$//' | sed 's/\///' | sed 's/\\//' | sed 's/\&//'
#version number, Settings->About phone->Build number
#FotaVersion=$(grep "ro.build.display.id=" "$1"|awk -F "=" '{print $NF}' )
FotaVersion=$(grep "ro.build.display.id=" "$1"|awk -F "=" '{print $NF}' )
#FotaVersion=$(grep "ro.sw.version=" "$1"|awk -F "=" '{print $NF}' )`date +_%Y%m%d-%H%M`
echo "ro.fota.version=$FotaVersion" | sed 's/\$//' | sed 's/\///' | sed 's/\\//' | sed 's/\&//'
echo "# end fota properties"
