
package com.zte.engineer;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ServiceManager;
import android.os.RemoteException;
import android.provider.Settings;
import android.os.Handler;
import android.content.res.Resources;
import android.location.LocationManager;
import android.location.LocationListener;
import android.location.Location;
import android.content.ContentResolver;

public class AutoTest extends Activity {

    private static final String TAG = "AutoTest";

    /* add by zhoudawei for factory reset 20110729 start */
    private int unusefulcode = 0;
    /* add by zhoudawei for factory reset 20110729 start */

    //chengrq start
    Boolean[] resultList;
    //end
	private List<String> result =  new ArrayList<String>();
    private List<Integer> reCode =  new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
		final ContentResolver resolver = this.getContentResolver();
		Settings.Secure.setLocationProviderEnabled(resolver, LocationManager.GPS_PROVIDER, true);
		LocationListener listener =new MyLocationListener();
		Intent it = new Intent("com.mediatek.ygps.YGPSService").setPackage("com.mediatek.ygps");
		this.startService(it);
		try {
		LocationManager mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		if (mLocationManager != null) {
			mLocationManager.requestLocationUpdates("gps", 0, 0,
					listener);
		}
		}catch(Exception e){
		}		
        initTestList();

        int index = 0;
        Intent intent = MyApplication.AUTO_TEST_INTENTS.get(index);

        startActivityForResult(intent, index);
    }

	private class MyLocationListener implements LocationListener{

		@Override
		public void onLocationChanged(Location location) {
		
		}

		@Override
		public void onProviderDisabled(String provider) {

		}

		@Override
		public void onProviderEnabled(String provider) {

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {

		}
    	
    }	
	
    private void initTestList() {
        Util.log(TAG, "auto test list count:"
                + MyApplication.AUTO_TEST_INTENTS.size());

        //chengrq start
        int count = MyApplication.AUTO_TEST_INTENTS.size() - 1;
        resultList = new Boolean[count];
        for(int i=0;i<count;i++) {
            resultList[i] = false; 
        }

        //writeFlagEx();
        //end
		for(int i = 0; i< count-1; i++){
			Resources r = getResources();
			String name = r.getString(MyApplication.KEYS_AUTO_TEST.get(i));
			Util.log(TAG, "auto test list name:" + name);
			result.add(name);
		}
		if (result.size()>0){
			for (int i = 0; i < result.size(); i++) {
				reCode.add(10);
			}
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MyApplication.AUTO_TEST_INTENTS.size() - 2) {    // total_result
            resultCode = ZteActivity.RESULT_PASS;
            String name = getResources().getString(MyApplication.KEYS_AUTO_TEST.get(requestCode));
            Util.log("lq_autoTest","onActivityResult 3 requestCode = "+requestCode + " resultCode = " + resultCode + "; name = " + name);
        }
		if (resultCode>10&&requestCode<reCode.size()) {
			reCode.set(requestCode, 20);
		}
        int index = requestCode + 1;
        /* add by zhoudawei for factory reset 20110729 start */
        unusefulcode++;
        if (index < unusefulcode) {
            return;
        }
        /* add by zhoudawei for factory reset 20110729 start */

        if (index >= MyApplication.AUTO_TEST_INTENTS.size()) {
            finish();
            return;
        }

        //android.util.Log.e("chengrq","requestCode:"+requestCode);
        //android.util.Log.e("chengrq","resultCode:"+resultCode);
        if (resultCode != ZteActivity.RESULT_FAIL) {
            resultList[requestCode] = true;
        }
        // The last item before factory reset.
        final int lastItemIndex = MyApplication.AUTO_TEST_INTENTS.size() - 1;
        if (index == (lastItemIndex)) { // the index is real index + 1
            boolean flag = true;
            for (int i = 0; i < lastItemIndex; i++) {
                //android.util.Log.e("chengrq","list_result:"+list_result[i]);
                if (resultList[i] == false) {
                    flag = false;
                    break;
                }
            }
			Util.log(TAG,"flag = " + flag);
            //if (flag) {
            //    writeAutoTestFullPassedFlag(flag);
            //}
        }
		if (index == 16) {
			android.util.Log.d("zengtao", "open air");
		    Intent intentAir = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
			intentAir.putExtra("state", true);
			//Settings.Global.putInt(getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 1);			
			//sendBroadcast(intentAir);
			Settings.Secure.putInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE, android.provider.Settings.Secure.LOCATION_MODE_OFF); 
		}
		
		if (index == 29) {
			android.util.Log.d("zengtao", "open air");
		    //Intent intentAir = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
			//intentAir.putExtra("state", false);
			//Settings.Global.putInt(getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0);			
			//sendBroadcast(intentAir);			
		}

        mStep = index;
        android.util.Log.d("lq_AutoTest", "------> index = " + index + "; mStep = " + mStep);
        mHandler.removeCallbacks(mRunnable);
        mHandler.post(mRunnable);
        //startActivityForResult(index);
    }

    private void writeAutoTestFullPassedFlag(boolean isFullPassed) {
        IBinder binder = ServiceManager.getService("NvRAMAgent");

        NvRAMAgent agent = NvRAMAgent.Stub.asInterface(binder);

        byte[] buff = null;

        if (agent != null) {
            try {
                final int productInfoLid = MyApplication.getProductInfoLID();
                buff = agent.readFile(productInfoLid);
                android.util.Log.e("chengrq", "buff:"
                        +  buff[MyApplication.BIT_FLAG_IF_AUTO_TEST_FULL_PASSED]);
                if (isFullPassed) {
                    buff[MyApplication.BIT_FLAG_IF_AUTO_TEST_FULL_PASSED] = 1;
                } else {
                    buff[MyApplication.BIT_FLAG_IF_AUTO_TEST_FULL_PASSED] = 0;
                }
                agent.writeFile(productInfoLid,buff);
            } catch (RemoteException e) {
                android.util.Log.w("chengrq", "NvRAMAgent read file fail");
            }
        }
    }

    private int mStep = 0;
    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            mHandler.removeCallbacks(mRunnable);
            if (mStep >= 26 && mStep <= 29) {
                android.util.Log.d("lq_AutoTest", "------> run mStep = " + mStep);
                try {
                    Thread.sleep(2000);
                } catch (Exception e) {
                }
            }
			if (mStep == MyApplication.AUTO_TEST_INTENTS.size()-2) {
				Intent intent = MyApplication.AUTO_TEST_INTENTS.get(mStep);
				intent.putStringArrayListExtra("result", (ArrayList<String>) result);
				intent.putIntegerArrayListExtra("recode", (ArrayList<Integer>) reCode);
				startActivityForResult(intent, mStep);
			}else {
				Intent intent = MyApplication.AUTO_TEST_INTENTS.get(mStep);
				startActivityForResult(mStep);
			}
        }
    };

    private void startActivityForResult(int index) {
        Intent intent = MyApplication.AUTO_TEST_INTENTS.get(index);
        startActivityForResult(intent, index);
    }
}
