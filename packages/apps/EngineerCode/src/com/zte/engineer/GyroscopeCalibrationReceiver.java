
package com.zte.engineer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.os.Bundle;
import android.widget.Toast;

public class GyroscopeCalibrationReceiver extends BroadcastReceiver {
    private static final String TAG = "EngineerCode.GyroscopeCalibrationReceiver";
	private Toast mToast;
	private Context mContext;

    /**
     * Receive com.zte.engineer.action.LAUNCHER_TEST only.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
		mContext = context;
		String action = intent.getAction();
		System.out.println("BootReceiver---testrui---action = " + action);
        if (action.equals("send_gyroscope_calibration_value")) {
			Bundle bundle = new Bundle();
            bundle = intent.getExtras();
            int value = bundle.getInt("gyroscope_calibration_value");
			int result = 0;
			System.out.println("BootReceiver111---testrui---value = " + value);
			if (value != 0) {
			    result = Util.doGyroscopeCalibration(Util.TOLERANCE_20);
			} else {
				result = Util.clearGyroscopeCalibration();
			}
			if (result == Util.RET_SUCCESS) {
                    showToast("success");
            }else{
                    showToast("fail");
            }
        }
    }
	
	private void showToast(String msg) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(mContext, msg, Toast.LENGTH_SHORT);
        mToast.show();
    }

}
