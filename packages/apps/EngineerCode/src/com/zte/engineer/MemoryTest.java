
package com.zte.engineer;

import java.io.File;

import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.provider.Settings;
import android.view.KeyEvent;

public class MemoryTest extends ZteActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.normal);

        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long totalSize = Util.getTotalBytes(stat) / 1024 / 1024; // MB
        long availableSize = Util.getAvailableBytes(stat) / 1024 / 1024; // MB

        int flashSize = getGuessFlashSize();

        TextView mTextView = (TextView) findViewById(R.id.normal_textview);
        TextView mMemoryTotal = (TextView) findViewById(R.id.normal_textview2);
        TextView mMemoryAvailable = (TextView) findViewById(R.id.normal_textview3);

        mTextView.setText(R.string.memory);
        mMemoryTotal.setText(String.format(getResources().getString(R.string.flash_total), flashSize));
        mMemoryAvailable.setText(String.format(getResources().getString(R.string.sd_available),
                Long.toString(availableSize)));

        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);
    }

    private int getGuessFlashSize() {
        long sysImgSize = Util.getTotalBytes(
                new StatFs(Environment.getRootDirectory().getAbsolutePath())) / 1024 / 1024; // MB
        long dataImgSize = Util.getTotalBytes(
                new StatFs(Environment.getDataDirectory().getAbsolutePath())) / 1024 / 1024; // MB
        float flashSize = (int)((sysImgSize + dataImgSize) / 1024.0); // GB

        int aboutFlashSize = 0;
        for (int i = 0; ;i++) {
            int aboutVal = (int)Math.pow(2, i);
            if (aboutVal > flashSize) {
                aboutFlashSize = aboutVal;
                break;
            }
        }
        return aboutFlashSize;
    }	

	
    @Override
    protected void onResume() {
        // registerReceiver(screenoff, new IntentFilter(Screenoff));
        Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 1);
        super.onResume();

        // PhoneWindowManager.setKeyTestState(true);
    }

    @Override
    protected void onPause() {
        // unregisterReceiver(screenoff);
        Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 0);
        super.onPause();

        // PhoneWindowManager.setKeyTestState(false);
    }		
	
}
