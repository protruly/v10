
package com.zte.engineer;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.util.Log;
import android.widget.Toast;
import android.widget.GridView;
import android.content.ContentResolver;
import android.provider.Settings;
import android.location.LocationManager;
import android.location.LocationListener;
import android.location.Location;

public class EngineerCode extends Activity {

    private static final String STATE = "StateArrayList";

    public static final String AUTOTEST = "AUTOTEST";
    public static final String NORMALTEST = "NORMALTEST";

    ListView list;
    MyAdapter adapter;
    ItemContent[] items;

    GridView mGridView;

    private boolean autoTest = false;
    private boolean startAuto = false;
    private boolean autoTestFactory = false;
    private int nowPosition = 0;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null != getIntent().getStringExtra(AUTOTEST)) {
            autoTest = true;
            autoTestFactory = true;
        }
        setContentView(R.layout.main_grid);

        try {
            PackageManager pm = getPackageManager();
            PackageInfo pInfo = pm.getPackageInfo(getPackageName()
                    , PackageManager.GET_CONFIGURATIONS);
            setTitle(getString(R.string.app_name) + " v" + pInfo.versionName);
        } catch (PackageManager.NameNotFoundException pnnf) {
            System.out.println(pnnf);
        }

        // list =(ListView)findViewById(R.id.MainList);
        mGridView = (GridView) findViewById(R.id.MainGrid);

        items = new ItemContent[MyApplication.KEYS_MANUAL_TEST.size()];
        Resources r = getResources();
        ArrayList<SaveItems> itemState = null;
        if (null != savedInstanceState) {
            itemState = savedInstanceState.getParcelableArrayList(STATE);
        }
        if (null != itemState) {
            boolean checked = false;
            boolean pass = false;
            String title = null;
            for (int i = 0; i < MyApplication.KEYS_MANUAL_TEST.size(); i++) {
                checked = false;
                pass = false;
                title = r.getString(MyApplication.KEYS_MANUAL_TEST.get(i));
                for (Object o : itemState.toArray()) {
                    if (((SaveItems) o).title.equals(title)) {
                        checked = ((SaveItems) o).checked;
                        pass = ((SaveItems) o).pass;
                    }
                }
                items[i] = new ItemContent(title, checked, pass);
            }
        } else {
            for (int i = 0; i < MyApplication.KEYS_MANUAL_TEST.size(); i++) {
                items[i] = new ItemContent(
                        r.getString(MyApplication.KEYS_MANUAL_TEST.get(i)), false, false);
            }
        }

        adapter = new MyAdapter(this, R.layout.checkmark_list
                , R.id.text, /* R.id.checkbox,*/items);
        mGridView.setAdapter(adapter);
        // list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                implementItemClick(position);
            }
        };
        mGridView.setOnItemClickListener(mOnItemClickListener);
		final ContentResolver resolver = this.getContentResolver();
		Settings.Secure.setLocationProviderEnabled(resolver, LocationManager.GPS_PROVIDER, true);
		LocationListener listener =new MyLocationListener();
		Intent it = new Intent("com.mediatek.ygps.YGPSService").setPackage("com.mediatek.ygps");
		this.startService(it);
		try {
		LocationManager mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		if (mLocationManager != null) {
			mLocationManager.requestLocationUpdates("gps", 0, 0,
					listener);
		}
		}catch(Exception e){
		}		
    }

	private class MyLocationListener implements LocationListener{

		@Override
		public void onLocationChanged(Location location) {
		
		}

		@Override
		public void onProviderDisabled(String provider) {

		}

		@Override
		public void onProviderEnabled(String provider) {

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {

		}
    	
    }		
	
    private void implementItemClick(int position) {
        Intent intent = null;
        nowPosition = position;

        switch (MyApplication.KEYS_MANUAL_TEST.get(position)) {
            case R.string.camera: {
                if (android.hardware.Camera.getNumberOfCameras() <= 0) {
                    setItemFail(position);
                    return;
                }
            }
            break;

            case R.string.camera_front: {
                if (android.hardware.Camera.getNumberOfCameras() <= 0) {
                    setItemFail(position);
                    return;
                }
            }
            break;

            case R.string.common_camera: {
                if (android.hardware.Camera.getNumberOfCameras() <= 0) {
                    setItemFail(position);
                    return;
                }
            }
            break;

            case R.string.common_camera_front: {
                if (android.hardware.Camera.getNumberOfCameras() <= 0) {
                    setItemFail(position);
                    return;
                }
            }
            break;

            default:
                break;
        }
        intent = MyApplication.MANUAL_TEST_INTENTS.get(position);

        try {
            startActivityForResult(intent, position);
        } catch (ActivityNotFoundException ane) {
            System.err.println(ane);

            setItemFail(position);
        }
    }

    private void setItemFail(int position) {
        final int requestCode = position;
        items[requestCode].setChecked(true);
        items[requestCode].setPassed(false);
        adapter.replaceItems(items);
        mGridView.setAdapter(adapter);
        updateCompletedNum();
        mGridView.setSelection(requestCode);
        autoTest(requestCode);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCompletedNum();
        if (!startAuto) {
            startAuto = true;
            autoTest(-1);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (autoTestFactory && !autoTest) {
            startActivity(MyApplication.getFactoryResetIntent(false));
        }
    }

    private void updateCompletedNum() {
        /*
         * TextView t = (TextView)findViewById(R.id.completed); String s =
         * getString(R.string.completed); int count = items.length; int
         * completed = 0; for (int i = 0; i < count; i++) { if
         * (items[i].isChecked()) { completed++; } } s = s + completed + "/" +
         * count; t.setText(s);
         */
    }

    /**
     * requestCode is position of list, see @implementItemClick
     */
    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {

        if (resultCode == ZteActivity.RESULT_PASS) {
            items[requestCode].setChecked(true);
            items[requestCode].setPassed(true);
            adapter.replaceItems(items);
            mGridView.setAdapter(adapter);
            updateCompletedNum();
            mGridView.setSelection(requestCode);
            autoTest(requestCode);
        } else if (resultCode == ZteActivity.RESULT_FAIL) {
            items[requestCode].setChecked(true);
            items[requestCode].setPassed(false);
            adapter.replaceItems(items);
            mGridView.setAdapter(adapter);
            updateCompletedNum();
            mGridView.setSelection(requestCode);
            autoTest(requestCode);
        } else {
            boolean defaultPass = false;
            int [] specialTest = {R.string.radio_info, R.string.fm_test, R.string.camera,
                    R.string.camera_front, R.string.gps, R.string.radio_info, R.string.common_camera, R.string.common_camera_front,
                    /*R.string.ir,*/ /*R.string.recover_settings*/ };
            for (int testKey: specialTest) {
                int index = MyApplication.KEYS_MANUAL_TEST.indexOf(testKey);
                if (index == requestCode) {
                    defaultPass = true;
                    break;
                }
            }

            items[requestCode].setChecked(true);
            items[requestCode].setPassed(defaultPass);
            adapter.replaceItems(items);
            mGridView.setAdapter(adapter);
            updateCompletedNum();
            mGridView.setSelection(requestCode);
            autoTest(requestCode);
        }

    }

    public void autoTest(int requestCode) {
        if (autoTest && (requestCode + 1) < adapter.getCount() && (requestCode + 1) >= nowPosition) {
            implementItemClick(requestCode + 1);
        } else {
            autoTest = false;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        ArrayList<SaveItems> stateArray = new ArrayList<SaveItems>();
        for (ItemContent item : items) {
            stateArray.add(new SaveItems(item.isChecked(), item.isPassed(), item.getTitle()));
        }
        outState.putParcelableArrayList(STATE, stateArray);
        super.onSaveInstanceState(outState);
    }

    private class ItemContent {
        private boolean checked = false;
        private String title;
        private boolean pass = false;

        public ItemContent(String title, boolean checked, boolean pass) {
            this.title = title;
            this.checked = checked;
            this.pass = pass;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public void toggle() {
            checked = !checked;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setPassed(boolean pass) {
            this.pass = pass;
        }

        public boolean isPassed() {
            return pass;
        }
    }

    public class MyAdapter extends BaseAdapter {

        private Context mContext;
        private int layout;
        private int textID;
        // private int checkboxID;
        private ItemContent[] items;
        LayoutInflater mInflater;

        public MyAdapter(Context context, int layout, int textID,
                /* int checkboxID,*/ ItemContent[] items) {
            mInflater = LayoutInflater.from(context);
            this.layout = layout;
            this.textID = textID;
            // this.checkboxID = checkboxID;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.length;
        }

        @Override
        public Object getItem(int position) {
            return items[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflater.inflate(layout, null);
            }

            TextView mTextView = (TextView) convertView.findViewById(textID);
            // CheckBox mCheckBox = (CheckBox)
            // convertView.findViewById(checkboxID);

            mTextView.setText(items[position].getTitle());
            // mCheckBox.setClickable(false);
            // mCheckBox.setChecked(items[position].isChecked());
            convertView
                    .setBackgroundColor(items[position].isChecked() ? items[position].isPassed() ? Color
                            .rgb(0x00, 0x8b, 0x00) : Color.RED
                            : Color.rgb(0x30, 0x35, 0x39));

            return convertView;
        }

        public void replaceItems(ItemContent[] items) {
            this.items = items;
        }
    }
}

class SaveItems implements Parcelable {
    String title;
    boolean checked;
    boolean pass;

    public SaveItems(boolean checked, boolean pass, String title) {
        this.checked = checked;
        this.pass = pass;
        this.title = title;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeBooleanArray(new boolean[] {
                checked, pass
        });
        out.writeString(title);
    }

    public static final Parcelable.Creator<SaveItems> CREATOR = new Parcelable.Creator<SaveItems>() {
        public SaveItems createFromParcel(Parcel in) {
            return new SaveItems(in);
        }

        public SaveItems[] newArray(int size) {
            return new SaveItems[size];
        }
    };

    private SaveItems(Parcel in) {
        in.readBooleanArray(new boolean[] {
                checked, pass
        });
        title = in.readString();
    }
}
