package com.zte.engineer;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.SystemProperties;

import java.util.ArrayList;

public class MyApplication extends Application {

    //public static final boolean hasAgingTest = true;

    public static final ArrayList <Intent> AUTO_TEST_INTENTS = new ArrayList <Intent>();
    public static final ArrayList <Intent> MANUAL_TEST_INTENTS = new ArrayList <Intent>();

    public static final ArrayList <Integer> KEYS_AUTO_TEST = new ArrayList <Integer>();
    public static final ArrayList <Integer> KEYS_MANUAL_TEST = new ArrayList <Integer>();

    static {
        KEYS_AUTO_TEST.add(R.string.rf_flags);
        KEYS_AUTO_TEST.add(R.string.produce_information);
        KEYS_AUTO_TEST.add(R.string.key_test);
        KEYS_AUTO_TEST.add(R.string.touchpanel);
        KEYS_AUTO_TEST.add(R.string.memory);
        KEYS_AUTO_TEST.add(R.string.imei);
        KEYS_AUTO_TEST.add(R.string.SIM);
        KEYS_AUTO_TEST.add(R.string.gps);
        KEYS_AUTO_TEST.add(R.string.bt_detect);
        KEYS_AUTO_TEST.add(R.string.wifi_detect);		
        KEYS_AUTO_TEST.add(R.string.earphone_audio_loop);
        KEYS_AUTO_TEST.add(R.string.fm_test);
        KEYS_AUTO_TEST.add(R.string.audio_loop);
        KEYS_AUTO_TEST.add(R.string.audio_refmic_loop);
        KEYS_AUTO_TEST.add(R.string.audio_receiver);		
        KEYS_AUTO_TEST.add(R.string.sd_info);
        KEYS_AUTO_TEST.add(R.string.vibrator);
        KEYS_AUTO_TEST.add(R.string.lcd);
        KEYS_AUTO_TEST.add(R.string.ringer);
        KEYS_AUTO_TEST.add(R.string.sensor);
		KEYS_AUTO_TEST.add(R.string.gyroscope);
        KEYS_AUTO_TEST.add(R.string.flashlight);
        KEYS_AUTO_TEST.add(R.string.backlight);
        KEYS_AUTO_TEST.add(R.string.battery_info);
		KEYS_AUTO_TEST.add(R.string.smartpa_calibration);
        /*KEYS_AUTO_TEST.add(R.string.lcd_off);*/
        /*KEYS_AUTO_TEST.add(R.string.imsi);*/
        /*KEYS_AUTO_TEST.add(R.string.radio_info);*/
        /*KEYS_AUTO_TEST.add(R.string.ir);*/
        /*KEYS_AUTO_TEST.add(R.string.tp_version);*/
        KEYS_AUTO_TEST.add(R.string.fingerprint_test);
        KEYS_AUTO_TEST.add(R.string.camera);
        KEYS_AUTO_TEST.add(R.string.camera_front);
        KEYS_AUTO_TEST.add(R.string.common_camera);			
        KEYS_AUTO_TEST.add(R.string.common_camera_front);
		KEYS_AUTO_TEST.add(R.string.total_result);	
        KEYS_AUTO_TEST.add(R.string.recover_settings);

        // MANUAL TEST LIST
		KEYS_MANUAL_TEST.add(R.string.produce_information);
        KEYS_MANUAL_TEST.add(R.string.rf_flags);
        /*KEYS_MANUAL_TEST.add(R.string.produce_information);*/
        KEYS_MANUAL_TEST.add(R.string.key_test);
        KEYS_MANUAL_TEST.add(R.string.touchpanel);
        KEYS_MANUAL_TEST.add(R.string.memory);
        KEYS_MANUAL_TEST.add(R.string.sd_info);
        KEYS_MANUAL_TEST.add(R.string.vibrator);
        KEYS_MANUAL_TEST.add(R.string.lcd);
        KEYS_MANUAL_TEST.add(R.string.audio_loop);
        KEYS_MANUAL_TEST.add(R.string.audio_refmic_loop);
        KEYS_MANUAL_TEST.add(R.string.audio_receiver);
        KEYS_MANUAL_TEST.add(R.string.ringer);
        KEYS_MANUAL_TEST.add(R.string.imei);
        KEYS_MANUAL_TEST.add(R.string.earphone_audio_loop);
        KEYS_MANUAL_TEST.add(R.string.fm_test);
        KEYS_MANUAL_TEST.add(R.string.sensor);
		KEYS_MANUAL_TEST.add(R.string.gyroscope);
        KEYS_MANUAL_TEST.add(R.string.camera);
        KEYS_MANUAL_TEST.add(R.string.camera_front);
		KEYS_MANUAL_TEST.add(R.string.common_camera);
		KEYS_MANUAL_TEST.add(R.string.common_camera_front);
        KEYS_MANUAL_TEST.add(R.string.flashlight);
        KEYS_MANUAL_TEST.add(R.string.backlight);
        KEYS_MANUAL_TEST.add(R.string.bt_detect);
        KEYS_MANUAL_TEST.add(R.string.wifi_detect);
        KEYS_MANUAL_TEST.add(R.string.battery_info);
        /*KEYS_MANUAL_TEST.add(R.string.lcd_off);*/
        KEYS_MANUAL_TEST.add(R.string.imsi);
        /*KEYS_MANUAL_TEST.add(R.string.radio_info);*/
        /*KEYS_MANUAL_TEST.add(R.string.ir);*/
        /*KEYS_MANUAL_TEST.add(R.string.tp_version);*/
        KEYS_MANUAL_TEST.add(R.string.SIM);
        KEYS_MANUAL_TEST.add(R.string.gps);
        KEYS_MANUAL_TEST.add(R.string.fingerprint_test);
		KEYS_MANUAL_TEST.add(R.string.smartpa_calibration);
        KEYS_MANUAL_TEST.add(R.string.recover_settings);
    };

    /* 重力传感器测试项 */// accelerometer sensor
    public static final boolean HAS_GRAVITY_SENSOR = true;

    public static final boolean HAS_STEPCOUNTER_SENSOR = false;

    /* 陀螺仪传感器测试项 */
    public static final boolean HAS_GYROSCOPE_SENSOR = true;

    /* 地磁传感器测�?*/
    public static final boolean HAS_MAGNETIC_SENSOR = true;

    /* 霍尔(皮套)传感器测�?*/
    public static final boolean HAS_HALL_SENSOR = true;

    /* 光线传感器测�?*/
    public static final boolean HAS_LIGHT_SENSOR = true;

    /* 距离传感器测试项 */
    public static final boolean HAS_PROXIMITY_SENSOR = true;

    /* led 指示灯功�?*/
    public static final boolean HAS_NOTIFY_LED_LIGHT = true;

    // 自动测试是否通过标志位的位置 1006，勿改！
    public static final int BIT_FLAG_IF_AUTO_TEST_FULL_PASSED = 1006;

    @Override
    public void onCreate() {
        super.onCreate();

        initAutoTestIntents();
        initManualTestIntents();
    }

    private void initAutoTestIntents() {
        AUTO_TEST_INTENTS.clear();
        initTestIntentsMatch(AUTO_TEST_INTENTS, KEYS_AUTO_TEST, true);
    }

    private void initManualTestIntents() {
        MANUAL_TEST_INTENTS.clear();
        initTestIntentsMatch(MANUAL_TEST_INTENTS, KEYS_MANUAL_TEST, false);
    }

    private void initTestIntentsMatch(final ArrayList<Intent> intentList
            , final ArrayList<Integer> testKeysId, final boolean isAutoTest) {
        for (Integer iKeyStringId : testKeysId) {
            switch (iKeyStringId) {
                case R.string.rf_flags:
                    intentList.add(newIntent(this, BoardCode.class, isAutoTest));
                    break;

                case R.string.produce_information:
                    intentList.add(newIntent(this, ProduceInfoListView.class, isAutoTest));
                    break;

                case R.string.key_test:
                    intentList.add(newIntent(this, KeyTest.class, isAutoTest));
                    break;

                case R.string.touchpanel:
                    intentList.add(newIntent(this, TouchScreenTest.class, isAutoTest));
                    break;

                case R.string.memory:
                    intentList.add(newIntent(this, MemoryTest.class, isAutoTest));
                    break;

                case R.string.sd_info:
                    intentList.add(newIntent(this, SDcardTest.class, isAutoTest));
                    break;

                case R.string.vibrator:
                    intentList.add(newIntent(this, VibratorTest.class, isAutoTest));
                    break;

                case R.string.lcd:
                    intentList.add(newIntent(this, LcdTestActivity.class, isAutoTest));
                    break;

                case R.string.audio_loop:
                    intentList.add(newIntent(this, AudioLoopTest.class, isAutoTest));
                    break;

                case R.string.audio_refmic_loop:
                    intentList.add(newIntent(this, AudioLoopRfTest.class, isAutoTest));
                    break;

                case R.string.audio_receiver:
                    intentList.add(newIntent(this, ReciverTest.class, isAutoTest));
                    break;
				case R.string.gyroscope:
						intentList.add(newIntent(this, GyroscopeTest.class, isAutoTest));
						break;

                case R.string.ringer:
                    intentList.add(newIntent(this, RingerTest.class, isAutoTest));
                    break;

                case R.string.imei:
                    intentList.add(newIntent(this, ImeiTest.class, isAutoTest));
                    break;

                case R.string.earphone_audio_loop:
                    intentList.add(newIntent(this, EarPhoneAudioLoopTest.class
                            , isAutoTest));
                    break;

                case R.string.fm_test:
                    intentList.add(getFmTestIntent(isAutoTest));
                    break;

                case R.string.sensor:
                    intentList.add(newIntent(this, SensorTest.class, isAutoTest));
                    break;

                case R.string.common_camera_front:
                    intentList.add(getCommonFrontCameraTestIntent(isAutoTest));
                    break;

                case R.string.common_camera:
                    intentList.add(getCommonBackCameraTestIntent(isAutoTest));
                    break;

                case R.string.camera_front:
                    intentList.add(getFrontCameraTestIntent(isAutoTest));
                    break;

                case R.string.camera:
                    intentList.add(getBackCameraTestIntent(isAutoTest));
                    break;

                case R.string.flashlight:
                    intentList.add(newIntent(this, FlashLightTest.class, isAutoTest));
                    break;

                case R.string.backlight:
                    intentList.add(newIntent(this, BacklightTest.class, isAutoTest));
                    break;

                case R.string.bt_detect:
                    intentList.add(newIntent(this, BTAddressTest.class, isAutoTest));
                    break;

                case R.string.wifi_detect:
                    intentList.add(newIntent(this, WifiAddressTest.class, isAutoTest));
                    break;

                case R.string.battery_info:
                    intentList.add(newIntent(this, BatteryLog.class, isAutoTest));
                    break;

                case R.string.lcd_off:
                    intentList.add(newIntent(this, LcdOffTest.class, isAutoTest));
                    break;

                case R.string.imsi:
                    intentList.add(newIntent(this, ImsiTest.class, isAutoTest));
                    break;

                case R.string.radio_info:
                    intentList.add(newIntent("com.android.settings",
                            "com.android.settings.RadioInfo", isAutoTest));
                    break;

                case R.string.ir:
                    //intentList.add(newIntent(this, IrTestActivity.class, isAutoTest));
                    intentList.add(newIntent("com.generalremote.ettest",
                            "com.general.ettest.Start", isAutoTest));
                    break;

                case R.string.tp_version:
                    intentList.add(newIntent(this, TPFirmwareVerTest.class, isAutoTest));
                    break;

                case R.string.SIM:
                    intentList.add(newIntent(this, SIMTest.class, isAutoTest));
                    break;

                case R.string.gps:
                    intentList.add(newIntent("com.mediatek.ygps"
                            , "com.mediatek.ygps.YgpsActivityTest", isAutoTest));
                    break;
					
				case R.string.total_result:
					intentList.add(newIntent(this, ResultList.class, isAutoTest));
                    break;                     
 
                case R.string.recover_settings:
                    intentList.add(getFactoryResetIntent(isAutoTest));
                    break;
                
				case R.string.temp_test:
				    intentList.add(newIntent(this, TempSenTest.class, isAutoTest));
                    break;

                case R.string.fingerprint_test:
                    intentList.add(newIntent(this, FingerPrintTest.class, isAutoTest));
                    break;
				case R.string.smartpa_calibration:
                    intentList.add(newIntent("com.eastaeon.smartpacalibration",
                            "com.eastaeon.smartpacalibration.SmartPAActivity", isAutoTest));
					break;
                default:
                    throw new RuntimeException("Miss intent match!!!");
            }
        }

    }

    private Intent newIntent(Context packageContext, Class<?> cls, boolean isAutoTest) {
        Intent intent = new Intent(packageContext, cls);
        intent.putExtra(ZteActivity.EXTRA_IS_AUTOTEST, isAutoTest);
        return intent;
    }

    private Intent newIntent(String packageName, String className, boolean isAutoTest) {
        Intent i = new Intent();
        i.setClassName(packageName, className);
        i.putExtra(ZteActivity.EXTRA_IS_AUTOTEST, isAutoTest);
        return i;
    }

    // AP_CFG_REEB_PRODUCT_INFO_LID defined in a path like:
    // vendor/mediatek/proprietary/custom/aeon6735m_65u_l/cgen/inc/Custom_NvRam_LID.h
    public static final int getProductInfoLID() {
        String[] exPlatforms = {"mt6752", "mt6795", "mt6735", "mt8163",
                "mt6580", "mt8173", "mt6755", "mt6797"};
        String prop1 = SystemProperties.get("ro.mediatek.platform");
        String prop2 = SystemProperties.get("ro.board.platform");
        for (String platform: exPlatforms) {
            if (platform.equalsIgnoreCase(prop1)
                    || platform.equalsIgnoreCase(prop2)) {
                return 59;
            }
        }
        return 36;
    }

    public static final Intent getBackCameraTestIntent(final boolean isAutoTest) {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= 23) { // M
            intent.setClassName("com.bql.camera", "com.android.camera.CameraLauncher");
        } else if (Build.VERSION.SDK_INT >= 16) { // JB 4.1
            intent.setClassName("com.android.gallery3d", "com.android.camera.CameraLauncher");
        } else {
            intent.setClassName("com.android.camera", "com.android.camera.CameraLauncher");
        }

        intent.setAction("android.media.action.IMAGE_CAPTURE");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.putExtra("android.intent.extras.CAMERA_FACING",
                android.hardware.Camera.CameraInfo.CAMERA_FACING_BACK);
        intent.putExtra("VR_MODE", true);

        intent.putExtra(ZteActivity.EXTRA_IS_AUTOTEST, isAutoTest);
        return intent;
    }

    public static final Intent getFrontCameraTestIntent(final boolean isAutoTest) {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= 23) { // M
            intent.setClassName("com.bql.camera", "com.android.camera.CameraLauncher");
        } else if (Build.VERSION.SDK_INT >= 16) { // JB 4.1
            intent.setClassName("com.android.gallery3d", "com.android.camera.CameraLauncher");
        } else {
            intent.setClassName("com.android.camera", "com.android.camera.CameraLauncher");
        }

        intent.setAction("android.media.action.IMAGE_CAPTURE");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.putExtra("android.intent.extras.CAMERA_FACING",
                android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT);
        intent.putExtra("VR_MODE", true);

        intent.putExtra(ZteActivity.EXTRA_IS_AUTOTEST, isAutoTest);
        return intent;
    }

    public static final Intent getFmTestIntent(final boolean isAutoTest) {
        Intent intent = new Intent("com.mediatek.fmradio.FmRadioEmActivity");
        if (Build.VERSION.SDK_INT >= 23) { // M+
            intent.setClassName("com.android.fmradio", "com.android.fmradio.FmEmActivity");
        } else if (Build.VERSION.SDK_INT >= 21) { // Build.VERSION_CODES.LOLLIPOP
            if ("mt6580".equals(SystemProperties.get("ro.mediatek.platform"))
                    || "mt6580".equals(SystemProperties.get("ro.board.platform"))) {
                intent.setClassName("com.android.fmradio", "com.android.fmradio.FmEmActivity");
            } else {
                intent.setClassName("com.mediatek.fmradio", "com.mediatek.fmradio.FmRadioEmActivity");
            }
        } else if (Build.VERSION.SDK_INT == 19) { //Build.VERSION_CODES.KITKAT
            intent.setClassName("com.mediatek.FMRadio", "com.mediatek.FMRadio.FMRadioEMActivity");
        }

        intent.putExtra(ZteActivity.EXTRA_IS_AUTOTEST, isAutoTest);
        return intent;
    }

    public static final Intent getFactoryResetIntent(final boolean isAutoTest) {
        Intent factoryIntent = new Intent();
        factoryIntent.setClassName("com.android.settings"
                , "com.android.settings.Settings$PrivacySettingsActivity");
        factoryIntent.putExtra("do_factory_reset", "FactoryMode");
        factoryIntent.putExtra(ZteActivity.EXTRA_IS_AUTOTEST, isAutoTest);
        return factoryIntent;
    }

    public static final Intent getCommonBackCameraTestIntent(final boolean isAutoTest) {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= 23) { // M
            intent.setClassName("com.bql.camera", "com.android.camera.CameraLauncher");
        } else if (Build.VERSION.SDK_INT >= 16) { // JB 4.1
            intent.setClassName("com.android.gallery3d", "com.android.camera.CameraLauncher");
        } else {
            intent.setClassName("com.android.camera", "com.android.camera.CameraLauncher");
        }

        intent.setAction("android.media.action.IMAGE_CAPTURE");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.putExtra("android.intent.extras.CAMERA_FACING",
                android.hardware.Camera.CameraInfo.CAMERA_FACING_BACK);

        intent.putExtra(ZteActivity.EXTRA_IS_AUTOTEST, isAutoTest);
        return intent;
    }

    public static final Intent getCommonFrontCameraTestIntent(final boolean isAutoTest) {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= 23) { // M
            intent.setClassName("com.bql.camera", "com.android.camera.CameraLauncher");
        } else if (Build.VERSION.SDK_INT >= 16) { // JB 4.1
            intent.setClassName("com.android.gallery3d", "com.android.camera.CameraLauncher");
        } else {
            intent.setClassName("com.android.camera", "com.android.camera.CameraLauncher");
        }

        intent.setAction("android.media.action.IMAGE_CAPTURE");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.putExtra("android.intent.extras.CAMERA_FACING",
                android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT);

        intent.putExtra(ZteActivity.EXTRA_IS_AUTOTEST, isAutoTest);
        return intent;
    }
}
