
package com.zte.engineer;


import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;

public class AudioRecAndPlay{

    private final String TAG = "AudioRecAndPlay";
	public static final int MODE_MAIN_MIC = 5551, MODE_SEC_MIC = 5552, MODE_HEADSET = 5553;
	
    private AudioManager audioManager;
    private MediaPlayer mPlayer;
    private MediaRecorder mRecorder;
    private String mFileName = Environment.getExternalStorageDirectory().getAbsolutePath()+"/EngineerCodeAudio.amr";
    private int preAudioMode;
    private boolean preSpeakerState;
    private int mStatus;
    private int currentVol;
	private int currentMode = -1;
	

    public AudioRecAndPlay(Activity activity, int mode){
        
        audioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
		preAudioMode = audioManager.getMode();
		preSpeakerState = audioManager.isSpeakerphoneOn();
		currentMode = mode;
		//audioManager.setMicrophoneMute(false);
        //audioManager.setMode(AudioManager.MODE_IN_CALL);
		//audioManager.setMicrophoneMute(false);
		currentVol = audioManager
                .getStreamVolume(AudioManager.STREAM_VOICE_CALL);

		if(mode == MODE_HEADSET){
			//audioManager.setMode(0);
			audioManager.setSpeakerphoneOn(false);
			audioManager.setParameters("Speakermic=on");
		} else if(mode == MODE_MAIN_MIC){
			//audioManager.setSpeakerphoneOn(true);
			//audioManager.setMode(2);
		} else{
			//audioManager.setSpeakerphoneOn(true);
			//audioManager.setMode(2);
		}
    }


    public void startRecord(){
        try {
			mRecorder = new MediaRecorder();
			mRecorder.reset();
			if(currentMode == MODE_SEC_MIC){
				 mRecorder.setAudioSource(MediaRecorder.AudioSource.VOICE_RECOGNITION);
			} else if(currentMode == MODE_MAIN_MIC){
				mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			} else{
				//audioManager.setMicrophoneMute(true);
				mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			}
			mRecorder.setAudioChannels(1);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setOutputFile(mFileName);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mRecorder.prepare();
			mRecorder.start();
			Log.d(TAG, "dzg recorder.start--------");
        } catch (IOException e) {
            Log.e(TAG, "dzg Record prepare() failed"+e.getMessage());
        }

        
    }

    public void stopRecord(){
        if (mRecorder != null) {
			try {  
                mRecorder.setOnErrorListener(null);  
                mRecorder.setOnInfoListener(null);    
                mRecorder.setPreviewDisplay(null);  
                mRecorder.stop();  
				Log.d(TAG, "dzg recorder.stop--------");
            } catch (IllegalStateException e) {  
                Log.e(TAG, Log.getStackTraceString(e));  
            }catch (RuntimeException e) {  
                Log.e(TAG, Log.getStackTraceString(e));  
            }catch (Exception e) {   
                Log.e(TAG, Log.getStackTraceString(e));  
            } 
            mRecorder.release();
            mRecorder = null;
        }
    }

    public void startPlay(){
        audioManager.setSpeakerphoneOn(false);

				
        audioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL,
                                 audioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL),
                                 AudioManager.FLAG_PLAY_SOUND);

        try {
            mPlayer = new MediaPlayer();
            mPlayer.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
			Log.d(TAG, "dzg MediaPlayer.start--------");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopPlay(){
        if (mPlayer != null) {
			if(mPlayer.isPlaying())
				mPlayer.stop();
			Log.d(TAG, "dzg MediaPlayer.stop--------");
            mPlayer.release();
            mPlayer = null;
        }
    }

    public void destroy(){
        audioManager.setSpeakerphoneOn(preSpeakerState);
        //audioManager.setMode(preAudioMode);
		Log.d(TAG, "dzg destroy--------");
        File file = new File(mFileName);
        if (file != null && file.exists()) {
            file.delete();
        }
        stopRecord();
        stopPlay();
    }
}
