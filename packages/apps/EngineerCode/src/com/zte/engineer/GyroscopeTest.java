/**
 * type                                 sensor
 * 1(Sensor.TYPE_ACCELEROMETER)         Accelerometer
 * 2(Sensor.TYPE_MAGNETIC_FIELD)        Magnetic
 * 3(Sensor.TYPE_ORIENTATION)           Orientation
 * 4(Sensor.TYPE_GYROSCOPE)             GyroScope
 * 5(Sensor.TYPE_LIGHT)                 Light
 * 8(Sensor.TYPE_PROXIMITY)             Proximity
 * 9(Sensor.TYPE_GRAVITY)               Gravity
 * 10(Sensor.TYPE_LINEAR_ACCELERATION)  Linear Acceleration
 * 11(Sensor.TYPE_ROTATION_VECTOR)      Rotation vector
 */

package com.zte.engineer;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.os.Handler;
import android.os.Message;

public class GyroscopeTest extends ZteActivity {

    private boolean isHallBroadcastRegisted = false;

    LinearLayout  gyroscope_layout;
    private float x, y, z;
    private SensorManager sensorMgr;
    private SensorEventListener lsn;
    TextView GyroscopeX, GyroscopeY, GyroscopeZ;
    boolean[] isTestXYZ = {
            false, false, false, false
    };
    int temp = 0;
    int temp1 = 2;
    boolean[] temp2 = {
            false, false, false, false
    };

    TextView tvHallStatus;
    TextView LightView;
    TextView ProximityView;

	private Spinner calibration;
	private ArrayAdapter adapter2;
	private Toast mToast;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.gyroscope_test);

        sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);

        initUi();

        initSensorListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initUi() {
        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);
		((Button) findViewById(R.id.btnPass)).setEnabled(false);

        gyroscope_layout = (LinearLayout) findViewById(R.id.gyroscope_layout);
        if (!MyApplication.HAS_GYROSCOPE_SENSOR)
            gyroscope_layout.setVisibility(View.GONE);
        GyroscopeX = (TextView) findViewById(R.id.gyroscope_x);
        GyroscopeY = (TextView) findViewById(R.id.gyroscope_y);
        GyroscopeZ = (TextView) findViewById(R.id.gyroscope_z);

		calibration = (Spinner)findViewById(R.id.calibration);
		
		adapter2 = ArrayAdapter.createFromResource(this, R.array.calibration_item, android.R.layout.simple_spinner_item);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 
        calibration.setAdapter(adapter2);
 
        calibration.setOnItemSelectedListener(new SpinnerXMLSelectedListener());
 
        calibration.setVisibility(View.VISIBLE);
    }
	
	private void showToast(String msg) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        mToast.show();
    }
	
    class SpinnerXMLSelectedListener implements OnItemSelectedListener{
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                long arg3) {
            mHandler.sendEmptyMessage(arg2);			
        }
 
        public void onNothingSelected(AdapterView<?> arg0) {
             
        }
         
    }
	
	 private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	int result = 0;
			if (msg.what == 0) {
			//do nothing
            } else if (msg.what == 1) {
			   result = Util.doGyroscopeCalibration(Util.TOLERANCE_20);
			 if (result == Util.RET_SUCCESS) {
				showToast("success");
			((Button) findViewById(R.id.btnPass)).setEnabled(true);
			}else{
				showToast("fail");
			}
            } else if (msg.what == 2) {
			    result = Util.doGyroscopeCalibration(Util.TOLERANCE_30);
		   if (result == Util.RET_SUCCESS) {
				showToast("success");
				((Button) findViewById(R.id.btnPass)).setEnabled(true);
			}else{
				showToast("fail");
			}
            }else if (msg.what == 3) {
			     result = Util.doGyroscopeCalibration(Util.TOLERANCE_40);
			if (result == Util.RET_SUCCESS) {
				showToast("success");
				((Button) findViewById(R.id.btnPass)).setEnabled(true);
			}else{
				showToast("fail");
			}
            }else if (msg.what == 4) {
                result = Util.clearGyroscopeCalibration();
			if (result == Util.RET_SUCCESS) {
				showToast("success");
			}else{
				showToast("fail");
			}
            }
        }
    };

    private void initSensorListener() {
        List<Sensor> sensors = sensorMgr.getSensorList(Sensor.TYPE_ALL);

        lsn = new SensorEventListener() {
            public void onSensorChanged(SensorEvent e) {
				if (e.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
					x = e.values[SensorManager.DATA_X];
				} else {
					x = e.values[SensorManager.DATA_X];
					y = e.values[SensorManager.DATA_Y];
					z = e.values[SensorManager.DATA_Z];
				}

                switch (e.sensor.getType()) {
                    case Sensor.TYPE_GYROSCOPE: {
                        GyroscopeX.setText(String.valueOf(x));
                        GyroscopeY.setText(String.valueOf(y));
                        GyroscopeZ.setText(String.valueOf(z));
                    }
                        break;
                    default: {
                    }
                        break;

                }

            }

            public void onAccuracyChanged(Sensor s, int accuracy) {
            }

        };

        for (Sensor s : sensors) {
            sensorMgr.registerListener(lsn, s, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    public void finishSelf(int result) {

        sensorMgr.unregisterListener(lsn);

        super.finishSelf(result);
    }


}
