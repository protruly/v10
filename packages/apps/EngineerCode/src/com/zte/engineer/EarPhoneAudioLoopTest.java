
package com.zte.engineer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.AudioSystem;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.util.Log;
import android.widget.Toast;
import android.view.KeyEvent;
import android.provider.Settings;
import android.view.View.OnClickListener;
import android.os.Handler;
import android.os.Message;
import android.Manifest;
import android.content.pm.PackageManager;
//import com.mediatek.audioprofile.AudioProfile;
//import com.mediatek.audioprofile.AudioProfileImpl;
//import com.mediatek.audioprofile.AudioProfileManagerImpl;

public class EarPhoneAudioLoopTest extends ZteActivity {

    private final static String LOGTAG = "EarPhoneAudioLoopTest";

    private int isHeadsetConnect;
    private AudioManager mAudioManager = null;
    // private AudioProfileImpl mProfile;
    private boolean soundeffect = false;
    private boolean running = false;
    private AlertDialog mDialog = null;

    TextView hookTextView;
	private boolean isRecord = true;
	private AudioRecAndPlay mAudioControl;
	private Button recordAndPlayBtn;
	private static final int startRecord = 103, stopAndPlay = 104;

    protected BroadcastReceiver mHeadsetReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int mState = 0;
            String action = intent.getAction();

            // check the action event
            if (action.equals(Intent.ACTION_HEADSET_PLUG)) {
                Bundle bundle = intent.getExtras();
                if (bundle != null)
                    mState = bundle.getInt("state");
                // changing status string
                // mProfile.setSoundEffectEnabled(soundeffect);
                //mAudioManager.setParameters("SET_LOOPBACK_TYPE=0");
                if (0 == mState) {
                    displayInsertEarphoneDialog();
                } else if (1 == mState) {
                    // mProfile.setSoundEffectEnabled(soundeffect);
                    cancleInsertEarphoneDialog();
					recordAndPlayBtn.setEnabled(true);
					mAudioControl = new AudioRecAndPlay(EarPhoneAudioLoopTest.this, AudioRecAndPlay.MODE_HEADSET);
                    //mAudioManager.setParameters("SET_LOOPBACK_TYPE=22,2");
                }

                isHeadsetConnect = mState;
            }

            if (action.equals("com.eastaeon.action.TEST_ENG_KEYCODE_HEADSETHOOK")) {
                onHeadsethookKeyPressed();
            }
        }
    };

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        Log.e("yuanwenchao", "Enter onKeyDown");
        switch (keyCode) {
            case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                Toast.makeText(this, getString(R.string.tips_key_previous_down)
                        , Toast.LENGTH_SHORT).show();
                break;
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                Toast.makeText(this, getString(R.string.tips_key_pause_down)
                        , Toast.LENGTH_SHORT).show();
                break;
            case KeyEvent.KEYCODE_HEADSETHOOK:
                Toast.makeText(this, getString(R.string.tips_key_hook_down)
                        , Toast.LENGTH_SHORT).show();
                break;
            case KeyEvent.KEYCODE_MEDIA_NEXT:
                Toast.makeText(this, getString(R.string.tips_key_next_down)
                        , Toast.LENGTH_SHORT).show();
                break;
        }
        return false;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // mProfile = (AudioProfileImpl)
        // AudioProfileManagerImpl.getInstance(this)
        // .getActiveProfile();
        // soundeffect = mProfile.getSoundEffectEnabled();
        // if (soundeffect == true) {
        // mProfile.setSoundEffectEnabled(false);
        // }
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.singlebuttonview);

        TextView mTextView = (TextView) findViewById(R.id.singlebutton_textview);
        mTextView.setText(R.string.earphone_audio_loop);

        hookTextView = (TextView) findViewById(R.id.singlebutton_textview_1);
        hookTextView.setText(R.string.key_earphone_hook);
        mAudioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
		
		recordAndPlayBtn = (Button)findViewById(R.id.singlebutton_play_button);
		recordAndPlayBtn.setOnClickListener(recordAndPlayListener);
		recordAndPlayBtn.setVisibility(View.VISIBLE);
		recordAndPlayBtn.setEnabled(false);
		

        isHeadsetConnect = AudioSystem.getDeviceConnectionState(
                AudioSystem.DEVICE_OUT_WIRED_HEADSET, "");

        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnPass)).setEnabled(false);
        mDialog = new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(getText(R.string.insert_earphone_warning).toString())
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }).create();

    }

    @Override
    public void onResume() {
        super.onResume();
		Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 1);
        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        filter.addAction("com.eastaeon.action.TEST_ENG_KEYCODE_HEADSETHOOK");
        registerReceiver(mHeadsetReceiver, filter);

        running = true;
        /*new Thread() {
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (java.lang.InterruptedException e) {
                }
                if (running) {
                    // if(soundeffect == true)
                    // {
                    // mProfile.setSoundEffectEnabled(false);
                    // }
                    mAudioManager.setParameters("SET_LOOPBACK_TYPE=22,2");
                }
            }
        }.start();*/
    }

    @Override
    public void onPause() {
        super.onPause();
		Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 0);
        unregisterReceiver(mHeadsetReceiver);
		if (mAudioControl != null) {
			mAudioControl.stopRecord();
			mAudioControl.stopPlay();
		}
        //running = false;
        //mAudioManager.setParameters("SET_LOOPBACK_TYPE=0");
        // mProfile.setSoundEffectEnabled(soundeffect);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //running = false;
        //mAudioManager.setParameters("SET_LOOPBACK_TYPE=0");
        // mProfile.setSoundEffectEnabled(soundeffect);
    }

    private void displayInsertEarphoneDialog() {
        mDialog.show();
    }

    private void cancleInsertEarphoneDialog() {
        mDialog.dismiss();
    }

    @Override
    public void finishSelf(int result) {
        running = false;
        //mAudioManager.setParameters("SET_LOOPBACK_TYPE=0");
        // mProfile.setSoundEffectEnabled(soundeffect);
        super.finishSelf(result);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_HEADSETHOOK:
                android.util.Log.d("feng", "--->onKeyDown-->KEYCODE_HEADSETHOOK");
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_HEADSETHOOK:
                android.util.Log.d("feng", "--->onKeyUp-->KEYCODE_HEADSETHOOK");
                if (null != hookTextView) {
                    hookTextView.setText("");
                    ((Button) findViewById(R.id.btnPass)).setEnabled(true);
                }
                return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    public void onHeadsethookKeyPressed() {
        if (null != hookTextView) {
            hookTextView.setText("");
            ((Button) findViewById(R.id.btnPass)).setEnabled(true);
        }
    }
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mAudioControl != null) {
			mAudioControl.destroy();
		}
	}
	
	OnClickListener recordAndPlayListener = new OnClickListener(){
		 @Override
         public void onClick(View arg0) {
			 if(isRecord){
				 recordAndPlayBtn.setText(R.string.play_music);
				 Message msg = mHandler.obtainMessage();
			     msg.what = startRecord;
			     mHandler.sendMessageDelayed(msg, 10);
				 isRecord = false;
			 } else{
				 recordAndPlayBtn.setText(R.string.record_music);
				 Message msg = mHandler.obtainMessage();
			     msg.what = stopAndPlay;
			     mHandler.sendMessageDelayed(msg, 10);
				 isRecord = true;
			 }
		 }
	};
	
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case startRecord:
			    if(mAudioControl != null)
					mAudioControl.stopPlay();
				    mAudioControl.startRecord();
				break;
			case stopAndPlay:
				if (mAudioControl != null) {
					mAudioControl.stopRecord();
					mAudioControl.startPlay();
				}
				break;
			}

		};
	};
}
