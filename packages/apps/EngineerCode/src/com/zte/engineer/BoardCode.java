
package com.zte.engineer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telephony.TelephonyManager;
import android.widget.TextView;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneProxy;
import com.android.internal.telephony.PhoneFactory;
import com.mediatek.telephony.TelephonyManagerEx;

import android.os.SystemProperties;
import android.util.Log;
import android.widget.Button;
import android.view.View;
import android.text.TextUtils;
import android.provider.Settings;
import android.view.KeyEvent;

public class BoardCode extends ZteActivity {

    private TextView mSNNumber;
    private TextView mMEIDNumber;
    private TextView mBtWifiFlag;
    private TextView mGSMFlag;
    private TextView mWCDMAFlag;
    private TextView mTDFlag;
    //private TextView mLTEFlag;
    private TextView mFddFlag;
    private TextView mTddFlag;
    private TextView mCDMAFlag;
    private TextView mCouplingFlag;
    private TextView mCalAndNSFT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.board_info);

        mSNNumber = (TextView) findViewById(R.id.sn_number);
        mMEIDNumber = (TextView) findViewById(R.id.meid_number);
        mBtWifiFlag = (TextView) findViewById(R.id.bt_wifi_flag);
        mGSMFlag = (TextView) findViewById(R.id.gsm_flag);
        mWCDMAFlag = (TextView) findViewById(R.id.wcdma_flag);
        mTDFlag = (TextView) findViewById(R.id.td_flag);
        // mLTEFlag = (TextView)findViewById(R.id.lte_flag);
        mFddFlag = (TextView) findViewById(R.id.fdd_flag);
        mTddFlag = (TextView) findViewById(R.id.tdd_flag);
        mCDMAFlag = (TextView) findViewById(R.id.cdma_flag);
        mCouplingFlag = (TextView) findViewById(R.id.coupling_flag);
        mCalAndNSFT = (TextView) findViewById(R.id.Cal_NSFT);

        // TelephonyManager telephonyManager=(TelephonyManager)
        // this.getSystemService(Context.TELEPHONY_SERVICE);
        String barcode = "";
        // barcode = TelephonyManager.getDefault().getSN();
        Phone mPhone = PhoneFactory.getPhone(0);
        // barcode = mPhone.getSN();
        barcode = SystemProperties.get("gsm.serial");
        Log.d("huang", "barcode : " + barcode);

        char cBT = barcode.length() < 43 ? '0' : barcode.charAt(42);
        char cWiFi = barcode.length() < 46 ? '0' : barcode.charAt(45);
        char cGsm1 = barcode.length() < 47 ? '0' : barcode.charAt(46);
        char cGsm2 = barcode.length() < 48 ? '0' : barcode.charAt(47);
        char cWcdma1 = barcode.length() < 49 ? '0' : barcode.charAt(48);
        char cWcdma2 = barcode.length() < 50 ? '0' : barcode.charAt(49);
        char cTD1 = barcode.length() < 51 ? '0' : barcode.charAt(50);
        char cTD2 = barcode.length() < 52 ? '0' : barcode.charAt(51);
        char g = barcode.length() < 53 ? '0' : barcode.charAt(52);
        char h = barcode.length() < 54 ? '0' : barcode.charAt(53);
        char i = barcode.length() < 55 ? '0' : barcode.charAt(54);
        char j = barcode.length() < 56 ? '0' : barcode.charAt(55);
        char k = barcode.length() < 57 ? '0' : barcode.charAt(56);
        char cRfCP = barcode.length() < 59 ? '0' : barcode.charAt(58);
        char cZCJZ1 = barcode.length() < 61 ? '0' : barcode.charAt(60);
        char cZCJZ2 = barcode.length() < 62 ? '0' : barcode.charAt(61);
        char cZCJZ3 = barcode.length() < 63 ? '0' : barcode.charAt(62);

        char cFDD1 = barcode.length() < 38 ? '0' : barcode.charAt(37);
        char cFDD2 = barcode.length() < 39 ? '0' : barcode.charAt(38);
        char cTDD1 = barcode.length() < 53 ? '0' : barcode.charAt(52);
        char cTDD2 = barcode.length() < 54 ? '0' : barcode.charAt(53);
        char cCdma1 = barcode.length() < 36 ? '0' : barcode.charAt(35);
        char cCdma2 = barcode.length() < 37 ? '0' : barcode.charAt(36);

        String strSNNum = barcode.length() < 16 ? barcode : barcode.substring(0, 16);

        StringBuffer sbBTWiFi = new StringBuffer();
        StringBuffer sbGsm = new StringBuffer();
        StringBuffer sbWcdma = new StringBuffer();
        StringBuffer sbTD = new StringBuffer();
        //StringBuffer sbLTE = new StringBuffer();
        //StringBuffer boardCode5 = new StringBuffer();
        StringBuffer sbRfCP = new StringBuffer();
        StringBuffer sbZCJZ = new StringBuffer();
        StringBuffer sbFDD = new StringBuffer();
        StringBuffer sbTDD = new StringBuffer();
        StringBuffer sbCdma = new StringBuffer();

        mSNNumber.setText(strSNNum);
        mMEIDNumber.setText(getMeid());
        // strSNNum.delete(0,strSNNum.length()-1);
        sbBTWiFi.append((isLetterOrNumber(cBT) ? cBT : "0") + " ");
        sbBTWiFi.append((isLetterOrNumber(cWiFi) ? cWiFi : "0"));
        mBtWifiFlag.setText(sbBTWiFi.toString());
        // strSNNum.delete(0,strSNNum.length()-1);
        sbGsm.append((isLetterOrNumber(cGsm1) ? cGsm1 : "0") + " ");
        sbGsm.append((isLetterOrNumber(cGsm2) ? cGsm2 : "0"));
        mGSMFlag.setText(sbGsm.toString());
        // strSNNum.delete(0,strSNNum.length()-1);
        sbWcdma.append((isLetterOrNumber(cWcdma1) ? cWcdma1 : "0") + " ");
        sbWcdma.append((isLetterOrNumber(cWcdma2) ? cWcdma2 : "0"));
        mWCDMAFlag.setText(sbWcdma.toString());
        sbTD.append((isLetterOrNumber(cTD1) ? cTD1 : "0") + " ");
        sbTD.append((isLetterOrNumber(cTD2) ? cTD2 : "0"));
        mTDFlag.setText(sbTD.toString());
        // strSNNum.delete(0,strSNNum.length()-1);

        /*sbLTE.append((isLetterOrNumber(g) ? g : "0") + " ");
        sbLTE.append((isLetterOrNumber(h) ? h : "0"));
        mLTEFlag.setText(boardCode4.toString());*/

        sbFDD.append((isLetterOrNumber(cFDD1) ? cFDD1 : "0") + " ");
        sbFDD.append((isLetterOrNumber(cFDD2) ? cFDD2 : "0"));
        mFddFlag.setText(sbFDD.toString());
        sbTDD.append((isLetterOrNumber(cTDD1) ? cTDD1 : "0") + " ");
        sbTDD.append((isLetterOrNumber(cTDD2) ? cTDD2 : "0"));
        mTddFlag.setText(sbTDD.toString());
        sbCdma.append((isLetterOrNumber(cCdma1) ? cCdma1 : "0") + " ");
        sbCdma.append((isLetterOrNumber(cCdma2) ? cCdma2 : "0"));
        mCDMAFlag.setText(sbCdma.toString());
        // strSNNum.delete(0,strSNNum.length()-1);

        /*boardCode5.append((isLetterOrNumber(i) ? i : "0") + " ");
        boardCode5.append((isLetterOrNumber(j) ? j : "0") + " ");
        boardCode5.append((isLetterOrNumber(k) ? k : "0"));*/

        sbRfCP.append((isLetterOrNumber(cRfCP) ? cRfCP : "0"));
        mCouplingFlag.setText(sbRfCP.toString());
        sbZCJZ.append((isLetterOrNumber(cZCJZ1) ? cZCJZ1 : "0") + " ");
        sbZCJZ.append((isLetterOrNumber(cZCJZ2) ? cZCJZ2 : "0") + " ");
        sbZCJZ.append((isLetterOrNumber(cZCJZ3) ? cZCJZ3 : "0"));
        mCalAndNSFT.setText(sbZCJZ.toString());

        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);
    }

	
    @Override
    protected void onResume() {
        // registerReceiver(screenoff, new IntentFilter(Screenoff));
        Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 1);
        super.onResume();

        // PhoneWindowManager.setKeyTestState(true);
    }

    @Override
    protected void onPause() {
        // unregisterReceiver(screenoff);
        Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 0);
        super.onPause();

        // PhoneWindowManager.setKeyTestState(false);
    }	
	
    private boolean isLetterOrNumber(char c) {
        return (c <= 'Z' && c >= 'A')
                || (c >= '0' && c <= '9')
                || (c <= 'z' && c >= 'a');
    }

    private String getMeid() {
        /*final Phone phone = PhoneFactory.getPhone(PhoneConstants.SIM_ID_2);
//        String mIMEI2 = TelephonyManagerEx.getDefault().getDeviceId(PhoneConstants.SIM_ID_2);
        if (phone.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
            String mIMEI2 = phone.getMeid();
            Log.d("lq_board", "getMeid mIMEI2 = " + mIMEI2);
            return mIMEI2;
        }*/
		String strMeid = SystemProperties.get("gsm.mtk.meid");
		if (!TextUtils.isEmpty(strMeid)) {
			strMeid = strMeid.toUpperCase();
            Log.d("lq_phone", "BoardCode getMeid strMeid = " + strMeid);
			return strMeid;
		}
        return null;
    }
}
