
package com.zte.engineer;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import libcore.io.IoUtils;
import java.text.DecimalFormat;
 
public class TempSenTest extends ZteActivity {

    private static final String TAG = "TEMPSEN";

    private TempSenControl mTempSenControl;
	
    private static final int DELAY_TIME = 500; // seconds

    // timer handle event
    private static final int LCDOFF_TIMER_EVENT_TICK = 1;
    
	TextView mTextView;
	
	
	
    // TIMER_EVENT_TICK handler
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case LCDOFF_TIMER_EVENT_TICK: {
                    // It will turn on LCD in onPause
                    temp_read_fun();
                }
                    break;
            }
			mHandler.sendEmptyMessageDelayed(LCDOFF_TIMER_EVENT_TICK, DELAY_TIME);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.temp_sen_test);
         mTempSenControl = new TempSenControl();
        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);
		 mTextView=(TextView)findViewById(R.id.temp_val); 
		 mTempSenControl.TempEnable(true);
		 temp_read_fun();
		mHandler.sendEmptyMessageDelayed(LCDOFF_TIMER_EVENT_TICK, DELAY_TIME);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		mTempSenControl.TempEnable(true);
        mHandler.sendEmptyMessageDelayed(LCDOFF_TIMER_EVENT_TICK, DELAY_TIME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeMessages(LCDOFF_TIMER_EVENT_TICK);
		mTempSenControl.TempEnable(false);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onBackPressed() {
        finish();;
    }
    
	public void  temp_read_fun()
	{
		int temp_raw_val,temp_Ta;
		double temp_val,temp_val_Ta;
		temp_raw_val=mTempSenControl.TempRead(1);
		temp_val=(temp_raw_val*0.02)-273.15;
		DecimalFormat fnum = new DecimalFormat("##0.00"); 
		
		temp_Ta = mTempSenControl.TempRead(2);
		temp_val_Ta=(temp_Ta*0.02)-273.15;
		
        String mstr=fnum.format(temp_val)+"℃";
		String mstr_ta=fnum.format(temp_val_Ta)+"℃";
		
		mstr = "To:"+temp_raw_val+"("+mstr +")"+"  Ta:"+temp_Ta+"("+mstr_ta +")"+"";
		
		mTextView.setText(mstr);
	}
	
	    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.btnPass:
                finishSelf(RESULT_PASS);
				mTempSenControl.TempEnable(false);
                break;
            case R.id.btnFail:
                finishSelf(RESULT_FAIL);
				mTempSenControl.TempEnable(false);
                break;
            default:
                finishSelf(RESULT_PASS);
                break;
        }
    }
}
