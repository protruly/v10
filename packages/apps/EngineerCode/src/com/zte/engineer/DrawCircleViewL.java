
package com.zte.engineer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;
import android.graphics.Path;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Message;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff.Mode;
import android.content.res.Resources;

import android.graphics.Color;

public class DrawCircleViewL extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = "DrawCircleViewL";
    Paint mPaint = null;
    Paint mPaintLine = null;
    Rect[] mCircleRects = new Rect[5];

    boolean[] hadFill = new boolean[5];
    final int RADIUS = 90;
    boolean bFirst = true;
    Canvas mCanvas = new Canvas();
    Point mCurPoint = null;
    String tipsRedraw = "";
    String tipsError = "";
    String tipsError1 = "";
    String tipsOk = "";
    SurfaceHolder mHolder = null;
    private boolean debug = false;

    public DrawCircleViewL(Context context, AttributeSet attrs) {
        super(context, attrs);
        Resources res = context.getResources();
        tipsRedraw = res.getString(R.string.redraw);
        tipsError = res.getString(R.string.draw_error);
        tipsError1 = res.getString(R.string.draw_error1);
        tipsOk = res.getString(R.string.draw_ok);
        mHolder = getHolder();
        mHolder.addCallback(this);

    }

    private void initPaint() {
        mPaint = new Paint();
        mPaintLine = new Paint();
        mPaintLine.setAntiAlias(true);
        mPaintLine.setColor(Color.GREEN);
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.BLACK);
    }

    private void setCircleRects(Rect[] rects) {

        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        height = height % 10 == 0 ? height : 10 * (height / 10);
        Log.i(TAG, String.format(
                "____setCircleRects(...)______measure width: %d, measure height : %d", width,
                height));
        Log.e("yuanwenchao", "width = " + width + " height =" + height);

        // The upper left corner
        rects[0] = new Rect(0, 0, RADIUS * 2, RADIUS * 2);
        // The upper right corner
        rects[1] = new Rect(width - RADIUS * 2, 0, width, RADIUS * 2);
        // The lower left corner
        rects[2] = new Rect(0, height - RADIUS * 2, RADIUS * 2, height);
        // The lower right corner
        rects[3] = new Rect(width - RADIUS * 2, height - RADIUS * 2, width, height);
        // center
        rects[4] = new Rect(width / 2 - RADIUS, height / 2 - RADIUS, width / 2 + RADIUS, height / 2
                + RADIUS);

    }

    public Paint paint = null;
    float preX;
    float preY;
    float preTempX;
    int upCount = 0;
    boolean[] isTouchCircle = {
            false, false, false, false, false
    };
    boolean isMoveOutLine, isDownOutLine = false;
    Path path = new Path();

    Path pathR = new Path();
    Path pathL = new Path();

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // TODO Auto-generated method stub
        int x = (int) event.getX();
        int y = (int) event.getY();
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        float tempX = (y - 70) * (width - 70) / (height - 70);

        Log.e("yuanwenchao2", "tempX = " + tempX + " Y = " + y);
        int action = event.getAction();

        final int len = mCircleRects.length;
        for (int i = 0; i < len; i++) {
            // final Point p = mPoint[i];
            if (mCircleRects[i].contains(x, y)) {
                Log.i(TAG, "______draw stroken circle no." + i);
                isTouchCircle[i] = true;
                Log.e("yuanwenchao", "isTouchCircle[i] = " + isTouchCircle[i] + " i = " + i);
                // hadFill[i]=true;
                // drawSolidCircle(i);

            }

        }
        // if(action == MotionEvent.ACTION_DOWN){
        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:

                if (x < tempX || x > tempX + 114) {

                    isDownOutLine = true;

                } else {
                    isDownOutLine = false;
                }
                path.moveTo(x, y);
                pathR.moveTo(tempX, y);
                pathL.moveTo(tempX + 114, y);
                preX = x;
                preY = y;
                preTempX = tempX;
                break;
            case MotionEvent.ACTION_MOVE:
                if ((x < tempX || x > tempX + 114) | isDownOutLine) {

                    isMoveOutLine = true;
                    isDownOutLine = true;

                }
                path.quadTo(preX, preY, x, y);
                pathR.quadTo(preTempX, preY, tempX, y);
                pathL.quadTo(preTempX + 114, preY, tempX + 114, y);
                preX = x;
                preY = y;
                preTempX = tempX;
                break;
            case MotionEvent.ACTION_UP:
                if (!(isDownOutLine | isMoveOutLine))
                    upCount++;

                Log.e("yuanwenchao", "Enter MotionEvent.ACTION_UP: else");
                if ((isTouchCircle[0] & isTouchCircle[3]) && (upCount == 1)) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            getContext().sendBroadcast(
                                    new Intent(TouchScreenTest.PRIVATE_ACTION).putExtra("TestPass",
                                            2));
                        }
                    }, 500);
                    Toast.makeText(getContext(), tipsOk, Toast.LENGTH_SHORT).show();
                    Log.e("yuanwenchao", "Enter test ok ");
                }

                else if ((isTouchCircle[1] & isTouchCircle[2]) && (upCount == 1)) {
                    upCount = 0;

                    isTouchCircle[0] = false;
                    isTouchCircle[3] = false;

                    Toast.makeText(getContext(), tipsRedraw, Toast.LENGTH_SHORT).show();
                    path = new Path();
                    pathR = new Path();
                    pathL = new Path();
                    Log.e("yuanwenchao", "Enter test error count == 1");
                } else if (upCount >= 1) {
					if (upCount >= 2) {
						Toast.makeText(getContext(), tipsError, Toast.LENGTH_SHORT).show();
					}
                    upCount = 0;

                    isTouchCircle[0] = false;
                    isTouchCircle[3] = false;

                    path = new Path();
                    pathR = new Path();
                    pathL = new Path();
                    Log.e("yuanwenchao", "Enter test error count > 2");
                } else if (isTouchCircle[0] || isTouchCircle[3]) {
                    Log.e("yuanwenchaoL", " 1111 upCount = " + upCount);
                    upCount = 0;

                    isTouchCircle[0] = false;
                    isTouchCircle[3] = false;

                    path = new Path();
                    pathR = new Path();
                    pathL = new Path();
                }

                break;
        // }
        }

        // Paint bmpPaint = new Paint();
        // canvas.drawBitmap(cacheBitmap, 0, 0,bmpPaint);
        // Log.e("yuanwenchao","Enter onDraw");

        Canvas canvas = mHolder.lockCanvas();
        if (canvas == null) {
            return true;
        }
        Paint p = new Paint();
        p.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
        canvas.drawPaint(p);
        p.setXfermode(new PorterDuffXfermode(Mode.SRC));

        if (isMoveOutLine | isDownOutLine) {
            path = new Path();
            pathR = new Path();
            pathL = new Path();

            isMoveOutLine = false;

        }
        drawSolidCircle(canvas);
        drawSlash(canvas);
        canvas.drawPath(path, paint);
        canvas.drawPath(pathR, paint);
        canvas.drawPath(pathL, paint);
        mHolder.unlockCanvasAndPost(canvas);
        return true;
        // return super.onTouchEvent(event);
    }

    private void drawSlash(Canvas canvas) {
        // draw hollow circle
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        mPaintLine.setStyle(Style.STROKE);

        canvas.drawLine(0, 70, width - 70, height, mPaintLine);
        canvas.drawLine(70, 0, width, height - 70, mPaintLine);

    }

    private void drawSlash() {
        // draw hollow circle
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        Canvas canvas = mHolder.lockCanvas(null);
        mPaintLine.setStyle(Style.STROKE);

        canvas.drawLine(0, 70, width - 70, height, mPaintLine);
        canvas.drawLine(70, 0, width, height - 70, mPaintLine);
        // canvas.drawRect(width/2-50,0,width/2+50,height/2-170,mPaintLine);
        // canvas.drawRect(width/2-50,height/2+170,width/2+50,height,mPaintLine);
        // canvas.drawRect(0,height/2-25,width/2-80,height/2+25,mPaintLine);
        // canvas.drawRect(width/2+80,height/2-25,width,height/2+25,mPaintLine);
        mHolder.unlockCanvasAndPost(canvas);

    }

    private void drawHollowCircle() {
        // draw hollow circle
        Canvas canvas = mHolder.lockCanvas(null);
        mPaint.setStyle(Style.STROKE);

        for (Rect r : mCircleRects) {
            canvas.drawCircle(r.centerX(), r.centerY(), RADIUS, mPaint);
        }

        mHolder.unlockCanvasAndPost(canvas);

    }

    private void drawSolidCircle(Canvas canvas) {
        // aeon lee modify start 2014-03-27
        // Canvas canvas = mHolder.lockCanvas();

        // draw solid circle
        // Log.i(TAG,"________draw fill circle INDEX: "+ index);
        for (int i = 0; i < mCircleRects.length; i++) {
            mPaint.setStyle(Style.STROKE);
            canvas.drawCircle(mCircleRects[i].centerX(), mCircleRects[i].centerY(), RADIUS, mPaint);
            if (hadFill[i] == true) {
                mPaint.setStyle(Style.FILL);
                canvas.drawCircle(mCircleRects[i].centerX(), mCircleRects[i].centerY(), RADIUS,
                        mPaint);
            }
        }
        // aeon lee modify end 2014-03-27
        /*if (fillAll(hadFill)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getContext().sendBroadcast(
                            new Intent(TouchScreenTest.PRIVATE_ACTION).putExtra("TestPass", 1));
                }
            }, 500);
        }*/
        // mHolder.unlockCanvasAndPost(canvas);
    }

    /**
     * @param bArray
     * @return Return true if all circles had been filled.Or false.
     */
    boolean fillAll(boolean[] bArray) {

        for (boolean b : bArray) {
            if (!b) {
                Log.i(TAG, "____fillAll____b :" + b);
                return false;
            }
        }
        return true;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        initPaint();
        setCircleRects(mCircleRects);
        drawHollowCircle();

        paint = new Paint(Paint.DITHER_FLAG);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(Color.WHITE);
        drawSlash();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub

    }

}
