
package com.zte.engineer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

public class BootReceiver extends BroadcastReceiver {
    private static final String TAG = "EngineerCode.BootReceiver";

    /**
     * Receive com.zte.engineer.action.LAUNCHER_TEST only.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Settings.System.putInt(context.getContentResolver(), "AEON_KEY_TEST", 0);
            Settings.System.putInt(context.getContentResolver(), "AEON_HALL_TEST", 0);
            return;
        }
    }

}
