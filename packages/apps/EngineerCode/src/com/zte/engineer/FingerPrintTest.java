package com.zte.engineer;

import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.CancellationSignal;

import android.graphics.Color;
import android.widget.Button;
import android.widget.TextView;

import android.util.Log;

public class FingerPrintTest extends ZteActivity {
	private static final String TAG = "FingerPrintTest";

	private boolean mEnrolling = false;
	private CancellationSignal mEnrollmentCancel;
	private byte[] mToken = new byte[69];
	private boolean isSuccess = false;

	private TextView mFingerprintText;
	private Button mPass;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.fingerprint_test);

		mFingerprintText = (TextView) findViewById(R.id.fingerprint_textview);

		mPass = (Button) findViewById(R.id.btnPass);
		mPass.setOnClickListener(this);
		mPass.setEnabled(false);
		((Button) findViewById(R.id.btnFail)).setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "onResume mEnrolling = " + mEnrolling);
		if (!mEnrolling) {
			startEnrollment();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (!isChangingConfigurations()) {
			Log.d(TAG, "onPause mEnrolling = " + mEnrolling);
			cancelEnrollment();
		}
	}

	private void startEnrollment() {
		mEnrollmentCancel = new CancellationSignal();
		getSystemService(FingerprintManager.class).enroll(mToken, mEnrollmentCancel, 0 /* flags */, mEnrollmentCallback);
		mEnrolling = true;
	}

	private void cancelEnrollment() {
		if (mEnrolling) {
			mEnrollmentCancel.cancel();
			mEnrolling = false;
		}
	}

	private FingerprintManager.EnrollmentCallback mEnrollmentCallback
			= new FingerprintManager.EnrollmentCallback() {
		@Override
		public void onEnrollmentProgress(int remaining) {
			Log.d(TAG, "==> onEnrollmentProgress");
			showSuccessText();
		}

		@Override
		public void onEnrollmentHelp(int helpMsgId, CharSequence helpString) {
			Log.d(TAG, "==> onEnrollmentHelp");
			showSuccessText();
		}

		@Override
		public void onEnrollmentError(int errMsgId, CharSequence errString) {
			Log.d(TAG, "==> onEnrollmentError");
			showSuccessText();
		}
	};

	private void showSuccessText() {
		if (!isSuccess && mEnrolling) {
			mFingerprintText.setText(getString(R.string.fingerprint_test_success));
			isSuccess = true;
			mPass.setEnabled(true);
		}
	}

	@Override
	public void finishSelf(int result) {
		cancelEnrollment();
		super.finishSelf(result);
	}
}
