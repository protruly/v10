
package com.zte.engineer;

import android.content.Context;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.internal.telephony.PhoneConstants;
import com.mediatek.telephony.TelephonyManagerEx;
import android.os.SystemProperties;
import android.provider.Settings;
import android.view.KeyEvent;

public class ImeiTest extends ZteActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.singlebuttonview);

        TextView mTextView = (TextView) findViewById(R.id.singlebutton_textview);
        mTextView.setText(R.string.imei);
        // Get Telephony Manager
        TelephonyManager telephonyManager = (TelephonyManager) this
                .getSystemService(Context.TELEPHONY_SERVICE);

        boolean isImeiWritted = false;
        if ("true".equals(SystemProperties.get("ro.mediatek.gemini_support"))) {
            // String mIMEI1 = telephonyManager.getDeviceIdGemini(0);
            // String mIMEI2 = telephonyManager.getDeviceIdGemini(1);
           // String mIMEI1 = TelephonyManagerEx.getDefault().getDeviceId(PhoneConstants.SIM_ID_1);
           // String mIMEI2 = TelephonyManagerEx.getDefault().getDeviceId(PhoneConstants.SIM_ID_2);
			String mIMEI1 = telephonyManager.getDeviceId(0);
			String mIMEI2 = telephonyManager.getDeviceId(1);
            TextView mTextViewIMEI = (TextView) findViewById(R.id.singlebutton_textview_2);
            mTextViewIMEI.setText(String.format(getResources().getString(R.string.display_IMEI_1),
                    mIMEI1));
            mTextViewIMEI = (TextView) findViewById(R.id.singlebutton_textview_3);
            mTextViewIMEI.setText(String.format(getResources().getString(R.string.display_IMEI_2),
                    mIMEI2));
            isImeiWritted = !TextUtils.isEmpty(mIMEI1) && !TextUtils.isEmpty(mIMEI2);
        } else {
            String mIMEI = telephonyManager.getDeviceId();

            // TextView mTextView =
            // (TextView)findViewById(R.id.singlebutton_textview);
            TextView mTextViewIMEI = (TextView) findViewById(R.id.singlebutton_textview_2);

            // mTextView.setText(R.string.IMEI_test);
            // Get and format IMEI string
            mTextViewIMEI.setText(String.format(getResources().getString(R.string.display_IMEI),
                    mIMEI));
            isImeiWritted = !TextUtils.isEmpty(mIMEI);
        }
        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);

        //((Button) findViewById(R.id.btnPass)).setEnabled(isImeiWritted);
    }
	
	
    @Override
    protected void onResume() {
        // registerReceiver(screenoff, new IntentFilter(Screenoff));
        Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 1);
        super.onResume();

        // PhoneWindowManager.setKeyTestState(true);
    }

    @Override
    protected void onPause() {
        // unregisterReceiver(screenoff);
        Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 0);
        super.onPause();

        // PhoneWindowManager.setKeyTestState(false);
    }	

}
