
package com.zte.engineer;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.android.internal.telephony.PhoneConstants;
import android.content.IntentFilter;
import com.android.internal.telephony.PhoneFactory;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.os.SystemProperties;
import android.util.Log;
import android.net.Uri;
import android.content.ComponentName;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import java.util.Timer;
import java.util.TimerTask;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;
import android.provider.Settings;
import android.view.KeyEvent;
import android.os.storage.StorageManager;

public class SIMTest extends ZteActivity {
    boolean callstatus = false;
    private int lastSignal;
    boolean Sim2State, Sim1State;
    private TimerTask timetask;
    private Phone mPhone_sim1, mPhone_sim2;
    private int signalDbm1, signalDbm2, signalAsu1, signalAsu2;
    TextView mTextView_singlesstrengh;
	
	private StorageManager mStorageManager;
    // YWC--START
    int phoneType;
    ArrayList<Object> gsmCells = new ArrayList<Object>();

    ArrayList<Object> cdmaCells = new ArrayList<Object>();
    private TelephonyManager telephonyManager;
    private PhoneStateListener phoneStateListener;

	private Button mPass;
	
    // YWC--END
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sim_test);
        mPhone_sim1 = PhoneFactory.getPhone(0);
        mPhone_sim2 = PhoneFactory.getPhone(1);
		
		mStorageManager = (StorageManager) getSystemService(Context.STORAGE_SERVICE);

        Button call = (Button) findViewById(R.id.singlebutton_call_button);
        Button call1 = (Button) findViewById(R.id.singlebutton_call_button1);

        mTextView_singlesstrengh = (TextView) findViewById(R.id.singlesstrengh_textview);
        Log.e("yuanwenchao", "Enter SIM ONCREAT");
        call.setOnClickListener(this);
        call1.setOnClickListener(this);
        TextView mTextView = (TextView) findViewById(R.id.singlebutton_textview);
        mTextView.setText(R.string.SIM);

        // Get Telephony Manager --YWC--START
        InitPhoneStateListener();
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CELL_LOCATION);
        final Handler handDelay = new Handler();
        timetask = new TimerTask() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                telephonyManager.listen(phoneStateListener,
                        PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
                Log.e("yuanwenchao", "Enter timer");
                handDelay.postDelayed(run, 1000);
                Log.e("yuanwenchao", " signalDbm1 = " + signalDbm1 + " signalAsu1 = " + signalAsu1
                        + " signalAsu2 = " + signalAsu2 + " signalDbm2 = " + signalDbm2);
            }
        };
        new Timer().schedule(timetask, 0, 1000);

        if (telephonyManager.getCellLocation() != null) {
            // GET Cell Location
            phoneStateListener.onCellLocationChanged(telephonyManager.getCellLocation());
        }
        // YWC--END

        if ("true".equals(SystemProperties.get("ro.mediatek.gemini_support"))) {

            // boolean Sim1State =
            // mGeminiPhone.isSimInsert(PhoneConstants.GEMINI_SIM_1)&
            // mGeminiPhone.isRadioOnGemini(PhoneConstants.GEMINI_SIM_1);
            // boolean Sim2State =
            // mGeminiPhone.isSimInsert(PhoneConstants.GEMINI_SIM_2)&
            // mGeminiPhone.isRadioOnGemini(PhoneConstants.GEMINI_SIM_2);
            // boolean Sim1State =
            // mGeminiPhone.getPhonebyId(PhoneConstants.GEMINI_SIM_1).getIccCard().hasIccCard();
            // boolean Sim2State =
            // mGeminiPhone.getPhonebyId(PhoneConstants.GEMINI_SIM_2).getIccCard().hasIccCard();
            Sim1State = TelephonyManager.getDefault().hasIccCard(PhoneConstants.SIM_ID_1);
            Sim2State = TelephonyManager.getDefault().hasIccCard(PhoneConstants.SIM_ID_2);

            TextView mTextViewIMEI = (TextView) findViewById(R.id.singlebutton_textview_2);
            if (Sim1State == true) {
                mTextViewIMEI.setText(String.format(getResources()
                        .getString(R.string.display_SIM_1),
                        getResources().getString(R.string.SIM_INSERT)));
            } else {
                mTextViewIMEI.setText(String.format(getResources()
                        .getString(R.string.display_SIM_1),
                        getResources().getString(R.string.SIM_NOT_INSERT)));
            }

            mTextViewIMEI = (TextView) findViewById(R.id.singlebutton_textview_3);
            if (Sim2State == true) {
                mTextViewIMEI.setText(String.format(getResources()
                        .getString(R.string.display_SIM_2),
                        getResources().getString(R.string.SIM_INSERT)));
            } else {
                mTextViewIMEI.setText(String.format(getResources()
                        .getString(R.string.display_SIM_2),
                        getResources().getString(R.string.SIM_NOT_INSERT)));
            }
        } else {
            boolean isSimInsert = TelephonyManager.getDefault().hasIccCard();
            Sim1State = isSimInsert;
            // TextView mTextView =
            // (TextView)findViewById(R.id.singlebutton_textview);
            TextView mTextViewIMEI = (TextView) findViewById(R.id.singlebutton_textview_2);

            // mTextView.setText(R.string.IMEI_test);
            // Get and format IMEI string
            if (isSimInsert == true) {
                mTextViewIMEI.setText(String.format(getResources().getString(R.string.display_SIM),
                        getResources().getString(R.string.SIM_INSERT)));
            } else {
                mTextViewIMEI.setText(String.format(getResources().getString(R.string.display_SIM),
                        getResources().getString(R.string.SIM_NOT_INSERT)));
            }
        }

		String[] storagePathList = mStorageManager.getVolumePaths();
        mPass = (Button) findViewById(R.id.btnPass);
		mPass.setOnClickListener(this);
		mPass.setEnabled(false);
		Log.d("zengtao", "storagePathList.length = "+storagePathList.length);
		Log.d("zengtao", "Sim2State = "+Sim2State);
		Log.d("zengtao", "Sim1State = "+Sim1State);
		if (Sim1State && Sim2State) {
			mPass.setEnabled(true);
		} else if (Sim2State && (storagePathList.length == 2)) {
			mPass.setEnabled(true);
		} else if (Sim1State && (storagePathList.length == 2)) {
			mPass.setEnabled(true);
		}
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);
        IntentFilter filterCallStatus = new IntentFilter();

        filterCallStatus.addAction("Intent.receiver_call_status");
        CallStatusReceiver callStatusReceiver = new CallStatusReceiver();

        registerReceiver(callStatusReceiver, filterCallStatus);
        Log.e("yuanwenchao", "cdmaCells : " + cdmaCells.toString());
        Log.e("yuanwenchao", "gsmCells : " + gsmCells.toString());
    }

    Runnable run = new Runnable() {
        public void run() {
            // Log.e("yuanwenchao","lastSignal = "+lastSignal);

            if ("true".equals(SystemProperties.get("ro.mediatek.gemini_support"))) {
                String signalstatus1 = "";
                String signalstatus2 = "";

                if (signalDbm1 <= -105) {
                    signalstatus1 = "(UNKNOWN)";
                } else if (signalDbm1 == 0) {
                    signalstatus1 = "(NO_SIM)";
                } else if (signalDbm1 >= -85) {
                    signalstatus1 = "(GREAT!)";
                } else if (signalDbm1 >= -92) {
                    signalstatus1 = "(GOOD!)";
                } else if (signalDbm1 >= -99) {
                    signalstatus1 = "(NORMAL)";
                } else {
                    signalstatus1 = "(POOR)";
                }
                if (signalDbm2 <= -105) {
                    signalstatus2 = "(UNKNOWN)";
                } else if (signalDbm2 == 0) {
                    signalstatus2 = "(NO_SIM)";
                } else if (signalDbm2 >= -85) {
                    signalstatus2 = "(GREAT!)";
                } else if (signalDbm2 >= -92) {
                    signalstatus2 = "(GOOD!)";
                } else if (signalDbm2 >= -99) {
                    signalstatus2 = "(NORMAL)";
                } else {
                    signalstatus2 = "(POOR)";
                }

                if (Sim1State & Sim2State) {
                    mTextView_singlesstrengh.setText("sim1 : " + signalDbm1 + "dbm " + signalAsu1
                            + "Aus " + signalstatus1 + "\n" + "sim2 : " + signalDbm2 + "dbm "
                            + signalAsu2 + "Aus " + signalstatus2);
                } else if (Sim1State) {
                    mTextView_singlesstrengh.setText("sim1 : " + signalDbm1 + "dbm " + signalAsu1
                            + "Aus " + signalstatus1 + "\n" + "sim2 : " + "0dbm " + "99Aus "
                            + "(NO_SIM)");
                } else if (Sim2State) {
                    mTextView_singlesstrengh.setText("sim1 : " + "0dbm " + "99Aus " + "(NO_SIM)" + "\n"
                            + "sim2 : " + signalDbm2 + "dbm " + signalAsu2 + "Aus " + signalstatus2);
                } else {
                    mTextView_singlesstrengh.setText("sim1 : " + "0dbm " + "99Aus " + "(NO_SIM)" + "\n"
                            + "sim2 : " + "0dbm " + "99Aus " + "(NO_SIM)");
                }
            } else {
                String signalstatus1_ = "";
                if (signalDbm1 <= -105) {
                    signalstatus1_ = "(UNKNOWN)";
                } else if (signalDbm1 == 0) {
                    signalstatus1_ = "(NO_SIM)";
                } else if (signalDbm1 >= -85) {
                    signalstatus1_ = "(GREAT!)";
                } else if (signalDbm1 >= -92) {
                    signalstatus1_ = "(GOOD!)";
                } else if (signalDbm1 >= -99) {
                    signalstatus1_ = "(NORMAL)";
                } else {
                    signalstatus1_ = "(POOR)";
                }
                if (Sim1State) {
                    mTextView_singlesstrengh.setText("sim : " + signalDbm1 + "dbm " + signalAsu1
                            + "Aus " + signalstatus1_);
                } else {
                    mTextView_singlesstrengh.setText("sim1 : " + "0dbm " + "99Aus " + "(NO_SIM)");
                }
            }
        }
    };

    private class CallStatusReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {

            Log.e("yuanwenchao", "receiver CallStatusReceiver");
            callstatus = true;
            Toast.makeText(SIMTest.this, getString(R.string.tips_call_succeed)
                    , Toast.LENGTH_LONG).show();
        }
    };

    /** INIT PhoneStateListener YWC--START */
    private void InitPhoneStateListener() {

        phoneStateListener = new PhoneStateListener() {

            @Override
            public void onCellLocationChanged(CellLocation location) {
                if (location == null) {
                    return;
                }

                if (location instanceof GsmCellLocation) {// gsm network
                    phoneType = 1;
                    GsmCell gsmCell = new GsmCell();
                    gsmCell.lac = ((GsmCellLocation) location).getLac();
                    gsmCell.cid = ((GsmCellLocation) location).getCid();
                    /** get mcc，mnc */
                    String mccMnc = telephonyManager.getNetworkOperator();
                    if (mccMnc != null && mccMnc.length() >= 5) {
                        gsmCell.mcc = mccMnc.substring(0, 3);
                        gsmCell.mnc = mccMnc.substring(3, 5);
                    }

                    gsmCell.time = System.currentTimeMillis();
                    if (gsmCell.lac != -1 && gsmCell.cid != -1) {
                        gsmCells.add(0, gsmCell);
                        // Collections.sort(gsmCells);
                        // /**at most keep three*/
                        // if (gsmCells.size() >3)
                        // gsmCells.remove(3);
                    }
                    // get the local message
                    List<NeighboringCellInfo> neighboringList = telephonyManager
                            .getNeighboringCellInfo();
                    if (neighboringList != null) {
                        for (NeighboringCellInfo ni : neighboringList) {
                            GsmCell gb = new GsmCell();
                            gb.mnc = mccMnc.substring(3, 5);
                            gb.lac = ni.getLac();
                            gb.cid = ni.getCid();

                            gb.time = System.currentTimeMillis();
                            gsmCells.add(gb);
                        }
                    }

                } else {// other cdma
                    try {
                        Class cdmaClass = Class.forName("android.telephony.cdma.CdmaCellLocation");
                        phoneType = 2;
                        CdmaCellLocation cdma = (CdmaCellLocation) location;
                        CdmaCell cdmaCell = new CdmaCell();
                        cdmaCell.stationId = cdma.getBaseStationId() >= 0 ? cdma.getBaseStationId()
                                : cdmaCell.stationId;
                        cdmaCell.networkId = cdma.getNetworkId() >= 0 ? cdma.getNetworkId()
                                : cdmaCell.networkId;
                        cdmaCell.systemId = cdma.getSystemId() >= 0 ? cdma.getSystemId()
                                : cdmaCell.systemId;
                        /** get mcc，mnc */
                        String mccMnc = telephonyManager.getNetworkOperator();
                        if (mccMnc != null && mccMnc.length() >= 5) {
                            cdmaCell.mcc = mccMnc.substring(0, 3);
                            cdmaCell.mnc = mccMnc.substring(3, 5);
                        }

                        cdmaCell.time = System.currentTimeMillis();
                        int lat = cdma.getBaseStationLatitude();
                        int lon = cdma.getBaseStationLongitude();
                        if (lat < Integer.MAX_VALUE && lon < Integer.MAX_VALUE) {
                            cdmaCell.lat = lat;
                            cdmaCell.lon = lon;
                        }
                        if (cdmaCell.stationId != -1 && cdmaCell.networkId != -1
                                && cdmaCell.systemId != -1) {
                            cdmaCells.add(0, cdmaCell);
                        }
                        List<NeighboringCellInfo> neighboringList = telephonyManager
                                .getNeighboringCellInfo();
                        // Start by luqiang, eastaeon, 2016.8.18 [NullPointerException]
                        if (null == neighboringList) {
                            Log.d("SIMTest", "no sim! ==> neighboringList is null!");
                        } else
                        // End by luqiang, eastaeon, 2016.8.18 [NullPointerException]
                        for (NeighboringCellInfo ni : neighboringList) {
                            CdmaCell cdmaBean = new CdmaCell();
                            cdmaBean.systemId = cdmaCell.systemId;
                            cdmaBean.lac = ni.getLac();
                            cdmaBean.cellid = ni.getCid();

                            cdmaCells.add(cdmaBean);
                        }
                    } catch (ClassNotFoundException classnotfoundexception) {
                    }
                }// end CDMA network
                super.onCellLocationChanged(location);
            }// end onCellLocationChanged

            @Override
            public void onServiceStateChanged(ServiceState serviceState) {
                super.onServiceStateChanged(serviceState);
            }

            @Override
            public void onSignalStrengthsChanged(SignalStrength signalStrength) {
                if ("true".equals(SystemProperties.get("ro.mediatek.gemini_support"))) {

                    signalDbm1 = mPhone_sim1.getSignalStrength().getDbm();
                    signalAsu1 = mPhone_sim1.getSignalStrength().getAsuLevel();
                    try {
                        signalDbm2 = mPhone_sim2.getSignalStrength().getDbm();
                        signalAsu2 = mPhone_sim2.getSignalStrength().getAsuLevel();
                    } catch (Exception e) {
                        signalDbm2 = -1;
                        signalAsu2 = -1;
                    }

                    if (-1 == signalDbm1) {
                        signalDbm1 = 0;
                    }
                    if (-1 == signalAsu1) {
                        signalAsu1 = 0;
                    }
                    if (-1 == signalDbm2) {
                        signalDbm2 = 0;
                    }
                    if (-1 == signalAsu2) {
                        signalAsu2 = 0;
                    }
                } else {
                    signalDbm1 = signalStrength.getDbm();
                    signalAsu1 = signalStrength.getAsuLevel();
                    if (-1 == signalDbm1) {
                        signalDbm1 = 0;
                    }
                    if (-1 == signalAsu1) {
                        signalAsu1 = 0;
                    }
                }

                super.onSignalStrengthsChanged(signalStrength);
                Log.e("yuanwenchao", "Enter onSignalStrengthsChanged");
            }
        };
    }// end InitPhoneStateListener YWC--END

    @Override
    public void onClick(View arg0) {

        switch (arg0.getId()) {

            case R.id.btnPass:
                if (callstatus == true) {
                    finishSelf(RESULT_PASS);
                    callstatus = false;
                } else {
                    finishSelf(RESULT_FAIL);
                    Toast.makeText(SIMTest.this
                            , R.string.tips_call_failed, Toast.LENGTH_LONG)
                            .show();
                }
                break;
            case R.id.btnFail:
                finishSelf(RESULT_FAIL);
                break;
            case R.id.singlebutton_call_button:
                dialer10010();
                break;
            case R.id.singlebutton_call_button1:
                dialer10086();
                break;
            default:
                finishSelf(RESULT_PASS);
                break;
        }
    }

    private void dialer10010() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:10010"));
        // ComponentName compname = new
        // ComponentName("com.android.dialer","com.android.dialer.DialtactsActivity");
        // intent.setComponent(compname);
        // intent.setType()
        Log.e("yuanwenchao", "Enter dialer");
        startActivity(intent);
    }

    private void dialer10086() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:10086"));
        // ComponentName compname = new
        // ComponentName("com.android.dialer","com.android.dialer.DialtactsActivity");
        // intent.setComponent(compname);
        // intent.setType()
        Log.e("yuanwenchao", "Enter dialer");
        startActivity(intent);
    }

    // YWC--START

    class GsmCell {
        public String mnc, mcc;
        public int lac, cid, signal;
        public Long time;

        public String toString() {
            return " mnc = " + mnc + " mcc = " + mcc + " lac = " + lac + " cid = " + cid
                    + " time = " + time;
        }
    }

    class CdmaCell {
        public int stationId, networkId, systemId, signal, lat, lon, lac, cellid;
        public String mcc, mnc;
        public Long time;

        public String toString() {
            return "stationId = " + stationId + " networkId = " + networkId + " systemId = "
                    + systemId
                    + " lat = " + lat + " lon = " + lon + " lac = " + lac + " cellid = " + cellid
                    + " mcc = " + mcc + " mnc = " + mnc + " time = " + time;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (timetask != null) {
            timetask.cancel();
        }
    }
    // YWC--END
	
    @Override
    protected void onResume() {
        // registerReceiver(screenoff, new IntentFilter(Screenoff));
        Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 1);
        super.onResume();

        // PhoneWindowManager.setKeyTestState(true);
    }

    @Override
    protected void onPause() {
        // unregisterReceiver(screenoff);
        Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 0);
        super.onPause();

        // PhoneWindowManager.setKeyTestState(false);
    }
}
