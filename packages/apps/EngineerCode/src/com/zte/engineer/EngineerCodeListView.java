
package com.zte.engineer;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class EngineerCodeListView extends Activity {

    private ListView listView = null;
    Resources r = null;
    private int[] stringsIDs = {
            R.string.produce_information, R.string.phone_test, R.string.recover_settings,
            R.string.battery_info
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        r = getResources();
        listView = new ListView(this);
        listView.setAdapter(new MyAdapter(getBaseContext()));
        listView.setOnItemClickListener(new MyOnItemClickListener());
        setContentView(listView);

    }

    private class MyOnItemClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = null;
            switch (stringsIDs[position]) {
                case R.string.phone_test:
                    intent = new Intent();
                    intent.setAction("com.zte.engineer.action.TEST_LIST");
                    break;
                default:
                    int index = MyApplication.KEYS_MANUAL_TEST.indexOf(
                            stringsIDs[position]);
                    if (index >= 0) {
                        intent = MyApplication.MANUAL_TEST_INTENTS.get(index);
                    }

                    if (intent == null) {
                        index = MyApplication.KEYS_AUTO_TEST.indexOf(
                                stringsIDs[position]);
                        if (index >= 0) {
                            intent = MyApplication.AUTO_TEST_INTENTS.get(index);
                        }
                    }

                    if (intent == null) {
                        intent = new Intent();
                        intent.setClass(EngineerCodeListView.this, NotSupportNotification.class);
                        intent.putExtra("notification", getString(stringsIDs[position]));
                    }
                    break;
            }

            if (intent != null) {
                startActivity(intent);
            }
        }
    }

    private class MyAdapter extends BaseAdapter {

        private LayoutInflater mInflater;
        private Context mContext;

        public MyAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return stringsIDs.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (null == convertView) {
                convertView = mInflater.inflate(R.layout.list_text, null);
            }
            ((TextView) convertView.findViewById(R.id.list_text)).setText(r
                    .getString(stringsIDs[position]));
            return convertView;
        }
    }

}
