
package com.zte.engineer;

import java.lang.reflect.Method;
import android.util.Log;

import android.os.Bundle;
import android.view.View;
import android.app.Activity;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.content.ComponentName;

import android.widget.Button;
import android.widget.TextView;
import android.view.MotionEvent;

public class IrTestActivity extends ZteActivity {

    IrControl ida;
    byte Key1[] = {
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
    };
    byte Key2[] = {
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
    };
    byte Key3[] = {
            3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3
    };
    byte Key4[] = {
            4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4
    };
    byte Key5[] = {
            5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
    };
    byte Key6[] = {
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
    };
    byte Key7[] = {
            7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7
    };
    byte Key8[] = {
            8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8
    };
    byte Key9[] = {
            9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ir_test);
        TextView mTextView = (TextView) findViewById(R.id.singlebutton_textview);
        mTextView.setText(R.string.ir);

        ida = new IrControl(this);

        findViewById(R.id.btn0).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    view.setPressed(true);
                    int ret = ida.sendData(Key1);
                    Log.i("RemoteIr", "Sent Bytes : " + ret);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    view.setPressed(false);
                    ida.stopIR();
                }
                return true;
            }
        });

        findViewById(R.id.btn1).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    view.setPressed(true);
                    int ret = ida.sendData(Key2);
                    Log.i("RemoteIr", "Sent Bytes : " + ret);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    view.setPressed(false);
                    ida.stopIR();
                }
                return true;
            }
        });
        findViewById(R.id.btn2).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    view.setPressed(true);
                    int ret = ida.sendData(Key3);
                    Log.i("RemoteIr", "Sent Bytes : " + ret);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    view.setPressed(false);
                    ida.stopIR();
                }
                return true;
            }
        });
        findViewById(R.id.btn3).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    view.setPressed(true);
                    int ret = ida.sendData(Key4);
                    Log.i("RemoteIr", "Sent Bytes : " + ret);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    view.setPressed(false);
                    ida.stopIR();
                }
                return true;
            }
        });
        findViewById(R.id.btn4).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    view.setPressed(true);
                    int ret = ida.sendData(Key5);
                    Log.i("RemoteIr", "Sent Bytes : " + ret);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    view.setPressed(false);
                    ida.stopIR();
                }
                return true;
            }
        });
        findViewById(R.id.btn5).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    view.setPressed(true);
                    int ret = ida.sendData(Key6);
                    Log.i("RemoteIr", "Sent Bytes : " + ret);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    view.setPressed(false);
                    ida.stopIR();
                }
                return true;
            }
        });
        findViewById(R.id.btn6).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    view.setPressed(true);
                    int ret = ida.sendData(Key7);
                    Log.i("RemoteIr", "Sent Bytes : " + ret);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    view.setPressed(false);
                    ida.stopIR();
                }
                return true;
            }
        });
        findViewById(R.id.btn7).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    view.setPressed(true);
                    int ret = ida.sendData(Key8);
                    Log.i("RemoteIr", "Sent Bytes : " + ret);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    view.setPressed(false);
                    ida.stopIR();
                }
                return true;
            }
        });
        findViewById(R.id.btn8).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    view.setPressed(true);
                    int ret = ida.sendData(Key9);
                    Log.i("RemoteIr", "Sent Bytes : " + ret);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    view.setPressed(false);
                    ida.stopIR();
                }
                return true;
            }
        });

        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);
    }

}
