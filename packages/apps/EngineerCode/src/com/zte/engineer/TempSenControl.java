
package com.zte.engineer;

public class TempSenControl {

    public static final int FAIL = -1;
    public static final int SUCCESS = 0;

    static {
        System.loadLibrary("com_zte_engineer_jni_temp");
    }

    public native int TempRead(int num);
	public native int TempEnable(boolean enable);
}
