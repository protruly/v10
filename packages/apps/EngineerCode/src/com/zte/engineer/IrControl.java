
package com.zte.engineer;

import android.content.Context;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by brandshn on 2014. 9. 2..
 */

public class IrControl {

    Object objir = null;
    Method method_transmit;
    Method method_stop;

    private static final int BIT0 = 11;
    private static final int BIT1 = 34;

    public IrControl(Context ctx) {
        try {
            this.objir = ctx.getSystemService("remoteir");
            this.method_transmit = objir.getClass().getMethod("transmit", new Class[] {
                    byte[].class, int.class
            });
            this.method_stop = objir.getClass().getMethod("cancelTransmit", null);
        } catch (Exception e) {
        }

        if (objir == null)
            Log.d("RemoteIr", "No RemoteIR");
        if (method_transmit == null)
            Log.d("RemoteIr", "No Transmit Method");
        if (method_stop == null)
            Log.d("RemoteIr", "No Stop Method");
    }

    // Build IR buffer for I2C transfer
    private byte[] buildBuffer(int[] frame) {
        int frequency = frame[0];

        int size = ((frame.length - 1) * 2) + 6;
        byte[] buffer = new byte[size];
        int idx = 0;

        buffer[idx++] = (byte) 0x80;
        buffer[idx++] = (byte) ((frequency >> 16) & 0xff);
        buffer[idx++] = (byte) ((frequency >> 8) & 0xff);
        buffer[idx++] = (byte) (frequency & 0xff);
        buffer[idx++] = 0x00;
        buffer[idx++] = 0x00;

        for (int i = 1; i < frame.length; i++) {
            buffer[idx++] = (byte) ((frame[i] >> 8) & 0xff);
            buffer[idx++] = (byte) (frame[i] & 0xff);
        }

        return buffer;
    }

    // Get IR pulse frame for 15 Bytes
    private int[] getFrames(byte[] data) {
        ArrayList<Integer> frameList = new ArrayList<Integer>();
        frameList.add(38000);
        frameList.add(114);
        frameList.add(114);

        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < 8; j++) {
                int bit = (data[i] >> j) & 0x01;
                if (bit == 0) {
                    frameList.add(BIT0);
                    frameList.add(BIT0);
                } else {
                    frameList.add(BIT0);
                    frameList.add(BIT1);
                }
            }
        }

        frameList.add(11);
        frameList.add(1140);

        int[] frames = new int[frameList.size()];
        for (int i = 0; i < frameList.size(); i++) {
            frames[i] = frameList.get(i);
        }

        return frames;
    }

    // Send IR signal
    public int sendData(byte[] data) {
        if (objir == null || method_transmit == null)
            return -1;

        int ret = 0;
        try {
            byte[] buffer = buildBuffer(getFrames(data));
            ret = (Integer) method_transmit.invoke(objir, buffer, buffer.length);
        } catch (Exception e) {
            // Log.e("RemoteIr", e.toString());
        }

        return ret;
    }

    // Stop IR signal
    public int stopIR() {
        if (objir == null || method_transmit == null)
            return -1;

        int ret = 0;
        try {
            ret = (Integer) method_stop.invoke(objir);
        } catch (Exception e) {
            // Log.e("RemoteIr", e.toString());
        }

        return ret;
    }

}
