/**
 * type                                 sensor
 * 1(Sensor.TYPE_ACCELEROMETER)         Accelerometer
 * 2(Sensor.TYPE_MAGNETIC_FIELD)        Magnetic
 * 3(Sensor.TYPE_ORIENTATION)           Orientation
 * 4(Sensor.TYPE_GYROSCOPE)             GyroScope
 * 5(Sensor.TYPE_LIGHT)                 Light
 * 8(Sensor.TYPE_PROXIMITY)             Proximity
 * 9(Sensor.TYPE_GRAVITY)               Gravity
 * 10(Sensor.TYPE_LINEAR_ACCELERATION)  Linear Acceleration
 * 11(Sensor.TYPE_ROTATION_VECTOR)      Rotation vector
 */

package com.zte.engineer;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.widget.ImageView;

public class SensorTest extends ZteActivity {

    private boolean isHallBroadcastRegisted = false;

    LinearLayout g_sensor_layout, gyroscope_layout, magnetic_layout, hall_test_layout,
            light_layout, proximity_layout, step_counter_layout;
    private float x, y, z;
    private SensorManager sensorMgr;
    private SensorEventListener lsn;
    TextView GsensorX, GsensorY, GsensorZ;
    TextView MagneticX, MagneticY, MagneticZ;
    ImageView image;
    TextView GyroscopeX, GyroscopeY, GyroscopeZ;
    boolean[] isTestXYZ = {
            false, false, false, false
    };
    int temp = 0;
    int temp1 = 2;
    boolean[] temp2 = {
            false, false, false, false
    };
    int[] images = {
            R.drawable.jiantou, R.drawable.jiantou1, R.drawable.jiantou2, R.drawable.jiantou3,
            R.drawable.jiantou4, R.drawable.jiantou5, R.drawable.jiantou6, R.drawable.jiantou7,
            R.drawable.jiantou8, R.drawable.jiantou9, R.drawable.jiantou10, R.drawable.jiantou3_d,
            R.drawable.jiantou4_d, R.drawable.jiantou5_d, R.drawable.jiantou6_d,
            R.drawable.jiantou7_d

    };

    TextView tvHallStatus;
    TextView LightView;
    TextView ProximityView;
    TextView StepCounterView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sensortest);

        sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);

        // light_visible = sensorMgr.getDefaultSensor(Sensor.TYPE_LIGHT) !=
        // null;
        // proximity_visible = sensorMgr.getDefaultSensor(Sensor.TYPE_PROXIMITY)
        // != null;

        initUi();

        initSensorListener();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // initSensorListener();
        if (MyApplication.HAS_HALL_SENSOR) {
            Settings.Secure.putInt(getContentResolver(), "hall_test", 1);
            if (!isHallBroadcastRegisted) {
                IntentFilter filter = new IntentFilter();
                filter.addAction("com.eastaeon.action.HALLCLOSE");
                filter.addAction("com.eastaeon.action.HALLOPEN");
                registerReceiver(mHallSensorStatusReceiver, filter);
                isHallBroadcastRegisted = true;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (MyApplication.HAS_HALL_SENSOR) {
            Settings.Secure.putInt(getContentResolver(), "hall_test", 0);
        }
        if (isHallBroadcastRegisted) {
            unregisterReceiver(mHallSensorStatusReceiver);
            isHallBroadcastRegisted = false;
        }
    }

    private void initUi() {
        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);

        g_sensor_layout = (LinearLayout) findViewById(R.id.g_sensor_layout);
        step_counter_layout = (LinearLayout) findViewById(R.id.step_counter_layout);
        gyroscope_layout = (LinearLayout) findViewById(R.id.gyroscope_layout);
        magnetic_layout = (LinearLayout) findViewById(R.id.magnetic_layout);
        hall_test_layout = (LinearLayout) findViewById(R.id.hall_test_layout);
        light_layout = (LinearLayout) findViewById(R.id.light_layout);
        proximity_layout = (LinearLayout) findViewById(R.id.proximity_layout);
        if (!MyApplication.HAS_GRAVITY_SENSOR)
            g_sensor_layout.setVisibility(View.GONE);
        if (!MyApplication.HAS_STEPCOUNTER_SENSOR)
            step_counter_layout.setVisibility(View.GONE);    
        if (!MyApplication.HAS_GYROSCOPE_SENSOR)
            gyroscope_layout.setVisibility(View.GONE);
        if (!MyApplication.HAS_MAGNETIC_SENSOR)
            magnetic_layout.setVisibility(View.GONE);
        if (!MyApplication.HAS_HALL_SENSOR) {
            hall_test_layout.setVisibility(View.GONE);
        }
        if (!MyApplication.HAS_LIGHT_SENSOR) {
            light_layout.setVisibility(View.GONE);
        }
        if (!MyApplication.HAS_PROXIMITY_SENSOR) {
            proximity_layout.setVisibility(View.GONE);
        }

        GsensorX = (TextView) findViewById(R.id.g_sensor_x);
        GsensorY = (TextView) findViewById(R.id.g_sensor_y);
        GsensorZ = (TextView) findViewById(R.id.g_sensor_z);

				StepCounterView = (TextView) findViewById(R.id.step_counter);

        MagneticX = (TextView) findViewById(R.id.magnetic_x);
        MagneticY = (TextView) findViewById(R.id.magnetic_y);
        MagneticZ = (TextView) findViewById(R.id.magnetic_z);

        GyroscopeX = (TextView) findViewById(R.id.gyroscope_x);
        GyroscopeY = (TextView) findViewById(R.id.gyroscope_y);
        GyroscopeZ = (TextView) findViewById(R.id.gyroscope_z);
        image = (ImageView) findViewById(R.id.jiantou);
        image.setImageResource(R.drawable.jiantou);
        tvHallStatus = (TextView) findViewById(R.id.hall_status);
        LightView = (TextView) findViewById(R.id.light_lux);

        ProximityView = (TextView) findViewById(R.id.proximity);
    }

    private void initSensorListener() {
        List<Sensor> sensors = sensorMgr.getSensorList(Sensor.TYPE_ALL);

        lsn = new SensorEventListener() {
            public void onSensorChanged(SensorEvent e) {
				if (e.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
					x = e.values[SensorManager.DATA_X];
				} else {
                x = e.values[SensorManager.DATA_X];
                y = e.values[SensorManager.DATA_Y];
                z = e.values[SensorManager.DATA_Z];
				}

                switch (e.sensor.getType()) {
                    case Sensor.TYPE_ACCELEROMETER: {
                        /*
                         * if((x <= 1.0f)&(y <= 1.0f)&(z >= 9.0f)) {
                         * isTestXYZ[0] = 1;
                         * Log.e("yuanwenchao","Enter isTestXYZ[0]");
                         * image.setImageResource(R.drawable.jiantou); temp1 =0;
                         * } if((x <= 1.0f)&(y <= 1.0f)&(z <= -9.0f)) {
                         * isTestXYZ[1] = 1;
                         * Log.e("yuanwenchao","Enter isTestXYZ[1]");
                         * image.setImageResource(R.drawable.jiantou2); }
                         */
                        if ((z <= 1.0f) & (y <= 1.0f) & (x <= -9.0f)) {
                            isTestXYZ[0] = true;
                            Log.e("yuanwenchao", "Enter isTestXYZ[0]");
                            // image.setImageResource(R.drawable.jiantou2);
                            temp1 = 2;

                        }
                        if ((z <= 1.0f) & (y <= 1.0f) & (x >= 9.0f)) {
                            isTestXYZ[1] = true;
                            Log.e("yuanwenchao", "Enter isTestXYZ[1]");
                            // image.setImageResource(R.drawable.jiantou4);
                            temp1 = 2;
                        }
                        if ((x <= 1.0f) & (z <= 1.0f) & (y >= 9.0f)) {
                            isTestXYZ[2] = true;
                            Log.e("yuanwenchao", "Enter isTestXYZ[2]");
                            // image.setImageResource(R.drawable.jiantou1);
                            temp1 = 2;
                        }
                        if ((x <= 1.0f) && (z >= 1.0f) && (y <= -9.0f)) {
                            isTestXYZ[3] = true;
                            Log.e("yuanwenchao", "Enter isTestXYZ[3]");
                            // image.setImageResource(R.drawable.jiantou3);
                            temp1 = 2;
                        }

                        if (isTestXYZ[0] == true) {
                            Log.e("yuanwenchao", "temp2[0] = " + temp2[0]);
                            if (temp2[0] == false)
                                temp = temp + 2;

                            temp2[0] = true;

                        }
                        if (isTestXYZ[1] == true) {
                            if (temp2[1] == false)
                                temp = temp + 4;

                            temp2[1] = true;
                        }

                        if (isTestXYZ[2] == true) {
                            if (temp2[2] == false)
                                temp = temp + 1;
                            if ((temp == 3) || (temp == 4) || (temp == 5) || (temp == 6)
                                    || (temp == 7))
                                temp1 = 0;
                            else
                                temp1 = 1;
                            temp2[2] = true;
                        }
                        if (isTestXYZ[3] == true) {
                            if (temp2[3] == false)
                                temp = temp + 3;

                            temp2[3] = true;
                        }
                        Log.e("yuanwenchao", "temp1 = " + temp1 + " temp = " + temp);

                        /*
                         * temp = 0; for(int i : isTestXYZ) { temp = i+temp; }
                         * if(temp == 1) {
                         * ((Button)findViewById(R.id.sensor_pass
                         * )).setEnabled(true);
                         * Log.e("yuanwenchao1","temp1 = "+temp1); }
                         */

                        if (temp1 == 0) {
                            if (temp >= 8)
                                image.setImageResource(images[temp]);
                            else
                                image.setImageResource(images[temp + 8]);
                        } else {
                            image.setImageResource(images[temp]);
                        }
                        GsensorX.setText(String.valueOf(x));
                        GsensorY.setText(String.valueOf(y));
                        GsensorZ.setText(String.valueOf(z));
                    }
                        break;

                    case Sensor.TYPE_MAGNETIC_FIELD: {
                        MagneticX.setText(String.valueOf(x));
                        MagneticY.setText(String.valueOf(y));
                        MagneticZ.setText(String.valueOf(z));
                    }
                        break;

                    case Sensor.TYPE_GYROSCOPE: {
                        GyroscopeX.setText(String.valueOf(x));
                        GyroscopeY.setText(String.valueOf(y));
                        GyroscopeZ.setText(String.valueOf(z));
                    }
                        break;

                    case Sensor.TYPE_LIGHT: {
                        LightView.setText(String.format(getString(R.string.light_is),
                                String.valueOf(x)));
                    }
                        break;

                    case Sensor.TYPE_PROXIMITY: {
                        ProximityView.setText(String.valueOf(x));
                    }
                        break;

                    case Sensor.TYPE_STEP_COUNTER: {
                        StepCounterView.setText(String.valueOf(x));
                    }
                        break;    

                    default: {
                    }
                        break;

                }

            }

            public void onAccuracyChanged(Sensor s, int accuracy) {
            }

        };

        for (Sensor s : sensors) {
            sensorMgr.registerListener(lsn, s, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    public void finishSelf(int result) {

        sensorMgr.unregisterListener(lsn);

        super.finishSelf(result);
    }

    private BroadcastReceiver mHallSensorStatusReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if ("com.eastaeon.action.HALLOPEN".equals(action)) { // unwrap
                tvHallStatus.setText(R.string.magnet_not_found);
            } else if ("com.eastaeon.action.HALLCLOSE".equals(action)) { // wrap
                tvHallStatus.setText(R.string.found_magnet);
            }
        }
    };

    // Debug use another way.
    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (hall_test_visible) {
            if (keyCode == 87) { // KeyEvent.KEYCODE_F11
                tvHallStatus.setText(getString(R.string.magnet_not_found) + " F11");
            } else if (keyCode == 88) { // KeyEvent.KEYCODE_F12
                tvHallStatus.setText(getString(R.string.found_magnet) + " F12");
            }
        }
        return super.onKeyDown(keyCode, event);
    }*/
}
