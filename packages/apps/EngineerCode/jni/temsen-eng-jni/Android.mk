LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := com_zte_engineer_TempSenControl.cpp
LOCAL_C_INCLUDES := $(JNI_H_INCLUDE) \
                    frameworks/base/include/media
LOCAL_SHARED_LIBRARIES := \
    libnativehelper \
    libandroid_runtime \
    libcutils \
	libmedia
LOCAL_MODULE := libcom_zte_engineer_jni_temp
LOCAL_PRELINK_MODULE := false
include $(BUILD_SHARED_LIBRARY)

