#!/bin/bash


usage(){
local tools_name=$(basename $0)
printf "
Usage : ./${tools_name} [Options] PRODUCT  Actions

    Options:
      -l, -listp    : List the available project.
          -h,  -help    : Print this message and exit.
          -t, -tee      : Print log information on the standard-out.
          -q, -quiet    : Don't print log information on the standard-out.
          -j [N], cpu [N]
                        : Allow N jobs to compile at once,you must add a space between Options and [N].
          -m [MODULE]   : Only compile the [MODULE], you must add a space between Options and [MODULE].
        
    Actions:
        -n, --new             : Clean and perform a full build.
        -r, --remake          : retry perform a build.
        -c, --copy_target     : copy target to release only.
        -C, --copy_hmbird0     : copy hmbird code to alps dir
      
    Module:
        -i, --image  <bootimage|systemimage|userdataimage|update-api|kernel|otapackage>
            :Specify image to be build/re-build (bootimage|systemimage|userdataimage|recoveryimage).
            :Specify module to be build/re-build (kernel|otapackage).

    Project:
        -p, --project  <packages/app/Email/>
            :Specif Project pathdir to be build

    Example:
          ./${tools_name} -l
                        : 列出可编译的产品id

          ./${tools_name} [-v <user|eng|userdebug>] PRODUCT <-n|--new>
                        : 编译产品 PRODUCT 的user or eng or userdebug 版本

          ./${tools_name} PRODUCT -i <bootimage|systemimage|bootimage>
                        : 编译产品PRODUCT 的bootimage | systemimage | bootimage. 还可以是kernel,otapackage等模块

          ./${tools_name} GBW8901A01_A -p package/app/Email
                        : 编译产品PRODUCT的一个apk.还可是以framework.jar等project.

"
}


error(){
    local ret="$1";shift
    local info="$@"
    if [ "$ret" -ne 0 ];then
        echo "Error $ret: $info" >&2
        exit $ret
    fi
}


get_opts(){
    argv="$@"
    local build_variant=$(expr match  "$argv" '-opt=TARGET_BUILD_VARIANT=\(\w*\)')
    if [ "$build_variant" == "user" -o "$build_variant" == "userdebug" -o  "$build_variant" == "eng" ];then
        TARGET_BUILD_VARIANT=$build_variant
        shift
        echo $@
    fi

    opts=$(getopt -o cChi:lnp:ratv: --long copy_target,copy_hmbird,help,image:,listp,new,project:,remake,tee,target_build_variant:,clear_ccache -- "$@")     
    if [ $? -ne 0 ];then
        usage 
        exit 1
    fi
    eval set -- "$opts"
    while true 
    do
        case "$1" in 
            -c|--copy_target)
                COPY_TARGET=true
                shift
                ;;
            -C|--copy_hmbird)
                COPY_HMBIRD=true
                shift
                ;;
            -i|--image)
                MODULE=$2
                shift 2
                ;;
            -n|--new)
                ACTION=new
                shift
                ;;
            -p|--project)
                PROJECT_PATH=$2
                shift 2
                ;;
            -r|--remake)
                ACTION=remake
                shift 
                ;;
            -a|--android)
                ACTION=android
                shift 
                ;;
            -v|--target_build_variant)
                TARGET_BUILD_VARIANT=$2
                shift 2
                ;;
            -t|--tee)
               	DEBUG="-d" 
                shift 
                ;;
            -l|--listp)
                perl build/tools/listP.pl
                shift 
                exit 0
                ;;
            --clear_ccache)
                export HB_CLEAN_CCACHE="true"
                shift 
                ;;
            -h|--help)
                usage
                exit 0
                ;;
            --)
                shift
                break
                ;;
            *)
                usage
                exit 1
                ;;
        esac
    done

    if [ -z "$ACTION" -a -z "$MODULE" -a -z "$PROJECT_PATH" -a -z "$COPY_TARGET" -a -z "$COPY_HMBIRD" ];then
        echo "Error 1: Specify action to be build/re-build"
        usage
        exit 1
    fi

    PROTRULYPRODUCTID=${@}
    if [ ! -d "device/eastaeon/${@}" ];then
        echo "Error 2: *********** product directory not exist! ***********"
        exit 2
    fi

    if  [ -n "$PROJECT_PATH" -a ! -d "$PROJECT_PATH" ];then
        echo "Error 3: ***********$PROJECT_PATH not exist ************"
        exit 3
    fi
}

export_variable_from(){
    local file=$1
    if [ -e "$file" ];then
        while read line
        do
            #去掉空行，'#'开头，含有':=',不含有'='的行
            [ -z "$line" -o "${line:0:1}" == "#" ] && continue
            [ -z "$(expr match "$line" '.*\(=\).*')" ] && continue
            [ -n "$(expr match "$line" '.*\(:=\).*')" ] && continue
            [ -n "$(expr match "$line" '.*\(+=\).*')" ] && continue
            local key=`echo ${line%=*}`
            local value=`echo ${line#*=}`
            export "${key}"="${value}"
        done < $file
    else
        echo "$file not exist"
    fi
}


get_time() {
    time=`date -Iseconds`
    time=${time%+0800}
    time=${time//-/}
    time=${time//:/}
    time=${time//T/}
    time=${time:0:(${#time}-2)}
    echo $time
}


copy_to_alps(){
    sourcedir="$1"
    destdir="$2"
    if [ -d "$sourcedir" ];then
        echo "copy $sourcedir to  $destdir "
        pushd $sourcedir >/dev/null
        find . -path "*\.git" -prune -o -type f -follow -print | cpio -pdmu --quiet $destdir
        popd >/dev/null
    fi
}

prebuild(){
    HB_PROJECT=$PROTRULYPRODUCTID
    TARGET_PRODUCT=`grep  -w 'PRODUCT_NAME' device/eastaeon/${PROTRULYPRODUCTID}/full_${PROTRULYPRODUCTID}.mk | sed 's/ //g' | grep '^PRODUCT_NAME' | awk -F= '{print $2}'`
    export TARGET_PRODUCT HB_PROJECT PROTRULYPRODUCTID

    if [ -f hmbird/hmb_project/${PROTRULYPRODUCTID}.mk ];then
        export_variable_from "hmbird/hmb_project/${PROTRULYPRODUCTID}.mk"
    fi

    mkdir -p $LOGPATH

    #version=$(get_time)

    #if [ -n ${TARGET_BUILD_VARIANT} ] && [ ${TARGET_BUILD_VARIANT} == "user" ];then
    #    HB_RO_NEW_SW_VERSION="PROTRULY-${HMBPROJECTID}-BETA1.0.1-${version}"
    #else
    #    HB_RO_NEW_SW_VERSION="PROTRULY-${HMBPROJECTID}-ALPHA1.0.1-${version}"
    #fi

    #export HB_RO_NEW_SW_VERSION
    #HB_RO_HB_HBZNVERNUMBER=${HB_RO_NEW_SW_VERSION}

    #HB_ROM_PRE_SW_VERSION=`grep -oP 'ro.sw.version=\K\S+(?=\")' build/tools/buildinfo.sh`
    #echo "HB_RO_SW_VERSION:${HB_RO_NEW_SW_VERSION}"
    #echo "HB_ROM_PRE_SW_VERSION:${HB_ROM_PRE_SW_VERSION}"
    #sed -i "s/${HB_ROM_PRE_SW_VERSION}/${HB_RO_NEW_SW_VERSION}/g" build/tools/buildinfo.sh
}

build_copy_hb_code(){

    copy_to_alps "./hmbird" "${HB_BUILD_ROOT_DIR}"
}

build_android(){
    #echo "$BUILD_TOOLS --build_variant $TARGET_BUILD_VARIANT --android_build $TARGET_PRODUCT --log_file $LOGPATH/$TARGET_PRODUCT-android"
    $BUILD_TOOLS --build_variant $TARGET_BUILD_VARIANT --android_build $TARGET_PRODUCT --log_file $LOGPATH/$TARGET_PRODUCT-android
    error "$?" "************ build_android ************"
}

build_update_api(){
    #echo "$BUILD_TOOLS  --build_variant $TARGET_BUILD_VARIANT --update-api $TARGET_PRODUCT --log_file $LOGPATH/$TARGET_PRODUCT-update-api"
    $BUILD_TOOLS  --build_variant $TARGET_BUILD_VARIANT --update-api $TARGET_PRODUCT --log_file $LOGPATH/$TARGET_PRODUCT-update-api
    error "$?" "*********** build_update_api ************"
}

build_clean(){
    #echo "$BUILD_TOOLS --build_variant $TARGET_BUILD_VARIANT --clean_build $TARGET_PRODUCT --log_file $LOGPATH/$TARGET_PRODUCT-clean"
    $BUILD_TOOLS --build_variant $TARGET_BUILD_VARIANT --clean_build $TARGET_PRODUCT --log_file $LOGPATH/$TARGET_PRODUCT-clean
    error "$?" "*********** build_clean *************"
}

build_module(){
    local image=$1
    if `echo  "systemimage bootimage userdataimage" | grep -qw "$image" `;then
        $BUILD_TOOLS --build_variant $TARGET_BUILD_VARIANT --image $image  $TARGET_PRODUCT --log_file $LOGPATH/$TARGET_PRODUCT-$image
    else
        $BUILD_TOOLS --build_variant $TARGET_BUILD_VARIANT --module $image  $TARGET_PRODUCT --log_file $LOGPATH/$TARGET_PRODUCT-$image  --debug
    fi
    error "$?" "************ build_$image ************"
}

build_project(){
    local project_path=$1
    $BUILD_TOOLS --build_variant $TARGET_BUILD_VARIANT --project $PROJECT_PATH  $TARGET_PRODUCT  --log_file $LOGPATH/$TARGET_PRODUCT-mmm
    error "$?" "*********** build_project ************"
}



get_release_version(){
    HB_RO_HB_HBZNVERNUMBER=`cat hmbird/hmb_project/${HMBPROJECTID}_Release_Number`
    echo "VERSION:"${HB_RO_HB_HBZNVERNUMBER}
    HB_RELEASE_DIR="release/$HB_RO_HB_HBZNVERNUMBER"

    HB_RELEASE_OTA_DIR_ROOT=${HB_RELEASE_DIR}
    HB_RELEASE_IMG_DIR=${HB_RELEASE_DIR}_IMG
    HB_RELEASE_OTA_TMP_DIR=${HB_RELEASE_OTA_DIR_ROOT}
    HB_RELEASE_OTA_DIR=${HB_RELEASE_OTA_DIR_ROOT}
    HB_RELEASE_MODEM_DIR="$HB_RELEASE_DIR/bp_image"
}



copy_results_to_release(){
    [ -d "$HB_RELEASE_DIR" ] && rm -rf $HB_RELEASE_DIR
    mkdir -p $HB_RELEASE_DIR

    [ -d "$HB_RELEASE_IMG_DIR" ] && rm -rf $HB_RELEASE_IMG_DIR
    mkdir -p $HB_RELEASE_IMG_DIR

    PRODUCT_OUT_ROOT="out/target/product"
    HB_TARGET_PRODUCT=${TARGET_PRODUCT/full_}
    TARGET_PRODUCT_OUT_ROOT="$PRODUCT_OUT_ROOT/${HB_TARGET_PRODUCT}"

    if [ -d "${TARGET_PRODUCT_OUT_ROOT}" ];then
        cp -f ${TARGET_PRODUCT_OUT_ROOT}/*.img ${HB_RELEASE_IMG_DIR};
        cp -f ${TARGET_PRODUCT_OUT_ROOT}/*.bin ${HB_RELEASE_IMG_DIR}/;
        cp -f ${TARGET_PRODUCT_OUT_ROOT}/*.txt ${HB_RELEASE_IMG_DIR}/;

        cp ${TARGET_PRODUCT_OUT_ROOT}/obj/ETC/MDDB_InfoCustomApp*/MDDB_InfoCustomApp* ${HB_RELEASE_IMG_DIR}
        cp ${TARGET_PRODUCT_OUT_ROOT}/obj/CGEN/APDB_MT6797_S01_alps-mp-m0.mp9_W* ${HB_RELEASE_IMG_DIR}
    else
        echo "Wanning: ${TARGET_PRODUCT_OUT_ROOT} does no exist"
    fi

    local bpl_file=$(find $TARGET_PRODUCT_OUT_ROOT/obj/ETC -type f -name "BPL*" 2>/dev/null )
    if [ -f "$bpl_file"  ];then
        cp -r $bpl_file ${HB_RELEASE_IMG_DIR}/
    fi

    find $TARGET_PRODUCT_OUT_ROOT/obj -type f -name "APDB*" -exec cp \{} ${HB_RELEASE_IMG_DIR} \;
    find $TARGET_PRODUCT_OUT_ROOT/system/etc/mddb -type f -name "BP*" -exec cp \{} ${HB_RELEASE_IMG_DIR} \;
    find $TARGET_PRODUCT_OUT_ROOT/system/etc/mddb -type f -name "MDDB_" -exec cp \{} ${HB_RELEASE_IMG_DIR} \;

    
    if [ -f "$TARGET_PRODUCT_OUT_ROOT/obj/KERNEL_OBJ/vmlinux" ];then
        cp "$TARGET_PRODUCT_OUT_ROOT/obj/KERNEL_OBJ/vmlinux" "${HB_RELEASE_IMG_DIR}"
    fi

    local ota_zip=$(find ${TARGET_PRODUCT_OUT_ROOT}/ -maxdepth 1 -type f -name ${TARGET_PRODUCT}-ota-*.zip 2>/dev/null)

    local ota_tmp_zip=$(find ${TARGET_PRODUCT_OUT_ROOT}/obj/PACKAGING/target_files_intermediates/ \
        -maxdepth 1 -type f -name ${TARGET_PRODUCT}-target_files-*.zip 2>/dev/null)
    if [ -f "$ota_zip" ];then
        rm -rf $HB_RELEASE_OTA_DIR_ROOT
        mkdir -p $HB_RELEASE_OTA_TMP_DIR
        mkdir -p $HB_RELEASE_OTA_DIR
        cp -r $ota_zip $HB_RELEASE_OTA_DIR/${HB_RO_HB_HBZNVERNUMBER}.zip
        local ota_md5=$(md5sum $HB_RELEASE_OTA_DIR/${HB_RO_HB_HBZNVERNUMBER}.zip |awk '{print $1}')
        echo "${HB_RO_HB_HBZNVERNUMBER}.zip=$ota_md5" > $HB_RELEASE_OTA_DIR/${HB_RO_HB_HBZNVERNUMBER}.md5
        cp -r $ota_tmp_zip $HB_RELEASE_OTA_TMP_DIR/${HB_RO_HB_HBZNVERNUMBER}_TARGET_FILES.zip
    fi

    local apuds_target_files=$(find ${TARGET_PRODUCT_OUT_ROOT}/ -maxdepth 1 -type f -name target_files-package.zip 2>/dev/null)

    if [ -n "${apuds_target_files}" ];then
        cp -r ${apuds_target_files} $HB_RELEASE_OTA_TMP_DIR/${HB_RO_HB_HBZNVERNUMBER}_APUDS_TARGET_FILES.zip
    fi

}


build_copy_target(){
    get_release_version
    copy_results_to_release
    error "$?" "*********** build_copy_target **********"

}

build_remake(){
    build_update_api
    build_android
    build_copy_target
}

build_new(){
    build_clean
    build_copy_hb_code
    build_remake
}


main(){
   get_opts "$@"

   prebuild

   if [ -n "$COPY_HMBIRD" ];then
       echo "********* BUILD COPY HBCODE *******"
       build_copy_hb_code
   fi

   if [ -n "$ACTION" ];then
       echo "********* BUILD $ACTION *********"
       build_$ACTION
   fi

   if [ -n "$MODULE" ];then
       echo "********* BUILD $MODULE*********"
       build_module $MODULE
   fi

   if [ -d "$PROJECT_PATH" ];then
       echo "********* BUILD $PROJECT_PATH *********"
       build_project $PROJECT_PATH
   fi

   if [ -n "$COPY_TARGET" -o "$MODULE" == "otapackage" ];then
       echo "********* BUILD COPY TARGET *********"
       build_copy_target
   fi

}

MODULE=""

ACTION=""

SHOWLOG=false

HB_BUILD_ROOT_DIR=$(cd `dirname $0`; pwd)

export HB_BUILD_ROOT_DIR

TARGET_BUILD_VARIANT="eng"

BUILD_TOOLS="$HB_BUILD_ROOT_DIR/build.sh $DEBUG --setup_ccache=true"

LOGPATH="BUILDING_LOG"

main "$@"
