#!/usr/bin/perl
@hm_platform_type = ("aeon");

$pmPath = "device/eastaeon";
chdir($pmPath);
@dirs = <*>;
$i = 0;
foreach $f (@dirs) {
	$result = 1;
	foreach $type (@hm_platform_type) {
		$result = $result && $f !~ /^$type.*$/;
	}
    next if ( $result );
    $f =~ /(.*)/;
    print $1;
    $i++;
    if ($i%3 == 0) {
        print "\n";
    } else {
        print " " x (26-length $f);
    }
}
if ($i%3 != 0) {
    print "\n";
}

exit 0;
