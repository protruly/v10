LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := AMAP
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED

LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/operator/app

LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/armeabi/libadcode-2.0.1.so \
        lib/armeabi/libAisound.so \
        lib/armeabi/libAitalk30.so \
        lib/armeabi/libangeo14.so \
        lib/armeabi/libappwatcher-1.0.1.so \
        lib/armeabi/libapssdk.so \
        lib/armeabi/libautonavimsc.so \
        lib/armeabi/libbusnavi-1.2.5.so \
        lib/armeabi/libdumpcrash-1.1.2.so \
        lib/armeabi/libgdinamapv4100.so \
        lib/armeabi/libgnustl_shared.so \
        lib/armeabi/libindoorpdr5.5.so \
        lib/armeabi/libindoorpdr5.6.so \
        lib/armeabi/libindoorpdr6.1.so \
        lib/armeabi/libiodetector1.1.so \
        lib/armeabi/libiodetector1.3.so \
        lib/armeabi/libIODetectorSDK-1.0.1.201509181200.so \
        lib/armeabi/libIvw35.so \
        lib/armeabi/libOfflinePoiSearchEngine-5.0.150923.so \
        lib/armeabi/libonlinelocation5.5.so \
        lib/armeabi/libonlinelocation5.6.so \
        lib/armeabi/libonlinelocation6.1.so \
        lib/armeabi/libserverkey-2.2.3.so \
        lib/armeabi/libShenmaSpeechSDK.so \
        lib/armeabi/libspeex.so \
        lib/armeabi/libtbt360.so \
        lib/armeabi/libwtbt-1.4.4.so


LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/armeabi-v7a/libackor_jni.so \
        lib/armeabi-v7a/libadcode-2.0.2.so \
        lib/armeabi-v7a/libAisound6.0.so  \
        lib/armeabi-v7a/libAjxLoader.so \
        lib/armeabi-v7a/libajx.so \
        lib/armeabi-v7a/libapssdk.so \
        lib/armeabi-v7a/libbusnavi-1.4.7.so \
        lib/armeabi-v7a/libdumpcrash-1.1.2.so  \
        lib/armeabi-v7a/libFusionLocation1.1.so  \
        lib/armeabi-v7a/libgdinamapv4sdk752ex.so \
        lib/armeabi-v7a/libgdinamapv4sdk752.so \
        lib/armeabi-v7a/libGNaviData.so \
        lib/armeabi-v7a/libGNaviGuide.so \
        lib/armeabi-v7a/libGNaviMap.so \
        lib/armeabi-v7a/libGNaviPos.so \
        lib/armeabi-v7a/libGNaviRoute.so \
        lib/armeabi-v7a/libGNaviSearch.so \
        lib/armeabi-v7a/libGNaviUtils.so \
        lib/armeabi-v7a/libgnustl_shared.so \
        lib/armeabi-v7a/libhealth.so \
        lib/armeabi-v7a/libindoorpdr6.8.so \
        lib/armeabi-v7a/libiodetector6.8.so \
        lib/armeabi-v7a/libIvw35.so \
        lib/armeabi-v7a/libjsc.so \
        lib/armeabi-v7a/liblicense_recog.so \
        lib/armeabi-v7a/liblinkProxy-1.0.0.so \
        lib/armeabi-v7a/libonlinelocation6.8.so \
        lib/armeabi-v7a/libRoadLineRebuildAPI.so \
        lib/armeabi-v7a/librtbt.so \
        lib/armeabi-v7a/libserverkey-2.2.3.so \
        lib/armeabi-v7a/libsgmain.so \
        lib/armeabi-v7a/libsgmainso-5.1.81.so \
        lib/armeabi-v7a/libsgsecuritybody.so \
        lib/armeabi-v7a/libsgsecuritybodyso-5.1.25.so \
        lib/armeabi-v7a/libShenmaSpeech5.so \
        lib/armeabi-v7a/libspeex.so \
        lib/armeabi-v7a/libsync_jni-3.4.1.so \
        lib/armeabi-v7a/libtnet-3.1.10.so \
        lib/armeabi-v7a/libweibosdkcore.so \
        lib/armeabi-v7a/libwtbt.so

LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

