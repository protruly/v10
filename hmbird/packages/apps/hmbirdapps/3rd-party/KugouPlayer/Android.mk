LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := KugouPlayer
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED

LOCAL_OVERRIDES_PACKAGES := Music

LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/operator/app

LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/armeabi/libandroidfanxingmedia.so \
        lib/armeabi/libandroidfanxing.so \
        lib/armeabi/libandroidktv.so \
        lib/armeabi/libApkPatchLibrary.so \
        lib/armeabi/libencrypt2.so \
        lib/armeabi/libencrypt.so \
        lib/armeabi/libentryexstd.so \
        lib/armeabi/libfdk-aac.so \
        lib/armeabi/libffmpeg.so \
        lib/armeabi/libfph.so \
        lib/armeabi/libfp.so \
        lib/armeabi/libgdinamapv4sdk752ex.so \
        lib/armeabi/libgdinamapv4sdk752.so \
        lib/armeabi/libjengine.so \
        lib/armeabi/libkgkey.so \
        lib/armeabi/libkguo.so \
        lib/armeabi/libkugouplayer.so \
        lib/armeabi/libLencryption.so \
        lib/armeabi/liblocSDK6a.so \
        lib/armeabi/libMMANDKSignature.so \
        lib/armeabi/libmodulegame.so \
        lib/armeabi/libopencore-amrnb.so \
        lib/armeabi/librtmp.so \
        lib/armeabi/libstdredi.so \
        lib/armeabi/libviper4android.so \
        lib/armeabi/libweibosdkcore.so \
        lib/armeabi/libzbar.so


LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

