LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := VrBrowser
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED


LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/armeabi-v7a/libfb_jpegturbo.so \
        lib/armeabi-v7a/libgifimage.so \
        lib/armeabi-v7a/libhttp_midware.so \
        lib/armeabi-v7a/libimagepipeline.so \
        lib/armeabi-v7a/libjni_filtershow_filters.so \
        lib/armeabi-v7a/libmp3lame.so \
        lib/armeabi-v7a/libpanorenderer.so \
        lib/armeabi-v7a/libstatic-webp.so \
        lib/armeabi-v7a/libtencentloc.so \
        lib/armeabi-v7a/libweibosdkcore.so


LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

