LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := Tuniu
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED

LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/operator/app

LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/armeabi-v7a/libandfix.so \
        lib/armeabi-v7a/libApkPatchLibrary.so \
        lib/armeabi-v7a/libBankCardScanSDK.so \
        lib/armeabi-v7a/libdu.so \
        lib/armeabi-v7a/libentryexpro.so \
        lib/armeabi-v7a/libfb.so \
        lib/armeabi-v7a/libfolly_json.so \
        lib/armeabi-v7a/libgifimage.so \
        lib/armeabi-v7a/libglog_init.so \
        lib/armeabi-v7a/libglog.so \
        lib/armeabi-v7a/libgnustl_shared.so \
        lib/armeabi-v7a/libgpuimage-library.so \
        lib/armeabi-v7a/libicu_common.so \
        lib/armeabi-v7a/libimagepipeline.so \
        lib/armeabi-v7a/libjpush217.so \
        lib/armeabi-v7a/libjsc.so \
        lib/armeabi-v7a/libreactnativejnifb.so \
        lib/armeabi-v7a/libreactnativejni.so \
        lib/armeabi-v7a/libtuniu10.so \
        lib/armeabi-v7a/libtuniu11.so \
        lib/armeabi-v7a/libtuniu12.so \
        lib/armeabi-v7a/libtuniu13.so \
        lib/armeabi-v7a/libtuniu14.so \
        lib/armeabi-v7a/libtuniu15.so \
        lib/armeabi-v7a/libtuniu2.so \
        lib/armeabi-v7a/libtuniu3.so \
        lib/armeabi-v7a/libtuniu4.so \
        lib/armeabi-v7a/libtuniu5.so \
        lib/armeabi-v7a/libtuniu6.so \
        lib/armeabi-v7a/libtuniu8.so \
        lib/armeabi-v7a/libtuniu9.so \
        lib/armeabi-v7a/libtuniucurlnetwork.so \
        lib/armeabi-v7a/libtuniudns.so \
        lib/armeabi-v7a/libtuniusec.so \
        lib/armeabi-v7a/libuptsmaddon.so \
        lib/armeabi-v7a/libweibosdkcore.so


LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

