LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := MJWeatherBox
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED

LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/armeabi/libdu.so \
        lib/armeabi/libencrypt.so \
        lib/armeabi/libgetuiext2.so \
        lib/armeabi/libijkplayer.so \
        lib/armeabi/libijksdl.so \
        lib/armeabi/libpl_droidsonroids_gif.so \
        lib/armeabi/libsgmain.so \
        lib/armeabi/libweibosdkcore.so

LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

