LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := qqmusic
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED

LOCAL_OVERRIDES_PACKAGES := Music

LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/operator/app

LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
    lib/armeabi/libckeygenerator.so \
    lib/armeabi/libckey.so \
    lib/armeabi/libdalvik_patch.so \
    lib/armeabi/libdesdecrypt.so \
    lib/armeabi/libexpress_verify.so \
    lib/armeabi/libfilescanner.so \
    lib/armeabi/libfingerprintjni.so \
    lib/armeabi/libfingerprintV2jni.so \
    lib/armeabi/libFormatDetector.so \
    lib/armeabi/libh.so \
    lib/armeabi/libimage_filter_common.so \
    lib/armeabi/libimage_filter_gpu.so \
    lib/armeabi/libinh.so \
    lib/armeabi/libmApptracker4Dau.so \
    lib/armeabi/libmresearch.so \
    lib/armeabi/libNativeRQD.so \
    lib/armeabi/libnetworkbase.so \
    lib/armeabi/libpbpinyin.so \
    lib/armeabi/libPlayerCore_neon.so \
    lib/armeabi/libQQMMANDKSignature.so \
    lib/armeabi/libsearch.so \
    lib/armeabi/libSongUrlFactory.so \
    lib/armeabi/libsta_jni.so \
    lib/armeabi/libsuperapp_base.so \
    lib/armeabi/libtmfe30.so \
    lib/armeabi/libTUpdateService.so \
    lib/armeabi/libTxCodec_neon.so \
    lib/armeabi/libvad.so \
    lib/armeabi/libwnsnetwork.so \
    lib/armeabi/libwtecdh.so \
    lib/armeabi/libWXVoice.so

LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

