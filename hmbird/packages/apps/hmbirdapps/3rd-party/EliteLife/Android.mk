LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := EliteLife
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED


LOCAL_MULTILIB := 64

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/arm64-v8a/libentryexpro.so \
        lib/arm64-v8a/libjcore113.so \
        lib/arm64-v8a/libpingpp.so \
        lib/arm64-v8a/libuptsmaddon.so \
        lib/arm64-v8a/libweibosdkcore.so

LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

