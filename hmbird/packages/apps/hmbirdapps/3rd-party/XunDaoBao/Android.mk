LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := XunDaoBao
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED

LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/operator/app

LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/armeabi/libffmpeg.so \
        lib/armeabi/libgdinamapv4sdk752ex.so \
        lib/armeabi/libgdinamapv4sdk752.so \
        lib/armeabi/libjcore100.so \
        lib/armeabi/libOMX.11.so \
        lib/armeabi/libOMX.14.so \
        lib/armeabi/libOMX.18.so \
        lib/armeabi/libOMX.9.so \
        lib/armeabi/libstlport_shared.so \
        lib/armeabi/libvao.0.so \
        lib/armeabi/libvplayer.so \
        lib/armeabi/libvscanner.so \
        lib/armeabi/libvvo.0.so \
        lib/armeabi/libvvo.7.so \
        lib/armeabi/libvvo.8.so \
        lib/armeabi/libvvo.9.so \
        lib/armeabi/libvvo.j.so


LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

