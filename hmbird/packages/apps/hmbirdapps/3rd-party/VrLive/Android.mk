LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := VrLive
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED


LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/armeabi-v7a/libaes.so  \
        lib/armeabi-v7a/libarcsoft_beauty_ex.so  \
        lib/armeabi-v7a/libArcSoftSpotlight.so  \
        lib/armeabi-v7a/libblasV8.so  \
        lib/armeabi-v7a/libblur.so  \
        lib/armeabi-v7a/libcimage_jni.so  \
        lib/armeabi-v7a/libcimage.so  \
        lib/armeabi-v7a/libcvface_api.so  \
        lib/armeabi-v7a/libdialchat.so  \
        lib/armeabi-v7a/libhw-codec-jni.so  \
        lib/armeabi-v7a/libjni_cvface_api.so  \
        lib/armeabi-v7a/libmpbase.so  \
        lib/armeabi-v7a/libpng16d.so  \
        lib/armeabi-v7a/librsjni.so  \
        lib/armeabi-v7a/libRSSupport.so  \
        lib/armeabi-v7a/libst_facebeauty.so  \
        lib/armeabi-v7a/libstlport_shared.so  \
        lib/armeabi-v7a/libstreamer.so  \
        lib/armeabi-v7a/libstreamuser.so  \
        lib/armeabi-v7a/libweibosdkcore.so


LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

