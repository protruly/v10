LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := TencentVideo
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED

LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/operator/app

LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/armeabi/libapollo_voice.so \
        lib/armeabi/libbitmaps.so \
        lib/armeabi/libbitmaps_x86.so \
        lib/armeabi/libcftutils.so \
        lib/armeabi/libckeygenerator.so \
        lib/armeabi/libgetproperty.so \
        lib/armeabi/libgifimage.so \
        lib/armeabi/libgifimage_x86.so \
        lib/armeabi/libimagepipeline.so \
        lib/armeabi/libimagepipeline_x86.so \
        lib/armeabi/libmemchunk.so \
        lib/armeabi/libmemchunk_x86.so \
        lib/armeabi/libmresearch.so \
        lib/armeabi/libNativeRQD.so \
        lib/armeabi/libp2plive.so \
        lib/armeabi/libp2pproxy.so \
        lib/armeabi/libpilog.so \
        lib/armeabi/libPlayerCore_neon.so \
        lib/armeabi/libqqlive_apollo_voice.so \
        lib/armeabi/libQQMMANDKSignature.so \
        lib/armeabi/libQrMod.so \
        lib/armeabi/libstlport_shared.so \
        lib/armeabi/libtencentloc.so \
        lib/armeabi/libTxCodec_neon.so \
        lib/armeabi/libvideowatch.so \
        lib/armeabi/libWasabiJni.so \
        lib/armeabi/libwebpimage.so \
        lib/armeabi/libwebpimage_x86.so \
        lib/armeabi/libwebp.so \
        lib/armeabi/libwebp_x86.so \
        lib/armeabi/libweibosdkcore.so \
        lib/armeabi/libwtecdh.so

LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

