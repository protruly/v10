LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := HMBGallery
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED


LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
            lib/armeabi/libhttp_midware.so \
            lib/armeabi/libjni_filtershow_filters.so \
            lib/armeabi/libpl_droidsonroids_gif.so \
            lib/armeabi/libpl_droidsonroids_gif_surface.so \
            lib/armeabi/libtencentloc.so \
            lib/armeabi-v7a/libblasV8.so \
            lib/armeabi-v7a/libhttp_midware.so \
            lib/armeabi-v7a/libjni_filtershow_filters.so \
            lib/armeabi-v7a/libpl_droidsonroids_gif.so \
            lib/armeabi-v7a/librs.convolve3x3.so \
            lib/armeabi-v7a/librs.grad.so \
            lib/armeabi-v7a/librs.grey.so \
            lib/armeabi-v7a/librsjni.so \
            lib/armeabi-v7a/librs.saturation.so \
            lib/armeabi-v7a/libRSSupport.so \
            lib/armeabi-v7a/librs.vignette.so \
            lib/armeabi-v7a/libtencentloc.so \
	    lib/armeabi-v7a/libfb_jpegturbo.so \
	    lib/armeabi-v7a/libgifimage.so \
	    lib/armeabi-v7a/libimagepipeline.so \
	    lib/armeabi/libfb_jpegturbo.so \
	    lib/armeabi/libgifimage.so \
	    lib/armeabi/libimagepipeline.so


LOCAL_OVERRIDES_PACKAGES := Gallery Gallery2

LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

